$(function(){
	currency();
	/*if (typeof jQuery === 'undefined') {
	  throw new Error('Bootstrap\'s JavaScript requires jQuery')
	}*/
	if(typeof masonry != 'undefined'){
		masonry();		
	}

	if(typeof datepick != 'undefined'){
		datepick();
	}
		
	if(typeof imageuploads != 'undefined'){
		imageuploads();		
	}

	if(typeof deletion != 'undefined'){
		deletion();
	}

	if(typeof selectiontwo != 'undefined'){
		selectiontwo();
	}

	$('.btn-add').click(function(e) {
		e.preventDefault();
		pre_insertion($(this));
	});


});

function masonry(){
	if($.fn.masonry){
		$('.config-list').masonry();		
	}
}

function datepick(){
	if($.fn.datepicker){
	    $('.datepicker').datepicker({
	        changeMonth:true,
	        changeYear:true,
	        dateFormat:'yy-mm-dd',
	        showButtonPanel:true,
	        beforeShow:function(textbox,instance){
	        	var slc = $(this);
	        	var slcp = slc.parents('.input-group');
	        	$('#ui-datepicker-div').appendTo($(slcp));
	        	if($(this).data('init') != undefined){
	        		$(this).datepicker('setDate', $(this).data('init'));
	        	}
	        	if(slc.data('year-range') != undefined){
	        		return{
	        			yearRange : slc.data('year-range')
	        		}
	        	}	        	
	        }
	    });		
	}
}

function currency(){
	if($.fn.autoNumeric){
		$('.currency').autoNumeric('init',{aSep:tsep,aDec:dsep});			
	}
}

function texteditor(){
	if($.fn.jqte){
		$('.editor').jqte();		
	}
}

function treesbtn(){
	$('.btn-drop').on('click',function(){
		var $parents = $(this).parents('.input-group');
		$parents.find('.drop-tree').toggleClass('active');
		if($parents.children('.drop-tree').hasClass('active')){
			$parents.find('.radio').removeClass('hide');
		}
		else{
			$parents.find('.radio').addClass('hide');
			$parents.find(':checked').parents('.radio').removeClass('hide').addClass('visible');
			
		}
	});	
}
/*
function swalalerts(args, callback){
    var $title  = (args.title != undefined) ? args.title : '';
    var $text   = (args.text != undefined) ? args.text : '';
    var $type   = (args.type != undefined) ? args.type : 'warning';
    var $cancel = (args.showCancelButton != undefined) ? args.showCancelButton : true;
    var $close  = (args.closeOnConfirm != undefined) ? args.closeOnConfirm : false;
    var $abort  = (args.closeOnCancel != undefined) ? args.closeOnCancel : true;

    swal({
        title:$title,
        text:$text,
        type:$type,
        showCancelButton:$cancel,
        confirmButtonColor:"#DD6B55",
        cancelButtonText:"Cancel",
        closeOnConfirm:$close,
        closeOnCancel: $abort,
        allowOutsideClick :true,
       },
       function (isConfirm){
            callback(isConfirm);
        }
    );
}
*/
function imageuploads(){
	$(".uploads-preview-input input:file").change(function (e){
		var $parents = $(this).parents('.input-group');
		var $holder  = $parents.next('.image-result');
		$holder.html('');
        var $name = '';
		$.each(this.files, function(index, val) {
			 $name += val.name+'; ';
		}); 
		$.each(e.originalEvent.srcElement.files, function(i, file) {
            var reader = new FileReader();
            reader.onloadend = function (f) {
            	$holder.append('<li class="list-group-item col-xs-12 col-sm-4"><img src="'+f.target.result+'" class="img-responsive"></li>');
            };
            reader.readAsDataURL(file);
        });
        $('.image-preview-filename').val($name);
    });	
}

function checkbox_image(){
	$('.checkbox-group .list-group-item').click(function() {
		$(this).toggleClass('img-check');
		var $input = $(this).children(':checkbox');
		if($(this).hasClass('img-check') == true){
			$input.prop('checked', true);
		}
		else{
			$input.prop('checked', false);
		}
	});
}