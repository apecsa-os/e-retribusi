/**
 * Module for displaying "Waiting for..." dialog using Bootstrap
 *
 * @author Eugene Maslovich <ehpc@em42.ru>
 */

var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
            '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
            '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
            '</div>' +
        '</div></div></div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         *                options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *                options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Loading';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null
            }, options);

            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        },
    };

})(jQuery);

$(document).ready(function() {
	var $table 		= $('[data-render = datatable]');
	var $url 		= $table.attr('data-sources');
    var $order      = $table.data('col-order') != undefined ? $table.data('col-order') : null;
    if($.fn.DataTable){
        var $datatable  = $table.DataTable( {
            "infoEmpty": "No records available",
            "bFilter": true,
            "bAutoWidth": false,
            "bLengthChange": true,
            "bsearching" :true,
            "deferRender" : true,
            "bProcessing": false,
            "bServerSide": true,
            "pagingType":'full_numbers',
            "lengthMenu": [5, 10, 25, 50, 100, 250, 500, 1000],
            "pageLength": 50,
            "destroy": true,
            "order" : ($order != null ) ? column_order($order) : [0, 'asc'],
            "sDom" : '<"top"l>rt<"bottom"ip><"clear">',
            "ajax" : {
                "url" : $url,
                "type" : "POST",
                "data" : function(d){
                    var search = {};
                    var adv = $('.advance_search').length;
                    if(adv > 0){
                        $('.advance_search :input').each(function(index, el) {
                            if($(el).hasClass('.btn') == false && $(el).is('button') == false && $(el).val() != '' && $(el).attr('name') != undefined){
                                search[$(el).attr('name')] = $(el).val();
                            }
                        });
                        d.form = search;
                    }                     
                }
            },
            "language": {
                "lengthMenu": "Display _MENU_ Records",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "thousands": tsep,
                "decimal": dsep,
                "paginate": {
                    "first":      '<i class="fa fa-angle-double-left"></i>',
                    "last":       '<i class="fa fa-angle-double-right"></i>',
                    "next":       '<i class="fa fa-angle-right"></i>',
                    "previous":   '<i class="fa fa-angle-left"></i>'
                },
            },
            "initComplete" :function(){
                insertion();
            },
            "drawCallback" : function(){
                updation();
                deletion();
            }       
        });
    }

    $('.column-filter, .filter-control, .advance-control').on('keyup', function(e) {
        if (e.keyCode == 13) {
            drawtable($datatable);
        }
    });

    $('.advance_search .btn-clearance').click(function() {
        $('.advance_search').find(':input').not('.btn, button').val('');
        drawtable($datatable);
    });

    $('.btn-redraw').on('click', function(e){
        drawtable($datatable);
    });

    $('.filter-control').on('click', function(){
    	drawtable($datatable);
    });

    $('.row-expanded').click(function() {
        var $parents = $(this).parents('.row-parent');
        $parents.nextUntil('.row-parent').toggle();
        $(this).toggleClass('on');
    });

    $('.checkall-control').change(function() {
        if($(this).is(':checked') == true){
            $('.check_menus').prop('checked',true);
        }
        else{
            $('.check_menus').prop('checked',false);               
        }
    });
} );

function column_order(order){
    $obj = [];
    $string = order.split(',');
    $.each($string, function(index, val) {
       $t = val.split(':');
       $obj.push($t); 
    });
    return $obj;
}

function drawtable(table){
    $('.column-filter').each(function(index, el) {
        var p = $(el).parents('td').index();
        if($(el).is('input:text') == true){
            var values = $(el).val();
        }       
        else{
            var values = $(el).val()+'_drpdwn';
        }
        table.column(p).search(values);
    });
    table.draw();
}

function insertion(){
    $('.btn-insert').on('click', function(e){
        e.preventDefault();
        pre_insertion($(this));
    }); 
}

function updation(){
    $('.btn-update').on('click', function(e){
        e.preventDefault();
        pre_insertion($(this));
    });   
}

function pre_insertion(e){
        var $data = [];
        $data['url']    = e.attr('href');
        $data['method'] = 'POST';
        $data['type']   = 'json';
        $data['data']   = {
            "token" : token
        };

        ajax($data).success(function(callback){
            if(callback.success == true){
                if(callback.modal != undefined){
                    load_modal(callback.modal);
                    after_modal();
                }

                if(callback.reload != undefined && callback.reload == true){
                    location.reload();
                }                               
            }
        });
}

function deletion(){
    $('.btn-delete').click(function(event) {
        event.preventDefault();
        var $data = [];
        var $posdata = new FormData();
        
        $posdata.append('stamp', $(this).attr('data-stamp'));
        $posdata.append('title', $(this).attr('data-title'));

        $data['url']    = siteurl+$(this).attr('data-url');
        $data['method'] = 'POST';
        $data['type']   = 'json';
        $data['data']   = $posdata;

        var $swaldata = {
            title: 'Delete',
            text: $(this).attr('data-title'),
            type: 'warning',
            showCancelButton:true,
        };
        swalalerts($swaldata, function(response){
            if(response == true){
                ajax($data).success(function(callback){
                    if(callback.success == true){
                        var $swaldata = {
                            title: 'Done',
                            text: callback.title+' '+callback.subtitle,
                            type: 'success',
                            showCancelButton:true,
                            closeOnConfirm:true
                        };
                        swalalerts($swaldata, function(done){
                            if(done == true){
                                if($.fn.DataTable){
                                    $('[data-render = datatable]').DataTable().draw();   
                                }
                                if(callback.reload != undefined && callback.reload == true){
                                    location.reload();
                                }
                            }
                        });
                    }
                });
            }
            else{
                swal('Cancelled','','error');
            }   
        });
    });
}

function ajax(data){
    return $.ajax({
        url: data.url,
        type: data.method,
        dataType: data.type,
        data: data.data,
        processData:false,
        contentType:false,
        beforeSend:function(){
            waitingDialog.show('Please wait...');
        }        
    }).success(function(){
        waitingDialog.hide();
    });
}


function load_modal(modaldata){

    var winHeight = $(window).height() * 0.75;

    var $title  = modaldata.title != undefined ? modaldata.title : '';
    var $body   = modaldata.body != undefined ? modaldata.body : '';

    $('#myModal').modal('show');
    $('#myModal').on('shown.bs.modal', function(event){
        var elem = $(event.relatedTarget);
        $('#myModal .modal-body').css('max-height',winHeight+'px');
        $('#myModal').appendTo(elem);
        $('#myModal .modal-title').html($title);
        $('#myModal .modal-body').html($body);
        if(typeof treesbtn == 'function'){
            treesbtn();     
        }
        if(typeof after_modal == 'function'){
            after_modal();  
        }
        if(typeof function_loader == 'function'){
            function_loader();
        }
    });
    $('#myModal').on('hidden.bs.modal', function(event){
        $('body').css('padding-right',0);
    });
}

function after_modal(){
    $('.form-submitting').submit(function(e) {
        e.preventDefault();
    });

    $('.btn-form').on('click', function() {
        var $form       = $(this).parents('form');
        var $posdata    = new FormData($form[0]);
        var $data       = [];

        if($form.find(':file').length > 0){
            $form.find(':file').each(function(index, el) {
                var $name       = $(el).attr('name');
                var $filedata   = $(el)[0].files;
                $.each($filedata, function(i, el) {
                    $posdata.append($name+'['+i+']', $filedata[i]);   
                });
            });
        }

        $data['url']    = $form.attr('action');
        $data['method'] = 'POST';
        $data['type']   = 'json';
        $data['data']   = $posdata;
        ajax($data).success(function(response){
            if(response.success == true){
                if($('[data-render = datatable]').length > 0 ){
                    $('[data-render = datatable]').DataTable().draw();
                }
                $('#myModal').modal('hide');

                if(response.reload != undefined && response.reload == true){
                    location.reload();
                }
            }
            else{
                event_after_submitted(response);
            }
        });
    });     
}

function function_loader(){
    if(typeof datepick == "function"){
        datepick(); 
    }
    if(typeof texteditor == "function"){
        texteditor();
    }
    if(typeof imageuploads == "function"){
        imageuploads();
    }
    if(typeof currency == "function"){
        currency();
    }
    if(typeof checkbox_image == "function"){
        checkbox_image();
    }

    if(typeof lazy_loader == 'function'){
        lazy_loader();
    }

    if(typeof text_autocomplete == 'function'){
        text_autocomplete();
    }

    if(typeof selectiontwo == 'function'){
        selectiontwo();
    }
}

function event_after_submitted($obj){
    var $msg = $obj.msg;
    if($('#myModal .modal-body .alert').length != 0){
        $('#myModal .modal-body .alert').remove();
    }

    if($obj.error != undefined){
        $('.required').find(':input').each(function(index, el) {
            var $name = $(el).attr('name');
            if($.inArray($name, $obj.error) !== -1){
                $(el).parents('.required').addClass('has-error');                
            }
            else{
                $(el).parents('.required').removeClass('has-error');
            }
        });
    }
    $($msg).prependTo('#myModal .modal-body');
}

function lazy_loader(){
    if($.fn.Lazy){
        $(".lazy-loader").Lazy({chainable: true});      
    }   
}

function selectiontwo(){
    if(typeof select2 != undefined && $('.select-two').length > 0){
        $('.select-two').select2({
            language:lang,
        });
    }   
}


function text_autocomplete(){
    $('.autocomplete').focus(function() {
        var d = $(this);
        _autocomplete(d);
    });
}

function _autocomplete(d){
        var dparent = $(d).parent();
        var resources = $(d).data('url');
        var minLength = $(d).data('min-length') != undefined ? $(d).data('min-length') : 3;
        var autolength = dparent.find('.ui-autocomplete').length;
        if(autolength == 0){
            $(d).autocomplete({
                minLength : minLength,
                appendTo: dparent,
                source : base + '/'+resources,
                focus : function(event, ui){
                    $(d).val(ui.item.label);
                    return false;
                }
            });

            $(d).data('ui-autocomplete')._renderItem = function(ul, item){
                var $li = $('<li>');
                if(item.img != undefined){
                    var $img = $('<img>');
                    $img.attr(item.img);
                    $li.append($img);
                }
                $li.append(item.label);
                $li.attr(item.attribute);
                return $li.appendTo(ul);
            };
        }    
}

function currencies(nStr) {
    nStr += '';
    nStr    = nStr.replace(/,/,'');
    var x   = nStr.split('.');
    var x1  = x[0];
    var x2  = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function currencies_reverse(str){
    return str.replace(/,/g,'');
}

function swalalerts(args, callback){
    var $title  = (args.title != undefined) ? args.title : '';
    var $text   = (args.text != undefined) ? args.text : '';
    var $type   = (args.type != undefined) ? args.type : 'warning';
    var $cancel = (args.showCancelButton != undefined) ? args.showCancelButton : true;
    var $close  = (args.closeOnConfirm != undefined) ? args.closeOnConfirm : false;
    var $abort  = (args.closeOnCancel != undefined) ? args.closeOnCancel : true;

    swal({
        title:$title,
        text:$text,
        type:$type,
        showCancelButton:$cancel,
        confirmButtonColor:"#DD6B55",
        cancelButtonText:"Cancel",
        closeOnConfirm:$close,
        closeOnCancel: $abort,
        allowOutsideClick :true,
       },
       function (isConfirm){
            callback(isConfirm);
        }
    );
}