<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Forgot extends Login_Controller
{
	function __construct()
	{
		parent::__construct();		
		$model = array(
			"forgot_model"
		);
		$this->load->model($model);
		$this->vars['_HOOK_EXPIRES'] 	= '+3 months';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->template = $this->session->userdata(DEF_APP.'_'.SESS_PUB.'template') != null ? $this->session->userdata(DEF_APP.'_'.SESS_PUB.'template') : 'default';		
	}

	function index()
	{
		$this->language = array('lost_password','retype','reset_password');
		$this->css 		= array('publogin');
	
		$this->loginview($this->template.'/public/forgot_password');
	}

	function reset_password(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata('__ci_last_regenerate')){
			$bahasa = $this->resources_model->translation(array('retype'), 0);
			if($this->validation($bahasa) == true){
				$email = $this->input->post('email', true);

				$check_email = $this->forgot_model->user_existence($email);
				if(!empty($check_email)){
					$this->load->library('parser');
					/* 
					*  *****************************************************************************************************************************
					*	MAILING
					*  *****************************************************************************************************************************
					*/
					$lang = $this->session->userdata(DEF_APP.'_'.SESS_PUB.'language') ? $this->session->userdata(DEF_APP.'_'.SESS_PUB.'language') : $this->config->item('language');						
					$mailconfig = $this->forgot_model->mail_config();
					$this->load->library('email', $mailconfig);
					
					$temp_data = array();
					$temp_data['_INFO_NAME'] 			= $this->vars['_INFO_NAME'];
					$temp_data['username']				= $check_email['username'];
					$temp_data['email']					= $check_email['email'];
					$temp_data['password']				= $check_email['rpassword'];

					$template = $this->parser->parse_string($mailconfig['reset_pass_'.$lang], $temp_data, true);

					$this->email
						 ->set_newline("\r\n")
						 ->from($mailconfig['smtp_user'],$this->vars['_INFO_NAME'])
						 ->to($check_email['email'])
						 ->subject('Restores account '.$check_email['username'])
						 ->message(htmlspecialchars_decode($template));

					if($this->email->send())
					{
					 	$action = true;
					 	self::set_flash('', 'Email has been submitted');	    	
					}
					else
					{
					 	$action = false;	    	
						self::set_flash('error', $this->email->print_debugger());
					}					
				}
				else{
					self::set_flash('error', 'Account not found');
				}
			}
			else{
				self::set_flash('validation error', '');
			}
		}
		else{
			self::set_flash('Error','Unauthorized restriction');
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	function validation($bahasa){
		$data = array(
				array("name"=>"email","message"=> 'email',"valid_type"=>"required|valid_email"),
				array("name"=>"email2","message"=> $bahasa['retype'].' email',"valid_type"=>"required|matches[email]"),
		);	
		return $this->validate($data);		
	}
}