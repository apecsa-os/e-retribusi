<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Assets extends FI_Controller
{
	private $assets_folder = array("../_vendor", '../_assets',"assets", "assets/uploads");

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$args 	= func_get_args();
		$assets = implode('/', $args);
		$mime  	= get_mime_by_extension($assets);
		
		if(!empty($args) && !empty($mime)){
			$files = $this->input->get('files') ? $this->input->get('files') : $assets;
			$files = explode(',', $files);
			$out   = '';
			foreach ($files as $key => $value) {
				foreach ($this->assets_folder as $f => $v) {
					if(file_exists($v.'/'.$value)){
						if ($mime == 'image/jpeg' || $mime == 'image/png' || $mime == 'image/x-png') {
							$assets = $v.'/'.$value;
							if($this->input->get('thumbs') == true){
								$width 	= $this->input->get('width', true) ? $this->input->get('width', true) : 70 ;
								$height = $this->input->get('height', true) ? $this->input->get('height', true) : 40 ;

								list($w, $h, $type, $attr) = getimagesize($assets);
								$newheight = round(($width / $w) * $h);
								$assets = self::resizer($assets, $width, $newheight);
							}
							$out .= file_get_contents($assets);
						}
						else{
							$out .= file_get_contents($v.'/'.$value);
						}
					}
				}
			}

			if($mime == 'image/jpeg' || $mime == 'image/png' || $mime == 'image/x-png' || $mime == 'font/woff2') {
				$cache = false;
			}
			else{
				$cache = true;
				$data['OUTPUT'] = $out;
				$out = $this->load->view('output', $data, true);			
			}
			self::outputs($out, $mime, $cache);				
		}
	}

	public function clear_cache(){
		$lang = array('cache_clears');
		$bahasa = $this->resources_model->translation($lang, 0);
		$p 	 	= $this->input->get('p');
		if($p == 'pub'){
			$folder = glob(APPPATH.'cache/public/files/*');
			foreach ($folder as $key => $value) {
				if(is_file($value))
		   			unlink($value);
			}
		}
		else{
			$cache = $this->cache->clean();			
		}
		self::set_flash('success', $bahasa['cache_clears']);
		redirect($_SERVER['HTTP_REFERER'],'location');
	}

	private function resizer($path, $width = 70, $height = 40){
		$fileinfo = get_file_info($path);
		$matches = preg_replace('/(.*)\/'.$fileinfo['name'].'$/', '$1', $path);
		$newpath = dirname($path).'/thumbnail';
		if(!is_dir($newpath)){
			mkdir($newpath, 0777, true);
		}

		$thumbnail_image = $newpath.'/'.$fileinfo['name'];
		if(file_exists($thumbnail_image)){
			$result = $thumbnail_image;			
		}
		else{
			$this->load->library('image_lib');
			$ext = array('GD','GD2','ImageMagick');
			$extens = 'GD';
			foreach ($ext as $key => $value) {
				if(extension_loaded($value)){
					$extens = $value;
				}
			}
			$config['image_library'] 	= $extens;
			$config['source_image'] 	= $path;
			$config['create_thumb'] 	= TRUE;
			$config['maintain_ratio'] 	= TRUE;
			$config['width']         	= $width;
			$config['height']         	= $height;
			$config['thumb_marker']		= '';
			$config['new_image']		= $newpath;
			$config['quality']     		= '100%';

			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$this->image_lib->clear();
			$result = $newpath.'/'.$fileinfo['name'];
			self::watermark($result);
		}

		return $result;
	}

	private function watermark($path){
		$config['source_image'] 	= $path;		
		$config['wm_text'] 			= $this->vars['_INFO_NAME'];
		$config['wm_type'] 			= 'text';
		$config['wm_font_path'] 	= BASEPATH.'fonts/texb.ttf';
		$config['wm_font_size'] 	= '10';
		$config['wm_font_color'] 	= 'f3f3f3';
		$config['wm_vrt_alignment'] = 'middle';
		$config['wm_hor_alignment'] = 'center';
		$config['wm_padding'] 		= '5';
		$this->image_lib->initialize($config);
		$this->image_lib->watermark();				
	}

    function outputs($page, $mime = 'text/html', $cache = true){
        // $cache   = $cache == true ? 7200 : 0;
        $cache = 0;
        $this->myhook
             ->mime($mime)
             ->charset('utf-8')
             ->status(200)
             ->expires('+1 year')
             ->cache($cache)
             ->header('Accept-Ranges: bytes')
             ->header('HTTP/1.0 200 OK')
             ->header('HTTP/1.1 200 OK')
             ->header('Cache-Control: no-cache, no-store, must-revalidate')
             ->header('Cache-Control: post-check=0, pre-check=0, max-age=604800')
             ->header('Pragma: no-cache')
             ->header("X-Content-Type-Options: nosniff")
             ->header('X-XSS-Protection: 1; mode=block')
             ->header("X-Frame-Options: SAMEORIGIN")
             ->output($page)
             ->buffering(true)
             ->display();        
    }
}