<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Accessdenied extends FI_Controller
{

    function __construct()
    {
    	parent::__construct();
    }

    function index(){
    	$this->load->view('errors/accessdenied');
    }
}