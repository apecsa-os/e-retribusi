<?php if(!defined('BASEPATH')) exit ('No Direct Script Access Allowed');

/**
* 
*/
class Logout extends Login_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}

	function index(){
		$userdata = $this->session->userdata();
		$userdata = preg_grep('/'.DEF_APP.'_'.SESS_ADMIN.'/', array_keys($userdata));
		$this->login_model->delete_cookies();
		$this->session->unset_userdata($userdata);
		$this->resources_model->clear_cache();
		redirect(BASE_PANEL.'login','location');
	}

	function clear_cache(){
		$cache = $this->resources_model->clear_cache();
		self::set_flash('success','',array("msg"=>"Cache telah dikosongkan!"));
		redirect($_SERVER['HTTP_REFERER'],'location');
	}
}