<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class History extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','delete_success', 'delete_failed', 'truncate');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');

		$this->vars['OUTPUT'] 			= $this->log_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->log_model->json_data();
		
		$this->outputs($data, 'application/json');
	}

	function truncate($timestamp=null, $command = null){
		if($this->vars['_DELETE'] == true){
			$truncate = $this->log_model->truncate();
			$flash = array(
				'success' 	=> $truncate != false ? true : false,
				'title'		=> $this->input->post('title'),
				'subtitle'	=> $truncate != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
			);			
			$this->outputs($flash, 'application/json');
		}
	}					
}