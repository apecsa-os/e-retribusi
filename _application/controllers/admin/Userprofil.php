<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Userprofil extends Admin_Controller
{

	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/userprofil_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array(
			'insert', 'edit', 'delete','form','group',
			'nickname', 'fullname','birthdate','gender','address','city','postcode',
			'male','female'
			);
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form','jqueryui');
		$this->js 						= array('jqueryui','form');
		
		$this->vars['OUTPUT'] 			= $this->userprofil_model->form();
		$this->pageView('output');		
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation(1) == true){
			$cond = array('userid' => $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'userid'));
			$post['province'] = $this->userprofil_model->get_province($post['city']);
			$this->userprofil_model->update($post, $cond);
			$this->log_model->insert_log($this->bahasa['edit'],$post['fullname']);
			self::set_flash('success');
		}
		else{
			self::set_flash('validation error', $post);
		}
		redirect($this->vars['_ROUTED']);
	}

	private function validation($number){
		if($number == 1){
			$data = array(
				array("name"=>"fullname","message"=>$this->bahasa['fullname'],"valid_type"=>"required|min_length[5]"),
				array("name"=>"DOB","message"=>$this->bahasa['DOB'],"valid_type"=>"required"),
				array("name"=>"email","message"=>"Email","valid_type"=>"required|valid_email"),			
			);			
		}
		elseif($number == 2){
			$data = array(
				array("name"=>"current","message"=>'Current password',"valid_type"=>"required"),
				array("name"=>"newpassword","message"=>'New password',"valid_type"=>"required|min_length[5]|differs[current]"),
				array("name"=>"repassword","message"=>"Retype password","valid_type"=>"required|matches[newpassword]"),			
			);			
		}
		return $this->validate($data);		
	}

	function password(){
		$this->css 						= array('form');		
		$this->vars['OUTPUT'] 			= $this->userprofil_model->change_password_form();
		$this->pageView('output_half');		
	}

	function update_password(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation(2) == true){
			$post['rpassword'] = $post['newpassword'];
			$post['password']  = sha1(md5(strtolower($this->input->post('newpassword',true)).$this->config->item('encryption_key')));

			$verify = $this->userprofil_model->details();
			if(!empty($verify) && $verify['rpassword'] == $post['current']){
				unset($post['newpassword'], $post['repassword'], $post['current']);

				$cond = array('userid' => $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'userid'));
				$this->userprofil_model->update($post, $cond);
				self::set_flash('success','Password has been change');				
			}
			else{
				self::set_flash('error','Your current password didn\'t match');
			}
		}
		else{
			self::set_flash('validation error', $post);
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function avatar(){
		$this->css 						= array('form');
		$this->js 						= array('form');

		// $this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->userprofil_model->avatar_form();
		$this->pageView('output_half');		
	}

	function update_avatar(){
		$this->post_check();
		$post = $this->input->post();
		if($_FILES && !empty($_FILES['avatar']['name'][0])){
			$this->load->library('uploader');
			$user = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
			$folder = glob(UPLOAD_PATH.'uploads/avatar/'.$user.'/*');
			foreach ($folder as $key => $value) {
				if(is_file($value))
		   			unlink($value);
			}
			$upload_file = $this->uploader->upload_foto('avatar',$_FILES, 'uploads/avatar/'.$user.'/');
			$image = array();
			foreach ($upload_file as $key => $value) 
			{
				if(empty($value['error'])){
					array_push($image, 'avatar/'.$user.'/'.$value['data']['raw_name'].$value['data']['file_ext']);						
				}
			}
			$post['avatar'] = implode(',',$image);
			
			$this->userprofil_model->update($post, array('userid' => $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'userid') ), true);
			$this->session->set_userdata(DEF_APP.'_'.SESS_ADMIN.'avatar', $post['avatar']);
		}
		else{
			self::set_flash('error','There\'s no file image uploaded ');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}	
}