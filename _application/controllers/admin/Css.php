<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Css extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/css_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','group','delete_success', 'delete_failed','error_valid_ux','error_valid_path');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');

		$this->vars['OUTPUT'] 			= $this->css_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->css_model->json_data();
		
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->css_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->css_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->css_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			if(!isset($post['id'])){
				$check_key = $this->css_model->details($post['cname'], 'cname');
				$action = true;
				if(empty($check_key)){
					$this->css_model->insert($post);
					$this->log_model->insert_log($this->bahasa['insert'],$post['cname']);
					$success = 'success';
					$msg = '';
				}
				else{
					$action 	= false;
					$success 	= 'error';
					$msg = 'css key name for \''.$post['cname'].'\' already exists';
				}
			}
			else{
				$cond = array('cid' => $post['id']);
				$this->css_model->update($post, $cond);
				$this->log_model->insert_log($this->bahasa['edit'],$post['cname']);
				$success = 'success';
				$msg 	 = 'Update success';
				$action  = true;
			}

			$flash = array(
				'success' 		=> $action, 
				'modal'			=> true, 
				'msg' 			=> self::set_flash($success, $msg, false),
			);
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}

		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'cid' => $this->input->post('stamp', true)
		);
		$attemp = $this->css_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"cname","message"=>$this->bahasa['name'],"valid_type"=>"required|min_length[3]"),
			array("name"=>"cpath","message"=>$this->bahasa['error_valid_path'],"valid_type"=>"required"),
			array("name"=>"cgroup","message"=>$this->bahasa['error_valid_ux'],"valid_type"=>"required"),

		);
		return $this->validate($data);		
	}

}