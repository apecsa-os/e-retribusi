<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Usergroup extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/usergroup_model",
			"admin/navigation_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form', 'access_control','group','delete_success', 'delete_failed');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');

		$this->vars['OUTPUT'] 			= $this->usergroup_model->table();
		$this->pageView('output_half');		
	}

	function renders(){
		$this->post_check();
		$data = $this->usergroup_model->json_data();
		
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->usergroup_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->usergroup_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->usergroup_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			if(!isset($post['id'])){
				$this->usergroup_model->insert($post);
				$this->log_model->insert_log($this->bahasa['insert'],$post['groupname']);				
			}
			else{
				$cond = array('groupid' => $post['id']);
				$this->usergroup_model->update($post, $cond);
				$this->log_model->insert_log($this->bahasa['edit'],$post['groupname']);
			}

			$flash = array(
				'success' 		=> true, 
				'modal'			=> true, 
				'msg' 			=> self::set_flash('success', '', false),
			);
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}

		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'groupid' => $this->input->post('stamp', true)
		);
		$attemp = $this->usergroup_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"groupname","message"=>$this->bahasa['group'],"valid_type"=>"required|min_length[3]|alpha"),
		);
		return $this->validate($data);		
	}

	/*
	* USERGROUP ACCESS CONTROL
	*/
	function access($id = null, $name = null){
		$this->css 						= array('form');
		$this->js 						= array('fiquery','form');
		$this->id = $id;

		$menus 	= $this->navigation_model->list_menu();
		$access = $this->usergroup_model->access_data($id);

		$this->vars['OUTPUT'] 			= $this->usergroup_model->form_access($menus, $access);
		$this->pageView('output');

		// $this->post_check();
		// $this->id = $id;
		// $menus 	= $this->navigation_model->list_menu();
		// $access = $this->usergroup_model->access_data($id);
		// $data = array(
		// 	'success' => true, 
		// 	'modal' => array(
		// 		"title" 	=> $this->bahasa['access_control'].' '.$name,
		// 		"body"		=> $this->usergroup_model->form_access($menus, $access),
		// 	),
		// );
		// $this->outputs($data, 'application/json');
	}

	function mod_access(){
		$this->post_check();
		$post 			= $this->input->post();

		if(isset($post['access'])){
			$details 		= $this->usergroup_model->access_data($post['groupid']);
			$access_array 	= array_column($details,'accessid');
			$menuarray 		= array_column($details,'menusid');
			$insert_data 	= array();
			$update_data	= array();
			foreach (array_values($post['access']) as $key => $value) {
				$insert = array_key_exists("inserting", $value) ? $value['inserting'] : 0;
				$update = array_key_exists("updating", $value) ? $value['updating'] : 0;
				$del 	= array_key_exists("deleting", $value) ? $value['deleting'] : 0;
				$print 	= array_key_exists("printing", $value) ? $value['printing'] : 0;

				$menus  = array_filter($details, function($items) use($value){
					if($value['menusid'] == $items['menusid']){
						return $items;
						exit();
					}
				});
				$menus = array_values($menus);
				if($menus){
					$update_data[] = array(
						"accessid"=> $menus[0]['accessid'],
						"menusid" => $value['menusid'],
						"viewing"	  => array_key_exists('viewing', $value) ? $value['viewing'] : 0,
						"inserting"  => $insert,
						"updating"  => $update,
						"deleting"  => $del,
						'printing' => $print
					);
				}
				else{
					$insert_data[] = array(
						"menusid" => $value['menusid'],
						"groupid" => $post['groupid'],
						"viewing"	  => array_key_exists('viewing', $value) ? $value['viewing'] : 0,
						"inserting"  => $insert,
						"updating"  => $update,
						"deleting"  => $del,
						'printing' => $print
					);				
				}
			}

			$delete_data = array_values(array_diff($menuarray, array_column(array_values($post['access']),'menusid')));
			if(count($insert_data) > 0){
				$this->db->insert_batch('usergroup_access', $insert_data);
			}
			if(count($update_data) > 0){
				$this->db->update_batch('usergroup_access', $update_data, 'accessid');
			}

			if(count($delete_data) > 0){
				// $this->db->where('groupid',$post['groupid'])
				// 			->where_in('menusid',$delete_data)
				// 			->delete('usergroup_access');
			}

			$this->log_model->insert_log('Ubah','kontrol akses');
			$flag 	 = 'success';
			$success = true;
			$msg 	 = 'Menu berhasil diubah';
			$url 	 = $this->vars['_ROUTED'];
		}
		else{
			$flag 	 = 'error';
			$success = false;
			$msg 	 = 'Menu gagal diubah';
			$url 	 = $_SERVER['HTTP_REFERER'];
		}

		self::set_flash($flag, $msg, false);
		redirect($url);
	}					
}