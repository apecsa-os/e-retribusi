<?php if(!defined('BASEPATH')) exit ('No Direct Script Access Allowed');

/**
* 
*/
class Login extends Login_Controller
{
	var $post;

	function __construct()
	{
		parent::__construct();
		$this->load->model('registration_model');
	}

	function index(){
		$this->css = array('login');
		$config = $this->query_model->config(5);
		$cdata 	= $config->get_config_data();

		if($this->input->post()) {
			if(isset($_POST['reg_code'])){
				self::registration();
			}
			else{
				if(!empty($cdata['captcha']) && $cdata['captcha'] == 'TRUE'){
					$cword = $this->session->userdata(DEF_APP.'captcha_word');
					$ctime = $this->session->userdata(DEF_APP.'captcha_time');
					if($this->input->post('captcha') == $cword){
						if(file_exists('assets/captcha/'.$ctime.'.jpg') == true){
							unlink('assets/captcha/'.$ctime.'.jpg');
						}
						$this->session->unset_userdata(DEF_APP.'captcha_word');
						$this->session->unset_userdata(DEF_APP.'captcha_time');
					}
					else{
						self::set_flash('error', 'Invalid security code');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}
				self::user_verification();				
			}
		}
		else{
			$this->language = array("username","password");
			if($this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'loggedin') == true){
	   			redirect(BASE_PANEL.'dashboard','location');
	   		}
	   		else{
				if($this->login_model->existence() > 0){
					$this->vars['captcha'] = !empty($cdata['captcha']) ? $cdata['captcha'] : '';
					$this->loginview('admin/login');				
				}
				else{
					array_push($this->language, 'fullname','retype','register');
					$this->vars['reg_code'] = uniqid();
					$this->loginview('admin/registration');	
				}
	   		}
		}
	}

/*
|| -----------------------------------------------------------------------------------------------
|| REGISTRATION
|| -----------------------------------------------------------------------------------------------
*/
	function registradation(){
		$bahasa = $this->resources_model->translation(array("username", "password", "fullname", "retype"), FALSE);

		$data = array(
			array("name"=>"name","message"=>$bahasa["fullname"],"valid_type"=>"required|max_length[100]"),
			array("name"=>"email","message"=>"Email","valid_type"=>"required|valid_email"),
			array("name"=>"username","message"=>$bahasa["username"],"valid_type"=>"required|max_length[45]"),
			array("name"=>"password","message"=>$bahasa["password"],"valid_type"=>"required|min_length[5]|max_length[20]"),
			array("name"=>"repass","message"=>$bahasa["retype"].' '.$bahasa['password'],"valid_type"=>"required|matches[password]"),

			);

		return $this->validate($data);
	}

	function registration(){
		if($this->registradation() == true){
			$data['fullname'] 	= $this->input->post('name',true);
			$data['email'] 		= $this->input->post('email',true);
			$data['username'] 	= strtolower($this->input->post('username',true));
			$data['password'] 	= sha1(md5(strtolower($this->input->post('password',true)).$this->config->item('encryption_key')));
			$data['rpassword']	= $this->input->post('password',true);
			$data['group']		= 1;
			$data['is_master']	= 'Y';
			$data['is_active']	= 1;
			$data['token']		= md5(uniqid().mt_rand(5,15));
			$data['created_at']	= date('Y-m-d H:i:s');

			$action = $this->registration_model->update_master_data($data);
			if($action == true){
				self::set_flash('super admin has been reset');
				redirect(BASE_PANEL,'location');
			}
			else{
				self::set_flash('error', 'Username &amp; password tidak sama');				
			}
		}
		else{
			self::set_flash('validation_error', $this->input->post());				
		}
		$this->session->set_flashdata($error);
		redirect($this->uri->uri_string());		
	}
// -----------------------------------------------------------------------------------------------
// VALIDATION SETTING
// -----------------------------------------------------------------------------------------------

	function validation($action = null){
		$data = array(
			array("name"=>"username","message"=>"username","valid_type"=>"required"),
			array("name"=>"password","message"=>"password","valid_type"=>"required|min_length[5]"),

			);

		return $this->validate($data);
	}

// -----------------------------------------------------------------------------------------------
// USER VERIFICATION
// mengecek apakah user telah terdaftar dalam database
// -----------------------------------------------------------------------------------------------

	function user_verification(){
		$page = $this->uri->uri_string();
		if($this->validation() == true)
		{
			$username = strtolower($this->input->post('username',true));
			$password = sha1(md5(strtolower($this->input->post('password',true)).$this->config->item('encryption_key')));
			$result = $this->login_model->verification($username, $password);
			if(!empty($result)){
				$this->login_model->set_session($result, SESS_ADMIN);
				$page = BASE_PANEL;
			}
			else{
				self::set_flash('error', 'invalid username & password');
			}
		}
		else
		{
			self::set_flash('validation error');
		}
		
		redirect($page);	
	}

}