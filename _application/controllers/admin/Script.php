<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Script extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/script_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','group','delete_success', 'delete_failed','error_valid_ux','error_valid_path');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');

		$this->vars['OUTPUT'] 			= $this->script_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->script_model->json_data();
		
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->script_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->script_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->script_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			if(!isset($post['id'])){
				$check_key = $this->script_model->details($post['sname'], 'sname');
				$action = true;
				if(empty($check_key)){
					$this->script_model->insert($post);
					$this->log_model->insert_log($this->bahasa['insert'],$post['sname']);
					$success = 'success';
					$msg = '';
				}
				else{
					$action 	= false;
					$success 	= 'error';
					$msg = 'javascript key name for \''.$post['sname'].'\' already exists';
				}
			}
			else{
				$cond = array('sid' => $post['id']);
				$this->script_model->update($post, $cond);
				$this->log_model->insert_log($this->bahasa['edit'],$post['sname']);
				$success = 'success';
				$msg 	 = 'Update success';
				$action  = true;
			}

			$flash = array(
				'success' 		=> $action, 
				'modal'			=> true, 
				'msg' 			=> self::set_flash($success, $msg, false),
			);
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}

		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'sid' => $this->input->post('stamp', true)
		);
		$attemp = $this->script_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"sname","message"=>$this->bahasa['name'],"valid_type"=>"required|min_length[3]"),
			array("name"=>"spath","message"=>$this->bahasa['error_valid_path'],"valid_type"=>"required"),
			array("name"=>"sgroup","message"=>$this->bahasa['error_valid_ux'],"valid_type"=>"required"),

		);
		return $this->validate($data);		
	}

}