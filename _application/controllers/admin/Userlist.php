<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Userlist extends Admin_Controller
{

	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/userlist_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','group','delete_success', 'delete_failed','username', 'fullname');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');

		$this->vars['OUTPUT'] 			= $this->userlist_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->userlist_model->json_data();
		
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->userlist_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->userlist_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->userlist_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			if(!isset($post['id'])){
				$post['rpassword'] = $this->input->post('username', true);
				$post['password'] 	= sha1(md5(strtolower($this->input->post('username',true)).$this->config->item('encryption_key')));
				$post['gender']		= 'male';
				$post['city'] 		= 1;
				$post['province'] 	= 21;
				$post['avatar']		= MALEAVA;
				$post['country'] 	= 107;
				$post['is_active'] 	= 1;
				$post['token']		= md5(uniqid().mt_rand(5,15));

				$this->userlist_model->insert($post);
				$this->log_model->insert_log($this->bahasa['insert'],$post['username']);				
			}
			else{
				$cond = array('userid' => $post['id']);
				$this->userlist_model->update($post, $cond);
				$this->log_model->insert_log($this->bahasa['edit'],$post['username']);
			}

			$flash = array(
				'success' 		=> true, 
				'modal'			=> true, 
				'msg' 			=> self::set_flash('success', '', false),
			);
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}

		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'userid' => $this->input->post('stamp', true)
		);
		$attemp = $this->userlist_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"username","message"=>$this->bahasa['name'],"valid_type"=>"required|min_length[5]|alpha"),
			array("name"=>"email","message"=>"Email","valid_type"=>"required|valid_email"),			
		);
		return $this->validate($data);		
	}
}