<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Pengumuman extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/pengumuman_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','group','delete_success', 'delete_failed',
			'announce', 'title','creator','priority','content','expire');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form','jqueryui','jqueryte');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','jqueryui','jqueryte');

		$this->vars['OUTPUT'] 			= $this->pengumuman_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->pengumuman_model->json_data();
		
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->pengumuman_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->pengumuman_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->pengumuman_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			if(!isset($post['id'])){
				$post['redaktur'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'userid');
				$this->pengumuman_model->insert($post,true);
				$this->log_model->insert_log($this->bahasa['insert'],$post['judul']);				
			}
			else{
				$cond = array('id' => $post['id']);
				$this->pengumuman_model->update($post, $cond);
				$this->log_model->insert_log($this->bahasa['edit'],$post['judul']);
			}

			$flash = array(
				'success' 		=> true, 
				'modal'			=> true, 
				'msg' 			=> self::set_flash('success', '', false),
			);
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}

		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->pengumuman_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"judul","message"=>$this->bahasa['title'],"valid_type"=>"required"),
		);
		return $this->validate($data);		
	}					
}