<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Kota extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/kota_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','group','delete_success', 'delete_failed','name', 'province','country','city');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');

		$this->vars['OUTPUT'] 			= $this->kota_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->kota_model->json_data();
		
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->kota_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->kota_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->kota_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			if(!isset($post['id'])){
				$this->kota_model->insert($post);
				$this->log_model->insert_log($this->bahasa['insert'],$post['city_name']);				
			}
			else{
				$cond = array('city_id' => $post['id']);
				$this->kota_model->update($post, $cond);
				$this->log_model->insert_log($this->bahasa['edit'],$post['city_name']);
			}

			$flash = array(
				'success' 		=> true, 
				'modal'			=> true, 
				'msg' 			=> self::set_flash('success', '', false),
			);
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}

		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'city_id' => $this->input->post('stamp', true)
		);
		$attemp = $this->kota_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"city_name","message"=>$this->bahasa['name'],"valid_type"=>"required|min_length[3]"),
		);
		return $this->validate($data);		
	}

}