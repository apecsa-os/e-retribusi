<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Navigasi extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/navigation_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','delete_success', 'delete_failed','order_numb','hide','show');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('fiquery','form');

		$nav_data 				= $this->navigation_model->data();
		$this->vars['OUTPUT'] 	= $this->navigation_model->tree($nav_data);
		$this->pageView('output_half');
	}

	function top(){
		$this->css 						= array('form');
		$this->js 						= array('fiquery','form');

		$nav_data 				= $this->navigation_model->data('top');
		$this->vars['OUTPUT'] 	= $this->navigation_model->tree($nav_data);
		$this->pageView('output_half');
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->navigation_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' menu '.$name,
					"body"		=> $this->navigation_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			$id 	= $this->input->post('id', true);
			
			$action = $this->navigation_model->update($post,array("id"=>$id));
			$logs 	= $this->log_model->insert_log('Ubah',$post['label']);			

			if($action){
				$flash = array(
					'success' 		=> true, 
					'modal'			=> true, 
					'msg' 			=> self::set_flash('success', '', false),
					'reload'		=> true
				);			
			}
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		$this->outputs($flash, 'application/json');		
	}

	private function validation(){
		$data = array(
			array("name"=>"label","message"=>"Nama tersebut","valid_type"=>"required|max_length[45]|callback_menu_names"),
		);
		return $this->validate($data);		
	}

	function menu_names($string){
		if(preg_match('/^[a-zA-Z\s]+$/', $string)){
			return TRUE;
		}
		else{
			$this->form_validation->set_message('menu_names', 'Nama menu tidak boleh mengandung karakter selain huruf alfabet dan spasi');
			return false;
		}
	}					
}