<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Ipfilter extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/ipfilter_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','group','delete_success', 'delete_failed','reason');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');

		$this->vars['OUTPUT'] 			= $this->ipfilter_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->ipfilter_model->json_data();
		
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->ipfilter_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->ipfilter_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->ipfilter_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			$post['ip_address'] = implode('.', $this->input->post('ip_address', true));
			if(!isset($post['id'])){
				$check_key = $this->ipfilter_model->details($post['ip_address'], 'ip_address');
				$action = true;
				if(empty($check_key)){
					$this->ipfilter_model->insert($post);
					$this->log_model->insert_log($this->bahasa['insert'],$post['ip_address']);
					$success = 'success';
					$msg = '';
				}
				else{
					$action 	= false;
					$success 	= 'error';
					$msg = 'Ip address : \''.$post['ip_address'].'\' already exists';
				}
			}
			else{
				$cond = array('id' => $post['id']);
				$this->ipfilter_model->update($post, $cond);
				$this->log_model->insert_log($this->bahasa['edit'],$post['ip_address']);
				$success = 'success';
				$msg 	 = 'Update success';
				$action  = true;
			}

			$flash = array(
				'success' 		=> $action, 
				'modal'			=> true, 
				'msg' 			=> self::set_flash($success, $msg, false),
			);
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}

		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->ipfilter_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"reason","message"=>$this->bahasa['reason'],"valid_type"=>"required"),

		);
		for ($i=0; $i < 4 ; $i++) { 
			$data[] = array("name"=>"ip_address[".$i."]","message"=>'Ip Address '.$i,"valid_type"=>"required|min_length[1]|max_length[3]");
		}
		return $this->validate($data);		
	}

}