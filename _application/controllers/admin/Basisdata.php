<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Basisdata extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$model = array(
			'admin/basisdata_model',
			"admin/log_model"			
		);
		$this->load->model($model);
	}

	function index(){
		$key 	= array('insert', 'edit', 'delete','form','download');
		$this->bahasa = $this->resources_model->translation($key, 0);
		$this->css 						= array('form');

		$this->vars['OUTPUT'] 			= $this->basisdata_model->form();
		$this->pageView('output_half');
	}

	function export(){
		$this->post_check();
		$post = $this->input->post();
		if($this->validation() == true){
			$force = $this->basisdata_model->export($post);
		}
		else{
			self::set_flash('validation error',$post,true);
			redirect($this->vars['_ROUTED']);
		}
	}

	function import(){
		$key 	= array('insert', 'edit', 'delete','form','upload');
		$this->bahasa = $this->resources_model->translation($key, 0);
		$this->css 						= array('form');

		$this->vars['OUTPUT'] 			= $this->basisdata_model->form_upload();
		$this->pageView('output_half');
	}

	function restore(){
		$this->post_check();
		if(array_key_exists('files', $_FILES) && !empty($_FILES['files']['name'])){
			ini_set("memory_limit", "256M");
			ini_set("max_execution_time", "-1");
			
			$file = $_FILES['files'];
	    	$string = read_file($file['tmp_name']);
	    	$string = preg_replace('!/\*.*?\*/!s', '', $string);

	    	$query  = explode(";\r\n", $string);
	    	$this->db->trans_start();
	    	foreach ($query as $key => $value) {
	    		$this->db->query(trim($value));
	    	}
	    	$this->db->trans_complete();

			if($this->db->trans_status() == FALSE){
	          	$this->db->trans_rollback();
	          	self::set_flash('error', 'Restoring database has been failed!');
	        }
	        else{
	          	$this->db->trans_commit();
	          	$logs = $this->log_model->insert_log('upload', $file['name']);
				self::set_flash('success','Database has been restored');
	        }	
		}
		else{
			self::set_flash('error','No file uploaded');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	private function validation(){
		$data = array(
			array("name"=>"name","message"=>'output name',"valid_type"=>"required|min_length[3]|alpha_dash"),
		);
		return $this->validate($data);		
	}
}