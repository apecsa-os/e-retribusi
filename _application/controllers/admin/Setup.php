<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Setup extends Admin_Controller
{
	var $groups;
	var $routed;
	var $title;
	var $id;

	function __construct()
	{
		parent::__construct();
		$model = array(
			'admin/setup_model',
			'admin/log_model',
		);

		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','delete_success', 'delete_failed','order_numb','hide','show');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 				= array('form','jqueryte');
		$this->js 				= array('fiquery','form','jqueryte','masonry');

		$data 					= $this->setup_model->data();
		$this->vars['OUTPUT']	= $this->setup_model->tree($data);
		$this->pageview('output');
	}

	/*
	* -------------------------------------------------------------------------
	* CONFIGURATION
	* -------------------------------------------------------------------------
	*/

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->setup_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' menu '.$name,
					"body"		=> $this->setup_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		if($_FILES && !empty($_FILES['configvalue']['name'][0])){
			$this->load->library('uploader');
			$folder = glob(UPLOAD_PATH.ICON_PATH.'*');
			foreach ($folder as $key => $value) {
				if(is_file($value))
		   			unlink($value);
			}
			$upload_file = $this->uploader->upload_foto('configvalue',$_FILES, ICON_PATH);
			$image = array();
			foreach ($upload_file as $key => $value) 
			{
				if(empty($value['error'])){
					array_push($image, ICON_PATH.$value['data']['raw_name'].$value['data']['file_ext']);						
				}
			}
			$post = implode(',',$image);
		}
		else{
			if($this->validation() == true  && $this->input->post('token') != null){
				$post = $this->input->post('configvalue',true);
			}
			else{
				$post 	= null; 
				$flash 	= array(
					'success' => false, 
					'modal'	  => true,
					'msg' 	  => self::set_flash('validation error', $post, false),
					'error'	  => array_keys($this->form_validation->error_array()),
				);
			}		
		}

		if($post != '' || $post != ' '){
			$id = $this->input->post('id', true);
			$config = array('configvalue' => $post, 'token' => $this->input->post('token',true));
			$action = $this->setup_model->update($config, array('idconfig' => $id) );

			$flash = array(
				'success' 	=> true,
				"modal"		=> true,
				"msg"		=>self::set_flash('success', '', false),
				"reload"	=> true
			);
		}

		$this->outputs($flash, 'application/json');	

		
	}

	private function validation(){
		$data = array(
			array("name"=>"configvalue","message"=>"konfigurasi","valid_type"=>"required"),
		);
		return $this->validate($data);		
	}

	function delete($id){
		$this->request_checker();
		
		$models = $this->sql_model->from('configuration');		
		$where = array("idconfig"=>$id);
		$models->real_deleting($where);
	}
	/*
	* ---------------------------------------------------------------------------- 
	* NEW CONFIGURATION
	* ---------------------------------------------------------------------------- 
	*/
	function newconfig(){
		$details = array();
		$index 	= $this->input->get('in',true);
		$c 		= $this->input->get('c',true);
		if($index > 3){
			$this->id = $c;	
			$details = $this->setup_model->details($this->id);			
			$data['PAGE_TITLE'] = 'New '.$this->title;
			$data['OUTPUT'] 	= $this->setup_model->newconfig_form($details);
			$data['OUTPUT']		.= $this->setup_model->svgicons();
			$this->pageview('output', $data);
		}
		else{
			redirect(404);
		}

	}

	function savenewconfig(){
		$this->request_checker();
		$getcgi = $this->input->get('cgi');
		$cgi = explode('-', $getcgi);
		$post = $this->input->post();
		if($this->configvalidation() == true){
			$models = $this->sql_model->from('configuration');

			if($post['idconfig'] == ''){
				unset($post['idconfig']);
				$post['configlabel'] 	= $post['configname'];
				$post['configname']		= $cgi[0].'_'.str_replace(' ', '_', $post['configname']);
				// $post['configtype']		= 'text';
				$option = explode("\n", $post['configoption']);
				if(count($option) > 0){
					foreach ($option as $key => $value) {
						$option['configoption'][$key] = trim($value);
						$option['configvalue'][$key]  = trim($value);
					}
					$post['configoption']		= implode(',', $option['configoption']);
					$post['configoptionvalue']	= implode(',', $option['configvalue']);
				}
				$action = $models->inserting($post);
			}
			else{
				$option = explode("\n", $post['configoption']);
				if(count($option) > 0){
					foreach ($option as $key => $value) {
						$option['configoption'][$key] = trim($value);
						$option['configvalue'][$key]  = trim($value);
					}
					$post['configoption']		= implode(',', $option['configoption']);
					$post['configoptionvalue']	= implode(',', $option['configvalue']);
				}
								
				$id = $post['idconfig'];
				$action = $models->updating($post,array("idconfig"=>$id));
			}

			if($action == true){
				self::set_flash('success');

				$page = '?cgi='.$this->input->get('cgi');
			}
		}
		else{
			self::set_flash('validation_error', $post);

			$string = '';
			if($post['idconfig']){
				$string = '&c='.$post['idconfig'];
			}				
			$page = '/newconfig?in='.$this->input->get('in').'&cgi='.$getcgi.$string;				
		}
		redirect($this->routed.$page);
	}

	private function configvalidation(){
		$data = array(
			array("name"=>"configname","message"=>"name","valid_type"=>"required"),
		);
		return $this->validate($data);		
	}

	function check_configname($string){
		$data = $this->sql_model->from('configuration')
								->where(array('configname'=>$string))
								->run();
		if(isset($data->result['result'][0]) && count($data->result['result']) > 0){
			$this->form_validation->set_message('check_configname', 'nama konfigurasi '.$string.' telah terdaftar');
			return false;
		}
		else{
			return true;
		}

	}		
}