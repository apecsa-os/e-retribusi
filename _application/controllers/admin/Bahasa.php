<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Bahasa extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/bahasa_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','translation','delete_success', 'delete_failed', 'key_lang');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');
		
		$this->vars['OUTPUT'] 			= $this->bahasa_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->bahasa_model->json_data();
		
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->bahasa_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->bahasa_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->bahasa_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			if(!isset($post['id'])){
				$check_key = $this->bahasa_model->details($post['key_bahasa'], 'key_bahasa');
				$action = true;
				if(empty($check_key)){
					$post['key_bahasa'] = strtolower($post['key_bahasa']);
					$this->bahasa_model->insert($post);
					$this->log_model->insert_log($this->bahasa['insert'],$post['key_bahasa']);
					$success = 'success';
					$msg = '';
				}
				else{
					$action 	= false;
					$success 	= 'error';
					$msg = 'key language for \''.$post['key_bahasa'].'\' already exists';
				}
			}
			else{
				$cond = array('idbahasa' => $post['id']);
				$action = $this->bahasa_model->update($post, $cond);
				$this->log_model->insert_log($this->bahasa['edit'],$post['key_bahasa']);
				$success = 'success';
				$msg = '';
			}

			$flash = array(
				'success' 		=> $action, 
				'modal'			=> true, 
				'msg' 			=> self::set_flash($success, $msg, false),
			);
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}

		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'idbahasa' => $this->input->post('stamp', true)
		);
		$attemp = $this->bahasa_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"key_bahasa","message"=>$this->bahasa['key_lang'],"valid_type"=>"required|min_length[3]|alpha_dash"),
			array("name"=>"indonesia","message"=>$this->bahasa['translation'].' indonesia',"valid_type"=>"required"),
			array("name"=>"english","message"=>$this->bahasa['translation'].' english',"valid_type"=>"required"),			
		);
		return $this->validate($data);		
	}
					
}