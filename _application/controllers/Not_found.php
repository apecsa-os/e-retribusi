<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Not_found extends FI_Controller
{

    function __construct()
    {
    	parent::__construct();
    }

    function index(){
    	$data['css'] = $this->resources_model->css_loader('public');
    	$output 	 = $this->load->view('errors/page_not_found', $data, true);
    	$this->outputs($output);
    }

    function outputs($page, $mime = 'text/html', $buffer = true){
        $this->myhook
             ->mime($mime)
             ->charset('utf-8')
             ->status(200)
             ->expires('+1 year')
             ->cache(true)
             ->header("Accept-Ranges: bytes")
             ->header("HTTP/1.0 200 OK")
             ->header("HTTP/1.1 200 OK")
             ->header("Cache-Control: no-cache, no-store, must-revalidate")
             ->header("Cache-Control: post-check=0, pre-check=0")
             ->header("Pragma: no-cache")
             ->header("X-Content-Type-Options: nosniff")
             ->header("X-XSS-Protection: 1; mode=block")
             ->header("X-Frame-Options: SAMEORIGIN")
             ->output($page)
             ->buffering()
             ->display();        
    }    
}