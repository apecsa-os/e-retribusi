<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Template_model extends CI_Model
{
	var $access;
	function __construct()
	{
		parent::__construct();
        $this->load->driver('cache',array('adapter' => 'file', 'backup' => 'file'));		
		$this->db->flush_cache();
		$this->access 	= explode(',',$this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'access_menu') );
	}
	
	function menu(){
		$sql = $this->db
					->select('
						id, pid, label, icon, controllers, display, posisi, urutan, slug
					')
					->from('menus')
					->where(array('display'=>1, 'must_login'=>'1'))
					->get()
					->result_array();

		$this->load->helper('navigation');
		$nav_func = TEMPLATE.'_sidebar_tree';
		$menu_left 	= self::filter_menu($sql, 'left');
		$menu_left 	= $nav_func($menu_left[0], $menu_left[1], $this->access);
		$menu_top 	= self::filter_menu($sql, 'top');
		$menu_top 	= self::tree_top($menu_top[0], $menu_top[1]);
		$menu['left'] 	= $menu_left;
		$menu['top'] 	= $menu_top;
		return $menu;
	}

	function filter_menu($menu, $pos){
		$filter 		= array_values(array_filter($menu, function($items) use($pos){
			if($items['posisi'] == $pos){
				return $items;
			}
		}));
		$min = !empty($filter) ? min(array_column($filter, 'pid')) : null;
		return array($filter, $min);		
	}

	private function tree_top($menu, $parent = 0, $level = 0){
		$tag = '';
		$result  	= array_values(array_filter($menu, function($items) use($parent){
			if($items['pid'] == $parent && in_array($items['id'], $this->access, true)){
				return $items;
			}
		}));
		if(!empty($result)){
			uasort($result, function($a, $b){
				if($a['urutan'] == $b['urutan']){
					return 0;
				}
				return ($a['urutan'] < $b['urutan']) ? -1 : 1;
			});
			$parentClass = 'nav navbar-top-links navbar-right';
			if($level > 0){
				$parentClass = 'dropdown-menu';
			}
			$tag .= '<ul class="'.$parentClass.'">';
			array_walk($result, function($items, $key) use($menu, $level, $parent, &$tag){
				if($items['pid'] == $parent && in_array($items['id'], $this->access, true)){
					$child = self::tree_top($menu, $items['id'], $level + 1);

					// $link = '#';
					$link = site_url(BASE_PANEL.$items['slug']);
					$liclass = 'dropdown';
					$attribut = ' class="dropdown-toggle" data-toggle="dropdown"';
					$caret 		= '<i class="fa fa-caret-down"></i> ';
					if(empty($child)){
						$liclass 	= '';
						$attribut 	= '';
						$caret 		= '';
						// $link = site_url(BASE_PANEL.$items['slug']);						
					}
					$tag .= '<li class="'.$liclass.'">';
						$tag .= '<a href="'.$link.'"'.$attribut.'>';
							$tag .= '<i class="fa fa-'.$items['icon'].'"></i> ';
							$tag .= ucwords($items['label']);
							$tag .= $caret;
						$tag .= '</a>';
						$tag .= $child;							
					$tag .= '</li>';
				}
			});
			$tag .= '</ul>';
		}
		return $tag;
	} 
	
	function breadcrumb(){
		$string  = $this->uri->uri_string();
		$segment = $this->uri->segment_array();
		$segment = array_values($segment);
		$bc 	 = '<ol class="breadcrumb">';
		$content = '';
		array_walk($segment, function($items, $key) use(&$content, $segment, $string){
			if(!is_numeric($items)){
				$active = '';
				$test = array_key_exists($key + 1, $segment) && is_numeric($segment[$key + 1]) ? '/'.$segment[$key + 1] : ''; 
				$href 	= '<a href="'.site_url( preg_replace('/'.$items.'(.*)/', $items, $string).$test).'">';
				$ehref	= '</a>';
				if(end($segment) == $items){
					$active = ' class="active"';
					$ehref 	= $href = '';
				}
				$content .= '<li'.$active.'>';
					$content .= $href;
					if($key == 0){
						$content .= '<i class="fa fa-home"></i>';
					}
					$text = ucwords(str_replace('-', ' ', $items));
					$content .= preg_replace('/[^a-zA-Z0-9]/', ' ', $text);
					$content .= $ehref;
				$content .= '</li>';				
			}
		});
		$bc = $content != '' ? $bc.$content.'</ol>' : '';
		return $bc;
	}		
}