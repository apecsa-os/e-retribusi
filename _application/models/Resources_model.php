<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Resources_model extends CI_Model
{
	var $hosted ;
	var $lasturl;

	function __construct()
	{
		parent::__construct();
		$url = $_SERVER['HTTP_HOST'];
		preg_match("/[^\.\/]+\.[^\.\/]+$/", $url, $matches);
		$this->hosted = $matches;
		
		$uri = $this->uri->uri_string();
		// $this->lasturl = !empty($uri) && !empty($GLOBALS['lookup']) ? md5($uri) : 'index';
		$this->lasturl = !empty($uri) || !empty($GLOBALS['lookup']) ? md5($uri) : md5('index');
	}

	function __destruct(){
		$var = get_defined_vars();
		foreach ($var as $key => $value) {
			unset($value);
		}
	}

	/* ------------------------------------------------------------------------------------------------------
	** LOAD WEB CONFIGURATION
	** ------------------------------------------------------------------------------------------------------
	*/ 

	function restrict($class="", $prefix=''){
		$sql = $this->db
					->select('a.label as title, a.slug, b.inserting, b.updating, b.deleting, b.viewing, b.printing')
					->from('menus a')
					->join('usergroup_access b','b.menusid = a.id')
					->where(array('b.groupid'=>$this->session->userdata(DEF_APP.'_'.$prefix.'gid'), "a.controllers" => $class, 'b.viewing' => 1))
					->get()
					->row_array();
		return $sql;
	}
	/* ------------------------------------------------------------------------------------------------------
	** LOAD WEB CONFIGURATION
	** ------------------------------------------------------------------------------------------------------
	*/ 

	function web_config(){
		$config = array();
        $q 		= $this->db
        				->select('a.idconfig, a.cgid, a.configname, a.configvalue, a.configtype, b.is_active')
        				->from('configuration a')
        				->join('config_groups b','b.cgid = a.cgid','LEFT')
        				->where(array('a.cgid <' => 3, "b.is_active" => 1))
        				->get()
        				->result();
        $array 	= array_filter($q, function($items) use(&$config){
        	if($items->configtype == "image")
        	{
        		$value = base_url($items->configvalue);
        	}
        	else{
        		$value = $items->configvalue;
        	}
        	$config['_'.strtoupper($items->configname)] = $value;
        }); 
	    $config['_OG_TYPE'] 		= 'website';
	    $config['_OG_DESC'] 		= $config['_INFO_DESC'];
	    $config['_OG_IMAGE']      	= $config['_INFO_LOGO'];

	    define('CURSYMBOL', $config['_CURSYMBOL']);
	    define('THOU_SEP', $config['_THOU_SEP']);
	    define('DEC_SEP', $config['_DEC_SEP']);
	    define('DEC_DIGIT', $config['_DEC_DIGIT']);
	    define('LDATE', $config['_LDATE']);

		return $config;
	}

	function translation($key_array = array(), $is_global = 1){
		$prefix  = !empty($GLOBALS['lookup']) ? SESS_ADMIN : SESS_PUB;
		$session = $this->session->userdata(DEF_APP.'_'.$prefix.'language') ? $this->session->userdata(DEF_APP.'_'.$prefix.'language') : 'indonesia';
		$this->db->select('key_bahasa, '.$session)
				 ->from('bahasa')
				 ->where('is_global', $is_global);
		if(!empty($key_array)){
		$this->db->or_where_in('key_bahasa',$key_array);
		}
		$data = $this->db->get();
		$data = $data->result_array();

		$result = array();
		array_walk($data, function($items, $key) use(&$result,$session){
			$result[$items['key_bahasa']] = $items[$session];
		});
		unset($data, $session);
		return $result;
	}
	
	/* ------------------------------------------------------------------------------------------------------
	** LOAD WEB SEO
	** ------------------------------------------------------------------------------------------------------
	*/ 

	function web_seo(){
        	$SQL = $this->db
        				->select('meta_name, meta_content')
        				->get('seo')
        				->result_array();
        	$meta = array();
        	array_walk($SQL, function($items) use(&$meta){
        		$meta[$items['meta_name']] = $items['meta_content'];
        	});
        return $meta;
	}

	/* ------------------------------------------------------------------------------------------------------
	** LOAD CSS
	** ------------------------------------------------------------------------------------------------------
	*/

	function css_loader($actor = 'public', $class= ''){
			$SQL = $this->db
						->select('cname, cpath, cdn, integrity, crossorigin, cgroup, order_idx, cdn_active')
	        			->where('is_active',1)
	        			->where('global',1)
	        			->or_where_in('cname', $class)
	        			->get('css')
	        			->result_array();

			uasort($SQL, function($a, $b){
				if($a['order_idx'] == $b['order_idx']){
					return 0;
				}
				return ($a['order_idx'] < $b['order_idx']) ? -1:1;
			});
			$files = array();
			array_map(function($a) use($actor, &$files){
				$group = array_map('trim', explode(',', $a['cgroup']));
				if(in_array($actor, $group, true) )
				{					
						if(!empty($this->hosted) && !empty($a['cdn'] && $a['cdn_active'] == '1')){
							$url 		= $a['cdn'];
							$integrity 	= !empty($a['integrity']) ? ' integrity="'.$a['integrity'].'"' : '';
							$cross 		= !empty($a['crossorigin']) ? ' crossorigin="'.$a['crossorigin'].'"' : '';
							$integrity .=  $cross;

							$files['single'][] = '<link rel="stylesheet" type="text/css" href="'.$url.'"'.$integrity.' />';
						}
						else{
							// $files['single'][] = '<link rel="stylesheet" type="text/css" href="'.base_url($a['cpath']).'" />';
							$files['combine'][] = $a['cpath'];
						}
				}
			}, $SQL);
			$single = array_key_exists('single', $files) ? implode('', $files['single']) : '';
			$combine = array_key_exists('combine', $files) ? '<link rel="stylesheet" type="text/css" href="'.base_url($GLOBALS['lookup'].MINIFIER.$this->lasturl.'.css?files='.implode(',', $files['combine'])).'"/>' : '';
		return $single.$combine;
	}
	/* ------------------------------------------------------------------------------------------------------
	** LOAD JAVASCRIPT
	** ------------------------------------------------------------------------------------------------------
	*/

	function js_loader($actor = 'public', $class= ''){
		$SQL = $this->db
					->select('sname, spath, cdn, integrity, crossorigin, sgroup, order_idx, cdn_active')
	       			->where('is_active',1)
	       			->where('global',1)
	       			->or_where_in('sname', $class)
	       			->get('javascript')
	       			->result_array();
		uasort($SQL, function($a, $b){
			if($a['order_idx'] == $b['order_idx']){
				return 0;
			}
			return ($a['order_idx'] < $b['order_idx']) ? -1:1;
		});	

		$files = array();	       			
		$js = array_map(function($a) use($actor, &$files){
			$group = explode(',', $a['sgroup']);
			if(in_array($actor, $group, true) )
			{
					if(!empty($this->hosted) && !empty($a['cdn'] && $a['cdn_active'] == '1')){
						$url 		= $a['cdn'];
						$integrity 	= !empty($a['integrity']) ? ' integrity="'.$a['integrity'].'"' : '';
						$cross 		= !empty($a['crossorigin']) ? ' crossorigin="'.$a['crossorigin'].'"' : '';
						$integrity .=  $cross;
						
						$files['single'][] = '<script type="text/javascript" src="'.$url.'"'.$integrity.'></script>';
					}
					else{
						$files['combine'][] = $a['spath'];
					}
			}
		}, $SQL);

		$single = array_key_exists('single', $files) ? implode('', $files['single']) : '';
		$combine = array_key_exists('combine', $files) ? '<script type="text/javascript" src="'.base_url($GLOBALS['lookup'].MINIFIER.$this->lasturl.'.js?files='.implode(',', $files['combine'])).'"></script>' : '';

		return $single.$combine;		
	}

	function ip_filter(){
		if($this->cache->get('ipfilter') == false)
		{
			$SQL = $this->db
						->select('ip_address, reason')
						->from('ip_filter')
						->get()
						->result_array();

			$this->cache->save('ipfilter',$SQL, 7200);
		}		
		
		$cache = $this->cache->get('ipfilter');
		$result = !empty($cache) ? array_values(array_column($cache,'ip_address')) : false;
		return $result;
	}

	function visitor(){
		$ip = $_SERVER['REMOTE_ADDR'];
		$visitor = $this->db
						->select('ipaddress')
						->from('visitor')
						->where('ipaddress', $ip)
						->get()
						->row();
		if(empty($visitor)){
			$this->load->library('geoplugin');
			$geo_data =  $this->geoplugin
							  ->geodata()
							  ->agent();
			$store_visitor = array(
				"ipaddress" => array_key_exists('geo', $geo_data) ? $geo_data->geo['request'] : $_SERVER['REMOTE_ADDR'],
				"country"	=> array_key_exists('geo', $geo_data) ? $geo_data->geo['countryName'] : 'N/A',
				"browser"	=> array_key_exists('browser', $geo_data) ? $geo_data->browser['browser'] : 'N/A',
				"latitude"	=> array_key_exists('geo', $geo_data) ? $geo_data->geo['latitude'] : 0,
				"longitude"	=> array_key_exists('geo', $geo_data) ? $geo_data->geo['longitude'] : 0,
			); 
			$this->db->insert('visitor', $store_visitor);
			return true;
		}
		else{
			return false;
		}
	}

	function message_counter(){
			$SQL = $this->db->from('inbox')
							->where(array("is_read"=>0))
							->count_all_results();
		return $SQL;
	}
	
	function clear_cache(){
		$cache = $this->cache->clean();

		return $cache;
	}		
}