<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Query_model extends CI_Model
{
	protected $mvars = array();
	var $query;
	var $callbacks;

	function config($id)
	{
		$result = array();
		$SQL = $this->db
					->select('configname, configvalue')
					->where('cgid',$id)
					->get('configuration')
					->result();
		array_walk($SQL, function($items) use(&$result){
			$result[$items->configname] = $items->configvalue; 
		});
		unset($SQL);
		$this->mvars =  $result;
		return $this;
	}

	function get_config_data(){
		return $this->mvars;
	}

	function initialize($page = 1, $total = 1, $limit = 10, $offset = 0){
		$this->query['page'] 	= $page;		
		$this->query['total'] 	= $total;
		$this->query['limit'] 	= $limit;
		$this->query['offset'] 	= $offset;
		return $this;
	}

	function page_info($info = 'data'){
		$page_data 					= ($this->query['page'] * $this->query['limit']);	
		$until		 				= $page_data < $this->query['total'] ? $page_data : $this->query['total'];
		$offset 					= $this->query['offset'] < $until ? $this->query['offset']+1 : 0;
		$this->query['info'] 		= $offset.' ~ <span>'.$until.'</span> / <span>'.$this->query['total'].'</span> '.$info;
		return $this;
	}

	function pagination($total = null,$limit = null){
		$this->load->library('pagination');
		$segment 							= $this->uri->segment_array();
		$config['base_url'] 				= site_url($this->uri->uri_string());
		$config['total_rows'] 				= !empty($this->query['total']) ? $this->query['total'] : $total;
		$config['per_page'] 				= !empty($this->query['limit']) ? $this->query['limit'] : $limit;
		$config['uri_segment'] 				= count($segment);
		$config['query_string_segment']		= 'page';
		$config['use_page_numbers'] 		= TRUE;
		$config['page_query_string'] 		= TRUE;
		$config['reuse_query_string'] 		= TRUE;
		$config['prefix']					= "";
		$config['suffix']					= "";
		$config['use_global_url_suffix']	= FALSE;
		$config['full_tag_open']			= '<div class="pagination-box"><ul class="pagination pagination-sm no-margin pull-right">';
		$config['full_tag_close']			= '</ul></div>';
		$config['first_link']				= '<i class="fa fa-angle-double-left" style="line-height:19px;"></i>';
		$config['first_tag_open']			= "<li>";
		$config['first_tag_close']			= "</li>";
		$config['first_url']				= "";
		$config['last_link']				= '<i class="fa fa-angle-double-right" style="line-height:19px;"></i>';		
		$config['last_tag_open']			= "<li>";
		$config['last_tag_close']			= "</li>";
		$config['next_link']				= '<i class="fa fa-angle-right icon" style="line-height:19px;"></i>';
		$config['next_tag_open']			= "<li>";
		$config['next_tag_close']			= "</li>";
		$config['prev_link']				= '<i class="fa fa-angle-left icon" style="line-height:19px;"></i>';
		$config['prev_tag_open']			= "<li>";
		$config['prev_tag_close']			= "</li>";
		$config['cur_tag_open']				= '<li class="active"><a href="#">';
		$config['cur_tag_close']			= '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open']				= "<li>";
		$config['num_tag_close']			= "</li>";

		$this->pagination->initialize($config);
		$this->query['links'] = ($this->pagination->create_links());
		return $this->query;
	}

}