<?php
/**
* 
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_model extends CI_Model
{
	function existence(){
		$number = $this->db->from('user')
						   ->where(array('group'=>1,'is_master'=>'Y','is_active'=>1))
						   ->count_all_results();
		return $number;
	}

	function verification($username,$password){
		$this->db
			 ->select('a.userid, a.username, a.avatar, a.group, a.token, b.groupname')
			 ->from('user a')
			 ->join('usergroup b','b.groupid = a.group','left')
			 ->where(array('a.password'=>$password,"a.is_active"=>1));

		if(preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/', $username) == true)
		{
			$this->db->where('email',$username);
		}
		else
		{
			$this->db->where('username',$username);
		}	

		$data = $this->db
					 ->get()
					 ->row();
		return $data;
	}	

	function set_access_data($id){
		$cond = array("a.groupid"=>$id, 'a.viewing' => 1);

		$data = $this->db
						->select('
							a.accessid, a.menusid, a.inserting, a.updating, a.deleting, a.printing, a.viewing,
							b.controllers
							')
						->from('usergroup_access a')
						->join('menus b','b.id = a.menusid')
						->where($cond)
						->get()
						->result_array();
		return $data;
	}

	function set_session($data, $prefix = ''){
		if($data){
			$referer = $_SERVER['HTTP_REFERER'];
			$matches = preg_match('/'.str_replace('/','',BASE_PANEL).'/', $referer);
			
			$session = array(
						DEF_APP."_".$prefix."userid"		=> $data->userid,
						DEF_APP."_".$prefix."username"		=> $data->username,
						DEF_APP."_".$prefix."avatar"		=> $data->avatar,
						DEF_APP."_".$prefix."gid"			=> $data->group,
						DEF_APP."_".$prefix."group"			=> $data->groupname,
						DEF_APP."_".$prefix."token"			=> $data->token,
						DEF_APP."_".$prefix."loggedin"		=> TRUE
						);
		
			if($matches == true){
				$access_data = array_column(self::set_access_data($data->group), 'menusid');
				uasort($access_data, function($a, $b){
					if($a == $b){
						return false;
					}
				return $a < $b ? -1 : 1;
				});
				$session[DEF_APP.'_'.$prefix.'access_menu'] = implode(',',$access_data);
				// $session[DEF_APP."_".$prefix."access_menu"] = implode(',',array_values(array_column(self::set_access_data($data->group),'menusid')) );
			}

			$this->session->set_userdata($session);
		}
	}

	function set_cookies($set = false, $cookie_data = array(), $expire = null){
		if($set == true){
			$cookie_data = (array) $cookie_data;
			$this->load->helper('cookie');
			if(!empty($cookie_data)){
				array_filter($cookie_data, function($items, $key) use($expire){
					$cookie = array(
					    'name'   => $key,
					    'value'  => $items,
					    'expire' => !empty($expire) ? $expire : '1209600',
					    'domain' => $_SERVER['REMOTE_HOST'],
					    'path'   => '/',
					    'prefix' => '',
					    'secure' => FALSE,
					    'httponly' => FALSE
					);
					set_cookie($cookie);			
				}, ARRAY_FILTER_USE_BOTH);				
			}		
		}
	}

	function delete_cookies(){
		$this->load->helper('cookie');
		$prefix 	 =  $this->config->item('cookie_prefix');
		$cookie_data = preg_filter('/'.$prefix.'/', '', array_keys($_COOKIE));

		array_filter($cookie_data, function($items, $key) use($cookie_data){
			$cookie = array(
		    	'name'   => $items,
		    	'domain' => array_key_exists('domain', $cookie_data) ? $cookie_data['domain'] : $_SERVER['REMOTE_HOST'],
		    	'path'   => array_key_exists('path', $cookie_data) ? $cookie_data['path'] : '/',
		    	'prefix' => array_key_exists('prefix', $cookie_data) ? $cookie_data['prefix'] : '',
			);

			delete_cookie($cookie);			
		}, ARRAY_FILTER_USE_BOTH);
	}

	function interpreter($data){
		if(!empty($data)){
			$this->load->library('encrypt');
			$username 	= $this->encrypt->decode($data['username'],ENCFK);
			$password	= $this->encrypt->decode($data['password'], ENCFK);
			$result = self::verification($username, $password);
			if(!empty($result)){
				$this->login_model->set_session($result, SESS_PUB);
			}
		}
	}
}