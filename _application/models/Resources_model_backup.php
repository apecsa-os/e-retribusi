<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Resources_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function __destruct(){
		$var = get_defined_vars();
		foreach ($var as $key => $value) {
			unset($value);
		}
	}

	/* ------------------------------------------------------------------------------------------------------
	** LOAD WEB CONFIGURATION
	** ------------------------------------------------------------------------------------------------------
	*/ 

	function restrict($class=""){
		$sql = $this->db
					->select('a.label as title, b.insert, b.update, b.delete')
					->from('menus a')
					->join('usergroup_access b','b.menusid = a.id')
					->where(array('b.groupid'=>$this->session->userdata(DEF_APP.'_gid'), "a.control_name" => $class))
					->get()
					->row_array();
		return $sql;
	}
	/* ------------------------------------------------------------------------------------------------------
	** LOAD WEB CONFIGURATION
	** ------------------------------------------------------------------------------------------------------
	*/ 

	function web_config(){
		$config = array();
        $q 		= $this->db
        				->select('a.idconfig, a.cgid, a.configname, a.configvalue, a.configtype, b.is_active')
        				->from('configuration a')
        				->join('config_groups b','b.cgid = a.cgid','LEFT')
        				->where(array('a.cgid <' => 3, "b.is_active" => 1))
        				->get()
        				->result();
        $array 	= array_filter($q, function($items) use(&$config){
        	if($items->configtype == "image")
        	{
        		$value = base_url($items->configvalue);
        	}
        	else{
        		$value = $items->configvalue;
        	}
        	$config['_'.strtoupper($items->configname)] = $value;
        }); 
	    $config['_OG_TYPE'] 		= 'website';
	    $config['_OG_DESC'] 		= $config['_INFO_DESC'];
	    $config['_OG_IMAGE']      	= $config['_INFO_LOGO'];

        define('CURSYMBOL',$config['_PREF_CURRPREFIX']);
        if(isset($config['_PREF_CURRENCY']) && $config['_PREF_CURRENCY'] == 1 ){
            define('THOU_SEP', '.');
            define('DEC_SEP',',');
            define('DEC_DIGIT','0');
            define('LDATE', 'd-m-Y');
        }
        else{
            define('THOU_SEP', ',');
            define('DEC_SEP','.');
            define('DEC_DIGIT','2');
            define('LDATE', 'Y-m-d');            
        }        
		return $config;
	}

	function translation($key_array = array()){
		$session = $this->session->userdata('language') ? $this->session->userdata('language') : 'indonesia';
		$this->db->select('key, '.$session)
				 ->from('bahasa')
				 ->where('is_global',1);
		if(!empty($key_array)){
		$this->db->or_where_in('key',$key_array);
		}
		$data = $this->db->get();
		$data = $data->result_array();

		$result = array();
		array_walk($data, function($items, $key) use(&$result,$session){
			$result[$items['key']] = $items[$session];
		});
		unset($data, $session);
		return $result;
	}
	
	/* ------------------------------------------------------------------------------------------------------
	** LOAD WEB SEO
	** ------------------------------------------------------------------------------------------------------
	*/ 

	function web_seo(){
		if($this->cache->get('seo') == false)
		{
        	$SQL = $this->db
        				->select('meta_name, meta_content')
        				->get('seo')
        				->result();
			$meta = array_map(function($items){
				if($items->meta_name == "icon"){
					$meta = '<link rel="shortcut icon" type="image/x-icon" href="'.base_url($items->meta_content).'"/>';
				}
				elseif ($items->meta_name == "charset") {
					$meta = '<meta charset="'.$items->meta_content.'"/>';
				}
				else{
					$meta = '<meta name="'.$items->meta_name.'" content="'.$items->meta_content.'" />';
				}
				return $meta;
			},$SQL);
			$this->cache->save('seo',$meta, 7200);			
		}
        
        $meta = $this->cache->get('seo');
        $meta = implode('',$meta);
        return $meta;
	}

	/* ------------------------------------------------------------------------------------------------------
	** LOAD CSS
	** ------------------------------------------------------------------------------------------------------
	*/

	function css_loader($actor = 'public', $class= ''){
			$SQL = $this->db
						->select('cname, cpath, cdn, integrity, crossorigin, cgroup, order_idx')
	        			->where('is_active',1)
	        			->group_start()
	        			->where('global',1)
	        			->or_like('include_in', $class)
	        			->group_end()
	        			->get('css')
	        			->result_array();

			uasort($SQL, function($a, $b){
				if($a['order_idx'] == $b['order_idx']){
					return 0;
				}
				return ($a['order_idx'] < $b['order_idx']) ? -1:1;
			});	
		return $SQL;
	}
	/* ------------------------------------------------------------------------------------------------------
	** LOAD JAVASCRIPT
	** ------------------------------------------------------------------------------------------------------
	*/

	function js_loader($actor = 'public', $class= ''){
		$SQL = $this->db
					->select('sname, spath, cdn, integrity, crossorigin, sgroup, order_idx')
	       			->where('is_active',1)
	       			->group_start()
	       			->where('global',1)
	       			->or_like('include_in', $class)
	       			->group_end()
	       			->get('javascript')
	       			->result_array();

		uasort($SQL, function($a, $b){
			if($a['order_idx'] == $b['order_idx']){
				return 0;
			}
			return ($a['order_idx'] < $b['order_idx']) ? -1:1;
		});		       			

		return $SQL;
	}

	function ip_filter(){
		if($this->cache->get('ipfilter') == false)
		{
			$SQL = $this->db
						->select('ip_address, reason')
						->from('ip_filter')
						->get()
						->result_array();

			$this->cache->save('ipfilter',$SQL, 7200);
		}		
		
		$cache = $this->cache->get('ipfilter');
		$result = array_values(array_column($cache,'ip_address'));
		return $result;
	}

	function message_counter(){
			$SQL = $this->db->from('inbox')
							->where(array("is_read"=>0))
							->count_all_results();
		return $SQL;
	}
	
	function clear_cache(){
		$cache = $this->cache->clean();

		return $cache;
	}		
}