<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Forgot_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array('fiform');
		$this->load->library($library);
	}

	function user_existence($data){
		$sql = $this->db
					->select('username, email, rpassword')
					->from('user')
					->where('email', $data)
					->get()
					->row_array();
		return $sql;
	}

	function mail_config(){
		$data =	$this->db
					 ->select('configname, configvalue')
					 ->from('configuration')
					 ->where_in('cgid', array(3,6))
					 ->get()
					 ->result();
		$result = array();
		foreach ($data as $key => $value) {
			$result[$value->configname] = $value->configvalue;
		}
		return $result;
	}		
}