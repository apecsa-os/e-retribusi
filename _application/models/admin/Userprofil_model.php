<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userprofil_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function details(){
		$id 	= $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'userid'); 
		$data = $this->db
					 ->select('fullname, nickname, gender, DOB, address, city, postcode, email, rpassword')
					 ->from('user')
					 ->where('userid', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form(){
		$data = self::details();
		$form = $this->fiform;

		$action = $this->bahasa['edit'];

		$fullname 	= !empty($data) ? $data['fullname'] : '';
		$nickname 	= !empty($data) ? $data['nickname'] : '';
		$gender 	= !empty($data) ? $data['gender'] 	: '';
		$DOB 		= !empty($data) ? $data['DOB'] 		: '';
		$address 	= !empty($data) ? $data['address'] 	: '';
		$city 		= !empty($data) ? $data['city'] 	: '';		
		$postcode 	= !empty($data) ? $data['postcode'] : '';
		$email 		= !empty($data) ? $data['email'] 	: '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('fullname','DOB','email')
					 ->field('text', $this->bahasa['fullname'], array('name' => 'fullname', "placeholder"=>$this->bahasa['fullname'], "value"=>$fullname) )
					 ->field('text', $this->bahasa['nickname'], array('name' => 'nickname', "placeholder"=>$this->bahasa['nickname'], "value"=>$nickname) )
					 ->new_row()
					 ->field('text', $this->bahasa['birthdate'], array('name' => 'DOB', "placeholder"=>$this->bahasa['birthdate'], "value"=>$DOB, 'class'=>'datepicker') )					 
					 ->field('radio', $this->bahasa['gender'], array('name' => 'gender', "option"=>array('male' => $this->bahasa['male'], 'female' => $this->bahasa['female']), "value"=>$gender, "inline" => true) )
					 ->new_row()
					 ->field('text', 'email', array('name' => 'email', 'placeholder' => 'email@mail.com', 'value' => $email) )
					 ->new_row()
					 ->field('textarea', $this->bahasa['address'], array('name' => 'address', 'placeholder' => $this->bahasa['address'], 'value' => $address) )
					 ->new_row()
					 ->field('dropdown', $this->bahasa['city'], array('name' => 'city', 'option' => self::city_option(), 'value' => $city, 'option_value' => 'city_id', 'option_label' => 'city_name') )
					 ->field('text', $this->bahasa['postcode'], array('name' => 'postcode', 'placeholder' => $this->bahasa['postcode'], 'value' => $postcode) )					 
					 ->btn($action, array('type' => 'submit','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	private function city_option(){
		$data = $this->db
					 ->select('city_name, city_id')
					 ->from('city')
					 ->get()
					 ->result_array();
		uasort($data, function($a, $b){
			if($a['city_name'] == $b['city_name']){
				return 0;
			}

			return $a['city_name'] < $b['city_name'] ? -1 : 1;
		});
		return $data;
	}

	function get_province($city_id){
		$data = $this->db
					 ->select('city_name, city_id, province')
					 ->from('city')
					 ->get()
					 ->row();
		return $data->province;
	}
	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			if(array_key_exists('id', $postdata)){
				unset($postdata['id']);				
			}
			$postdata = array_map('trim', $postdata);			
			$this->db->update('user', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('user', $where);
	}
	

	function change_password_form(){
		$form = $this->fiform;

		$action = $this->bahasa['edit'];

		$form = $form->create($this->vars['_ROUTED'].'/update_password', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('current','newpassword','repassword')
					 ->field('password', 'Current password', array('name' => 'current', "placeholder"=>'Current password') )
					 ->new_row()
					 ->field('password', 'New password', array('name' => 'newpassword', "placeholder"=>'New password') )					 
					 ->new_row()
					 ->field('password', 'Retype password', array('name' => 'repassword', 'placeholder' => 'Retype password') )
					 ->btn($action, array('type' => 'submit','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function avatar_form(){
		$form = $this->fiform;

		$action = $this->bahasa['edit'];
		$img 	= $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'avatar') ? $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'avatar') : MALEAVA;
		$form = $form->create($this->vars['_ROUTED'].'/update_avatar', array("class" => 'form-horizontal form-submitting','enctype' => 'multipart/form-data'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('current')
					 ->field('filesimage', 'Browse image', array('name' => 'avatar', 'value' => $this->vars['minifier'].$img, 'class' => 'lazy-loader') )
					 ->btn($action, array('type' => 'submit','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}	
}