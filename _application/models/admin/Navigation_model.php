<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Navigation_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fiform'
		);
		$this->load->library($library);
	}

	function data($posisi = null){
		$posisi = !empty($posisi) ? $posisi : 'left';
		$sql = $this->db 
					->select('id, pid, label, icon, display, urutan')
					->from('menus')
					->where('posisi',$posisi)
					->get()
					->result();
		uasort($sql, function($a, $b){
			if($a->urutan == $b->urutan){
				return 0;
			}
			return ($a->urutan < $b->urutan) ? -1 : 1;
		});
		return $sql;
	}
	function tree($data, $parent = 0, $index = null, $label = ''){
		$io = '';

		$close_io = '';
		if($parent == 0){
			$io = '<table class="table table-bordered table-hover table-striped">';
			$io .= '<thead>';
				$io .= '<tr>';
					$io .= '<td width="1%">&nbsp;</td>';
					$io .= '<th width="70%" class="text-center">Menu</th>';
					$io .= ($this->vars['_UPDATE'] == TRUE) ? '<th class="text-center">Control</th>' : '';
				$io .= '</tr>';
			$io .= '</thead>';
			$io .= '<tbody>';
			$close_io = '</tbody></table>';
		}

		$flag   = ' >> ';
		foreach ($data as $key => $value) {
			if($value->pid == $parent){
				$child 	= self::tree($data, $value->id, $index+1, $label.$value->label.$flag);
				$class  = ($index == 0) ? 'row-parent' : 'row-child';
				$io .= '<tr class="'.$class.'">';
					$io .= '<td>';
						$io .= (!empty($child) && $index == 0) ? '<a><i class="fa row-expanded"></i></a>' : '';
					$io .= '</td>';
					$io .= '<td>'.ucwords($label.$value->label).'</td>';
					if($this->vars['_UPDATE'] == TRUE){
						$io .= '<td class="text-center">';
						$io .= '<a href="'.site_url($this->vars['_ROUTED']).'/edit/'.$value->id.'/'.seo_url($value->label).'" class="btn btn-primary btn-xs btn-add"><i class="fa fa-edit"></i> Edit</a>';
						$io .= '</td>';
					}					
				$io .= '</tr>';
				$io .= $child;
			}
		}

		$io .= $close_io;
		return $io;
	}

	function details($id = null){
		if($id == null){
			$id = $this->id;
		}
		$src = $this->db
					->select('id, pid, label, icon, display, urutan, slug, posisi, must_login')
					->from('menus')
					->where(array('id'=>$id))
					->get()
					->row_array();	
		return $src;		
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		$readonly = array();
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['id']);
			$readonly = array('opt-exclude'=>$data['id']);
		}

		$label 		= !empty($data) ? $data['label'] 	: '';
		$pid 		= !empty($data) ? $data['pid'] 		: null;
		$icon 		= !empty($data) ? $data['icon'] 	: '';
		$urutan 	= !empty($data) ? $data['urutan'] 	: 1;
		$display 	= !empty($data) ? $data['display'] 	: 1;
		$posisi 	= !empty($data) ? $data['posisi'] 	: 'left';
		$must_login 	= !empty($data) ? $data['must_login'] 	: 1;


		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('label','pid')
					 ->field(
					 		'text', 
					 		'menus', 
					 		array('name' => 'label', "placeholder" => 'Menus', "value"=>$label) 
					 	)
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'is aktif', 
					 		array('name' => 'display', "option" => array($this->bahasa['hide'], $this->bahasa['show']), "value"=>$display, "inline" => true) 
					 	)					 						 
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'display on', 
					 		array('name' => 'posisi', "value"=>$posisi, 'option' => array('top' => 'top', 'left'=>'left'), "inline" => true)
					 	)
					 ->new_row()					 					 
					 ->field(
					 		'trees', 
					 		'menus directive', 
					 		array_merge(array('name' => 'pid', "selected"=>$pid, 'option' => self::menu_option($posisi), 'opt-index' => 'id', 'opt-label' => 'label', 'opt-group' => 'pid', 'opt-start' => 0), $readonly) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'fontawesome icon', 
					 		array('name' => 'icon', "placeholder"=>'Menus icon', "value"=>$icon) 
					 	)
					 ->new_row()
					 ->field(
					 		'number', 
					 		$this->bahasa['order_numb'], 
					 		array('name' => 'urutan', "placeholder"=>$this->bahasa['order_numb'], "value"=>$urutan) 
					 	)
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'login required', 
					 		array('name' => 'must_login', "value"=>$must_login, 'option' => array('No','Yes'), "inline" => true)
					 	)					 						 
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	private function menu_option($posisi = null){
		$posisi = !empty($posisi) ? $posisi : 'left';
		$data = $this->db
					 ->select('id, pid, label')
					 ->from('menus')
					 ->where(array('display' => 1, 'posisi' => $posisi))
					 ->get()
					 ->result();
		return $data;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);

			if($postdata['pid'] != 1){
				$get_parents = self::details($postdata['pid']);
				$postdata['slug'] 	= !empty($get_parents['slug']) ? $get_parents['slug'].'/'.seo_url($postdata['label']) : seo_url($postdata['label']);
				$postdata['posisi']	= $postdata['posisi'] != $get_parents['posisi'] ? $get_parents['posisi'] : $postdata['posisi'];				
			}
			else{
				$postdata['pid']	= NULL;
				$postdata['slug']	= seo_url($postdata['label']);
			}
			$result = $this->db->update('menus', $postdata, $where);
		}
		else{
			$result = false;
		}
		return $result;
	}

	function list_menu(){
		$data = $this->db
					->select('id,pid,label,icon,action_case')
					->from('menus')
					->where(array("display"=>1,"must_login"=>1))
					->get()
					->result();
		return $data;
	}	
}