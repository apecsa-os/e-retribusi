<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Log_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
		);
		$this->load->library($library);
	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('date','username','ip','module','notes');
		if($this->vars['_DELETE'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['truncate'], 
		     		array(
		     			'data-url' 		=> $this->vars['_ROUTED'].'/truncate',
		     			'data-stamp' 	=> strtotime(date('Y-m-d')),
		     			'data-title'	=> 'Truncate (erase) all data', 
		     			'class' => 'btn btn-sm btn-primary btn-delete' 
		     		), 
		     		'<i class="fa fa-trash"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0,1,2,3,4)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.date, b.username, a.ip_address, a.module, a.notes, a.id')
					 ->from('history a')
					 ->join('user b','b.userid = a.userid')
					 ->data_format('date','date_convertion')
					 ->data_format('module', 'capitalize')
					 ;		
		$data = $this->fitable->aodata();
		return $data;
	}

	function date_convertion($string){
		return date($this->vars['_LDATE'].' H:i:s', strtotime($string));
	}

	function capitalize($string){
		return ucfirst($string);
	}

	function insert_log($cmd = '', $notes = null){
		$logs = array(
					"date"		 => date('Y-m-d H:i:s'),
					"userid"	 => $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'userid'),
					"ip_address" => $_SERVER['REMOTE_ADDR'],
					"module"	 => $this->vars['_TITLE'],
					"notes"		 => $cmd.' : '.$notes
				);
		$sql = $this->db->insert('history',$logs);
		return $sql;		
	}

	function truncate(){
		return $this->db->truncate('history');
	}
}