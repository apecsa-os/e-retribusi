<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ipfilter_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
		$this->load->helper('text');		
	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('ip address','reason', 'action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0,1)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.ip_address, a.reason, a.id')
					 ->from('ip_filter a');
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 );
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function details($id = null, $key = 'id'){
		$data = $this->db
					 ->select('ip_address, reason, id')
					 ->from('ip_filter')
					 ->where($key, $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['id']);
		}

		$ip_address 	= !empty($data) ? explode('.',$data['ip_address'])	: '';
		$reason 		= !empty($data) ? $data['reason'] 					: '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('ip_address[0]','ip_address[1]','ip_address[2]','ip_address[3]')
					 ->field(
					 	'number', 
					 	'Ip address',
					 	array(
					 		'name' 			=> 'ip_address[0]', 
					 		"placeholder"	=> '000', 
					 		"value"			=> !empty($ip_address[0]) ? $ip_address[0] : '',
					 		'min'			=> 1,
					 		"max"			=> 255
					 		) 
					 	)
					 ->field(
					 	'number', 
					 	'&nbsp;', 
					 	array(
					 		'name' 			=> 'ip_address[1]', 
					 		"placeholder"	=> '000', 
					 		"value"			=> !empty($ip_address[1]) ? $ip_address[1] : '',
					 		'min'			=> 1,
					 		"max"			=> 255					 		
					 		) 
					 	)					 
					 ->field(
					 	'number', 
					 	'&nbsp;', 
					 	array(
					 		'name' 			=> 'ip_address[2]', 
					 		"placeholder"	=> '000', 
					 		"value"			=> !empty($ip_address[2]) ? $ip_address[2] : '',
					 		'min'			=> 1,
					 		"max"			=> 255
					 		) 
					 	)
					 ->field(
					 	'number', 
					 	'&nbsp;', 
					 	array(
					 		'name' 			=> 'ip_address[3]', 
					 		"placeholder"	=> '000', 
					 		"value"			=> !empty($ip_address[3]) ? $ip_address[3] : '',
					 		'min'			=> 1,
					 		"max"			=> 255
					 		) 
					 	)
					 ->new_row()
					 ->field(
					 	'text', 
					 	$this->bahasa['reason'], 
					 	array(
					 		'name' 			=> 'reason', 
					 		"placeholder"	=> $this->bahasa['reason'], 
					 		"value"			=> $reason
					 		) 
					 	)					 						 
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('ip_filter', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('ip_filter', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('ip_filter', $where);
	}	
}