<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provinsi_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column($this->bahasa['name'],$this->bahasa['country'], 'action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0,1)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.province_name, b.country_name, a.province_id')
					 ->join('country b','b.country_id = a.country')
					 ->from('province a');
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 );
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function details($id = null){
		$data = $this->db
					 ->select('province_name, country, province_id')
					 ->from('province')
					 ->where('province_id', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['province_id']);
		}

		$province_name 	= !empty($data) ? $data['province_name'] : '';
		$country 		= !empty($data) ? $data['country'] : '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('province_name')
					 ->field(
					 	'text', 
					 	$this->bahasa['name'].' ('.$this->bahasa['province'].')', 
					 	array(
					 		'name' 			=> 'province_name', 
					 		"placeholder"	=> $this->bahasa['name'], 
					 		"value"			=> $province_name
					 		) 
					 	)
					 ->new_row()
					 ->field(
					 	'dropdown', 
					 	$this->bahasa['country'], 
					 	array(
					 		'name' 			=> 'country', 
					 		"placeholder"	=> $this->bahasa['country'], 
					 		"value"			=> $country, 
					 		"option" 		=> $this->country_option(),
					 		"option_value"	=> 'country_id',
					 		"option_label"	=> 'country_name'
					 		) 
					 	)
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function country_option(){
		$data = $this->db
			 		 ->select('country_name, country_id')
			 		 ->from('country')
			 		 ->get()
			 		 ->result();
		return $data;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('province', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('province', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('province', $where);
	}	
}