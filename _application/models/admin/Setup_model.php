<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Setup_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);

		$this->load->library($library);
		$this->load->helper('text');		
	}

	function data(){
		$sql = $this->db 
					->select('a.idconfig, a.cgid, a.configlabel, a.configicon, a.configvalue, b.groupname, b.groupicon, a.configtype')
					->from('configuration a')
					->join('config_groups b','b.cgid = a.cgid')
					->where(array('b.is_active' => 1))
					->get()
					->result();
		uasort($sql, function($a, $b){
			if($a->groupname == $b->groupname){
				return 0;
			}
			return $a->groupname < $b->groupname ? -1 : 1;
		});
		return $sql;
	}
	function tree($data){
		$result = array();
		array_walk($data, function($items) use(&$result){
			$result[$items->groupname.'_'.$items->groupicon][] = $items;
		});

		$io = '<div class="row config-list">';
		$color = array("primary","success","info","danger","green","red","default","warning","yellow");
		array_filter($result, function($items, $key) use(&$io, $color){
			$rand = $color[array_rand($color)];
			$io .= '<div class="col-xs-12 col-sm-6 col-md-4">';
				$io .= '<div class="panel panel-default">';
					$io .= '<div class="panel-heading">';
						$split = explode('_', $key);
						$io .= '<i class="fa fa-'.$split[1].'"></i>';
						$io .= ucwords($split[0]);
					$io .= '</div>';
					$io .= '<div class="panel-body">';
					uasort($items, function($a, $b){
						if($a->configlabel == $b->configlabel){
							return 0;
						}
						return $a->configlabel < $b->configlabel ? -1 : 1;
					});
						foreach ($items as $key => $value) {
							$io .= '<div class="row space">';
								$io .= '<div class="col-xs-6 col-sm-6">';
									$endio = '';
									if($this->vars['_UPDATE'] == true){
										$io .= '<a href="'.parse_link($this->vars['_ROUTED'], 'edit',$value->idconfig,$value->configlabel).'" class="btn-add">';
										$endio = '</a>';
									}
										$io .= '<i class="fa fa-'.$value->configicon.'"></i> ';
										$io .= ucwords($value->configlabel);
									$io .= $endio; 
								$io .= '</div>';
								$io .= '<div class="col-xs-6 col-sm-6">';
									$cv = $value->configtype == 'password' ? '***************' : $value->configvalue;
									$io .= ellipsize($cv,15, 1);
								$io .= '</div>';								
							$io .= '</div>';
						}
					$io .= '</div>';
				$io .= '</div>';
			$io .= '</div>';
		}, ARRAY_FILTER_USE_BOTH);
		$io .= '</div>';
		return $io;
	}

	function details($id = null){
		$sql = $this->db 
					->select('idconfig, cgid, configlabel, configicon, configvalue, configname, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable')
					->from('configuration a')
					->where('idconfig',$id)
					->get()
					->row();	
		return $sql;		
	}

	function form($data = null){
		$form = $this->fiform;

		if($data->configtype == 'radio' || $data->configtype == 'dropdown'){
			$input = array('name'=>'configvalue', 'option'=>unserialize($data->configoption), 'value'=>$data->configvalue, "inline" => true);
		}
		elseif($data->configtype == 'editor'){
			$data->configtype = 'textarea';
			$input = array('name'=> 'configvalue', "placeholder" => $data->configlabel.' value', "value"=>htmlspecialchars_decode($data->configvalue),'class'=>'editor', 'id'=>$data->configname); 
		}
		elseif($data->configtype == 'filesimage'){
			$input = array('name'=> 'configvalue', "placeholder" => $data->configlabel.' value', "value"=>BASE_PANEL.MINIFIER.$data->configvalue); 
		}		
		else{
			$input = array('name'=> 'configvalue', "placeholder" => $data->configlabel.' value', "value"=>$data->configvalue, 'id'=>$data->configname); 
		}

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->hidden("id",$data->idconfig)
					 ->required('configvalue')
					 ->btn('Update', array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->field(
					 		$data->configtype,
					 		$data->configlabel,
					 		$input
					   )
					 ->retrieve()
					 ;
		return $form;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);
			$result = $this->db->update('configuration', $postdata, $where);
		}
		else{
			$result = false;
		}
		return $result;
	}

	private function menu_option(){
		$this->sql_model->from('menus')->where(array('disposition'=>"left","display"=>1));
		$data = $this->sql_model->run()
								->trees('id','label','pid');
		array_unshift($data, array('id'=>"0",'label'=>'Menu utama'));
		return $data;

	}

	function svgicons(){
		$icon = explode(',',file_get_contents('assets/themes/font.json'));
		asort($icon);
		$icon = implode(',', $icon).',';
		$replacement = '<li class="list-group-item"><a data-icon="$1"><i class="fa fa-$1"></i>$1</a></li>';
		$item = '<ul class="list-group iconselection hidden">';
		$item .= preg_replace('/(.*?)(,)/', $replacement, $icon);
		$item .= '</ul>';
		$item .= '<script>';
		$item .= '$("#iconselection").focusin(function(){
					$(".iconselection").appendTo($(this).parent()).removeClass("hidden");
					$(".iconselection li").click(function(){
						var icon = $(this).find("a").attr("data-icon");
						$("#iconselection").val(icon);
						$(".iconselection").addClass("hidden");
					});
				  });
				  $("#iconselection,.iconselection").click(function(e){
				  	e.stopPropagation();
				  })
				  $("body").click(function(evt){
				  	$(".iconselection").addClass("hidden");
				  })
				  ';
		$item .= '</script>';
		return $item;
	}

	function list_menu(){
		$data = $this->db
					->select('id,pid,label,icon')
					->from('menus')
					->where(array("display"=>1,"login_required"=>"y"))
					->get()
					->result();
		return $data;
	}	
}