<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usergroup_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"title" 		=> $this->vars['_TITLE']." data",
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('group', 'action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.groupname, a.groupid')
					 ->from('usergroup a');
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 )
			 ->set_action(
				 	$this->bahasa['access_control'], 
				 	array(
				 		'href' => site_url($this->vars['_ROUTED'].'/access/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-info btn-update' 
				 	), 
				 	'<i class="fa fa-check-circle"></i>'
				);
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function details($id = null){
		$data = $this->db
					 ->select('groupid, groupname')
					 ->from('usergroup')
					 ->where('groupid', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['groupid']);
		}

		$groupname = !empty($data) ? $data['groupname'] : '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_atoken'))
					 ->required('groupname')
					 ->field('text', $this->bahasa['group'], array('name' => 'groupname', "placeholder"=>$this->bahasa['group'], "value"=>$groupname) )
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('usergroup', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('usergroup', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('usergroup', $where);
	}

	function group_items(){

	}

 	function access_data($id){
		$data = $this->db
					->from('usergroup_access')
					->where(array('groupid'=>$id))
				 	->get()
				 	->result_array();
		return $data;
	}

	function form_access($menus, $access_data){
		$html 			= '<div id="access_control">';
		$html 			.= form_open(site_url($this->vars['_ROUTED'].'/mod_access'), array('class'=>'form-submitting'));
		$html 			.= form_hidden(array("groupid"=>$this->id));
		$html 			.= '<div class="text-right access-btn">';
		$button = array(
			"type" 		=> "submit",
			"content"	=> '<i class="fa fa-save"></i> '.$this->bahasa['access_control'],
			"class"		=> "btn btn-primary btn-form",
		);
		if(count($access_data) > 0){
			$checked = 'checked';
		}
		else{
			$checked = '';
		}
		$html 			.= form_button($button);
		$html 			.= '</div>';
		$html 			.= '<div class="control-group">';
		$html 			.= '<input type="checkbox" class="checkall-control" '.$checked.' /> Check all';
		$html 			.= self::form_access_content($menus,$access_data);
		$html 			.= '</div>';
		$html 			.= '<div class="text-right access-btn">';
		$html 			.= form_button($button);
		$html 			.= '</div>';		
		$html 			.= form_close();
		$html 			.= '</div>';
		return $html;
	}

	private function form_access_content($data, $access_data, $parent=0, $level = 0){
		$content = '';
		$class 	 = '';
		$index 	 = '';
		$icon 	 = '';
		if($parent > 0){
			$class = 'list-child';
			$index = '-index';
			$icon  = '<i class="fa fa-level-up fa-rotate-90"></i>';
		}
		foreach ($data as $key => $value) {
			if($value->pid == $parent){
				$child = self::form_access_content($data, $access_data, $value->id, ($level + 1));
				$content .= '<ul class="menu-list list-group access-list '.$class.'">';
					$content .= '<li class="list-group-item">';
								$access_auth = array_filter($access_data, function($items) use($value){
									if($items['menusid'] == $value->id){
										return $items;
									}
								});
								$checked_id 	= '';
								$checked_view 	= '';
								$checked_insert	= '';
								$checked_update = '';
								$checked_delete	= '';
								if(isset($access_auth) && count($access_auth) > 0){
									$access_auth = array_values($access_auth);
									if($access_auth[0]['menusid'] == $value->id){
										$checked_id = ' checked="checked"';
									}
									if($access_auth[0]['view'] == true){
										$checked_view = ' checked="checked"';
									}
									if($access_auth[0]['insert'] == true){
										$checked_insert = ' checked="checked"';
									}
									if($access_auth[0]['update'] == true){
										$checked_update = ' checked="checked"';
									}
									if($access_auth[0]['delete'] == true){
										$checked_delete = ' checked="checked"';
									}
								} 	
								$content .= $icon.' ';
								$content .= '<label>';
								// $content .= '<input type="checkbox" name="access['.$value->id.'][menusid]" value="'.$value->id.'"'.$checked_id.' class="check_menus" />&nbsp;';
								$content .= ucwords($value->label);
								$content .= '</label>';
									$content .= '<div class="access-group" style="float:right;">';
										$content .= '<label>';
										$content .= '<input type="checkbox" name="access['.$value->id.'][menusid]" value="'.$value->id.'"'.$checked_id.' class="check_menus" />';
										$content .= 'View';
										$content .= '</label>';

										$content .= '<label>';
										$content .='<input type="checkbox" name="access['.$value->id.'][insert]" value="1"'.$checked_insert.' />';
										$content .= 'Insert';
										$content .= '</label>';

										$content .= '<label>';
										$content .='<input type="checkbox" name="access['.$value->id.'][update]" value="1"'.$checked_update.' />';
										$content .= 'Update';
										$content .= '</label>';

										$content .= '<label>';
										$content .='<input type="checkbox" name="access['.$value->id.'][delete]" value="1"'.$checked_delete.' />';
										$content .= 'Delete';
										$content .= '</label>';

									$content .= '</div>';
						$content .= !empty($child) ? $child : '';
					$content .='</li>';
				$content .= '</ul>';
			}
		}
		return $content;
	}	
}