<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userlist_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('username','group','name','gender','action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0,1,2,3)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.username, a.email, b.groupname, a.gender, a.userid')
					 ->from('user a')
					 ->join('usergroup b', 'b.groupid = a.group')
					 ->where('userid >', 1);
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 );
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function details($id = null){
		$data = $this->db
					 ->select('userid, username, email, group')
					 ->from('user')
					 ->where('userid', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['userid']);
		}

		$username 	= !empty($data) ? $data['username'] : '';
		$group 		= !empty($data) ? $data['group'] : 2;
		$email 		= !empty($data) ? $data['email'] : '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('username','email')
					 ->field('text', $this->bahasa['username'], array('name' => 'username', "placeholder"=>$this->bahasa['username'], "value"=>$username) )
					 ->field('dropdown', 'group', array('name'=>"group", "option" => self::group_option(), "value" => $group, "option_value" => 'groupid', "option_label" => 'groupname' ) )
					 ->new_row()
					 ->field('text', 'email', array('name' => 'email', 'placeholder' => 'email@mail.com', 'value' => $email) )
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	private function group_option(){
		$data = $this->db
					 ->select('groupname, groupid')
					 ->from('usergroup')
					 ->get()
					 ->result_array();
		return $data;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			// unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('user', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			// unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('user', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('user', $where);
	}
	
}