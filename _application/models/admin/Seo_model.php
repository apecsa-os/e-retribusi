<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seo_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" => "datatable", 
		     		"data-sources" => site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		     	)
		     ->attribute('title',$this->vars['_TITLE'].' data')
		     ->set_column(
		     		'meta', 
		     		'content',
		     		'action'
		     	);
		if($this->vars['_INSERT'] == true){
		$this->fitable
			 ->add_buttons(
					$this->bahasa['insert'], 
						array(
							'href' => site_url($this->vars['_ROUTED'].'/insert'), 
							'class' => 'btn btn-sm btn-primary btn-insert' 
						), 
					'<i class="fa fa-plus"></i>'
				);
		}			   
		$output = $this->fitable
						->filtered_column(0,1)
					   	->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
			 ->select('a.meta_name, a.meta_content, a.id')
			 ->from('seo a');

		if($this->vars['_UPDATE'] == true){
			$this->fitable
			 	 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 );
		}
		if($this->vars['_DELETE'] == true){
			$this->fitable
			 	 ->set_action(
					$this->bahasa['delete'], 
					array(
						'data-url' => $this->vars['_ROUTED'].'/delete', 
						'data-stamp' => '%1$d', 
						'data-title' => '%2$s', 
						'class' => 'btn btn-xs btn-danger btn-delete' 
					), 
					'<i class="fa fa-trash"></i>' 
			 	 );				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function details($id = null, $key = 'id'){
		$data = $this->db
					 ->select('meta_name, meta_content, id')
					 ->from('seo')
					 ->where($key, $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		$readonly = false;
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['id']);
			$readonly = true;
		}

		$meta_name 		= !empty($data) ? $data['meta_name'] : '';
		$meta_content 	= !empty($data) ? $data['meta_content'] : '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('meta_name','meta_content')
					 ->field(
					 		'text', 
					 		'meta name', 
					 		array('name' => 'meta_name', "placeholder" => 'meta name', "value"=>$meta_name, "readonly"=>$readonly) 
					 	)					 
					 ->new_row()					 
					 ->field(
					 		'text', 
					 		'meta content', 
					 		array('name' => 'meta_content', "placeholder"=>'meta_content', "value"=>$meta_content) 
					 	)
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);			
			$this->db->insert('seo', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);
			
			$result = $this->db->update('seo', $postdata, $where);
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('seo', $where);
	}
	
}