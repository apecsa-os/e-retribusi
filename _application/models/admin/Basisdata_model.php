<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Basisdata_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform',
			'dbforce'
		);
		$this->load->library($library);
	}

	function form(){
		$form = $this->fiform;

		$action = $this->bahasa['download'];

		$form = $form->create($this->vars['_ROUTED'].'/export', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('name','format')
					 ->field(
					 		'text', 
					 		'output name', 
					 		array('name' => 'name', "placeholder" => 'Output name') 
					 	)					 
					 ->new_row()					 
					 ->field(
					 		'radio', 
					 		'export as', 
					 		array('name' => 'format', 'option' => array('txt'=>"Text","sql"=>"SQL"), "value"=>'sql','inline' => true) 
					 	)
					 ->new_row()
					 ->field(
					 		'dropdown', 
					 		'select table (optional)', 
					 		array('name' => 'tbl_selected[]', "option"=>array_combine($this->db->list_tables(), $this->db->list_tables()), "multiple" => "multiple") 
					 	)
					 ->new_row()
					 ->field(
					 		'checkbox', 
					 		'add statement', 
					 		array(
					 			'name' 		=> 'statement[]', 
					 			"option"	=>	array(
					 								'drop_db'		=> 'drop database schema before create',
					 								'create_db' 	=> 'add create database schema',
					 								'drop_table'	=> 'add drop table',
					 								'create_table' 	=> 'add create table',
					 								'insert_data'	=> 'add insert data',
					 								'fk_check'		=> 'foreign key check',
					 								'include_notes'	=> 'include notes'
					 							),
					 			"value"		=> array('drop_db','create_db','drop_table','create_table','insert_data','fk_check','include_notes'),
					 			"inline"	=> FALSE,
					 			) 
					 	)
					 ->btn($action, array('type' => 'submit','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function export($data){
		$name 			= !empty($data['name']) ? $data['name'] : '@db@';
		$format 		= !empty($data['format']) ? $data['format'] : 'SQL';
		$table 			= !empty($data['tbl_selected']) ? $data['tbl_selected'] :array();
		$drop_db 		= !empty($data['statement']) && in_array('drop_db', $data['statement']) ? 1 : 0;
		$create_db 		= !empty($data['statement']) && in_array('create_db', $data['statement']) ? 1 : 0;
		$drop_table 	= !empty($data['statement']) && in_array('drop_table', $data['statement']) ? 1 : 0;
		$create_table 	= !empty($data['statement']) && in_array('create_table', $data['statement']) ? 1 : 0;
		$insert_data 	= !empty($data['statement']) && in_array('insert_data', $data['statement']) ? 1 : 0;
		$fk_check 		= !empty($data['statement']) && in_array('fk_check', $data['statement']) ? 1 : 0;
		$notes 			= !empty($data['statement']) && in_array('include_notes', $data['statement']) ? 1 : 0;

		$force 	= $this->dbforce
						->filename($name)
						->format($format)
						->table($table)
						->notes($notes)
						->fk_check($fk_check)
						->add_drop_db($drop_db)
						->create_db($create_db)
						->create_table($create_table)
						->add_drop_table($drop_table)
						->add_insert($insert_data)
						->force();
		return $force;
	}

	function form_upload(){
		$form = $this->fiform;

		$action = $this->bahasa['upload'];

		$form = $form->create($this->vars['_ROUTED'].'/restore', array("class" => 'form-horizontal form-submitting','enctype'=>'multipart/form-data'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('name')
					 ->field(
					 		'file', 
					 		'Select file', 
					 		array('name' => 'files') 
					 	)					 
					 ->btn($action, array('type' => 'submit','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}	
}