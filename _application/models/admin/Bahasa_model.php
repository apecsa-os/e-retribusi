<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bahasa_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" => "datatable", 
		     		"data-sources" => site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		     	)
		     ->attribute('title',$this->vars['_TITLE'].' data')
		     ->set_column(
		     		$this->bahasa['key_lang'], 
		     		$this->bahasa['translation'].' Indonesia', 
		     		$this->bahasa['translation'].' English', 
		     		'action'
		     	);
		if($this->vars['_INSERT'] == true){
		$this->fitable
			 ->add_buttons(
					$this->bahasa['insert'], 
						array(
							'href' => site_url($this->vars['_ROUTED'].'/insert'), 
							'class' => 'btn btn-sm btn-primary btn-insert' 
						), 
					'<i class="fa fa-plus"></i>'
				);
		}			   
		$output = $this->fitable
						->filtered_column(0,1,2)
					   	->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
			 ->select('a.key_bahasa, a.indonesia, a.english, a.idbahasa')
			 ->from('bahasa a');

		if($this->vars['_UPDATE'] == true){
			$this->fitable
			 	 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 );
		}
		if($this->vars['_DELETE'] == true){
			$this->fitable
			 	 ->set_action(
					$this->bahasa['delete'], 
					array(
						'data-url' => $this->vars['_ROUTED'].'/delete', 
						'data-stamp' => '%1$d', 
						'data-title' => '%2$s', 
						'class' => 'btn btn-xs btn-danger btn-delete' 
					), 
					'<i class="fa fa-trash"></i>' 
			 	 );				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function details($id = null, $key = 'idbahasa'){
		$data = $this->db
					 ->select('key_bahasa, indonesia, english, idbahasa')
					 ->from('bahasa')
					 ->where($key, $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		$readonly = false;
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['idbahasa']);
			$readonly = true;
		}

		$key 		= !empty($data) ? $data['key_bahasa'] : '';
		$indonesia 	= !empty($data) ? $data['indonesia'] : '';
		$english 	= !empty($data) ? $data['english'] : '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('key_bahasa','indonesia','english')
					 ->field(
					 		'text', 
					 		$this->bahasa['key_lang'], 
					 		array('name' => 'key_bahasa', "placeholder" => $this->bahasa['key_lang'], "value"=>$key, "readonly"=>$readonly) 
					 	)					 
					 ->new_row()					 
					 ->field(
					 		'text', 
					 		$this->bahasa['translation'].' indonesia', 
					 		array('name' => 'indonesia', "placeholder"=>$this->bahasa['translation'], "value"=>$indonesia) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		$this->bahasa['translation'].' english', 
					 		array('name' => 'english', "placeholder"=>$this->bahasa['translation'], "value"=>$english) 
					 	)
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);			
			$this->db->insert('bahasa', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);
			
			$result = $this->db->update('bahasa', $postdata, $where);
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('bahasa', $where);
	}
	
}