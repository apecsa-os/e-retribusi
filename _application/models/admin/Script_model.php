<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Script_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
		$this->load->helper('text');		
	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column($this->bahasa['name'],'spath','cdn','sgroup','active', 'action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0,1,2,3,4,5)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.sname, a.spath, a.cdn, a.sgroup, a.is_active, a.sid')
					 ->from('javascript a')
					 ->data_format('spath', 'ellips')
					 ->data_format('cdn','ellips')
					 ->data_format('is_active', 'converter');
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 );
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function ellips($string){
		return ellipsize($string,15, 1);
	}

	function converter($number){
		return $number == 1 ? '<span class="label label-primary">1</span> Enabled' : '<span class="label label-default">0</span> Disabled';
	}

	function details($id = null, $key = 'sid'){
		$data = $this->db
					 ->select('sname, spath, cdn, sgroup, is_active, sid, integrity, crossorigin, global, order_idx')
					 ->from('javascript')
					 ->where($key, $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['sid']);
		}

		$sname 			= !empty($data) ? $data['sname'] 		: '';
		$spath 			= !empty($data) ? $data['spath'] 		: '';
		$cdn 			= !empty($data) ? $data['cdn'] 			: '';
		$integrity 		= !empty($data) ? $data['integrity'] 	: '';
		$crossorigin 	= !empty($data) ? $data['crossorigin'] 	: '';
		$sgroup 		= !empty($data) ? $data['sgroup'] 		: '';
		$is_active 		= !empty($data) ? $data['is_active'] 	: 1 ;
		$global 		= !empty($data) ? $data['global'] 		: '' ;
		$order_idx 		= !empty($data) ? $data['order_idx'] 	: 1 ;

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('sname','spath','sgroup')
					 ->field(
					 	'text', 
					 	$this->bahasa['name'], 
					 	array(
					 		'name' 			=> 'sname', 
					 		"placeholder"	=> $this->bahasa['name'], 
					 		"value"			=> $sname
					 		) 
					 	)
					 ->field(
					 	'number', 
					 	'sorting index', 
					 	array(
					 		'name' 			=> 'order_idx', 
					 		"placeholder"	=> 'order index number', 
					 		"value"			=> $order_idx,
					 		"min"			=> 1					 		
					 		) 
					 	)					 
					 ->new_row()
					 ->field(
					 	'text', 
					 	'filepath', 
					 	array(
					 		'name' 			=> 'spath', 
					 		"placeholder"	=> 'relative file path', 
					 		"value"			=> $sname
					 		) 
					 	)
					 ->new_row()					 
					 ->field(
					 	'text', 
					 	'cdn source', 
					 	array(
					 		'name' 			=> 'cdn', 
					 		"placeholder"	=> 'full url cdn resources', 
					 		"value"			=> $cdn
					 		) 
					 	)
					 ->new_row()
					 ->field(
					 	'text', 
					 	'cdn integrity key', 
					 	array(
					 		'name' 			=> 'integrity', 
					 		"placeholder"	=> 'key access cdn file', 
					 		"value"			=> $integrity
					 		) 
					 	)
					 ->field(
					 	'text', 
					 	'cdn crossorigin', 
					 	array(
					 		'name' 			=> 'crossorigin', 
					 		"placeholder"	=> 'cdn user crossorigin type', 
					 		"value"			=> $crossorigin
					 		) 
					 	)
					 ->new_row()					 						 					 						 
					 ->field(
					 	'text', 
					 	'used for', 
					 	array(
					 		'name' 			=> 'sgroup',
					 		"placeholder"	=> 'User experience template',
					 		"value"			=> $sgroup, 
					 		) 
					 	)
					 ->field(
					 	'radio', 
					 	'active', 
					 	array(
					 		'name' 			=> 'is_active',
					 		"option"		=> array('0'=>'disabled','1'=>'enable'), 
					 		"value"			=> $is_active,
					 		"inline"		=> true 
					 		) 
					 	)
					 ->new_row()					 						 					 						 
					 ->field(
					 	'radio', 
					 	'set to all page', 
					 	array(
					 		'name' 			=> 'global',
					 		"option"		=> array('0'=>'disabled','1'=>'enable'), 
					 		"value"			=> $global, 
					 		) 
					 	)					 						 
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('javascript', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('javascript', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('javascript', $where);
	}	
}