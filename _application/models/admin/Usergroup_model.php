<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usergroup_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('group', 'action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.groupname, a.groupid')
					 ->from('usergroup a');
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 )
			 ->set_action(
				 	$this->bahasa['access_control'], 
				 	array(
				 		'href' => site_url($this->vars['_ROUTED'].'/access/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-info' 
				 	), 
				 	'<i class="fa fa-check-circle"></i>'
				);
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function details($id = null){
		$data = $this->db
					 ->select('groupid, groupname')
					 ->from('usergroup')
					 ->where('groupid', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['groupid']);
		}

		$groupname = !empty($data) ? $data['groupname'] : '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_atoken'))
					 ->required('groupname')
					 ->field('text', $this->bahasa['group'], array('name' => 'groupname', "placeholder"=>$this->bahasa['group'], "value"=>$groupname) )
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('usergroup', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('usergroup', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('usergroup', $where);
	}

	function group_items(){

	}

 	function access_data($id){
		$data = $this->db
					->from('usergroup_access')
					->where(array('groupid'=>$id))
				 	->get()
				 	->result_array();
		return $data;
	}

	function form_access($menus, $access_data){
		$html 	= '<div id="access_control">';
			$html 	.= form_open(site_url($this->vars['_ROUTED'].'/mod_access'), array('class'=>'form-submitting'));
			$html 	.= form_hidden(array("groupid"=>$this->id));
			$button = array(
			"type" 		=> "submit",
			"content"	=> '<i class="fa fa-save"></i> '.$this->bahasa['access_control'],
			"class"		=> "btn btn-primary btn-sm btn-form",
			);
				$html .= '<div class="table-responsive">';
					$html .= '<table class="table table-striped table-hovered table-access">';
						$html .= '<thead>';
							$html .= '<tr>';
								$checked = count($access_data) > 0 ? 'checked' : '';
								$html .= '<th colspan="2"><input type="checkbox" class="checkall-control" '.$checked.' title="check all" />Check all</th>';
								$html .= '<td colspan="5" class="text-right">'.form_button($button).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<th rowspan="2" class="text-center" style="vertical-align:middle;" width="10px">&nbsp;</th>';
								$html .= '<th rowspan="2" class="text-center" style="vertical-align:middle;">Menu</th>';
								$html .= '<th colspan="5" class="text-center">Priviledge</th>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<th width="10%" class="text-center">View</th>';
								$html .= '<th width="10%" class="text-center">Insert</th>';
								$html .= '<th width="10%" class="text-center">Update</th>';
								$html .= '<th width="10%" class="text-center">Delete</th>';
								$html .= '<th width="10%" class="text-center">Print</th>';
							$html .= '</tr>';
						$html .= '</thead>';
						$html .= '<tbody>';
							$html .= self::form_access_content($menus,$access_data);
						$html .= '</tbody>';
					$html .= '</table>';
				$html .= '</div>';		
			$html 	.= form_close();
		$html 	.= '</div>';
		return $html;
	}

	private function form_access_content($data, $access_data, $parent=0, $level = 0){
		$content = '';
		foreach ($data as $key => $value) {
			if($value->pid == $parent){
				$child = self::form_access_content($data, $access_data, $value->id, ($level + 1));
				$access_auth = array_filter($access_data, function($items) use($value){
					if($items['menusid'] == $value->id){
						return $items;
					}
				});
				$priviledge = !empty($value->action_case) ? array_map('trim',explode(',', $value->action_case)) : array();
				$access_auth = !empty($access_auth) ? array_values($access_auth) : '';
				
				$trclass = ($level == 0) ? 'row-parent' : 'row-child';
				$content .= '<tr class="'.$trclass.'">';
					$padding = ($level * 15) + 15 ;
					$content .= '<td>';
						$content .= ($level == 0 && $child != '') ? '<a><i class="fa row-expanded"></i></a>' : '';
					$content .= '</td>';
					$content .= '<td style="padding-left:'.$padding.'px;">';
						$content .= ucwords($value->label);
					$content .= '</td>';

					$content .= '<td class="text-center">';
					$content .= '<input type="hidden" name="access['.$value->id.'][menusid]" value="'.$value->id.'" />';
						if(in_array('view', $priviledge) == true){
							$checked = !empty($access_auth) && !empty($access_auth[0]['viewing']) && $access_auth[0]['viewing'] == TRUE ? ' checked="checked"' : ''; 
							$content .= '<input type="checkbox" name="access['.$value->id.'][viewing]" value="1"'.$checked.' class="check_menus" />';
						}
						else{
							$content .= '&nbsp;';
						}
					$content .= '</td>';

					$content .= '<td class="text-center">';
						if(in_array('insert', $priviledge) == true){
							$checked = !empty($access_auth) && !empty($access_auth[0]['inserting']) && $access_auth[0]['inserting'] == TRUE ? ' checked="checked"' : ''; 
							$content .= '<input type="checkbox" name="access['.$value->id.'][inserting]" value="1"'.$checked.' class="check_menus" />';
						}
						else{
							$content .= '&nbsp;';
						}
					$content .= '</td>';

					$content .= '<td class="text-center">';
						if(in_array('update', $priviledge) == true){
							$checked = !empty($access_auth) && !empty($access_auth[0]['updating']) && $access_auth[0]['updating'] == TRUE ? ' checked="checked"' : ''; 
							$content .= '<input type="checkbox" name="access['.$value->id.'][updating]" value="1"'.$checked.' class="check_menus" />';
						}
						else{
							$content .= '&nbsp;';
						}
					$content .= '</td>';

					$content .= '<td class="text-center">';
						if(in_array('delete', $priviledge) == true){
							$checked = !empty($access_auth) && !empty($access_auth[0]['deleting']) && $access_auth[0]['deleting'] == TRUE ? ' checked="checked"' : ''; 
							$content .= '<input type="checkbox" name="access['.$value->id.'][deleting]" value="1"'.$checked.' class="check_menus" />';
						}
						else{
							$content .= '&nbsp;';
						}
					$content .= '</td>';

					$content .= '<td class="text-center">';
						if(in_array('printed', $priviledge) == true){
							$checked = !empty($access_auth) && !empty($access_auth[0]['printing']) && $access_auth[0]['printing'] == TRUE ? ' checked="checked"' : ''; 
							$content .= '<input type="checkbox" name="access['.$value->id.'][printing]" value="1"'.$checked.' class="check_menus" />';
						}
						else{
							$content .= '&nbsp;';
						}
					$content .= '</td>';
				$content .= '</tr>';
				$content .= !empty($child) ? $child : '';
			}
		}
		return $content;
	}	
}