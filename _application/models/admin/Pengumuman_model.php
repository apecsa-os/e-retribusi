<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengumuman_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column($this->bahasa['title'],$this->bahasa['creator'], 'priority', 'from', 'until', 'action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0,1,2,3,4)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.judul, b.fullname, a.priority, a.created_at, a.expire, a.id')
					 ->from('pengumuman a')
					 ->join('user b','b.userid = a.redaktur')
					 ->data_format('created_at','date_converter')
					 ->data_format('expire','date_converter')
					 ->data_format('priority','labeled');
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 );
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function date_converter($string){
		return local_date($string);
	}

	function labeled($number){
		switch ($number) {
			case 1:
				$msg = '<span class="label label-primary">'.$number.'</span> Medium';			
				break;
			case 2:
				$msg = '<span class="label label-danger">'.$number.'</span> High';			
				break;			
			default:
				$msg = '<span class="label label-default">'.$number.'</span> Low';
				break;
		}
		return $msg;		
	}

	function details($id = null){
		$data = $this->db
					 ->select('judul, redaktur, priority, expire, expire_time, isi, id')
					 ->from('pengumuman')
					 ->where('id', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['id']);
		}

		$judul 		= !empty($data) ? $data['judul'] : '';
		$priority 	= !empty($data) ? $data['priority'] : '';
		$isi 		= !empty($data) ? $data['isi'] : '';
		$expire 	= !empty($data) ? $data['expire'] : '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('judul')
					 ->field('text', $this->bahasa['title'], array('name' => 'judul', "placeholder"=>$this->bahasa['name'], "value"=>$judul) )
					 ->new_row()
					 ->field('radio', $this->bahasa['priority'], array('name' => 'priority', "option"=>array('low','medium','high'), "value"=>$priority, "inline" => true) )
					 ->new_row()
					 ->field('textarea', $this->bahasa['content'], array('name' => 'isi', "placeholder"=>$this->bahasa['content'], "value"=>$isi, 'class' => 'editor') )
					 ->new_row()
					 ->field('text', $this->bahasa['expire'], array('name' => 'expire', "placeholder"=>$this->bahasa['expire'], "value"=>$expire, 'class'=>'datepicker') )
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('pengumuman', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('pengumuman', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('pengumuman', $where);
	}
	
}