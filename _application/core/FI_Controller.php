<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class FI_Controller extends CI_Controller
{
    var $vars;
    var $template;
    var $language = array();
    var $css;
    var $js;

    function __construct()
    {
       parent::__construct();
       date_default_timezone_set('Asia/Jakarta');
       $this->load->model(array('resources_model','template_model','query_model'));
       $this->vars              = self::default_reference();
       $this->vars['minifier']  = $GLOBALS['lookup'].MINIFIER;
       // $GLOBALS['minifier']      = $GLOBALS['lookup'].MINIFIER;
       
       $this->vars['_SESS_PREFIX']      = empty($GLOBALS['lookup']) ? 'p' : 'a';
    }

    /* ------------------------------------------------------------------------------------------------------
    ** LOAD DEFAULT CONFIGURATION
    ** ------------------------------------------------------------------------------------------------------
    */
    function default_reference(){
        $q   = $this->resources_model->web_config();
        return $q;
    }

     function load_config($flag){
        $css                    = !empty($this->css) ? $this->css : null;
        $js                     = !empty($this->js) ? $this->js : null;
        $this->vars['_CSS']     = $this->resources_model->css_loader($flag, $css);
        $this->vars['_JS']      = $this->resources_model->js_loader($flag, $js);
    }

    /* ------------------------------------------------------------------------------------------------------
    ** SESSION FLASH
    ** ------------------------------------------------------------------------------------------------------
    */
    function flash( $flashdata = null){
        $html   = '';
        $flashdata  = $this->session->flashdata('error') != null ? $this->session->flashdata('error') : $flashdata;
        if(!empty($flashdata)){
            $class  = isset($flashdata['class']) ? $flashdata['class'] : 'danger';
            $icon   = isset($flashdata['icon']) ? $flashdata['icon'] : 'times-circle';
            $msg    = isset($flashdata['msg']) ? $flashdata['msg'] : 'Required Field';
            $title  = isset($flashdata['title']) ? ucwords($flashdata['title']) : 'error';

            $html    = '<div class="alert alert-'.$class.'">';
            $html    .= '<div class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></div>';
            $html    .= '<div class="title">'.$title.'</div>';
            $html    .= '<ol class="validation-notif">'.$msg.'</ol>';
            $html    .= '</div>';
        }
        return $html;       
    }

    function set_flash($cond, $param = array(), $use_session = true){
        if ($cond == 'validation error') {
            $flash['title'] = $cond;            
            $flash['array'] = $this->form_validation->error_array();
            $flash['cache'] = $param;
            $flash['msg']   = validation_errors();
        }
        else{
            $flash['title'] = $cond;
            $flash['class'] = $cond == 'error' ? 'danger'       : 'success';
            $flash['icon']  = $cond == 'error' ? 'times-circle'  : 'check-square';
            $flash['msg']   = is_array($param) && array_key_exists('msg', $param) ? $param['msg'] : (!empty($param) && is_string($param) ? $param  : '');
            if(is_array($param) && array_key_exists('param', $param)){
                $flash['param'] = $param['param'];
            }
        }

        if($use_session == true){
            $this->session->set_flashdata(array('error' => $flash));
            $flash = null;
        }

        return self::flash($flash);
    }

    /* ------------------------------------------------------------------------------------------------------
    ** VALIDATION DATA
    ** ------------------------------------------------------------------------------------------------------
    */
    function validate($data){
        foreach ($data as $key => $value) {
        $this->form_validation->set_rules($value['name'], $value['message'], $value['valid_type']);
        }
        $this->form_validation->set_error_delimiters('<li>','</li>');

        return $this->form_validation->run();
    } 

    function post_check(){
        if($_SERVER['REQUEST_METHOD'] != "POST"){
            redirect(404);
        }       
    }

    function loadMyController(){
        $this->load->helper('directory');
        $custom = directory_map(CUSTOM_PATH.'core');
        foreach ($custom as $key => $value) {
            if(preg_match('/^.*\.(php)$/i', $value) && $value != 'FI_Controller.php'){
                include_once(CUSTOM_PATH.'core/'.$value);
            }
        }

        $custom = directory_map(APPPATH.'core');
        foreach ($custom as $key => $value) {
            if(preg_match('/^.*\.(php)$/i', $value)){
                include_once(APPPATH.'core/'.$value);
            }
        }
    }                           
}
foreach (glob(CUSTOM_PATH."core/*.php") as $filename) {
    if(preg_match('/(FI_Controller)/', $filename) == false){
        include_once($filename);
    }
}
foreach (glob(APPPATH."core/*.php") as $filename) {
    include_once($filename);
}
