<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Admin_Controller extends FI_Controller
{
    function __construct(){
        parent:: __construct();
        if($this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token') == null && $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'loggedin') == null ){
            redirect(BASE_PANEL.'login');
        }
        else{
            $filter = $this->resources_model->ip_filter();
            if(in_array($_SERVER['REMOTE_ADDR'], $filter) == true){
                redirect('accessdenied');    
            }


            $this->template                 = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'template') != null ? $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'template') : ''.TEMPLATE.'';
            
            self::restrict();            
        }
    }

    function restrict(){
        $class                  = get_called_class();
        $restrict               = $this->resources_model->restrict($class, SESS_ADMIN);

        $this->vars['_TITLE']    = $restrict['title'];
        $this->vars['_ROUTED']   = BASE_PANEL.$restrict['slug'];
        $this->vars['_INSERT']   = $restrict['inserting'];
        $this->vars['_UPDATE']   = $restrict['updating'];
        $this->vars['_DELETE']   = $restrict['deleting'];
        $this->vars['_VIEWING']   = $restrict['viewing'];
        $this->vars['_PRINTING']   = $restrict['printing'];
    }


    function pageview($page = ''){
        $this->load->model('template_model');

        if($_SERVER['REMOTE_ADDR'] == '::1'){
            $profiler = FALSE;
        }
        else{
            $profiler = FALSE;
        }
        self::load_config('admin');
        $this->vars['_PROFILER']      = $profiler;        
        $this->vars['_MENU']          = $this->template_model->menu();
        $this->vars['_BREADCRUMB']    = $this->template_model->breadcrumb();
        $this->vars['_FLASH']         = self::flash();
        $this->vars['_CONTAINER']     = $page != '' ? $this->load->view($page, $this->vars, true) : '';
        $this->vars['_INDEX_BODY']    = $this->load->view('template/'.TEMPLATE.'/admin',$this->vars,true);
        self::master_page();
    }

    function restrict_msg(){
        return $this->load->view('errors/restrict_access');
    }

    /* ------------------------------------------------------------------------------------------------------
    ** TEMPLATE INDEX
    ** ------------------------------------------------------------------------------------------------------
    */
    function master_page($hooking = true){

        if(!isset($this->vars['_TITLE'])){
            $this->vars['_TITLE'] = ucwords($this->vars['_INFO_TITLE']);
        }

        $page = $this->load->view('template/'.TEMPLATE.'/index',$this->vars,$hooking);
        
        if($hooking == true){
            self::outputs($page);    
        }
    }

    function outputs($page, $mime = 'text/html', $buffer = true){
        $this->myhook
             ->mime($mime)
             ->charset('utf-8')
             ->status(200)
             ->expires('-10 year')
             ->cache(0)
             ->header("Accept-Ranges: bytes")
             ->header("HTTP/1.0 200 OK")
             ->header("HTTP/1.1 200 OK")
             ->header("Cache-Control: no-cache, no-store, must-revalidate", false)
             ->header("Cache-Control: post-check=0, pre-check=0", false)
             ->header("Pragma: no-cache")
             ->header("X-Content-Type-Options: nosniff")
             ->header("X-XSS-Protection: 1; mode=block")
             ->header("X-Frame-Options: SAMEORIGIN")
             ->output($page)
             ->buffering()
             ->display();        
    }          
}