<?php
$cache = array();
if($this->session->flashdata('error') != null){
	$error = $this->session->flashdata('error');
	$cache = array_key_exists('form_cache', $error) ? $error['form_cache'] : '';
	$erray = array_key_exists('error_array', $error) ? $error['error_array'] : '';
}
?>
<div class="login-wrapper">
	<div class="login-box">
		<?php echo $_FLASH;?>
		<h4 class="title">
			<i class="fa fa-unlock-alt"></i> Please enter your information detail					
		</h4>
		<form method="post" class="form-horizontal">
			<input type="hidden" name="reg_code" value="<?php echo $this->session->userdata('reg_code');?>">		
			<div class="form-group">
					<?php
						$error_class = !empty($erray) && array_key_exists("name", $erray) ? 'has-error' : 'has-warning';
					?>
				<div class="input-group <?php echo $error_class;?>">
					<span class="input-group-addon strong"><i class="fa fa-user"></i></span>
					<input type="text" name="name" class="form-control input-sm" placeholder="<?php echo $bahasa['fullname'];?>" value="<?php echo isset($cache['name']) ? htmlentities($cache['name']): '' ; ?>" />			
				</div> 
			</div>
			<div class="form-group">
					<?php
						$error_class = !empty($erray) && array_key_exists("email", $erray) ? 'has-error' : 'has-warning';
					?>				
				<div class="input-group <?php echo $error_class;?>">
					<span class="input-group-addon strong"><i class="fa fa-at"></i></span>
					<input type="text" name="email" class="form-control input-sm" placeholder="Email" value="<?php echo isset($cache['email']) ? htmlentities($cache['email']) : '' ; ?>" />							
				</div> 
			</div>
			<div class="form-group">
					<?php
						$error_class = !empty($erray) && array_key_exists("username", $erray) ? 'has-error' : 'has-warning';
					?>				
				<div class="input-group <?php echo $error_class;?>">
					<span class="input-group-addon strong"><i class="fa fa-user"></i></span>
					<input type="text" name="username" class="form-control input-sm" placeholder="<?php echo $bahasa['username'];?>" value="<?php echo isset($cache['username']) ? htmlentities($cache['username']): '' ; ?>" />							
				</div> 
			</div>					 
			<div class="form-group">
					<?php
						$error_class = !empty($erray) && array_key_exists("password", $erray) ? 'has-error' : 'has-warning';
					?>					
				<div class="input-group <?php echo $error_class;?>">
					<span class="input-group-addon strong"><i class="fa fa-key"></i></span>
					<input type="password" name="password" class="form-control input-sm" placeholder="<?php echo $bahasa['password'];?>"/>							
				</div>					 
			</div> 
			<div class="form-group">
					<?php
						$error_class = !empty($erray) && array_key_exists("repass", $erray) ? 'has-error' : 'has-warning';
					?>					
				<div class="input-group <?php echo $error_class;?>">
					<span class="input-group-addon strong"><i class="fa fa-key"></i></span>
					<input type="password" name="repass" class="form-control input-sm" placeholder="<?php echo $bahasa['retype']. ' ' .$bahasa['password'];?>" />							
				</div> 
			</div> 				
			<div class="form-group text-right"> 
				<button type="submit" class="btn btn-primary btn-sm"><?php echo $bahasa['register'];?>
				</button> 
			</div>
		</form>
	</div>
	<div class="text-center">
		<?php echo $_INFO_NAME;?> Copyright &copy; 2016
	</div>
</div>	
<?php
$this->output->enable_profiler(isset($PROFILER) ? $PROFILER : FALSE);
?>