<div class="login-wrapper">
	<div class="login-box">
		<div class="text-center">
				<i class="fa fa-leaf fa-3x" style="color:#69AA46;"></i>
				<span style="color: #DD5A43;font-size: 35px; margin-left: 34px;"><?php echo $_INFO_NAME;?></span>
				<span style="color:#ccc;" id="id-text2">Admin</span>
		</div>
		<?php echo $_FLASH;?>
		<h4 class="title" style="color: #999; font-size: 12px;">
			<i class="fa fa-unlock-alt"></i> Please enter your login detail					
		</h4>
		<form method="post" class="form-horizontal">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon strong"><i class="fa fa-user"></i></span>
					<input type="text" name="username" class="form-control input-sm" placeholder="Username or email" />							
				</div> 
			</div>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon strong"><i class="fa fa-key"></i></span>
					<input type="password" name="password" class="form-control input-sm" placeholder="Password"/>							
				</div>					 
			</div>
			<?php 
				if(!empty($captcha) && $captcha == 'TRUE'){
			?>
			<div class="form-group">
				<div class="row">
					<div class="col-xs-12 col-sm-5">
						<?php
							echo mycaptcha();
						?>
					</div>
					<div class="col-xs-12 col-sm-7">
						<div class="input-group">
							<span class="input-group-addon strong"><i class="fa fa-key"></i></span>
							<input type="text" name="captcha" class="form-control input-sm" placeholder="Please enter security code" />
						</div>
					</div>					
				</div>
			</div>
			<?php
				}
			?>
			<div class="form-group text-right"> 
				<button type="submit" class="btn btn-primary btn-sm">Sign in
				</button> 
			</div>
		</form>
	</div>
	<div class="text-center">
		<?php echo $_INFO_NAME;?> Copyright &copy; 2016 - <?php echo date('Y');?>
	</div>
</div>	
<?php
	$this->output->enable_profiler(isset($PROFILER) ? $PROFILER : FALSE);
?>