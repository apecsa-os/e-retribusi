<style type="text/css">
    .error-template
    {
        padding: 40px 15px;
	    text-align: center;
	    border: 1px solid #ccc;
	    background: rgba(255, 255, 255, 0.48);
    }
    .error-actions
    {
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .error-actions .btn
    {
        margin-right: 10px;
    }
    .message-box h1
    {
        color: #252932;
        font-size: 98px;
        font-weight: 700;
        line-height: 98px;
        text-shadow: rgba(61, 61, 61, 0.3) 1px 1px, rgba(61, 61, 61, 0.2) 2px 2px, rgba(61, 61, 61, 0.3) 3px 3px;
    }	
</style>
<div class="error-template">
    <h1>
        :) Oops!</h1>
    <h2>
        All Transaction has been turn off for several reason</h2>
    <h1>
        We’ll be back soon!</h1>
    <div>
        <p>
            Sorry for the inconvenience but we’re performing some maintenance at the moment.
            we’ll be back online shortly!</p>
        <p>
            - <?php echo $_INFO_NAME;?> -</p>
    </div>
</div>