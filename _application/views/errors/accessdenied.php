<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="id" dir="ltr">

<head>
	<title>Access denied</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('../_vendor/bootstrap/3.3.4/css/bootstrap.min.css');?>"> 
	<style type="text/css">
	.container{
		margin: 15em auto;
	}
	.panel{
		box-shadow: 0 0 5px rgba(0,0,0,.5);
		-webkit-box-shadow:0 0 5px rgba(0,0,0,.5);
	}

	@media screen and (max-width: 767px){
		.panel{
			text-align: center;
		}
		img{
			margin: 0 auto;
		}
	}
	</style>
</head>

<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
						<img src="<?php echo base_url('/assets/image/image/warn.png');?>" class="img-responsive" />
					</div>
					<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
						<h1>Oops error: IP Blocked</h1>
						<p>
							The owner of this website has banned your ip address <?php echo $_SERVER['REMOTE_ADDR'];?> 
						</p>
						<p> <a href="<?php echo site_url();?>">Back to homepage</a></p>						
					</div>
				</div>			
			</div>
		</div>
	</div>
</body>

</html>
