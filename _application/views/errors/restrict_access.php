<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-warning">
					<div class="media">
						<div class="media-left">
							<img src="<?php echo base_url('/assets/image/image/warn.png');?>" width="50" height="50">
						</div>
						<div class="media-body">
							<h3 class="media-heading">Oops error: Access Denied</h3>
							<p>
								You don't have permissions to access this resources.<br/>
								Ask website administrator to make changes to this resources.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>			
	</div>
</div>