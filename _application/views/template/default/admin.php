		<nav class="navbar navbar-default navbar-static-top"> 
			<div class="navbar-header"> 
				<button type="button" class="navbar-toggle collapsed" 
				data-toggle="collapse" data-target=".navbar-collapse" 
				aria-expanded="false"> 
				<span><i class="fa fa-th-large"></i> Menu</span> 
				</button>
				<a class="navbar-brand" href="<?php echo site_url(BASE_PANEL);?>"><?php echo $_INFO_NAME;?></a>  
			</div> 
				<?php echo $_MENU['top'];?>	
			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<?php echo $_MENU['left'];?>	
				</div>
			</div>
		</nav>
	<div id="page-wrapper">
		<div id="content-header" class="mini">
			<h1>
			<?php echo isset($_TITLE) ? ucwords($_TITLE) : '';?>
				<a href="<?php echo site_url(BASE_PANEL.'assets/clear/cache?p=pub');?>" class="btn btn-primary sb-delete btn-xs float-right;" title="clear public cache"><i class="fa fa-globe"></i> <span class="hidden-xs">Public Cache</span></a>			
				<a href="<?php echo site_url(BASE_PANEL.'assets/clear/cache');?>" class="btn btn-primary sb-delete btn-xs float-right;" title="clear panel cache"><i class="fa fa-desktop"></i> <span class="hidden-xs">Panel Cache</span></a>
			</h1>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<?php echo $_BREADCRUMB;?>		
			</div>
		</div>
		<?php echo $_FLASH;?>
		<div class="row">
			<div class="col-xs-12">
				<?php echo $_CONTAINER;?>				
			</div>
		</div>
		<div id="footer" class="row">
			<div class="col-xs-12 text-center">
				2015 &copy; <?php echo $_INFO_NAME;?>.
			</div>
		</div>
	</div>
<?php
	$this->output->enable_profiler(isset($_PROFILER) ? $_PROFILER : FALSE);
?>		
