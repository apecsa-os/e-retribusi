<!DOCTYPE html>
<html lang="id" dir="ltr">

<head>
	<title><?php echo $_INFO_SUBTITLE.' - '.strtolower($_INFO_NAME);?></title>
  <link rel="icon" href="<?php echo base_url($minifier.$_INFO_LOGO);?>" /> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta http-equiv='Content-Type' content='text/html'/>
  <?php
  	if(!empty($_META)){
  		$meta_desc 	= array_key_exists('description', $_META) ? $_META['description'] : $_INFO_DESC;
  		$meta_geo 	= array_key_exists('geo.placename', $_META) ? $_META['geo.placename'] : 'Indonesia';
  		$meta_lg 	= array_key_exists('language', $_META) ? $_META['language'] : 'id';
  		$meta_sub 	= array_key_exists('subject', $_META) ? $_META['subject'] : $_INFO_SUBTITLE;
  		$meta_auth	= array_key_exists('author', $_META) ? $_META['author'] : $_INFO_OWNER;
  		$meta_aud 	= array_key_exists('audience', $_META) ? $_META['audience'] : 'all';
  		echo '<meta name="description" content="'.$meta_desc.'" />';
  		echo '<meta name="geo.placename" content="'.$meta_geo.'"/>';
  		echo '<meta name="language" content="'.$meta_lg.'" />';
  		echo '<meta name="subject" content="'.$meta_sub.'"/>';
  		echo '<meta name="Author" content="'.$meta_auth.'"/>';
  		echo '<meta name="audience" content="'.$meta_aud.'"/> ';
      echo array_key_exists('keywords', $_META) ? '<meta name="keywords" content="'.$_META['keywords'].'" />' : '';
  	}
  ?>
 
  <meta property="og:url" content="<?php echo site_url($this->uri->uri_string());?>" />
  <meta property="og:type" content="<?php echo $_OG_TYPE;?>" />
  <meta property="og:title" content="<?php echo $_TITLE;?>" />
  <meta property="og:description" content="<?php echo $_OG_DESC;?>" />
  <meta property="og:image" content="<?php echo base_url(MINIFIER.$_OG_IMAGE);?>" />
	<?php echo !empty($_CSS) ? $_CSS : '';?>
</head>

<body>
	<?php echo $_INDEX_BODY;?>
	<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h3 class="modal-title" id="myModalLabel">Modal title</h3>
	      </div>
	      <div id="myModalBody" class="modal-body"></div>
	    </div>
	  </div>
	</div>
<?php
    $this->output->enable_profiler(isset($_PROFILER) ? $_PROFILER : FALSE);
?>	 	
	<script type="text/javascript">
		var siteurl = '<?php echo site_url(); ?>';
		var base = '<?php echo base_url($this->uri->uri_string());?>';
		var curl = document.location;
		var lang = '<?php echo $this->session->userdata(DEF_APP.'_'.$_SESS_PREFIX.'language') ? $this->session->userdata(DEF_APP.'_'.$_SESS_PREFIX.'language') : "en"; ?>';
		var tsep = '<?php echo $_THOU_SEP;?>';
		var dsep = '<?php echo $_DEC_SEP;?>';
		var ddig = '<?php echo $_DEC_DIGIT;?>';
		var token = '<?php echo $this->session->userdata(DEF_APP.'_'.$_SESS_PREFIX.'token') ? $this->session->userdata(DEF_APP.'_'.$_SESS_PREFIX.'token') : '';?>';
	</script>
	<?php echo !empty($_JS) ? $_JS : '';?>
</body>

</html>
