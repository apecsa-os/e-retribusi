<!DOCTYPE html>
<html lang="id" dir="ltr">

<head>
	<title><?php echo $_INFO_SUBTITLE.' - '.strtolower($_INFO_NAME);?></title>
  <link rel="icon" href="<?php echo base_url($minifier.$_INFO_LOGO);?>" /> 
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta http-equiv='Content-Type' content='text/html'/>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content='<?php echo !empty($_META) && array_key_exists('description', $_META) ? $_META['description'] : $_INFO_DESC;?>'/>
  <meta name='geo.placename' content='<?php echo !empty($_META) && array_key_exists('geo.placename', $_META) ? $_META['geo.placename'] : 'Indonesia';?>'/>
  <meta name="language" content="id" />
  <meta name='subject' content='<?php echo !empty($_META) && array_key_exists('subject', $_META) ? $_META['subject'] : $_INFO_SUBTITLE;?>'/>
  <meta name='Author' content='<?php echo !empty($_META) && array_key_exists('author', $_META) ? $_META['author'] : $_INFO_OWNER;?>'/>
  <meta name='audience' content='all'/>

	<meta property="og:url"           content="<?php echo site_url($this->uri->uri_string());?>" />
	<meta property="og:type"          content="<?php echo $_OG_TYPE;?>" />
	<meta property="og:title"         content="<?php echo $_TITLE;?>" />
	<meta property="og:description"   content="<?php echo $_OG_DESC;?>" />
	<meta property="og:image"         content="<?php echo $_OG_IMAGE;?>" />
	<?php echo $_CSS;?>
</head>

<body>
	<?php echo $_INDEX_BODY;?>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
	      </div>
	      <div id="myModalBody" class="modal-body"></div>
	    </div>
	  </div>
	</div> 
<?php
    $this->output->enable_profiler(isset($_PROFILER) ? $_PROFILER : FALSE);
?>	 	
	<script type="text/javascript">
		var siteurl = '<?php echo site_url(); ?>';
		var base = '<?php echo base_url($this->uri->uri_string());?>';
		var curl = document.location;
		var lang = '<?php echo $this->session->userdata(DEF_APP.'_'.$_SESS_PREFIX.'language') ? $this->session->userdata(DEF_APP.'_'.$_SESS_PREFIX.'language') : "id";?>';
		var tsep = '<?php echo $_THOU_SEP;?>';
		var dsep = '<?php echo $_DEC_SEP;?>';
		var ddig = '<?php echo $_DEC_DIGIT;?>';
		var token = '<?php echo $this->session->userdata(DEF_APP.'_'.$_SESS_PREFIX.'token') ? $this->session->userdata(DEF_APP.'_'.$_SESS_PREFIX.'token') : '';?>';
	</script>
	<?php echo $_JS;?>
</body>

</html>
