<div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo site_url(BASE_PANEL);?>" class="site_title"><i class="fa fa-paw"></i> <span><?php echo $_INFO_NAME;?></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url(BASE_PANEL.MINIFIER.$this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'avatar'));?>" alt="<?php echo $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');?>" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <?php echo $_MENU['left'];?>
              </div>
            </div>
            <!-- /sidebar menu -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <?php echo $_MENU['top'];?>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="page-title">
            <div class="title_left">
              <h1><?php echo isset($_TITLE) ? ucwords($_TITLE) : '';?></h1>
            </div>
            <div class="title_right">
              <div class="col-xs-12 text-right">
                <a href="<?php echo site_url(BASE_PANEL.'assets/clear/cache?p=pub');?>" class="btn btn-primary sb-delete btn-md float-right;" title="clear public cache"><i class="fa fa-globe"></i> <span class="hidden-xs">Clear Public Cache</span></a>      
                <a href="<?php echo site_url(BASE_PANEL.'assets/clear/cache');?>" class="btn btn-primary sb-delete btn-md float-right;" title="clear panel cache"><i class="fa fa-desktop"></i> <span class="hidden-xs">Clear Panel Cache</span></a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <?php echo $_BREADCRUMB;?>    
            </div>
          </div>
          <?php echo $_FLASH;?>
          <div class="row">
            <div class="col-xs-12">
              <div class="x_panel">
                <div class="x_content">
                  <?php echo $_CONTAINER;?>       
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            2015 &copy; <?php echo $_INFO_NAME;?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>