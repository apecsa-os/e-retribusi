<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* DISCLAIMER
* LIBRARY MYHOOK - LIBRARY UNTUK MENGOMPRESS FILE OUTPUT SEKALIGUS MENYERTAKAN HTTP HEADER REQUEST
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
class Myhook
{
	protected $mime = 'text/html';
	protected $charset = 'utf-8';
	protected $status = 200;
	protected $last_mod;
	protected $expires;
	protected $output;
	protected $caching = false;


	function __construct(){
		$this->last_mod = gmdate('D, d M Y H:i:s').' GMT';
		// $this->expires 	= gmdate('D, d M Y H:i:s', strtotime('+1 year')).' GMT';
	}

	function mime($mime = 'text/html'){
		$this->mime = $mime;
		return $this;
	}

	function charset($charset = 'utf-8'){
		$this->charset = $charset;
		return $this;
	}

	function status($status = 200){
		$this->status = $status;
		return $this;
	}

	function header($header = '', $replace = TRUE){
		$this->header[] = array($header, $replace);
		return $this;
	}

	function expires($date = '+1 year'){
		if(is_string($date) && $date != 0){
			$this->expires = gmdate('D, d M Y H:i:s', strtotime($date)).' GMT';
		}
		else{
			$this->expires = 0;
		}
		return $this;
	}

	function cache($set = true, $time = 7200){
		if($set == true){
			$this->caching = $time;
		}
		return $this;
	}


	function output($output = ''){
		$this->output = $output;
		return $this;
	}

	function buffering( $buff = true){
		if($this->mime == 'application/json'){
			$output = json_encode($this->output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
			$this->output = $output;
		}
		elseif(($this->mime == 'text/css' || $this->mime == 'text/plain' || $this->mime == 'application/x-javascript' || $this->mime == 'text/html' && $buff == true)){
			$output = preg_replace(
			array(
				'/(\t)/',
				'#^\s*//.+$#m',
				'/\/+\s.*\n$/',
				self::adv_pattern(),
				// '/\r|\n/',
				self::buff_comments(),
				self::buff_regex(),
				self::html_comments()
			),
			array(
				'',
				'',
				'',
				' ',
				'',
				'',
				''
			), $this->output);
			$this->output = trim($output);
		}

		return $this;		
	}

	function etag($etag = ''){
		if(empty($etag)){
			$etag = md5($this->output);
		}

		$this->header[] = 'Etag: '.$etag;
		return $this;
	}

	function display(){
		$ci = & get_instance();


		$ci->output
		   ->set_status_header($this->status)
		   ->set_content_type($this->mime, $this->charset);

		foreach ($this->header as $key => $value) {
			$ci->output->set_header($value[0], $value[1]);
		}

		if($this->caching != false){
			// $ci->output->cache($this->caching);
		}
		$ci->output
		   // ->set_header('Last-Modified : '.$this->last_mod)
		   ->set_header('Expires: '.$this->expires)
		   ->set_output($this->output)
		   ->_display();
		exit;

	}

	function adv_pattern(){
		$pattern = <<<'LOD'
						~
						# definitions : 
						(?(DEFINE) (?<tagBL> pre | code | textarea | script)
     								(?<tagContent> < (\g<tagBL>) \b .*? </ \g{-1} > )
     								(?<tags> < [^>]* > )
     								(?<cdata> <!\[CDATA .*? ]]> )

     								(?<exclusionList> \g<tagContent> | \g<cdata> | \g<tags>)
						)

						# pattern :
							\g<exclusionList> (*SKIP) (*FAIL) | \s+
							~xsi
LOD;
		return $pattern;				
	}

	private function buff_comments(){
		$commentary = '!/\*.*?\*/!s';
		return $commentary;
	}

	private function html_comments(){
		$commentary = '/<!--(.*?)*-->/';
		return $commentary;
	}

	private function buff_regex(){

		$re = '%
		        (?>            
		          [^\S ]\s    
		        | \s{2,}       
		        )
		        (?=            
		          [^<]*+       
		          (?:          
		            <          
		            (?!/?(?:textarea|pre|script)\b)
		            [^<]*+     
		          )*+          
		          (?:          
		            <          
		            (?>textarea|pre|script)\b
		          | \z         
		          )            
		        ) 
		        %Six';
		return $re;		
	}	
}