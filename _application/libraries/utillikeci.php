<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Dbforce extends CI_Model
{
	var $pref;
	var $last_query;

	function __construct(){
		parent::__construct();
		$this->load->dbutil();
		$this->pref['filename'] = $this->db->database;
		$this->pref['format']	= 'gzip';
		$this->pref['newline']  = "\r\n";
	}

	function filename($filename){
		$this->pref['filename'] = $filename;
		return $this;
	}

	function format($format = 'gzip'){
		$this->pref['format'] = $format;
		return $this;
	}

	function table($table){
		if(!empty($table) && !is_array($table)){
			$this->pref['table'][] = $table;			
		}
		elseif(!empty($table) && is_array($table)){
			$this->pref['table'] = $table;
		}
		else{
			show_error('Anda belum menentukan tabel yang akan di backup');
		}
		return $this;
	}

	function ignore($table){
		if(!empty($table) && !is_array($table)){
			$this->pref['ignore'][] = $table;			
		}
		elseif(!empty($table) && is_array($table)){
			$this->pref['ignore'] = $table;
		}
		else{
			show_error('Anda belum menentukan tabel pengecualian yang akan di backup');
		}
		return $this;
	}

	function add_drop($set = TRUE){
		$this->pref['add_drop'] = $set;
		return $this;
	}

	function add_insert($set = TRUE){
		$this->pref['add_insert'] = $set;
		return $this;
	}

	function newline($set = "\n"){
		$this->pref['newline'] = $set;
		return $this;
	}

	function fk_check($set = TRUE){
		$this->pref['fk_check'] = $set;
		return $this;
	}

	function force(){
		$this->load->helper('download');
		$backup = $this->dbutil->backup($this->pref);
		$files = $this->pref['filename'].'.'.$this->pref['format'];
		force_download($files, $backup);
	}	
}