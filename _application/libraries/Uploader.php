<?php if(!defined('BASEPATH')) exit('Tidak Diperkenankan Memakai Skrip Langsung');
/**
* DISCLAIMER
* LIBRARY UPLOADER - LIBRARY UNTUK MENGUPLOAD FILE
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
class Uploader{	
	var $file_path;

	function upload_foto($input_name,$files,$folder_name='', $types='gif|jpg|png',$encrypt = TRUE){
		$this->file_path = UPLOAD_PATH.$folder_name;
		$ci = & get_instance();
		$ci->load->library('upload');
		$config=array(
			"upload_path" => $this->file_path,
			"allowed_types" => $types,
			"overwrite" => FALSE,
			"remove_spaces" => TRUE,
			"file_ext_tolower"=>TRUE,
			"encrypt_name"=> $encrypt
			);

		$dir_exist = true;
		if(!is_dir($this->file_path)){
			mkdir($this->file_path, 0777, true);
		}

		$result = array();
		if(is_array($files[$input_name]['name'])){
			foreach ($files[$input_name]['name'] as $key => $value) {
				$_FILES[$input_name]['name'] = $value;
				$_FILES[$input_name]['type'] = $files[$input_name]['type'][$key];
				$_FILES[$input_name]['tmp_name'] = $files[$input_name]['tmp_name'][$key];
				$_FILES[$input_name]['error'] =$files[$input_name]['error'][$key];
				$_FILES[$input_name]['size'] = $files[$input_name]['size'][$key];

				$ci->upload->initialize($config);
				$mini = '';
				if (!$ci->upload->do_upload($input_name)) {
					array_push($result, array("data"=>null,"error"=>$ci->upload->display_errors()));
				}
				else{
					array_push($result, array("data"=>$ci->upload->data(), "error"=>null));
				}
			}			
		}
		else{
			$ci->upload->initialize($config);
			$mini = '';
			if (!$ci->upload->do_upload($input_name)) {
				array_push($result, array("data"=>null,"error"=>$ci->upload->display_errors()));
			}
			else{
				array_push($result, array("data"=>$ci->upload->data(), "error"=>null));
			}			
		}

		return $result;

	}
}