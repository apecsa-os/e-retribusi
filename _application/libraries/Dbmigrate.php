<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* DISCLAIMER
* LIBRARY DATABASE MIGRATION FOR CREATING MIGRATION CLASS
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
class Dbmigrate
{
	protected $newline = "\n";
	protected $newtab  = "\t\t";
	protected $cschema = array();
	function __construct(){
		$ci = &get_instance();
		$db = & DB();
		$ci->load->helper('file');
		$this->dbase = $db->database;
		self::save_dir();
	}

/*
* TABLE EXCEPTION
* this function is used for defined the ignorable table on down function
*/
	function table_exception(){
		$this->etable = func_get_args();
		return $this;
	}

/*
* SAVE DIRECTION
* this function is used for defined the directory where the file is saved
*/
	function save_dir($dir = null){
		$this->save_dir = !empty($dir) ? $dir : APPPATH.'migrations/'.date('YmdHis').'_';
		return $this;
	}

/*
* INFORMATION SCHEMA
* GET DATABASE SCHEMA
*/
	private function information_schema_config(){
		$db = & DB();
		$schema = array(
			'dsn'	=> '',
			'hostname' => $db->hostname,
			'username' => $db->username,
			'password' => $db->password,
			'database' => 'information_schema',
			'dbdriver' => $db->dbdriver,
			'dbprefix' => $db->dbprefix,
			'pconnect' => $db->pconnect,
			'db_debug' => $db->db_debug,
			'cache_on' => $db->cache_on,
			'cachedir' => $db->cachedir,
			'char_set' => $db->char_set,
			'dbcollat' => $db->dbcollat,
			'swap_pre' => $db->swap_pre,
			'encrypt' => $db->encrypt,
			'compress' => $db->compress,
			'stricton' => $db->stricton,
			'failover' => $db->failover,
			'save_queries' => $db->save_queries
		);	
		return $schema;
	}
/*
* FIELD SCHEMA
* GET DATABASE FIELD SCHEMA
*/
	function field_schema(){
		$ci = &get_instance();
		$db = & DB();

		$schema 	= self::information_schema_config();

		$ci->load->database($schema);
		if(!empty($this->table)){
			$db->where_in('TABLE_NAME', $this->table);
		}
		$sql = $db->select('TABLE_NAME, COLUMN_NAME, COLUMN_DEFAULT, IS_NULLABLE, EXTRA, COLUMN_KEY, COLUMN_TYPE')
				  ->from('information_schema.COLUMNS')
				  ->where('TABLE_SCHEMA', $this->dbase)
				  ->get();

		$sql = $sql->result_array();
		$result = array();
		array_filter($sql, function($items, $key) use(&$result){
			$result[$items['TABLE_NAME']][] = $items;
		}, ARRAY_FILTER_USE_BOTH);
		$this->fschema = $result; 		 
		return $this;
	}
/*
* RELATED SCHEMA
* GET RELATED FIELD SCHEMA
*/
	function related_schema(){
		$ci = &get_instance();
		$db = & DB();

		$schema = self::information_schema_config();
		$ci->load->database($schema);
		if(!empty($this->table)){
			$db->where_in('TABLE_NAME', $this->table);
		}		
		$sql = $db->select('TABLE_NAME, CONSTRAINT_NAME, COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME')
				  ->from('information_schema.KEY_COLUMN_USAGE')
				  ->where(array('CONSTRAINT_SCHEMA' => $this->dbase, 'REFERENCED_TABLE_SCHEMA !=' => NULL))
				  ->get()
				  ->result_array();
		$this->rschema = $sql;
		return $this;		
	}


	function generate($filename = null){
		$filename = !empty($filename) ? strtolower($filename) : $this->dbase;

		self::field_schema();
		self::related_schema();

		$rschema 	 = array_unique(array_column($this->rschema, 'TABLE_NAME'));
		$result = '';

		// create class Migration for database name
		$result .= '<?php';
		$result .= $this->newline;
		$result .= 'defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');';
		$result .= $this->newline;
		$result .= 'class Migration_'.ucfirst(str_replace(' ', '_', $filename)).' extends CI_Migration {';
		
		// adding line break
		$result .= $this->newline.$this->newtab;

		// create new function up
		$result .= 'public function up(){';

		// adding line break and tabbing the line
		$result .= $this->newline.$this->newtab;

		// looping table from field schemata
		foreach ($this->fschema as $key => $value) {

			// adding line break and tabbing the line
			$result .= $this->newtab;

			// add notes on files
			$result .= '/*'.$this->newline.$this->newtab.$this->newtab;
			$result .= '************************************************************'.$this->newline.$this->newtab.$this->newtab;
			$result .= '* Add table '.$key.$this->newline.$this->newtab.$this->newtab;
			$result .= '************************************************************';
			$result .= $this->newline.$this->newtab.$this->newtab;
			$result .= '*/';

			// adding line breaks twice
			$result .= $this->newline.$this->newline;

			// adding tabbing twice
			$result .= $this->newtab.$this->newtab;

			// set array field
			$result .= '$this->dbforge->add_field(array(';

			// looping column field from table
			foreach ($value as $f => $g) {

				// if this column is primary key then set fkey, this fkey used for defined forge table key
				if($g['COLUMN_KEY'] == 'PRI'){
					$fkey = $g['COLUMN_NAME'];
				}
				
				// adding line breaks and tabbing the line third
				$result .= $this->newline.$this->newtab.$this->newtab.$this->newtab;

					// new array migration field
					$result .= '\''.$g['COLUMN_NAME'].'\' => array(';
					
					// adding line breaks and tabbing the line third
					$result .= $this->newline.$this->newtab.$this->newtab.$this->newtab;

						// get data type of column name
						preg_match('/[\w]+/', $g['COLUMN_TYPE'], $type_data);
						
						// get length of character
						preg_match('/[\d]+/', $g['COLUMN_TYPE'], $length);

						// define data type of table column
						$result .= "'type' => '".strtoupper($type_data[0])."',";

						// adding line breaks twice and tabbing the line third
						$result .= $this->newline.$this->newtab.$this->newtab.$this->newtab;

						// defined constraint length
						$result .= !empty($length[0]) ? "'constraint' => '".$length[0]."',".$this->newline.$this->newtab.$this->newtab.$this->newtab : '';
						
						// is default column settled ? if yes, defined default value 
						$result .= !empty($g['COLUMN_DEFAULT']) ? "'default' => '".$g['COLUMN_DEFAULT']."',".$this->newline.$this->newtab.$this->newtab.$this->newtab : ''; 
						
						// is column allowed nullable value ? if yes then set TRUE
						$result .= $g['IS_NULLABLE'] == 'YES' ? "'null' => TRUE,".$this->newline.$this->newtab.$this->newtab.$this->newtab : '';
						
						// is column is primary key ? if yes then set unsigned value
						$result .= $g['COLUMN_KEY'] == 'PRI' ? "'unsigned' => TRUE,".$this->newline.$this->newtab.$this->newtab.$this->newtab : '';
						
						// defined auto increment on primary column key 
						$result .= $g['COLUMN_KEY'] == 'PRI' ? "'auto_increment' => TRUE,".$this->newline.$this->newtab.$this->newtab.$this->newtab : '';
					$result .= '),';

					// adding line breaks and tabbing the line				
				$result .= $this->newline.$this->newtab.$this->newtab;
			}
			$result .= '));';

			// adding line breaks and tabbing the line
			$result .= $this->newline.$this->newtab.$this->newtab;

			// adding table key as primary key
			$result .= '$this->dbforge->add_key(\''.$fkey.'\', TRUE);';

			// adding line breaks and tabbing the line
			$result .= $this->newline.$this->newtab.$this->newtab;

			// adding insert statement
			$cschema = implode(', ',array_column($value, 'COLUMN_NAME'));

			/*
			* ###################################################################################################
			* INSERT STATEMENT
			* ###################################################################################################
			*/
			$ci = &get_instance();
			$db = & DB();
			
			$data = $db->select('*')
						 ->from($key)
						 ->get()
						 ->result_array();

			foreach ($data as $k => $v) {
				$v = array_map(function($items) use($db){
					if(is_null($items)){
						return 'NULL';
					}
					elseif(is_numeric($items)){
						return intval($items);
					}
					elseif(is_bool($items)){
						return (bool) $items;
					}
					elseif(is_float($items)){
						return floatval($items);
					}
					else{
						return '\''.$db->escape_str($items).'\'';
					}
				}, $v);

				$insert_statement = implode(',',array_values($v));
				$result .= '$this->dbforge->add_field("INSERT INTO `'.$key.'`('.$cschema.') VALUES ('.$insert_statement.')");';

				// adding line breaks and tabbing the line
				$result .= $this->newline.$this->newtab.$this->newtab;
			}

			// adding line breaks and tabbing the line
			$result .= $this->newline.$this->newtab.$this->newtab;

			/*
			* ###################################################################################################
			* END INSERT STATEMENT
			* ###################################################################################################
			*/
			// Adding related schemas
			if(in_array($key, $rschema) == true){
				// loop table relational
				foreach ($this->rschema as $rk => $rv) {
					if($rv['TABLE_NAME'] == $key){

						// adding field into dbforge
						$result .= '$this->dbforge->add_field(';

						// adding relation constrain query
						$result .= '\'ALTER TABLE `'.$rv['TABLE_NAME'].'` ';
						$result .= "ADD CONSTRAINT `".$rv['CONSTRAINT_NAME']."` FOREIGN KEY (`".$rv['COLUMN_NAME']."`) REFERENCES `".$rv['REFERENCED_TABLE_NAME']."` (`".$rv['REFERENCED_COLUMN_NAME']."`) ON DELETE CASCADE ON UPDATE CASCADE'";
						$result .= ');';

						// adding line breaks and tabbing the line
						$result .= $this->newline.$this->newtab.$this->newtab;
					}
				}
				// adding line breaks and tabbing the line
				$result .= $this->newline.$this->newtab.$this->newtab;
			}

			// create table forge
			$result .= '$this->dbforge->create_table(\''.$key.'\');';
			
			// adding line breaks and tabbing the line
			$result .= $this->newline.$this->newline.$this->newtab;
		}
		// closing the up function
		$result .= '}';

		// adding line breaks and tabbing the line
		$result .= $this->newline.$this->newline.$this->newtab;

		// create down function
		$result .= 'public function down(){';
		
		// adding line breaks and tabbing the line
		$result .= $this->newline;
		$result .= $this->newtab.$this->newtab;

		// function down to dropping table
		foreach ($this->fschema as $key => $value) {
			// if the table has in database schema and not ignorable then create drop statement
			if(!empty($this->etable) && !in_array($key, $this->etable)){
				$result .= '$this->dbforge->drop_table(\''.$key.'\');';

				// adding line breaks and tabbing the line
				$result .= $this->newline.$this->newtab.$this->newtab;	
			}
		}
		$result .= '// lakukan function down secara manual';
		$result .= $this->newline.$this->newtab;
		$result .= '}';
		$result .= $this->newline;
		$result .= '}';

		// write into file and save it into directory
		if ( ! write_file($this->save_dir.$filename.'.php', $result))
		{
		    $msg = FALSE;
		}
		else
		{
		    $msg = TRUE;
		}
		return $msg;
	}
}