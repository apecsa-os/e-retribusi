<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* DISCLAIMER
* LIBRARY FITABLE FOR GENERATE CODEIGNITER - DATATABLE 
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
class Fitable extends CI_Model
{
	var $callback;
	var $table;
	protected $action;
	protected $columns;
	protected $format;

	function __construct(){
		parent::__construct();
	}
/*
* QUERY SELECT
* param string $param
*/
	function select($param){
		$this->db->start_cache();
		// $this->columns = preg_filter('/.*\.(.*)/i', '$1', explode(',', $param));
		$this->columns = array_map('trim', explode(',', $param));
		foreach ($this->columns as $key => $value) {
			$field = explode(' ', $value);
			$this->columns[$key] = $field[0]; 
		}
		$this->db->select($param);
		return $this;
	}
/*
* QUERY FROM
* param string $table
*/
	function from($table){
		$this->table = $table;
		$this->db->from($table);
		return $this;
	}
/*
* QUERY JOIN
* param string $table
* param string $param
* param string $flags (LEFT, RIGHT, INNER, OUTTER)
*/
	function join($table, $param, $flags = 'LEFT'){
		$this->db->join($table, $param, $flags);
		return $this;
	}
/*
* QUERY WHERE
* param string $key / array $key => [$key => $value]
* param string $value
*/
	function where($key, $value = null){
		if( ! is_array($key)){
			$this->db->where($key, $value);
		}
		else{
			$this->db->where($key);
		}
		return $this;
	}
/*
* QUERY OR WHERE
* param string $key / array $key => [$key => $value]
* param string $value
*/
	function or_where($key, $value = null){
		if( ! is_array($key)){
			$this->db->or_where($key, $value);
		}
		else{
			$this->db->or_where($key);
		}
		return $this;
	}
/*
* QUERY WHERE IN
* param string $key
* param array $value
*/
	function where_in($key, $value = array()){
		$this->db->where_in($key, $value);
		return $this;
	}
/*
* QUERY OR WHERE IN
* param string $key
* param array $value
*/
	function or_where_in($key, $value = array()){
		$this->db->or_where_in($key, $value);
		return $this;
	}
/*
* QUERY LIKE
* param string $param | array $param => [key => val]
* param string $value
* param string $flags (BOTH, BEFORE, AFTER)
*/
	function like($param, $value = null, $flags='BOTH'){
		if( ! is_array($param)){
			$this->db->like($param, $value);
		}
		else{
			array_filter($key, function ($items, $a){
				$this->db->like($a, $items);
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;
	}
/*
* QUERY OR LIKE
* param string $param | array $param => [key => val]
* param string $value
* param string $flags (BOTH, BEFORE, AFTER)
*/
	function or_like($param, $value = null, $flags='BOTH'){
		if( ! is_array($param)){
			$this->db->or_like($param, $value);
		}
		else{
			array_filter($key, function ($items, $a){
				$this->db->or_like($a, $items);
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;
	}
/*
* QUERY GROUP BY
*/
	function group_by($field){
		if(!empty($field)){
			$this->db->group_by($field);			
		}
		return $this;
	}	
/*
* QUERY ORDER BY
*/
	private function order_by(){
		$columns 	= $this->input->post('order');
		if(!empty($columns)){
			$cols 		= $columns[0]['column'];
			$dir 		= $columns[0]['dir'];

			$this->db->order_by($this->columns[$cols], $dir);			
		}
		return $this;
	}

	private function limit(){
		$limit 	= $this->input->post('length') ? $this->input->post('length') : 10;
		$offset	= $this->input->post('start') ? $this->input->post('start') : 0;
		$this->db->limit($limit, $offset);
		return $this;
	}

/*
* data_format
* Formatting field data
* param string $param => field database
* param string $value => format function name
*/

	function data_format($param, $value){
		if( ! is_array($param)){
			$this->format[$param] = $value;
		}
		else{
			array_filter($key, function ($items, $a){
				$this->format[$a] = $items;
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;		
	}

	function data_hide(){
		$args = func_get_args();
		$this->data_hide = $args;
		return $this;
	}
/*
* SET ACTION
* MENGKONVERSI FIELD KOLOM TERAKHIR PADA TABLE MENJADI TOMBOL AKSI
* param string $title
* param string $attribute
* param string $icon
*/
	function set_action($title = '', $attribute = '', $icon = ''){
		if(is_array($attribute)){
			// $attribute 	= implode(' ', array_filter($attribute, function(&$items, $key){
			// 	$items = $key.'="'.$items.'"';
			// 	return $items;
			// }, ARRAY_FILTER_USE_BOTH));	
			$attr = '';
			foreach ($attribute as $key => $value) {
				$attr .= $key.'="'.$value.'" ';
			}
		}
		$attr .= ' title="'.ucfirst($title).'"';
		$this->action[] = '<a '.$attr.'>'.$icon.'<span class="hidden-xs hidden-sm hidden-md">'.ucfirst($title).'</span></a>';
		return $this;
	}
/*
* SEARCHABLE
* MENDAPATKAN DATA HTTP GET KOLOM SEARCH
*/
	function searchable(){
		$post_columns = $this->input->post('columns');
		if(!empty($post_columns)){
			array_walk($post_columns, function($items, $key){
				if($items['search']['value'] != '' && preg_match('/(_drpdwn)/', $items['search']['value']) == false){
					$this->db->like($this->columns[$key], $items['search']['value']);				
				}
				elseif($items['search']['value'] != '' && preg_match('/(_drpdwn)/', $items['search']['value']) == true && $items['search']['value'] != '_drpdwn'){
					$this->db->where($this->columns[$key], str_replace('_drpdwn', '', $items['search']['value']));
				}
			});			
		}
		
		$post_form = $this->input->post('form');
		if(!empty($post_form)){
			array_walk($post_form, function($items, $key){
				if($items != '' && preg_match('/(_drpdwn)/', $items) == false){
					$this->db->like($key, $items);
				}
				elseif($items != '' && preg_match('/(_drpdwn)/', $items) == true && $items != '_drpdwn'){
					$this->db->where($key, $items);
				}
			});	
		}
		return $this;
	}
/*
* AODATA
* RETRIEVE DATA MENJADI DATATABLE DATA FORMAT
*/
	function aodata($fetch_as = 'array'){
		// stop database caching if exist
		$this->db->stop_cache();
		if($query_total = $this->db->get()){
			// count total number of row
		    $this->callback['recordsTotal'] = $query_total->num_rows();
		}

		// call function searchable
		self::searchable();

		// stop database caching
		$this->db->stop_cache();
		if($query_total = $this->db->get()){
			// count total number of row of filtered data
		    $this->callback['recordsFiltered'] = $query_total->num_rows();
		}

		// call function searchable
		self::searchable();
		// call function order by
		self::order_by();
		// call function limit
		self::limit();
		$data = $this->db->get();
		// get result data
		$data = $data->result_array($fetch_as);
		if(!empty($this->format)){
			// get all debug parameters
			$debug = debug_backtrace();

			// get path information from file which connected with this library
			$files = pathinfo($debug[0]['file']);
			// get file name 
			$class = strtolower($files['filename']);
			$format = new $class;
		}
		else{
			$format = null;
		}

		// looping data result
		$data = array_map(function($items) use ($format){
			// set initialize row item of each data
			$obj = $items;

			// get all keys of items
			$field_array = array_keys($items);
			

			// get data id from each row
			$id 	= array_pop($items);

			// get data title from each row. The title is set from first index of select statement
			$title 	= $items[$field_array[0]];

			// if there's has formatting data, then passing the value into definition function
			if(!empty($this->format)){
				foreach ($this->format as $key => $value) {
					if(array_key_exists($key, $items) == true){
						$items[$key] = $format->$value($items[$key], $obj, $id);
					}
				}
			}

			// if the table has action method button, then create the button !
			if(isset($this->action) && !empty($this->action)){
				$items[] = sprintf(implode(' ', $this->action), $id, preg_replace('/[^a-zA-Z0-9]/', '-', $title));	
			}
			// else display the data normally
			else{
				$formatid = !empty($this->format) && array_key_exists(end($field_array), $this->format) ? $this->format[end($field_array)] : '';
				$items[] =  !empty($formatid) ? $format->$formatid($id, $items) : '';
			}

			// if there's a data to hide then remove the element of data hide from items
			if(!empty($this->data_hide)){
				foreach ($this->data_hide as $key => $value) {
					unset($items[$value]);
				}
			}
			return array_values($items);
		}, $data);

		$this->callback['data']  = $data;
		$this->db->flush_cache();
		return $this->callback;
	}
/*
* LAST QUERY
* RETRIEVE QUERY TERAKHIR YANG BERHASIL DI EKSEKUSI
*/
	function last_query(){
		$this->db->get();
		return $this->db->last_query();
	}

/*
* GENERATE TABLE
*/
/*
* ATTRIBUTE
* MENDEFINISIKAN ATRIBUT TABEL
* param string $param | array $param
* param string $value
*/
	function attribute($param, $value = null){
		if(!is_array($param)){
			$this->table['attribute'][$param] = $value;			
		}
		else{
			array_filter($param, function($items, $key){
				$this->table['attribute'][$key] = $items;
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;
	}
/*
* SET COLUMN
* MEMBUAT TABLE THEAD KOLOM
* param arguments => args1, args2, args3, etc
*/
	function set_column(){
		$args = func_get_args();
		$this->table['column'] = $args;
		return $this;
	}
/*
* ADD BUTTONS
* MEMBUAT TABLE BUTTON DILUAR TABEL CELL
* param string $title
* param string $attribute | array $attribute
* param string $icon
*/
	function add_buttons($title = '', $attribute = '', $icon = ''){
		if(is_array($attribute)){
			// $attribute 	= implode(' ', array_filter($attribute, function(&$items, $key){
			// 	$items = $key.'="'.$items.'"';
			// 	return $items;
			// }, ARRAY_FILTER_USE_BOTH));
			$attr = '';
			foreach ($attribute as $key => $value) {
				$attr .= $key.'="'.$value.'" ';
			}	
		}

		$this->table['buttons'][] = '<a '.$attr.'>'.$icon.ucfirst($title).'</a>';
		return $this;		
	}
/*
* FILTERED COLUMN
* MENDEFINISIKAN COLUM INDEX YANG SEARCHABLE
* param argument => no index kolom, contoh (0, 1, 3) => kolom 0, 1, dan 3 akan memiliki input text pada table header, sedangkan kolom 2 tidak searchable
*/
	function filtered_column(){
		$args = func_get_args();
		$this->table['filter'] = $args;
		return $this;
	}

	function filter_type($array){
		$this->table['cfilter'] = $array;
		return $this;
	}


	function advance_search($title = '', $form = ''){
		$this->table['advsrc_title'] = $title;
		$this->table['advsrc'] = $form;
		return $this;
	}
/*
* SET COLUMN WIDTH
* MENGATUR LEBAR KOLOM
* param argument => $array [index => width [ , index => width ]]
*/
	function column_width($array = array()){
		$this->table['colwidth'] = $array;
		return $this;
	}	
/*
* GENERATE
* MENGENERATE TABLE BOOTSTRAP - DATATABLE
*/
	function generate(){
		$tags = '';
		if(!empty($this->table['advsrc'])){
		$tags = '<div class="row">';
			$tags .= '<div class="col-xs-12">';
				$tags .= '<div class="x_panel">';
					$tags .= '<div class="x_title" style="padding-bottom:35px;">';
						$tags .= '<h2>'.$this->table['advsrc_title'].'</h2>';
						$tags .= '<ul class="nav navbar-right panel_toolbox">';
							$tags .= '<li>';
								$tags .= '<a class="collapse-link"><i class="fa fa-chevron-up"></i></a>';
							$tags .= '</li>';
						$tags .= '</ul>';
					$tags .= '</div>';
					$tags .= '<div class="x_content advance_search">';
						$tags .= $this->table['advsrc'];
					$tags .= '</div>';
				$tags .= '</div>';
			$tags .= '</div>';
		$tags .= '</div>';			
			unset($this->table['advsrc']);
		}
		if(isset($this->table['buttons']) && !empty($this->table['buttons'])){
			$tags .= '<div class="table-button text-right">';
				$tags .= '<div class="btn btn-group">';
				foreach ($this->table['buttons'] as $key => $value) {
					$tags .= $value;
				}
				$tags .= '</div>';
			$tags .= '</div>';
		}
		$tags .= '<div class="table-responsive">';
		$tags .= '<table class="table table-striped table-bordered table-hover dt-responsive" ';
		foreach ($this->table['attribute'] as $key => $value) {
			$tags .= $key.'="'.$value.'"';
		}
		$tags .= '>';
		$tags .= '<thead>';
			if(!empty($this->table['filter'])){
				$tags .= '<tr class="col-filters">';
					foreach ($this->table['column'] as $key => $value) {
						if(in_array($key, $this->table['filter']) == true){
							$filters = '<div class="input-group input-group-sm">';
							if(!empty($this->table['cfilter']) && array_key_exists($key, $this->table['cfilter']) == true){
								if(is_array($this->table['cfilter'][$key])){
									$filters .= '<select name="search['.$key.']" class="form-control column-filter">';
										$filters .= '<option value="">Choose '.$value.'</option>';
										foreach ($this->table['cfilter'][$key] as $c => $d) {
											$filters .= '<option value="'.$c.'">'.ucwords($d).'</option>';
										}
									$filters .= '</select>';
								}
								else{
									$filattribute = preg_replace('/(.*?)[:](.*?)[,]/', '$1="$2" ', $this->table['cfilter'][$key].',');
									$filters .= '<input type="text" name="search['.$key.']" '.$filattribute.' placeholder="'.ucfirst($value).'" />';
								}
							}
							else{
								$filters .= '<input type="text" name="search['.$key.']" class="form-control column-filter" placeholder="'.ucfirst($value).'" />';
							}
							$filters .= '<span class="input-group-btn" style="width:1%;">';
							$filters .= '<button class="btn btn-default filter-control">';
							$filters .= '<i class="fa fa-search"></i>';
							$filters .= '</button>';
							$filters .= '</span>';
							$filters .= '</div>';
						}
						else{
							$filters = '&nbsp';
						}
							$tags .= '<td>';
							$tags .= $filters;
							$tags .= '</td>';				
					}	
				$tags .= '</tr>';				
			}
			$tags .= '<tr>';
				foreach ($this->table['column'] as $key => $value) {
					$width = !empty($this->table['colwidth']) && array_key_exists($key, $this->table['colwidth']) ? 'width="'.$this->table['colwidth'][$key].'"' : '';
					$tags .= '<th '.$width.'>';
						$tags .= ucwords($value);
					$tags .= '</th>';
				}
			$tags .= '</tr>';
		$tags .= '</thead>';
		$tags .= '<tfoot>';
		$tags .= '</tfoot>';
		$tags .= '</table>';
		$tags .= '</div>';
		$this->table = '';
		return $tags;
	}

}