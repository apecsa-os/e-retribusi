<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* DISCLAIMER
* LIBRARY DBFORCE FOR BACKUP DATABASE
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
class Dbforce extends CI_Model
{
	protected $newlines 	= "\r\n";
	protected $dschema 		= "DROP SCHEMA IF EXISTS ";
	protected $cschema		= "CREATE SCHEMA IF NOT EXISTS ";
	protected $drop_tbl		= "DROP TABLE IF EXISTS ";
	protected $ctbl			= "CREATE TABLE IF NOT EXISTS ";
	protected $related 		= array();
	protected $constraint 	= array();

	function __construct()
	{
		parent::__construct();
		$this->dbase = $this->db->database;
	}

/*
* CREATE OBJREADER
* INISIALISASI TABEL YANG ADA DI DATABASE
* WAJIB ADA
*/
	function initialize(){
		$this->list = $this->db->list_tables();
		return $this;
	}
/*
* NEWLINES
* DEFINISIKAN JENIS BARIS BARU YANG AKAN DIGUNAKAN
* param string lines => \r\n atau \n atau kosongkan jika tanpa baris pemisah
*/
	function newlines($lines){
		$this->newlines = $lines;
		return $this;
	}
/*
* CREATE DATABASE
* DEFINISKAN APAKAH AKAN MENYISIPKAN QUERY PERINTAH CREATE DB PADA SCRIPT
* param bool set => true / false
*/
	function create_db($set = true){
		$this->create_db = $set;
		return $this;
	}
/*
* CREATE TABLE
* DEFINISKAN APAKAH AKAN MENYISIPKAN QUERY PERINTAH CREATE TABLE PADA SCRIPT
* param bool set => true / false
*/
	function create_table($set = true){
		$this->create_table = $set;
		return $this;
	}
/*
* ADD DROP DB
* DEFINISKAN APAKAH AKAN MENYISIPKAN QUERY PERINTAH DROP DB PADA SCRIPT
* param bool set => true / false
*/
	function add_drop_db($set = true){
		$this->drop_db = $set;
		return $this;
	}
/*
* ADD DROP TABLE
* DEFINISKAN APAKAH AKAN MENYISIPKAN QUERY PERINTAH DROP TABLE PADA SCRIPT
* param bool set => true / false
*/
	function add_drop_table($set = true){
		$this->drop_table = $set;
		return $this;
	}
/*
* ADD INSERT
* DEFINISKAN APAKAH AKAN MENYISIPKAN QUERY PERINTAH INSERT QUERY PADA SCRIPT
* param bool set => true / false
*/
	function add_insert($set = true){
		$this->insert = $set;
		return $this;
	}
/*
* TABLE
* DEFINISKAN TABLE YANG AKAN DI EKSEKUSI (ABAIKAN JIKA SEMUA TABEL AKAN DIEKSEKUSI)
* param string / array table => [table_name]
*/
	function table($table = null){
		if(!empty($table)){
			if(is_array($table)){
				$this->table = $table;
			}
			else{
				$this->table[] = $table;
			}			
		}
		return $this;
	}
/*
* CREATE NOTES
* DEFINISKAN APAKAH AKAN MENYISIPKAN KOMENTAR PADA SCRIPT
* param bool command => true / false
*/
	function notes($command = true){
		$this->notes = $command;
		return $this;
	}
/*
* FOREIGN KEY CHECK
* DEFINISKAN APAKAH AKAN MENYISIPKAN PERINTAH PENGECEKAN FOREIGN KEY
* param bool command => true / false
*/
	function fk_check($command = 1){
		$this->fk_check = $command;
		return $this;
	}
/*
* FOREIGN KEY CHECK
* SET FORMAT OUTPUT
* param string format => txt / sql 
*/
	function format($format = 'txt'){
		$this->format = $format;
		return $this;
	}
/*
* FILENAME
* SET NAMA OUTPUT FILE
* param string filename
* DEFAULT nama database
*/

	function filename($filename){
		$this->filename = $filename;
		return $this;
	}

/*
* INFORMATION SCHEMA
* GET DATABASE SCHEMA
*/
	private function information_schema_config(){
		$init = $this->db;
		$schema = array(
			'dsn'	=> '',
			'hostname' => $init->hostname,
			'username' => $init->username,
			'password' => $init->password,
			'database' => 'information_schema',
			'dbdriver' => $init->dbdriver,
			'dbprefix' => $init->dbprefix,
			'pconnect' => $init->pconnect,
			'db_debug' => $init->db_debug,
			'cache_on' => $init->cache_on,
			'cachedir' => $init->cachedir,
			'char_set' => $init->char_set,
			'dbcollat' => $init->dbcollat,
			'swap_pre' => $init->swap_pre,
			'encrypt' => $init->encrypt,
			'compress' => $init->compress,
			'stricton' => $init->stricton,
			'failover' => $init->failover,
			'save_queries' => $init->save_queries
		);	
		return $schema;
	}
/*
* FIELD SCHEMA
* GET DATABASE FIELD SCHEMA
*/
	function field_schema(){
		$schema 	= self::information_schema_config();
		$this->load->database($schema);
		if(!empty($this->table)){
			$this->db->where_in('TABLE_NAME', $this->table);
		}
		$sql 		= $this->db
							->select('TABLE_NAME, COLUMN_NAME, COLUMN_DEFAULT, IS_NULLABLE, EXTRA, COLUMN_KEY, COLUMN_TYPE')
							->from('information_schema.COLUMNS')
							->where('TABLE_SCHEMA', $this->dbase)
							->get();
		$sql = $sql->result_array();
		$result = array();
		array_filter($sql, function($items, $key) use(&$result){
			$result[$items['TABLE_NAME']][] = $items;
		}, ARRAY_FILTER_USE_BOTH);
		$this->fields = $result; 
		return $this;
	}
/*
* RELATED SCHEMA
* GET RELATED FIELD SCHEMA
*/
	function related_schema(){
		$schema = self::information_schema_config();
		$this->load->database($schema);
		if(!empty($this->table)){
			$this->db->where_in('TABLE_NAME', $this->table);
		}		
		$sql 		= $this->db
							->select('TABLE_NAME, CONSTRAINT_NAME, COLUMN_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME')
							->from('information_schema.KEY_COLUMN_USAGE')
							->where(array('CONSTRAINT_SCHEMA' => $this->dbase, 'REFERENCED_TABLE_SCHEMA !=' => NULL))
							->get()
							->result_array();
		array_filter($sql, function($items, $key){
			$this->related[$items['TABLE_NAME']][] = 'ADD KEY `'.$items['CONSTRAINT_NAME'].'` (`'.$items['COLUMN_NAME'].'`)';
			$this->constraint[$items['TABLE_NAME']][] = 'ADD CONSTRAINT `'.$items['CONSTRAINT_NAME'].'` FOREIGN KEY (`'.$items['COLUMN_NAME'].'`) REFERENCES `'.$items['REFERENCED_TABLE_NAME'].'` (`'.$items['REFERENCED_COLUMN_NAME'].'`) ON DELETE CASCADE ON UPDATE CASCADE';
 		}, ARRAY_FILTER_USE_BOTH);
		return $this;		
	}

/*
* MESSAGE
* SISIPAN CATATAN PADA SETIAP BARIS QUERY
*/
	function msg($names, $type = null){
		$notes = '';
		if($this->notes == TRUE){
			$notes = '/*'.$this->newlines;
			$notes .= '* *********************************************'.$this->newlines;
			if($type == 'DB'){
				$tbl = '* SCHEMA ';			
			}
			elseif ($type == 'DATA') {
				$tbl = '* DATA FOR TABLE `'.$this->dbase.'`';
			}
			else{
				$tbl = '* TABLE `'.$this->dbase.'`';			
			}
			$notes .= $tbl.'.`'.$names.'`'.$this->newlines;
			$notes .= '* *********************************************'.$this->newlines;
			$notes .= '*/'.$this->newlines;			
		}
		return $notes;
	}
/*
* GENERATE
* GENERATE DATABASE QUERY OUTPUT
*/
	function generate(){
		self::field_schema();
		self::related_schema();
		$this->load->database('default');

		$sql = '';
		$sql .= 'SET FOREIGN_KEY_CHECKS='.$this->fk_check.';'.$this->newlines;
		$sql .= 'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";'.$this->newlines;

		if($this->drop_db == TRUE){
			$sql .= $this->newlines;
			$sql .= self::msg($this->dbase, 'DB');
			$sql .= $this->newlines;
			$sql .= $this->dschema.'`'.$this->dbase.'`;'.$this->newlines;
			$sql .= $this->newlines;
			$sql .= self::msg($this->dbase, 'DB');
		}

		if($this->create_db == TRUE){
			$sql .= $this->newlines;
			$sql .= $this->cschema.'`'.$this->dbase.'` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;'.$this->newlines;
			$sql .= 'USE `'.$this->dbase.'`;';
			$sql .= $this->newlines;			
			$sql .= $this->newlines;
		}

		$this->columns = array();
		array_filter($this->fields, function($items, $key) use(&$sql){
			if($this->drop_table == TRUE){
				$sql .= self::msg($key);
				$sql .= $this->newlines;
				$sql .= $this->drop_tbl.'`'.$key.'`;'.$this->newlines;
			}
			$this->columns[$key]['key']		  = implode('`,`',array_column($items, "COLUMN_NAME"));
			$this->columns[$key]['statement'] = 'INSERT INTO `'.$key.'` (`'.implode('`,`',array_column($items, "COLUMN_NAME")).'`) VALUES';

			/*
			* CREATE TABLE STATEMENT
			*/			
			if($this->create_table == TRUE){
				$sql .= $this->newlines;
				$sql .= $this->ctbl.'`'.$key.'`(';
				$sql .= $this->newlines;
				foreach ($items as $k => $v) {
					$nulled = "";
					if(is_string($v['COLUMN_DEFAULT']) || is_numeric($v['COLUMN_DEFAULT'])){
						$v['COLUMN_DEFAULT'] = '\''.$v['COLUMN_DEFAULT'].'\'';
					}
					elseif($v['COLUMN_DEFAULT'] == null){
						$v['COLUMN_DEFAULT'] = 'NULL';
					}

					$default = " DEFAULT ".$v['COLUMN_DEFAULT'];
					if($v['IS_NULLABLE'] == "NO"){
						$default = '';
						$nulled = " NOT NULL";
					}
					if($v['EXTRA'] == "auto_increment"){
						$default = " AUTO_INCREMENT";
					}
					if($v['COLUMN_KEY'] == "PRI"){
						$primary = $v['COLUMN_NAME'];
					}
					$commas = array_key_exists($k+1, $items) ? ',' : '';					
					$sql .= "\t".'`'.$v['COLUMN_NAME'].'` '.$v['COLUMN_TYPE'].''.$nulled.$default.',';
					$sql .= $this->newlines;
				}
				$sql .= 'PRIMARY KEY (`'.$primary.'`)';
				$sql .= $this->newlines;
				$sql .= ')';
				$sql .= 'ENGINE=InnoDB DEFAULT CHARSET=latin1;';			
				$sql .= $this->newlines;
			}

			if($this->insert == TRUE){
				/*
				* INSERT STATEMENT
				*/
				$get_data = $this->db
								 ->select($this->columns[$key]['key'])
								 ->from($key)
								 ->get()
								 ->result_array();
				if(!empty($get_data)){
					$sql .= $this->newlines;
					$sql .= $this->columns[$key]['statement'];
					$sql .= $this->newlines;
					foreach ($get_data as $k => $v) {
						$v = array_map(function($items){
							if(is_null($items)){
								return 'NULL';
							}
							elseif(is_numeric($items)){
								return intval($items);
							}
							elseif(is_bool($items)){
								return (bool) $items;
							}
							elseif(is_float($items)){
								return floatval($items);
							}
							else{
								return '\''.$this->db->escape_str($items).'\'';
							}
						}, $v);

						$commas = ';';
						if(array_key_exists($k+1, $get_data)){
							$commas = ',';
						}
						$sql .= '('.implode(',',array_values($v)).')'.$commas.$this->newlines;
					}
				}					
			}

		}, ARRAY_FILTER_USE_BOTH);
		$sql .= $this->newlines;

		if(!empty($this->related)){
			uasort($this->related, function($a, $b){
				return !is_array($a) ? -1: 1;
			});			
		}

		/*
		* ALTER TABLE STATEMENT
		*/
		if($this->create_table == TRUE){
			array_filter($this->related, function($items, $key) use(&$sql){
				$sql .= $this->newlines;
				$sql .= self::msg($key);		
				$sql .= $this->newlines;
				$sql .= 'ALTER TABLE `'.$key.'`';
				$sql .= $this->newlines;
				$sql .= is_array($items) ? implode(','.$this->newlines, $items) : $items;
				$sql .= ';';
				$sql .= $this->newlines;
			}, ARRAY_FILTER_USE_BOTH);			
		}

		/*
		* CREATE CONSTRAINT STATEMENT
		*/
		if($this->create_table == TRUE){
			array_filter($this->constraint, function($items, $key) use(&$sql){
				$sql .= $this->newlines;
				$sql .= self::msg($key);			
				$sql .= $this->newlines;
				$sql .= 'ALTER TABLE '.$this->db->protect_identifiers($key);
				$sql .= $this->newlines;
				$sql .= implode(','.$this->newlines, $items).';';
				$sql .= $this->newlines;
			}, ARRAY_FILTER_USE_BOTH);			
		}
		unset(
			$this->dbase, $this->filename, $this->format, $this->notes, $this->fk_check, $this->drop_db, $this->fields, $this->related, $this->constraint,
			$this->create_db, $this->drop_table, $this->insert, $this->create_table, $this->dschema, $this->cschema, $this->drop_tbl, $this->ctbl, $this->newlines
		);

		$sql .= 'SET FOREIGN_KEY_CHECKS=1;';
		return $sql;
	}
/*
* FORCE
* UNDUH HASIL GENERATE DATABASE KEDALAM SEBUAH FILE
*/
	function force(){
		$this->load->helper('download');
		$filename 	= !empty($this->filename) ? $this->filename : $this->dbase;
		$format 	= $this->format;
		$onlyzip 	= ($this->format == 'gzip') || ($this->format == 'zip') ? 'sql' : $this->format;
		$files 		= $filename.'.'.$format;
		$query 		= self::generate();
		if($format != 'gzip' || $format != 'zip'){
			force_download($files, $query);			
		}
		else{
			$this->load->library('zip');
			$this->zip->add_data($filename.'.sql', $query);
			$this->zip->archive(APPPATH.'backup/'.$filename.'.'.$format);
			$this->zip->download($filename.'.'.$format);
		}
	}

}