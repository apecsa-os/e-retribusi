<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* DISCLAIMER
* LIBRARY GEOPLUGIN FOR GET VISITOR INFORMATION
* COPYRIGHT : 2016 
* ORIGINAL AUTHOR : LUTHFI SATRIA RAMDHANI
* 1. DILARANG UNTUK MENGAMBIL ALIH HAK CIPTA PEMBUATAN TANPA PERSETUJUAN AUTHOR
* 2. DILARANG MENGHAPUS BARIS DISCLAIMER INI
* 3. DILARANG MENGKOMERSILKAN LIBRARY INI DALAM BENTUK APAPUN KEPADA SIAPAPUN TANPA PERSETUJUAN ATAU TANPA MEMBERIKAN PENGHARGAAN ATAU KONTRIBUSI ATAU DONASI KEPADA SAYA SEBAGAI PENULIS
* 4. ANDA DIPERSILAHKAN MENGUBAH ATAU MENAMBAHKAN ATAU MENYISIPKAN KODE DI DALAM LIBRARY INI DAN MENAMBAHKAN NAMA ANDA SEBAGAI AUTHOR TANPA MENGHILANGKAN NAMA AUTHOR SEBELUMNYA
* 5. ANDA PASTI TAHU DAN MERASAKAN SUSAH PAYAHNYA MEMBUAT SEBUAH KODE PROGRAM, JADI HARGAILAH SESAMA PROGRAMMER
* 6. SEGALA BENTUK KECURANGAN INSYAALLAH AKAN MENDAPATKAN BALASAN DI DUNIA MAUPUN AKHIRAT, TETAPLAH JUJUR KAWAN
*
* TERIMA KASIH
*
*
* SEGALA PERTANYAAN DAN DISKUSI DAPAT ANDA KIRIMKAN KE ALAMAT EMAIL DI BAWAH INI :
* luthfi_its@yahoo.com
*/
class Geoplugin
{
	var $geo;
	var $browser;
	function __construct(){
	}
	/*
		param $geodata = array(
			$request, $status, $credit, $city, $region, $areaCode, $dmaCode, $countryCode, $continentCode, $latitude, $longitude, 
			$regionCode, $regionName, $currencyCode, $currencySymbol, $currencySymbol_UTF8, $currencyConverter
		) 
	*/
	function geodata(){
		$geo =  unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
		$result = array();
		foreach ($geo as $key => $value) {
			$result[str_replace('geoplugin_', '', $key)] = $value;
		}
		$this->geo = $result;
		return $this;
	}

	function agent(){
		$ci = &get_instance();
		$ci->load->library('user_agent');

		if ($ci->agent->is_browser())
		{
			$this->browser['browser'] 	 = $ci->agent->browser().' '.$ci->agent->version();
		}
		elseif ($ci->agent->is_robot())
		{
			$this->browser['robot'] 	 = $ci->agent->robot();
		}
		elseif ($ci->agent->is_mobile())
		{
			$this->browser['mobile'] 	= $ci->agent->mobile();		    
		}
			// $this->browser['agent'] = $_SERVER['HTTP_USER_AGENT'];

		if ($ci->agent->is_referral())
		{
			$this->browser['referrer'] 	= $ci->agent->referrer();
		}

		return $this;
	}
}