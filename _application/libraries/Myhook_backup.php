<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Myhook
{
	protected $expires;
	protected $output;
	protected $mime 		= 'text/html';
	protected $charset 		= 'utf-8';
	protected $mod;
	protected $cache;

	function set_charset($charset){
		$this->charset = $charset;
	}

	function set_mime($mime){
		$this->mime = $mime;
		return $this;
	}

	function set_modified($datetime){
		$this->mod = $datetime;
		return $this;
	}

	function set_expires($expires){
		$this->expires = $expires;
		return $this;
	}

	function set_hookput($output, $buffer = false){
		if($this->mime == 'application/json'){
			$output = json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		}
		elseif(($this->mime == 'text/css' || $this->mime == 'text/plain' || $this->mime == 'application/x-javascript' || $this->mime == 'text/html') && $buffer == true){
			$output = preg_replace(
			array(
				'/(\t)/',
				'#^\s*//.+$#m',
				'/\r|\n/',
				self::buff_comments(),
				self::buff_regex(),
			),
			'', $output);
		}

		$this->output = $output;
		return $this;
	}

	function cache_time($time){
		$time = !empty($time) ? $time : '';
		$this->cache = $time;
		return $this;
	}

	function buff_whitespace(){
		$ws = '/([^\S]\s|\r\n)/';
		return $ws;
	}

	function buff_comments(){
		$commentary = '!/\*.*?\*/!s';
		return $commentary;
	}

	function buff_regex(){

		$re = '%
		        (?>            
		          [^\S ]\s    
		        | \s{2,}       
		        )
		        (?=            
		          [^<]*+       
		          (?:          
		            <          
		            (?!/?(?:textarea|pre|script)\b)
		            [^<]*+     
		          )*+          
		          (?:          
		            <          
		            (?>textarea|pre|script)\b
		          | \z         
		          )            
		        ) 
		        %Six';
		return $re;		
	}

	function tampil(){
		$ci = & get_instance();
		
		$maxage = 0;
		if(!empty($this->cache)){
			$ci->output->cache($this->cache);
			$maxage = 60*60*24*7*30;
		}

		$cache  = false;
		$last_mod = gmdate('D, d M Y H:i:s', strtotime($this->expires)).' GMT';
		if($this->expires != 0){
			$this->expires 	= gmdate('D, d M Y H:i:s', strtotime($this->expires)).' GMT';
			$last_mod 		= gmdate('D, d M Y H:i:s').' GMT';
			$cache  		= true;
		}

		$ci->output
			 ->set_status_header(200)
			 ->set_content_type($this->mime, 'utf-8')
			 ->set_header('Accept-Ranges: bytes')
			 ->set_header('HTTP/1.0 200 OK')
			 ->set_header('HTTP/1.1 200 OK')
			 ->set_header('Last-Modified: '.$last_mod)
			 ->set_header('Expires:'.$this->expires)
			 // ->set_header('Etag: '.md5($this->output))
			 ->set_header('Cache-Control: max-age='.$maxage)
			 ->set_header('Cache-Control: post-check=0, pre-check=0', $cache)
			 ->set_header('Pragma: no-cache')
			 ->set_header("X-Content-Type-Options: nosniff")
			 ->set_header('X-XSS-Protection: 1; mode=block')
			 ->set_header("X-Frame-Options: SAMEORIGIN")				 
			 ->set_output($this->output)		
			 ->_display();
		exit;		
	}
}