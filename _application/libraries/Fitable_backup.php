<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fitable extends CI_Model
{
	var $query;
	var $flags;
	function __construct()
	{
		parent::__construct();
	}

	function select($param){
		$this->query['select'] = $param;
		return $this;
	}

	function from($param){
		$this->query['from'] = $param;
		return $this;
	}

	function join($param, $value = null, $flags='LEFT'){
		if( ! is_array($param)){
			$this->query['join'][$param] = $value;
			$this->flags['join'][] 		 = $flags;
		}
		else{
			$flags = $value;
			array_filter($key, function ($items, $a){
				$this->query['join'][$a] = $items;
				$this->flags['join'][]      = $flags;
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;		
	}

	function where($key, $value = null){
		if( ! is_array($key)){
			$this->query['where'][$key] = $value;
		}
		else{
			array_filter($key, function ($items, $a){
				$this->query['where'][$a] = $items;
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;
	}

	function or_where($key, $value = null){
		if( ! is_array($key)){
			$this->query['or_where'][$key] = $value;
		}
		else{
			array_filter($key, function ($items, $a){
				$this->query['or_where'][$a] = $items;
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;
	}

	function where_in($key, $value = null){
		if( ! is_array($key)){
			$this->query['where_in'][$key] = $value;
		}
		else{
			array_filter($key, function ($items, $a){
				$this->query['where_in'][$a] = $items;
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;
	}

	function or_where_in($key, $value = null){
		if( ! is_array($key)){
			$this->query['or_where_in'][$key] = $value;
		}
		else{
			array_filter($key, function ($items, $a){
				$this->query['or_where_in'][$a] = $items;
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;
	}

	function like($param, $value = null, $flags='BOTH'){
		if( ! is_array($param)){
			$this->query['like'][$param] = $value;
			$this->flags['like'][] 		 = $flags;
		}
		else{
			$flags = $value;
			array_filter($key, function ($items, $a){
				$this->query['like'][$a] = $items;
				$this->flags['like'][]   = $flags;
			}, ARRAY_FILTER_USE_BOTH);
		}
		return $this;		
	}

	function order_by($param, $flags = 'ASC'){
		$this->query['order_by'][$param] = $flags;
		return $this;
	}

	function limit($limit, $offset){
		$limit = $this->input->post('length') ? $this->input->post('length') : 10;
		$this->query['limit'] = array($limit, $offset);
		return $this;
	}

	function generate(){
		foreach ($this->query as $key => $value) {
			if($key == 'join' || $key == 'like'){
				$i = 0;
				foreach ($value as $ch => $v) {
					$this->db->$key($ch, $v, $this->flags[$key][$i]);
					$i++;
				}
			}
			elseif ($key == 'where_in') {
				foreach ($value as $ch => $v) {
					$this->db->$key($ch, $v);
				}	
			}
			elseif ($key == 'order_by') {
				foreach ($value as $ch => $v) {
					$this->db->order_by($ch, $v);
				}
			}
			else{
				$this->db->$key($value);				
			}
		}
		$query = $this->db->get();
		$data = $query->result();
		return $this->db->last_query();
		return $this;
	}
}