<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Myhook
{
	protected $mime = 'text/html';
	protected $charset = 'utf-8';
	protected $status = 200;
	protected $last_mod;
	protected $expires;
	protected $output;
	protected $caching = false;


	function __construct(){
		// $this->last_mod = gmdate('D, d M Y H:i:s').' GMT';
		// $this->expires 	= gmdate('D, d M Y H:i:s', strtotime('+1 year')).' GMT';
	}

	function mime($mime = 'text/html'){
		$this->mime = $mime;
		return $this;
	}

	function charset($charset = 'utf-8'){
		$this->charset = $charset;
		return $this;
	}

	function status($status = 200){
		$this->status = $status;
		return $this;
	}

	function header($header = ''){
		$this->header[] = $header;
		return $this;
	}

	function expires($date = '+1 year'){
		if(is_string($date) || $date >= 0){
			$this->expires = gmdate('D, d M Y H:i:s', strtotime($date)).' GMT';
		}
		else{
			$this->expires = 0;
		}
		return $this;
	}

	function cache($set = true, $time = 7200){
		if($set == true){
			$this->caching = $time;
		}
		return $this;
	}


	function output($output = ''){
		$this->output = $output;
		return $this;
	}

	function buffering( $buff = true){
		if($this->mime == 'application/json'){
			$output = json_encode($this->output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
			$this->output = $output;
		}
		elseif(($this->mime == 'text/css' || $this->mime == 'text/plain' || $this->mime == 'application/x-javascript' || $this->mime == 'text/html' && $buff == true)){
			$output = preg_replace(
			array(
				'/(\t)/',
				'#^\s*//.+$#m',
				'/\r|\n/',
				self::buff_comments(),
				self::buff_regex(),
			),
			'', $this->output);
			$this->output = $output;
		}

		return $this;		
	}

	function etag($etag = ''){
		if(empty($etag)){
			$etag = md5($this->output);
		}

		$this->header[] = 'Etag: '.$etag;
		return $this;
	}

	function display(){
		$ci = & get_instance();


		$ci->output
		   ->set_status_header($this->status)
		   ->set_content_type($this->mime, $this->charset);

		foreach ($this->header as $key => $value) {
			$ci->output->set_header($value);
		}

		if($this->caching != false){
			$ci->output->cache($this->caching);
		}
		$ci->output
		   // ->set_header('Last-Modified : '.$this->last_mod)
		   ->set_header('Expires: '.$this->expires)
		   ->set_output($this->output)
		   ->_display();
		exit;

	}

	private function buff_comments(){
		$commentary = '!/\*.*?\*/!s';
		return $commentary;
	}

	private function buff_regex(){

		$re = '%
		        (?>            
		          [^\S ]\s    
		        | \s{2,}       
		        )
		        (?=            
		          [^<]*+       
		          (?:          
		            <          
		            (?!/?(?:textarea|pre|script)\b)
		            [^<]*+     
		          )*+          
		          (?:          
		            <          
		            (?>textarea|pre|script)\b
		          | \z         
		          )            
		        ) 
		        %Six';
		return $re;		
	}	
}