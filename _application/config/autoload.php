<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$autoload['packages'] = array();


$autoload['libraries'] = array(
							'database',
							'session',
							'form_validation',
							'myhook'
							);


$autoload['drivers'] = array(
						);


$autoload['helper'] = array(
						"url",
						"path",
						"html",
						"file",
						"myfunc"
					);

$autoload['config'] = array(
					);

$autoload['language'] = array();

$autoload['model'] = array();
