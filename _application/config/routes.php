<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once( BASEPATH .'database/DB.php');
$db = & DB();

$uri        		= $_SERVER['REQUEST_URI'];
$lookup     		= preg_match('/'.str_replace('/','',BASE_PANEL).'/', $uri); 
$is_panels  		= $lookup == true ? BASE_PANEL : '';
$GLOBALS['lookup'] 	= $is_panels;

$route['default_controller'] 	= 'main';
$route['404_override'] 			= 'not_found';
$route['translate_uri_dashes'] 	= FALSE;

$route['^'.$is_panels.MINIFIER.'(.*)$']		= 'assets/index/$1';
$route['^'.$is_panels.'assets/clear/(.*)$']	= 'assets/clear_cache/$1';

