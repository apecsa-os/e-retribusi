<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* BUILD DEFAULT TEMPLATE SIDE BAR TREE
*/

function default_sidebar_tree($menu, $parent = 0, $access_point, $level = 0){
		$tag = '';
		$result  	= array_values(array_filter($menu, function($items) use($parent, $access_point){
			if($items['pid'] == $parent && in_array($items['id'], $access_point, true)){
				return $items;
			}
		}));
		if(!empty($result)){
			switch ($level) {
				case 1:
					$class = ' nav-second-level';
					break;
				case 2:
					$class = ' nav-third-level';
					break;
				case 3:
					$class = ' nav-fourth-level';
					break;				
				default:
					$class = '';
					break;
			}
			uasort($result, function($a, $b){
				if($a['urutan'] == $b['urutan']){
					return 0;
				}
				return ($a['urutan'] < $b['urutan']) ? -1 : 1;
			});

			$id = '';
			if($parent == 0){
				$id = ' id="side-menu"';
			}
			$tag .= '<ul class="nav'.$class.'"'.$id.'>';
			array_walk($result, function($items) use($menu, $level, $parent, &$tag, $access_point){
				if($items['pid'] == $parent && in_array($items['id'], $access_point, true)){
					$child = default_sidebar_tree($menu, $items['id'], $access_point, $level + 1);

					$link = '#';
					if(!empty($items['controllers'])){
						$link = site_url(BASE_PANEL.$items['slug']);						
					}
					$arrow = '<span class="fa arrow"></span>';
					if(empty($child)){
						// $link = site_url(BASE_PANEL.$items['slug']);						
						$arrow = '';
					}
					// $active = in_array(seo_url($items['label']), $this->uri->segment_array()) == true || empty($items['slug'])? ' class="active"' : '';
					$active = '';
					$tag .= '<li'.$active.'>';
						$tag .= '<a href="'.$link.'">';
							$tag .= '<i class="fa fa-'.$items['icon'].'"></i> ';
							$tag .= ucwords($items['label']);
							$tag .= $arrow;
						$tag .= '</a>';
						$tag .= $child;							
					$tag .= '</li>';
				}
			});
			$tag .= '</ul>';
		}
		return $tag;
}

function gentelella_sidebar_tree($menu, $parent = 0, $access_point, $level = 0){
		$tag = '';
		$result  	= array_values(array_filter($menu, function($items) use($parent, $access_point){
			if($items['pid'] == $parent && in_array($items['id'], $access_point, true)){
				return $items;
			}
		}));
		if(!empty($result)){
			$class = ($level > 0) ? 'child_menu' : 'side-menu';
			uasort($result, function($a, $b){
				if($a['urutan'] == $b['urutan']){
					return 0;
				}
				return ($a['urutan'] < $b['urutan']) ? -1 : 1;
			});

			$id = '';
			$tag .= '<ul class="nav '.$class.'">';
			array_walk($result, function($items) use($menu, $level, $parent, &$tag, $access_point){
				if($items['pid'] == $parent && in_array($items['id'], $access_point, true)){
					$child = gentelella_sidebar_tree($menu, $items['id'], $access_point, $level + 1);

					$link = '';
					if(empty($child)){
						$link = 'href="'.site_url(BASE_PANEL.$items['slug']).'"';						
					}
					$arrow = '<span class="fa fa-chevron-down"></span>';
					if(empty($child)){
						$arrow = '';
					}
					// $active = in_array(seo_url($items['label']), $this->uri->segment_array()) == true || empty($items['slug'])? ' class="active"' : '';
					$active = '';
					$tag .= '<li'.$active.'>';
						$tag .= '<a '.$link.'>';
							$hidexs = ($level > 0) ? ' hidden-xs' : '';
							$tag .= '<i class="fa fa-'.$items['icon'].$hidexs.'"></i>';
							$tag .= ucwords($items['label']);
							$tag .= $arrow;
						$tag .= '</a>';
						$tag .= $child;							
					$tag .= '</li>';
				}
			});
			$tag .= '</ul>';
		}
		return $tag;	
}
