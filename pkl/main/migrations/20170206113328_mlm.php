<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Mlm extends CI_Migration {
		public function up(){
				/*
				************************************************************
				* Add table bahasa
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'idbahasa' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'key_bahasa' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'indonesia' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'english' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'is_global' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'default' => '1',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('idbahasa', TRUE);
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (2,'kategori','Kategori','Category',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (3,'filter','Saring','Filter',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (4,'search','Cari','Search',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (5,'sort','Urutkan','Sort',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (7,'welcome','Selamat datang','Welcome',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (8,'guest','Pengunjung','Guest',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (10,'browse','Telusur','Browse',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (15,'detail','Rincian','Detail',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (25,'description','Deskripsi','Description',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (28,'quality','Kualitas','Quality',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (29,'accuracy','Akurasi','Accuration',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (30,'report','Lapor','Report',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (33,'load_more','Tampilkan lebih banyak','Load more',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (34,'here','Disini','Here',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (35,'sign_in','Masuk','Sign in',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (36,'rememberme','Ingat saya','Remember me',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (37,'or','Atau','Or',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (38,'login_with','Masuk dengan','Login with',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (39,'doesnt_have_account','Tidak punya akun','Doesn\\\'t have account',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (40,'username','Nama user','Username',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (41,'password','Kata kunci','Password',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (42,'not_match','Nama user dan kata kunci tidak sama','Username and password not matches',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (43,'help','Bantuan','Help',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (44,'register','Daftar','Register',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (45,'profile','Profil','Profile',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (46,'sign_out','Keluar','Logout',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (47,'fullname','Nama lengkap','Fullname',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (48,'retype','Ketik ulang','Retype',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (49,'form','Formulir','form',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (50,'group','Nama grup pengguna','User group name',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (51,'edit','Ubah','Edit',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (52,'delete_success','Berhasil dihapus','Has been deleted',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (53,'delete_failed','Gagal dihapus','Delete failed',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (54,'insert','Tambah','Add',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (55,'delete','Hapus','Delete',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (56,'access_control','Kontrol akses','Access control',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (57,'translation','Terjemahan','Translation',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (58,'key_lang','Kunci bahasa','Language key',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (59,'download','Unduh','Download',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (60,'upload','Unggah','upload',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (61,'order_numb','Nomor urut','Order number',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (62,'hide','Sembunyi','Hide',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (63,'show','Tampil','Show',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (64,'truncate','Bersihkan','Truncate',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (65,'name','Nama','Name',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (66,'phone_code','Kode telepon','Phone code',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (67,'province','Provinsi','District',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (68,'country','Negara','Country',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (71,'city','Kota','City',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (72,'error_valid_ux','Halaman tempat sumber ini akan diinstal (login/admin/public), pisahkan dengan tanda koma.','the page of this resources will be installed (login/admin/public), separate with comma',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (73,'error_valid_path','dokumen root menuju sumber daya ini berikut ekstensi berkas','document root path with file extentions',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (74,'reason','Alasan','Reason',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (75,'nickname','Panggilan','Nickname',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (76,'birthdate','Tanggal lahir','Birthdate',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (77,'gender','Jenis kelamin','Gender',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (78,'address','Alamat','Address',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (79,'postcode','Kode pos','Postal Code',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (80,'male','Laki - laki','Male',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (81,'female','Perempuan','Female',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (83,'announce','Pengumuman','Announcement',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (84,'title','Judul','Title',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (85,'content','Isi','Content',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (86,'creator','Pembuat','Creator',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (87,'priority','Prioritas','Priority',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (88,'expire','Kadaluarsa','Expire',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (89,'login_match','Username dan password tidak sama','Username and password not matches',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (90,'have_account','Sudah mempunyai akun','Have account',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (91,'date','Tanggal','Date',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (92,'month','Bulan','Month',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (93,'year','Tahun','Year',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (94,'activate','Aktivasi','Activate',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (95,'contact_support','Hubungi CS','Contact customer support',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (96,'register_success','Pendaftaran berhasil, silahkan cek kotak surat pada email anda dan lakukan aktivasi segera mungkin','Registration succeed, please check you mailbox and activate your account immediately',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (97,'login_required','Maaf, anda harus login dahulu','Sorry, you must logged in first',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (98,'quantity','Jumlah','Quantity',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (99,'notes','Catatan','Notes',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (102,'submitted','Terima kasih atas timbal balik yang anda berikan','Thank you for your feed back',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (103,'error_submitted','Terjadi kesalahan pemrosesan data','Something error occur on data processing',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (110,'calculate','Hitung','Calculate',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (114,'phone_number','Nomor telepon','Phone number',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (117,'proceed','Proses','Proceed',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (121,'method','Metode','Method',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (122,'days','Harian','Days',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (124,'shipping','Pengiriman','Shipping',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (125,'payout','Pembayaran','Pembayaran',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (126,'tracking','Pelacakan','Tracking',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (127,'help','Bantuan','Help',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (130,'pending','Tertunda','Pending',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (134,'success','Sukses','Success',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (139,'required','harus terisi','are required',1)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (141,'abort','Batalkan','Abort',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (142,'confirm','Konfirmasi','Confirmation',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (154,'cache_clears','Cache telah dikosongkan','Cache has been clear',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (156,'print','Cetak','Print',0)");
				$this->dbforge->add_field("INSERT INTO `bahasa`(idbahasa, key_bahasa, indonesia, english, is_global) VALUES (168,'visitor','Pengunjung','Visitor',0)");
				
				$this->dbforge->create_table('bahasa');

				/*
				************************************************************
				* Add table bank
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'name' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						),
				
						'bankcode' => array(
						'type' => 'VARCHAR',
						'constraint' => '3',
						'null' => TRUE,
						),
				
						'accountno' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'accountname' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						),
				
						'accountarea' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						),
				
						'status' => array(
						'type' => 'CHAR',
						'constraint' => '1',
						),
				
						'created' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'createdby' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'updated' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'updatedby' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (1,'BANK ARTHA GRAHA','','','','','N','2009-04-03 09:21:33','Admin','2012-08-29 17:54:03','admin')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (2,'BANK BNI 46',NULL,1111111111,'PT. SINTESA DUTA SEJAHTERA','JAKARTA','Y','2009-05-06 12:14:30','Admin','2013-05-31 18:10:07','GENSEI01')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (3,'BANK BUKOPIN',NULL,'','','','N','2009-08-13 13:43:59','Admin','2012-08-29 17:45:53','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (4,'BANK COMMONWEALTH',NULL,'','','','N','2009-08-15 12:40:09','Admin','2012-08-29 17:45:19','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (5,'BANK CIMB NIAGA',NULL,'','','','N','2009-08-13 13:45:09','Admin','2012-08-29 17:44:44','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (6,'BANK DANAMON INDONESIA',NULL,'','','','N','2009-08-13 13:45:14','Admin','2012-08-29 17:44:19','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (7,'BANK DKI',NULL,'','','','N','2009-08-13 13:45:35','Admin','2012-08-29 17:43:54','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (8,'BANK EKONOMI',NULL,'','','','N','2009-08-13 13:45:50','Admin','2012-08-29 17:43:28','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (9,'BANK INTERNATIONAL INDONESIA',NULL,'','','','N','2009-08-13 13:46:14','Admin','2012-08-29 17:42:55','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (10,'BANK MANDIRI',NULL,123456789,'PT. SINTESA DUTA SEJAHTERA','CABANG JAKARTA','Y','2009-08-13 13:47:01','Admin','2013-05-31 18:08:54','GENSEI01')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (11,'BANK MASPION',NULL,'','','','N','2009-08-13 13:47:31','Admin','2012-08-29 17:41:45','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (12,'BANK MAYORA',NULL,'','','','N','2009-08-13 13:47:48','Admin','2012-08-29 17:38:46','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (13,'BANK MEGA',NULL,'','','','N','2009-08-13 13:48:13','Admin','2012-08-29 17:38:09','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (14,'BANK PANIN',NULL,'','','','N','2009-08-13 13:48:48','Admin','2012-08-29 17:37:45','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (15,'BANK PERMATA',NULL,'','','','N','2009-08-13 13:49:26','Admin','2012-08-29 17:37:09','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (16,'BANK RAKYAT INDONESIA (BRI)',NULL,123456789,'PT. SINTESA DUTA SEJAHTERA','JAKARTA','Y','2009-08-18 13:20:31','Admin','2013-05-31 18:05:19','GENSEI01')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (17,'BANK TABUNGAN NEGARA',NULL,'','','','N','2009-08-18 13:20:51','Admin','2012-08-29 17:50:24','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (18,'BANK WINDU KENTJANA',NULL,'','','','N','2012-08-29 17:48:27','TT00','2012-08-29 17:51:32','TT00')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (19,'BCA',NULL,123456789,'PT. SINTESA DUTA SEJAHTERA','JAKARTA','Y','2012-08-29 17:51:09','TT00','2013-05-31 18:08:32','GENSEI01')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (20,'CITIBANK',NULL,'','','','N','2012-08-29 17:52:16','TT00','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (21,'OCBC NISP',NULL,'','','','N','2012-08-29 17:53:26','TT00','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (22,'RABOBANK',NULL,'','','','N','2012-08-29 17:55:30','TT00','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (23,'UOB BUANA',NULL,'','','','N','2012-08-29 17:56:08','TT00','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `bank`(id, name, bankcode, accountno, accountname, accountarea, status, created, createdby, updated, updatedby) VALUES (24,'BANK MESTIKA',NULL,'','','','N','2013-01-22 12:06:26','TRIXTEN','2013-01-22 13:15:48','LISDA')");
				
				$this->dbforge->create_table('bank');

				/*
				************************************************************
				* Add table banner
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'title' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'subtitle' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'description' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'image' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'label' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'is_publish' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'default' => '1',
						'null' => TRUE,
						),
				
						'expdate' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'created_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'updated_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'deleted_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `banner`(id, title, subtitle, description, image, label, is_publish, expdate, created_at, updated_at, deleted_at) VALUES (1,'viusid','','Nutrisi yang diaktivasi secara molekuler, yang berguna melawan virus penyakit dan meningkatkan sistem imun tubuh.','banner/9a57b9a34ed1a14a9e8594732d9a9f99.jpg','',1,'2017-02-27','2017-01-27 10:35:35','2017-01-27 10:35:35',NULL)");
				
				$this->dbforge->create_table('banner');

				/*
				************************************************************
				* Add table city
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'city_id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'city_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'province' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'postal_code' => array(
						'type' => 'VARCHAR',
						'constraint' => '5',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('city_id', TRUE);
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (1,'Kab. Aceh Barat',21,23681)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (2,'Kab. Aceh Barat Daya',21,23764)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (3,'Kab. Aceh Besar',21,23951)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (4,'Kab. Aceh Jaya',21,23654)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (5,'Kab. Aceh Selatan',21,23719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (6,'Kab. Aceh Singkil',21,24785)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (7,'Kab. Aceh Tamiang',21,24476)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (8,'Kab. Aceh Tengah',21,24511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (9,'Kab. Aceh Tenggara',21,24611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (10,'Kab. Aceh Timur',21,24454)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (11,'Kab. Aceh Utara',21,24382)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (12,'Kab. Agam',32,26411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (13,'Kab. Alor',23,85811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (14,'Kota Ambon',19,97222)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (15,'Kab. Asahan',34,21214)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (16,'Kab. Asmat',24,99777)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (17,'Kab. Badung',1,80351)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (18,'Kab. Balangan',13,71611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (19,'Kota Balikpapan',15,76111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (20,'Kota Banda Aceh',21,23238)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (21,'Kota Bandar Lampung',18,35139)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (22,'Kab. Bandung',9,40311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (23,'Kota Bandung',9,40115)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (24,'Kab. Bandung Barat',9,40721)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (25,'Kab. Banggai',29,94711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (26,'Kab. Banggai Kepulauan',29,94881)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (27,'Kab. Bangka',2,33212)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (28,'Kab. Bangka Barat',2,33315)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (29,'Kab. Bangka Selatan',2,33719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (30,'Kab. Bangka Tengah',2,33613)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (31,'Kab. Bangkalan',11,69118)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (32,'Kab. Bangli',1,80619)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (33,'Kab. Banjar',13,70619)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (34,'Kota Banjar',9,46311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (35,'Kota Banjarbaru',13,70712)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (36,'Kota Banjarmasin',13,70117)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (37,'Kab. Banjarnegara',10,53419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (38,'Kab. Bantaeng',28,92411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (39,'Kab. Bantul',5,55715)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (40,'Kab. Banyuasin',33,30911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (41,'Kab. Banyumas',10,53114)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (42,'Kab. Banyuwangi',11,68416)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (43,'Kab. Barito Kuala',13,70511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (44,'Kab. Barito Selatan',14,73711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (45,'Kab. Barito Timur',14,73671)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (46,'Kab. Barito Utara',14,73881)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (47,'Kab. Barru',28,90719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (48,'Kota Batam',17,29413)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (49,'Kab. Batang',10,51211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (50,'Kab. Batang Hari',8,36613)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (51,'Kota Batu',11,65311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (52,'Kab. Batu Bara',34,21655)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (53,'Kota Bau-Bau',30,93719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (54,'Kab. Bekasi',9,17837)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (55,'Kota Bekasi',9,17121)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (56,'Kab. Belitung',2,33419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (57,'Kab. Belitung Timur',2,33519)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (58,'Kab. Belu',23,85711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (59,'Kab. Bener Meriah',21,24581)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (60,'Kab. Bengkalis',26,28719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (61,'Kab. Bengkayang',12,79213)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (62,'Kota Bengkulu',4,38229)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (63,'Kab. Bengkulu Selatan',4,38519)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (64,'Kab. Bengkulu Tengah',4,38319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (65,'Kab. Bengkulu Utara',4,38619)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (66,'Kab. Berau',15,77311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (67,'Kab. Biak Numfor',24,98119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (68,'Kab. Bima',22,84171)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (69,'Kota Bima',22,84139)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (70,'Kota Binjai',34,20712)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (71,'Kab. Bintan',17,29135)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (72,'Kab. Bireuen',21,24219)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (73,'Kota Bitung',31,95512)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (74,'Kab. Blitar',11,66171)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (75,'Kota Blitar',11,66124)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (76,'Kab. Blora',10,58219)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (77,'Kab. Boalemo',7,96319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (78,'Kab. Bogor',9,16911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (79,'Kota Bogor',9,16119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (80,'Kab. Bojonegoro',11,62119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (81,'Kab. Bolaang Mongondow (Bolmong)',31,95755)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (82,'Kab. Bolaang Mongondow Selatan',31,95774)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (83,'Kab. Bolaang Mongondow Timur',31,95783)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (84,'Kab. Bolaang Mongondow Utara',31,95765)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (85,'Kab. Bombana',30,93771)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (86,'Kab. Bondowoso',11,68219)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (87,'Kab. Bone',28,92713)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (88,'Kab. Bone Bolango',7,96511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (89,'Kota Bontang',15,75313)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (90,'Kab. Boven Digoel',24,99662)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (91,'Kab. Boyolali',10,57312)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (92,'Kab. Brebes',10,52212)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (93,'Kota Bukittinggi',32,26115)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (94,'Kab. Buleleng',1,81111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (95,'Kab. Bulukumba',28,92511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (96,'Kab. Bulungan (Bulongan)',16,77211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (97,'Kab. Bungo',8,37216)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (98,'Kab. Buol',29,94564)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (99,'Kab. Buru',19,97371)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (100,'Kab. Buru Selatan',19,97351)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (101,'Kab. Buton',30,93754)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (102,'Kab. Buton Utara',30,93745)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (103,'Kab. Ciamis',9,46211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (104,'Kab. Cianjur',9,43217)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (105,'Kab. Cilacap',10,53211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (106,'Kota Cilegon',3,42417)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (107,'Kota Cimahi',9,40512)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (108,'Kab. Cirebon',9,45611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (109,'Kota Cirebon',9,45116)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (110,'Kab. Dairi',34,22211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (111,'Kab. Deiyai (Deliyai)',24,98784)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (112,'Kab. Deli Serdang',34,20511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (113,'Kab. Demak',10,59519)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (114,'Kota Denpasar',1,80227)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (115,'Kota Depok',9,16416)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (116,'Kab. Dharmasraya',32,27612)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (117,'Kab. Dogiyai',24,98866)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (118,'Kab. Dompu',22,84217)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (119,'Kab. Donggala',29,94341)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (120,'Kota Dumai',26,28811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (121,'Kab. Empat Lawang',33,31811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (122,'Kab. Ende',23,86351)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (123,'Kab. Enrekang',28,91719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (124,'Kab. Fakfak',25,98651)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (125,'Kab. Flores Timur',23,86213)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (126,'Kab. Garut',9,44126)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (127,'Kab. Gayo Lues',21,24653)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (128,'Kab. Gianyar',1,80519)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (129,'Kab. Gorontalo',7,96218)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (130,'Kota Gorontalo',7,96115)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (131,'Kab. Gorontalo Utara',7,96611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (132,'Kab. Gowa',28,92111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (133,'Kab. Gresik',11,61115)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (134,'Kab. Grobogan',10,58111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (135,'Kab. Gunung Kidul',5,55812)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (136,'Kab. Gunung Mas',14,74511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (137,'Kota Gunungsitoli',34,22813)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (138,'Kab. Halmahera Barat',20,97757)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (139,'Kab. Halmahera Selatan',20,97911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (140,'Kab. Halmahera Tengah',20,97853)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (141,'Kab. Halmahera Timur',20,97862)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (142,'Kab. Halmahera Utara',20,97762)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (143,'Kab. Hulu Sungai Selatan',13,71212)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (144,'Kab. Hulu Sungai Tengah',13,71313)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (145,'Kab. Hulu Sungai Utara',13,71419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (146,'Kab. Humbang Hasundutan',34,22457)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (147,'Kab. Indragiri Hilir',26,29212)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (148,'Kab. Indragiri Hulu',26,29319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (149,'Kab. Indramayu',9,45214)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (150,'Kab. Intan Jaya',24,98771)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (151,'Kota Jakarta Barat',6,11220)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (152,'Kota Jakarta Pusat',6,10540)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (153,'Kota Jakarta Selatan',6,12230)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (154,'Kota Jakarta Timur',6,13330)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (155,'Kota Jakarta Utara',6,14140)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (156,'Kota Jambi',8,36111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (157,'Kab. Jayapura',24,99352)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (158,'Kota Jayapura',24,99114)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (159,'Kab. Jayawijaya',24,99511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (160,'Kab. Jember',11,68113)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (161,'Kab. Jembrana',1,82251)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (162,'Kab. Jeneponto',28,92319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (163,'Kab. Jepara',10,59419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (164,'Kab. Jombang',11,61415)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (165,'Kab. Kaimana',25,98671)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (166,'Kab. Kampar',26,28411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (167,'Kab. Kapuas',14,73583)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (168,'Kab. Kapuas Hulu',12,78719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (169,'Kab. Karanganyar',10,57718)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (170,'Kab. Karangasem',1,80819)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (171,'Kab. Karawang',9,41311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (172,'Kab. Karimun',17,29611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (173,'Kab. Karo',34,22119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (174,'Kab. Katingan',14,74411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (175,'Kab. Kaur',4,38911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (176,'Kab. Kayong Utara',12,78852)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (177,'Kab. Kebumen',10,54319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (178,'Kab. Kediri',11,64184)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (179,'Kota Kediri',11,64125)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (180,'Kab. Keerom',24,99461)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (181,'Kab. Kendal',10,51314)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (182,'Kota Kendari',30,93126)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (183,'Kab. Kepahiang',4,39319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (184,'Kab. Kepulauan Anambas',17,29991)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (185,'Kab. Kepulauan Aru',19,97681)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (186,'Kab. Kepulauan Mentawai',32,25771)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (187,'Kab. Kepulauan Meranti',26,28791)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (188,'Kab. Kepulauan Sangihe',31,95819)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (189,'Kab. Kepulauan Seribu',6,14550)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (190,'Kab. Kepulauan Siau Tagulandang Biaro (Sitaro)',31,95862)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (191,'Kab. Kepulauan Sula',20,97995)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (192,'Kab. Kepulauan Talaud',31,95885)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (193,'Kab. Kepulauan Yapen (Yapen Waropen)',24,98211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (194,'Kab. Kerinci',8,37167)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (195,'Kab. Ketapang',12,78874)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (196,'Kab. Klaten',10,57411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (197,'Kab. Klungkung',1,80719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (198,'Kab. Kolaka',30,93511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (199,'Kab. Kolaka Utara',30,93911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (200,'Kab. Konawe',30,93411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (201,'Kab. Konawe Selatan',30,93811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (202,'Kab. Konawe Utara',30,93311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (203,'Kab. Kotabaru',13,72119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (204,'Kota Kotamobagu',31,95711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (205,'Kab. Kotawaringin Barat',14,74119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (206,'Kab. Kotawaringin Timur',14,74364)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (207,'Kab. Kuantan Singingi',26,29519)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (208,'Kab. Kubu Raya',12,78311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (209,'Kab. Kudus',10,59311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (210,'Kab. Kulon Progo',5,55611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (211,'Kab. Kuningan',9,45511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (212,'Kab. Kupang',23,85362)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (213,'Kota Kupang',23,85119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (214,'Kab. Kutai Barat',15,75711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (215,'Kab. Kutai Kartanegara',15,75511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (216,'Kab. Kutai Timur',15,75611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (217,'Kab. Labuhan Batu',34,21412)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (218,'Kab. Labuhan Batu Selatan',34,21511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (219,'Kab. Labuhan Batu Utara',34,21711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (220,'Kab. Lahat',33,31419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (221,'Kab. Lamandau',14,74611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (222,'Kab. Lamongan',11,64125)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (223,'Kab. Lampung Barat',18,34814)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (224,'Kab. Lampung Selatan',18,35511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (225,'Kab. Lampung Tengah',18,34212)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (226,'Kab. Lampung Timur',18,34319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (227,'Kab. Lampung Utara',18,34516)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (228,'Kab. Landak',12,78319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (229,'Kab. Langkat',34,20811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (230,'Kota Langsa',21,24412)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (231,'Kab. Lanny Jaya',24,99531)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (232,'Kab. Lebak',3,42319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (233,'Kab. Lebong',4,39264)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (234,'Kab. Lembata',23,86611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (235,'Kota Lhokseumawe',21,24352)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (236,'Kab. Lima Puluh Koto/Kota',32,26671)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (237,'Kab. Lingga',17,29811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (238,'Kab. Lombok Barat',22,83311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (239,'Kab. Lombok Tengah',22,83511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (240,'Kab. Lombok Timur',22,83612)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (241,'Kab. Lombok Utara',22,83711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (242,'Kota Lubuk Linggau',33,31614)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (243,'Kab. Lumajang',11,67319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (244,'Kab. Luwu',28,91994)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (245,'Kab. Luwu Timur',28,92981)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (246,'Kab. Luwu Utara',28,92911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (247,'Kab. Madiun',11,63153)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (248,'Kota Madiun',11,63122)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (249,'Kab. Magelang',10,56519)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (250,'Kota Magelang',10,56133)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (251,'Kab. Magetan',11,63314)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (252,'Kab. Majalengka',9,45412)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (253,'Kab. Majene',27,91411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (254,'Kota Makassar',28,90111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (255,'Kab. Malang',11,65163)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (256,'Kota Malang',11,65112)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (257,'Kab. Malinau',16,77511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (258,'Kab. Maluku Barat Daya',19,97451)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (259,'Kab. Maluku Tengah',19,97513)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (260,'Kab. Maluku Tenggara',19,97651)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (261,'Kab. Maluku Tenggara Barat',19,97465)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (262,'Kab. Mamasa',27,91362)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (263,'Kab. Mamberamo Raya',24,99381)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (264,'Kab. Mamberamo Tengah',24,99553)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (265,'Kab. Mamuju',27,91519)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (266,'Kab. Mamuju Utara',27,91571)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (267,'Kota Manado',31,95247)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (268,'Kab. Mandailing Natal',34,22916)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (269,'Kab. Manggarai',23,86551)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (270,'Kab. Manggarai Barat',23,86711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (271,'Kab. Manggarai Timur',23,86811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (272,'Kab. Manokwari',25,98311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (273,'Kab. Manokwari Selatan',25,98355)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (274,'Kab. Mappi',24,99853)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (275,'Kab. Maros',28,90511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (276,'Kota Mataram',22,83131)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (277,'Kab. Maybrat',25,98051)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (278,'Kota Medan',34,20228)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (279,'Kab. Melawi',12,78619)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (280,'Kab. Merangin',8,37319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (281,'Kab. Merauke',24,99613)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (282,'Kab. Mesuji',18,34911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (283,'Kota Metro',18,34111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (284,'Kab. Mimika',24,99962)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (285,'Kab. Minahasa',31,95614)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (286,'Kab. Minahasa Selatan',31,95914)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (287,'Kab. Minahasa Tenggara',31,95995)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (288,'Kab. Minahasa Utara',31,95316)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (289,'Kab. Mojokerto',11,61382)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (290,'Kota Mojokerto',11,61316)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (291,'Kab. Morowali',29,94911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (292,'Kab. Muara Enim',33,31315)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (293,'Kab. Muaro Jambi',8,36311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (294,'Kab. Muko Muko',4,38715)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (295,'Kab. Muna',30,93611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (296,'Kab. Murung Raya',14,73911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (297,'Kab. Musi Banyuasin',33,30719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (298,'Kab. Musi Rawas',33,31661)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (299,'Kab. Nabire',24,98816)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (300,'Kab. Nagan Raya',21,23674)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (301,'Kab. Nagekeo',23,86911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (302,'Kab. Natuna',17,29711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (303,'Kab. Nduga',24,99541)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (304,'Kab. Ngada',23,86413)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (305,'Kab. Nganjuk',11,64414)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (306,'Kab. Ngawi',11,63219)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (307,'Kab. Nias',34,22876)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (308,'Kab. Nias Barat',34,22895)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (309,'Kab. Nias Selatan',34,22865)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (310,'Kab. Nias Utara',34,22856)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (311,'Kab. Nunukan',16,77421)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (312,'Kab. Ogan Ilir',33,30811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (313,'Kab. Ogan Komering Ilir',33,30618)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (314,'Kab. Ogan Komering Ulu',33,32112)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (315,'Kab. Ogan Komering Ulu Selatan',33,32211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (316,'Kab. Ogan Komering Ulu Timur',33,32312)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (317,'Kab. Pacitan',11,63512)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (318,'Kota Padang',32,25112)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (319,'Kab. Padang Lawas',34,22763)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (320,'Kab. Padang Lawas Utara',34,22753)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (321,'Kota Padang Panjang',32,27122)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (322,'Kab. Padang Pariaman',32,25583)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (323,'Kota Padang Sidempuan',34,22727)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (324,'Kota Pagar Alam',33,31512)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (325,'Kab. Pakpak Bharat',34,22272)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (326,'Kota Palangka Raya',14,73112)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (327,'Kota Palembang',33,31512)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (328,'Kota Palopo',28,91911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (329,'Kota Palu',29,94111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (330,'Kab. Pamekasan',11,69319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (331,'Kab. Pandeglang',3,42212)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (332,'Kab. Pangandaran',9,46511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (333,'Kab. Pangkajene Kepulauan',28,90611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (334,'Kota Pangkal Pinang',2,33115)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (335,'Kab. Paniai',24,98765)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (336,'Kota Parepare',28,91123)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (337,'Kota Pariaman',32,25511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (338,'Kab. Parigi Moutong',29,94411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (339,'Kab. Pasaman',32,26318)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (340,'Kab. Pasaman Barat',32,26511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (341,'Kab. Paser',15,76211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (342,'Kab. Pasuruan',11,67153)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (343,'Kota Pasuruan',11,67118)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (344,'Kab. Pati',10,59114)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (345,'Kota Payakumbuh',32,26213)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (346,'Kab. Pegunungan Arfak',25,98354)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (347,'Kab. Pegunungan Bintang',24,99573)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (348,'Kab. Pekalongan',10,51161)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (349,'Kota Pekalongan',10,51122)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (350,'Kota Pekanbaru',26,28112)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (351,'Kab. Pelalawan',26,28311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (352,'Kab. Pemalang',10,52319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (353,'Kota Pematang Siantar',34,21126)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (354,'Kab. Penajam Paser Utara',15,76311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (355,'Kab. Pesawaran',18,35312)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (356,'Kab. Pesisir Barat',18,35974)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (357,'Kab. Pesisir Selatan',32,25611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (358,'Kab. Pidie',21,24116)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (359,'Kab. Pidie Jaya',21,24186)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (360,'Kab. Pinrang',28,91251)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (361,'Kab. Pohuwato',7,96419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (362,'Kab. Polewali Mandar',27,91311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (363,'Kab. Ponorogo',11,63411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (364,'Kab. Pontianak',12,78971)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (365,'Kota Pontianak',12,78112)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (366,'Kab. Poso',29,94615)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (367,'Kota Prabumulih',33,31121)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (368,'Kab. Pringsewu',18,35719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (369,'Kab. Probolinggo',11,67282)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (370,'Kota Probolinggo',11,67215)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (371,'Kab. Pulang Pisau',14,74811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (372,'Kab. Pulau Morotai',20,97771)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (373,'Kab. Puncak',24,98981)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (374,'Kab. Puncak Jaya',24,98979)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (375,'Kab. Purbalingga',10,53312)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (376,'Kab. Purwakarta',9,41119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (377,'Kab. Purworejo',10,54111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (378,'Kab. Raja Ampat',25,98489)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (379,'Kab. Rejang Lebong',4,39112)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (380,'Kab. Rembang',10,59219)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (381,'Kab. Rokan Hilir',26,28992)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (382,'Kab. Rokan Hulu',26,28511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (383,'Kab. Rote Ndao',23,85982)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (384,'Kota Sabang',21,23512)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (385,'Kab. Sabu Raijua',23,85391)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (386,'Kota Salatiga',10,50711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (387,'Kota Samarinda',15,75133)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (388,'Kab. Sambas',12,79453)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (389,'Kab. Samosir',34,22392)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (390,'Kab. Sampang',11,69219)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (391,'Kab. Sanggau',12,78557)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (392,'Kab. Sarmi',24,99373)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (393,'Kab. Sarolangun',8,37419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (394,'Kota Sawah Lunto',32,27416)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (395,'Kab. Sekadau',12,79583)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (396,'Kab. Selayar (Kepulauan Selayar)',28,92812)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (397,'Kab. Seluma',4,38811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (398,'Kab. Semarang',10,50511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (399,'Kota Semarang',10,50135)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (400,'Kab. Seram Bagian Barat',19,97561)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (401,'Kab. Seram Bagian Timur',19,97581)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (402,'Kab. Serang',3,42182)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (403,'Kota Serang',3,42111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (404,'Kab. Serdang Bedagai',34,20915)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (405,'Kab. Seruyan',14,74211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (406,'Kab. Siak',26,28623)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (407,'Kota Sibolga',34,22522)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (408,'Kab. Sidenreng Rappang/Rapang',28,91613)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (409,'Kab. Sidoarjo',11,61219)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (410,'Kab. Sigi',29,94364)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (411,'Kab. Sijunjung (Sawah Lunto Sijunjung)',32,27511)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (412,'Kab. Sikka',23,86121)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (413,'Kab. Simalungun',34,21162)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (414,'Kab. Simeulue',21,23891)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (415,'Kota Singkawang',12,79117)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (416,'Kab. Sinjai',28,92615)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (417,'Kab. Sintang',12,78619)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (418,'Kab. Situbondo',11,68316)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (419,'Kab. Sleman',5,55513)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (420,'Kab. Solok',32,27365)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (421,'Kota Solok',32,27315)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (422,'Kab. Solok Selatan',32,27779)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (423,'Kab. Soppeng',28,90812)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (424,'Kab. Sorong',25,98431)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (425,'Kota Sorong',25,98411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (426,'Kab. Sorong Selatan',25,98454)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (427,'Kab. Sragen',10,57211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (428,'Kab. Subang',9,41215)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (429,'Kota Subulussalam',21,24882)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (430,'Kab. Sukabumi',9,43311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (431,'Kota Sukabumi',9,43114)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (432,'Kab. Sukamara',14,74712)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (433,'Kab. Sukoharjo',10,57514)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (434,'Kab. Sumba Barat',23,87219)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (435,'Kab. Sumba Barat Daya',23,87453)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (436,'Kab. Sumba Tengah',23,87358)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (437,'Kab. Sumba Timur',23,87112)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (438,'Kab. Sumbawa',22,84315)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (439,'Kab. Sumbawa Barat',22,84419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (440,'Kab. Sumedang',9,45326)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (441,'Kab. Sumenep',11,69413)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (442,'Kota Sungaipenuh',8,37113)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (443,'Kab. Supiori',24,98164)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (444,'Kota Surabaya',11,60119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (445,'Kota Surakarta (Solo)',10,57113)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (446,'Kab. Tabalong',13,71513)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (447,'Kab. Tabanan',1,82119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (448,'Kab. Takalar',28,92212)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (449,'Kab. Tambrauw',25,98475)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (450,'Kab. Tana Tidung',16,77611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (451,'Kab. Tana Toraja',28,91819)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (452,'Kab. Tanah Bumbu',13,72211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (453,'Kab. Tanah Datar',32,27211)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (454,'Kab. Tanah Laut',13,70811)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (455,'Kab. Tangerang',3,15914)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (456,'Kota Tangerang',3,15111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (457,'Kota Tangerang Selatan',3,15332)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (458,'Kab. Tanggamus',18,35619)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (459,'Kota Tanjung Balai',34,21321)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (460,'Kab. Tanjung Jabung Barat',8,36513)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (461,'Kab. Tanjung Jabung Timur',8,36719)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (462,'Kota Tanjung Pinang',17,29111)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (463,'Kab. Tapanuli Selatan',34,22742)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (464,'Kab. Tapanuli Tengah',34,22611)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (465,'Kab. Tapanuli Utara',34,22414)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (466,'Kab. Tapin',13,71119)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (467,'Kota Tarakan',16,77114)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (468,'Kab. Tasikmalaya',9,46411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (469,'Kota Tasikmalaya',9,46116)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (470,'Kota Tebing Tinggi',34,20632)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (471,'Kab. Tebo',8,37519)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (472,'Kab. Tegal',10,52419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (473,'Kota Tegal',10,52114)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (474,'Kab. Teluk Bintuni',25,98551)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (475,'Kab. Teluk Wondama',25,98591)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (476,'Kab. Temanggung',10,56212)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (477,'Kota Ternate',20,97714)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (478,'Kota Tidore Kepulauan',20,97815)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (479,'Kab. Timor Tengah Selatan',23,85562)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (480,'Kab. Timor Tengah Utara',23,85612)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (481,'Kab. Toba Samosir',34,22316)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (482,'Kab. Tojo Una-Una',29,94683)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (483,'Kab. Toli-Toli',29,94542)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (484,'Kab. Tolikara',24,99411)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (485,'Kota Tomohon',31,95416)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (486,'Kab. Toraja Utara',28,91831)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (487,'Kab. Trenggalek',11,66312)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (488,'Kota Tual',19,97612)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (489,'Kab. Tuban',11,62319)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (490,'Kab. Tulang Bawang',18,34613)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (491,'Kab. Tulang Bawang Barat',18,34419)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (492,'Kab. Tulungagung',11,66212)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (493,'Kab. Wajo',28,90911)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (494,'Kab. Wakatobi',30,93791)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (495,'Kab. Waropen',24,98269)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (496,'Kab. Way Kanan',18,34711)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (497,'Kab. Wonogiri',10,57619)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (498,'Kab. Wonosobo',10,56311)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (499,'Kab. Yahukimo',24,99041)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (500,'Kab. Yalimo',24,99481)");
				$this->dbforge->add_field("INSERT INTO `city`(city_id, city_name, province, postal_code) VALUES (501,'Kota Yogyakarta',5,55222)");
				
				$this->dbforge->create_table('city');

				/*
				************************************************************
				* Add table config_groups
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'cgid' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'groupname' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'groupicon' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'is_active' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('cgid', TRUE);
				$this->dbforge->add_field("INSERT INTO `config_groups`(cgid, groupname, groupicon, is_active) VALUES (1,'information','info-circle',1)");
				$this->dbforge->add_field("INSERT INTO `config_groups`(cgid, groupname, groupicon, is_active) VALUES (2,'preference','puzzle-piece',1)");
				$this->dbforge->add_field("INSERT INTO `config_groups`(cgid, groupname, groupicon, is_active) VALUES (3,'mail','envelope',1)");
				$this->dbforge->add_field("INSERT INTO `config_groups`(cgid, groupname, groupicon, is_active) VALUES (4,'sosial media','link',1)");
				$this->dbforge->add_field("INSERT INTO `config_groups`(cgid, groupname, groupicon, is_active) VALUES (5,'web config','desktop',1)");
				$this->dbforge->add_field("INSERT INTO `config_groups`(cgid, groupname, groupicon, is_active) VALUES (6,'mail template','puzzle-piece',1)");
				$this->dbforge->add_field("INSERT INTO `config_groups`(cgid, groupname, groupicon, is_active) VALUES (7,'raja ongkir','cogs',1)");
				
				$this->dbforge->create_table('config_groups');

				/*
				************************************************************
				* Add table configuration
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'idconfig' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'cgid' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'configname' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'configlabel' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'configvalue' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'configicon' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'configtype' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'configoption' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'configoptionvalue' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'configdesc' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'configuseicon' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'editable' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'deletable' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('idconfig', TRUE);
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (1,2,'pref_mode','mode',1,'tags','radio','a:2:{i:0;s:11:\"maintenance\";i:1;s:7:\"publish\";}','0,1','Aktifkan mode maintenance atau publish website di web',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (2,1,'info_name','name','MLM System','font','text',NULL,NULL,'Nama website',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (3,1,'info_title','title','MLM System','header','text',NULL,NULL,'Judul website yang akan ditampilkan di website',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (4,1,'info_subtitle','subtitle','MLM System','strikethrough','text',NULL,NULL,'Sub judul website yang akan ditampilkan di web',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (5,1,'info_desc','description','Sistem yang dibuat untuk menentukan siswa terbaik','paragraph','textarea',NULL,NULL,'Keterangan mengenai website',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (6,1,'info_owner','owner','luthfi','qrcode','text',NULL,NULL,'Pemilik website',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (7,1,'info_address','address','griya kencana','map-marker','text',NULL,NULL,'Alamat pemilik atau kantor website',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (8,1,'info_phone','phone',1234,'phone','text',NULL,NULL,'No telf pemilik atau kantor website',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (9,1,'info_fax','fax',12345465,'fax','text',NULL,NULL,'No fax kantor bila ada',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (10,1,'info_reg','reg no',1234,'gavel','text',NULL,NULL,'No legalitas perusahaan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (12,1,'info_logo','logo','image/icon/4e815cff459f2e6e20700d56d4a3ad88.png','image','filesimage',NULL,NULL,'Logo website, ukuran ( panjang x lebar ) gambar yang disarankan 100 pixel x 100 pixel',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (13,4,'social_fb','facebook','N/A','facebook-square','text',NULL,NULL,'Alamat URL sosial media facebook',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (14,4,'social_twitter','twitter',NULL,'twitter-square','text',NULL,NULL,'Alamat URL sosial media twitter',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (15,4,'social_google','google',NULL,'google-plus','text',NULL,NULL,'Alamat URL sosial media google',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (16,4,'social_instagram','instagram',NULL,'instagram','text',NULL,NULL,'Alamat URL sosial media instagram',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (19,3,'protocol','protocol','mail','random','radio','a:3:{s:4:\"mail\";s:4:\"mail\";s:8:\"sendmail\";s:8:\"sendmail\";s:4:\"smtp\";s:4:\"smtp\";}','mail,sendmail,smtp','Jenis komunikasi data yang digunakan untuk mengirim pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (20,3,'smtp_host','smtp host','smpt_host','building','text',NULL,NULL,'Penyedia komunikasi data untuk pengiriman pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (21,3,'smtp_port','smtp port',25,'terminal','number',NULL,NULL,'Nomor port yang disediakan penyedia komunikasi pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (22,3,'smtp_user','smtp user','smpt_username','user','text',NULL,NULL,'Nama akun pengguna yang terdaftar di penyedia komunikasi pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (23,3,'smtp_pass','smtp pass','smpt_password','unlock','password',NULL,NULL,'Password akun sesuai nama akun yang terdaftar di penyedia komunikasi pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (24,3,'smtp_timeout','smtp timeout',5,'clock-o','number',NULL,NULL,'Batas waktu pengiriman pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (25,3,'wordwrap','wordwrap','TRUE','crosshair','radio','a:2:{s:5:\"FALSE\";s:2:\"NO\";s:4:\"TRUE\";s:3:\"YES\";}','TRUE,FALSE','Aktifkan atau non aktifkan baris baru pada pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (26,3,'wrapchars','wrapchars',76,'paw','number',NULL,NULL,'Jumlah karakter setiap baris baru',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (27,3,'mailtype','mailtype','html','envelope','radio','a:3:{s:4:\"text\";s:4:\"Text\";i:0;s:4:\"html\";i:1;s:4:\"HTML\";}','text,html','Jenis pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (28,3,'charset','charset','iso-8859-1','ticket','radio','a:2:{s:5:\"utf-8\";s:5:\"utf-8\";s:10:\"iso-8859-1\";s:10:\"iso-8859-1\";}','utf-8,iso-8859-1','Charset pengiriman pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (29,3,'validate','validasi pesan','FALSE','send-o','radio','a:2:{s:5:\"FALSE\";s:2:\"NO\";s:4:\"TRUE\";s:3:\"YES\";}','TRUE,FALSE','Aktifkan atau non aktifkan validasi pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (30,3,'priority','prioritas email',3,'wheelchair','radio','a:5:{i:1;s:6:\"urgent\";i:2;s:4:\"high\";i:3;s:6:\"medium\";i:4;s:3:\"low\";i:5;s:6:\"lowest\";}','1,2,3,4,5','Jenis prioritas pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (31,3,'newline','newline','\\n','arrow-circle-o-down','text',NULL,NULL,'Karakter untuk membuat baris baru',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (32,3,'bcc_batch_mode','enable_bcc','FALSE','cc','radio','a:2:{s:5:\"FALSE\";s:2:\"NO\";s:4:\"TRUE\";s:3:\"YES\";}','TRUE,FALSE','Aktifkan atau non aktifkan fitur BCC pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (33,3,'bcc_batch_size','bcc_size',200,'circle','number',NULL,NULL,'Jumlah email yang dapat di input pada fitur BCC',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (34,3,'mailpath','mailpath','/usr/sbin/sendmail','ticket','text',NULL,NULL,'Jalur pesan server',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (35,3,'smtp_crypto','smpt mode','ssl','ticket','radio','a:2:{s:3:\"tls\";s:3:\"TLS\";s:3:\"ssl\";s:3:\"SSL\";}','tls,ssl','Mode yang digunakan penyedia komunikasi pesan',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (37,5,'captcha','captcha security','FALSE','key','radio','a:2:{s:5:\"FALSE\";s:2:\"NO\";s:4:\"TRUE\";s:3:\"YES\";}','TRUE,FALSE','Create captcha security on login',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (38,5,'product_limit','product list limit',4,'cube','number',NULL,NULL,'batas jumlah maksimal product feature',0,1,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (39,5,'brand_limit','brand limit',10,'coffee','number',NULL,NULL,'batas jumlah maksimal merk yang akan ditampilkan',0,1,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (40,5,'max_review','limit ulasan',2,'comments','number',NULL,NULL,'batas maksimal ulasan yg akan ditampilkan',0,1,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (41,5,'max_comment','limit komentar',5,'comments','number',NULL,NULL,'batas maksimal komentar yg akan ditampilkan',0,1,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (42,6,'actmail_english','mail activation (en)','<h1>Hello {username}</h1><p>Your account has been registered.</p><p>To activate your account, please click this link below :</p><p>{activation_url}</p><p> </p><p>If url above is not worked, you can visit this alternative url:</p><p>{activation_url_alt}</p><p> </p><p>then, fill the form within username and activation code correctly</p><p>username : {username}</p><p>activation code : {key}</p><p> </p><p>If you have any question about activation account or any other matter, please feel free to contact us at {support_url} or by phone at.</p><p> </p><p>Thank You</p><p>{_INFO_NAME}</p>','puzzle-piece','editor',NULL,NULL,'template aktivasi bahasa inggris',0,1,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (43,6,'actmail_indonesia','mail activation (id)','<h1>Terima kasih {username}</h1><h4>telah memesan aplikasi di lusaraproject.com</h4><p><br></p><p>Untuk harga aplikasi :</p><p>paket beli 2 aplikasi langsung : Rp . 2.000.000</p><hr id=\"null\"><p>Belum mau beli? ga apa apa, donasinya aja seikhlasnya gan. Sebagai penghargaan buat programmer yang sudah susah payah siang dan malem berkutat depan laptop.</p><p>Cuma dapat paket 1 aplikasi aja gan. tapi sudah include segala resources sama librarynya ko. jangan khawatir.</p><hr id=\"null\"><p>Silahkan transfer pembelian / donasi anda ke :</p><p>Rek BCA Syariah</p><p>No Rek : 1020899464</p><p>Atas nama : luthfi satria ramdhani</p><p><br></p><p>Setelah melakukan pembayaran, silahkan klik link berikut :</p><p>{activation_url}</p><p><br></p><p>Jangan lupa sertakan / unggah bukti transfer atau pembelian anda di formulir yang telah disediakan.</p><p><br></p><p>Kami akan memverifikasi pembelian anda terlebih dahulu, setelah kami terima. Kami akan mengirimkan alamat url ke email anda segera mungkin.</p><p>Jika menemui kesulitan silahkan kontak kami atau kirim email kepada kami di</p><p>luthfi_its@yahoo.com</p><p><br></p><p>Terima kasih</p><p></p>','puzzle-piece','editor',NULL,NULL,'template aktivasi bahasa indonesia',0,1,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (44,5,'activation_expiry','activation expiry',5,'calendar','number',NULL,NULL,'Batas maksimal hari kadaluarsa proses aktivasi',0,1,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (45,5,'taxes','pajak pembelian',10,'calculator','number',NULL,NULL,'Pajak yang dikenakan kepada pembeli setiap kali bertransaksi ( dalam %)',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (46,5,'reply_count','limit balasan',2,'comments','number',NULL,NULL,'Batas maksimal balasan komentar yang akan ditampilkan',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (47,2,'cursymbol','simbol mata uang','Rp.','dollar','text',NULL,NULL,'Simbol mata uang',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (48,2,'thou_sep','simbol ribuan','.','dollar','text',NULL,NULL,'Simbol pembatas mata uang untuk ribuan',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (49,2,'dec_sep','simbol desimal',',','dollar','text',NULL,NULL,'Simbol pembatas mata uang desimal',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (50,2,'dec_digit','jumlah digit angka desimal',0,'dollar','number',NULL,NULL,'Jumlah angka digit desimal',1,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (51,2,'ldate','format tanggal','d-m-Y','calendar','radio','a:2:{s:5:\"d-m-Y\";s:10:\"dd-mm-yyyy\";s:5:\"Y-m-d\";s:10:\"yyyy-mm-dd\";}',NULL,'Format tanggal',0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (52,7,'api_key','API key raja ongkir','raja ongkir api key','pencil','text',NULL,NULL,NULL,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `configuration`(idconfig, cgid, configname, configlabel, configvalue, configicon, configtype, configoption, configoptionvalue, configdesc, configuseicon, editable, deletable) VALUES (53,7,'origin','asal pengiriman',79,'pencil','text',NULL,NULL,NULL,0,0,0)");
				
				$this->dbforge->add_field('ALTER TABLE `configuration` ADD CONSTRAINT `fk_cgid` FOREIGN KEY (`cgid`) REFERENCES `config_groups` (`cgid`) ON DELETE CASCADE ON UPDATE CASCADE');
				
				$this->dbforge->create_table('configuration');

				/*
				************************************************************
				* Add table country
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'country_id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'country_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'country_code' => array(
						'type' => 'VARCHAR',
						'constraint' => '5',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('country_id', TRUE);
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (1,'Afghanistan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (2,'Akrotiri',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (3,'Albania',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (4,'Algeria',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (5,'American Samoa',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (6,'Andorra',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (7,'Angola',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (8,'Anguilla',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (9,'Antigua and Barbuda',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (10,'Argentina',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (11,'Armenia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (12,'Aruba',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (13,'Ashmore and Cartier Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (14,'Australia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (15,'Austria',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (16,'Azerbaijan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (17,'Bahamas',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (18,'Bahrain',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (19,'Baker Island',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (20,'Bangladesh',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (21,'Barbados',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (22,'Belarus',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (23,'Belgium',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (24,'Belize',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (25,'Benin',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (26,'Bermuda',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (27,'Bhutan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (28,'Bolivia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (29,'Bosnia and Herzegovina',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (30,'Botswana',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (31,'Bouvet Island',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (32,'Brazil',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (33,'British Indian Ocean Territory',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (34,'British Virgin Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (35,'Brunei',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (36,'Bulgaria',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (37,'Burkina Faso',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (38,'Burma',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (39,'Burundi',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (40,'Cambodia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (41,'Cameroon',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (42,'Canada',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (43,'Cape Verde',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (44,'Cayman Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (45,'Central African Republic',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (46,'Chad',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (47,'Chile',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (48,'China',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (49,'Christmas Island',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (50,'Clipperton Island',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (51,'Cocos (Keeling) Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (52,'Colombia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (53,'Comoros',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (54,'Congo',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (55,'Cook Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (56,'Coral Sea Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (57,'Costa Rica',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (58,'Cote d Ivoire',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (59,'Croatia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (60,'Cuba',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (61,'Cyprus',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (62,'Czech Republic',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (63,'Denmark',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (64,'Dhekelia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (65,'Djibouti',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (66,'Dominica',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (67,'Dominican Republic',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (68,'Ecuador flag',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (69,'Egypt',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (70,'El Salvador',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (71,'Equatorial Guinea',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (72,'Eritrea',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (73,'Estonia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (74,'Ethiopia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (75,'European Union',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (76,'Falkland Islands flag',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (77,'Faroe Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (78,'Fiji',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (79,'Finland',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (80,'France',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (81,'French Polynesia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (82,'French Southern and Antarctic Lands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (83,'Gabon',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (84,'Gambia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (85,'Georgia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (86,'Germany',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (87,'Ghana',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (88,'Gibraltar',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (89,'Greece',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (90,'Greenland',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (91,'Grenada',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (92,'Guam',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (93,'Guatemala',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (94,'Guernsey',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (95,'Guinea Bissau',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (96,'Guinea',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (97,'Guyana',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (98,'Haiti',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (99,'Heard Island and McDonald Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (100,'Holy See',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (101,'Honduras',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (102,'Hong Kong',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (103,'Howland Island',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (104,'Hungary',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (105,'Iceland',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (106,'India',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (107,'Indonesia',62)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (108,'Iran',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (109,'Iraq',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (110,'Ireland',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (111,'Isle of Man',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (112,'Israel',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (113,'Italy',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (114,'Jamaica flag',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (115,'Jan Mayen',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (116,'Japan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (117,'Jarvis Island',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (118,'Jersey',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (119,'Johnston Atoll',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (120,'Jordan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (121,'Kazakhstan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (122,'Kenya',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (123,'Kingman Reef',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (124,'Kiribati',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (125,'Kuwait',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (126,'Kyrgyzstan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (127,'Laos',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (128,'Latvia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (129,'Lebanon',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (130,'Lesotho',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (131,'Liberia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (132,'Libya',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (133,'Liechtenstein',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (134,'Lithuania',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (135,'Luxembourg',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (136,'Macau',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (137,'Macedonia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (138,'Madagascar',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (139,'Malawi',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (140,'Malaysia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (141,'Maldives',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (142,'Mali',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (143,'Malta',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (144,'Marshall Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (145,'Mauritania',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (146,'Mauritius',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (147,'Mayotte',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (148,'Mexico',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (149,'Micronesia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (150,'Midway Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (151,'Moldova',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (152,'Monaco',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (153,'Mongolia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (154,'Montenegro',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (155,'Montserrat',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (156,'Morocco',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (157,'Mozambique',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (158,'Namibia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (159,'Nauru',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (160,'Navassa Island',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (161,'Nepal',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (162,'Netherlands Antilles',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (163,'Netherlands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (164,'New Caledonia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (165,'New Zealand',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (166,'Nicaragua',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (167,'Niger',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (168,'Nigeria',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (169,'Niue',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (170,'Norfolk Island',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (171,'North Korea',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (172,'Northern Mariana Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (173,'Norway',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (174,'Oman',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (175,'Pakistan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (176,'Palau',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (177,'Palmyra Atoll',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (178,'Panama',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (179,'Papua New Guinea',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (180,'Paraguay',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (181,'Peru',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (182,'Philippines',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (183,'Pitcairn Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (184,'Poland',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (185,'Portugal',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (186,'Puerto Rico',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (187,'Qatar',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (188,'Romania',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (189,'Russia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (190,'Rwanda',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (191,'Saint Barthelemy',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (192,'Saint Helena',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (193,'Saint Kitts and Nevis',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (194,'Saint Lucia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (195,'Saint Martin',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (196,'Saint Pierre and Miquelon',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (197,'Saint Vincent and the Grenadines',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (198,'Samoa',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (199,'San Marino',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (200,'Sao Tome and Principe',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (201,'Saudi Arabia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (202,'Senegal',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (203,'Serbia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (204,'Seychelles',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (205,'Sierra Leone',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (206,'Singapore',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (207,'Slovakia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (208,'Slovenia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (209,'Solomon Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (210,'Somalia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (211,'South Africa',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (212,'South Georgia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (213,'South Korea',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (214,'Spain',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (215,'Sri Lanka',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (216,'Sudan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (217,'Suriname',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (218,'Svalbard',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (219,'Swaziland',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (220,'Sweden',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (221,'Switzerland',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (222,'Syria',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (223,'Taiwan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (224,'Tajikistan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (225,'Tanzania',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (226,'Thailand',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (227,'Timor Leste',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (228,'Togo',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (229,'Tokelau',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (230,'Tonga',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (231,'Trinidad and Tobago',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (232,'Tunisia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (233,'Turkey',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (234,'Turkmenistan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (235,'Turks and Caicos Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (236,'Tuvalu',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (237,'Uganda',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (238,'Ukraine',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (239,'United Arab Emirates',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (240,'United Kingdom',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (241,'United States',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (242,'Uruguay',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (243,'US Pacific Island Wildlife Refuges',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (244,'Uzbekistan',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (245,'Vanuatu',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (246,'Venezuela',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (247,'Vietnam',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (248,'Virgin Islands',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (249,'Wake Island',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (250,'Wallis and Futuna',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (251,'Yemen',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (252,'Zambia',NULL)");
				$this->dbforge->add_field("INSERT INTO `country`(country_id, country_name, country_code) VALUES (253,'Zimbabwe',NULL)");
				
				$this->dbforge->create_table('country');

				/*
				************************************************************
				* Add table css
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'cid' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'cname' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'cpath' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'cdn' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'integrity' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'crossorigin' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'cgroup' => array(
						'type' => 'VARCHAR',
						'constraint' => '200',
						'null' => TRUE,
						),
				
						'is_active' => array(
						'type' => 'CHAR',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'order_idx' => array(
						'type' => 'INT',
						'constraint' => '11',
						'default' => '1',
						'null' => TRUE,
						),
				
						'include_in' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'global' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'default' => '1',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('cid', TRUE);
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (1,'bootstrap','bootstrap/3.3.4/css/bootstrap.min.css','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css','sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u','anonymous','admin,public,login',1,2,NULL,1)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (2,'font_awesome','font-awesome/4.5.0/css/font-awesome.css','https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css','sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN','anonymous','admin,public,login',1,3,NULL,1)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (3,'select2','select2/css/select2.min.css','https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css',NULL,NULL,'admin,public',1,96,NULL,0)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (4,'cpanel','themes/default/cpanel.css',NULL,NULL,NULL,'admin',1,97,NULL,1)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (6,'sbadmin2','sbadmin2/css/sb-admin-2.css',NULL,NULL,NULL,'admin',1,7,NULL,1)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (7,'login','themes/default/login.css',NULL,NULL,NULL,'login',1,99,NULL,0)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (8,'jqueryui','jquery-ui/1.11.4/jquery-ui.min.css','http://code.jquery.com/ui/1.12.1/jquery-ui.min.js','sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=','anonymous','admin,public',1,1,NULL,0)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (9,'tokenfield','tokenfield/bootstrap-tokenfield.min.css',NULL,NULL,NULL,'admin',0,100,NULL,0)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (10,'typeahead','tokenfield/tokenfield-typeahead.min.css',NULL,NULL,NULL,'admin',0,101,NULL,0)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (11,'swal','swal/sweetalert.min.css','https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css',NULL,NULL,'admin,public,login',1,95,NULL,1)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (15,'content','themes/default/css/content.css',NULL,NULL,NULL,'public',1,101,NULL,1)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (21,'bdatatable','datatable/css/dataTables.bootstrap.min.css','https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css',NULL,NULL,'admin',1,94,'usergroup',0)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (22,'form','themes/default/css/form.css',NULL,NULL,NULL,'admin,login,public',1,107,NULL,0)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (23,'jqueryte','jqueryte/jquery-te-1.4.0.css',NULL,NULL,NULL,'admin',1,8,NULL,0)");
				$this->dbforge->add_field("INSERT INTO `css`(cid, cname, cpath, cdn, integrity, crossorigin, cgroup, is_active, order_idx, include_in, global) VALUES (24,'publogin','themes/default/css/login.css',NULL,NULL,NULL,'login',1,4,NULL,0)");
				
				$this->dbforge->create_table('css');

				/*
				************************************************************
				* Add table history
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'date' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'userid' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'ip_address' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'module' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'notes' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (1,'2017-02-03 15:44:38',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (2,'2017-02-03 16:18:04',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (3,'2017-02-05 16:20:01',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (4,'2017-02-05 16:20:43',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (5,'2017-02-05 16:21:17',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (6,'2017-02-05 16:27:17',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (7,'2017-02-05 17:36:53',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (8,'2017-02-05 17:41:48',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (9,'2017-02-05 17:42:21',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (10,'2017-02-05 17:42:26',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (11,'2017-02-05 17:42:38',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (12,'2017-02-05 17:44:51',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (13,'2017-02-05 17:46:17',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (14,'2017-02-05 17:46:35',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (15,'2017-02-05 17:47:27',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (16,'2017-02-05 17:47:52',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (17,'2017-02-05 17:55:49',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (18,'2017-02-05 17:56:54',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (19,'2017-02-05 17:57:45',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (20,'2017-02-05 17:57:58',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (21,'2017-02-05 17:58:08',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (22,'2017-02-05 17:58:14',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (23,'2017-02-05 18:02:22',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (24,'2017-02-05 18:03:08',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (25,'2017-02-05 18:03:57',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (26,'2017-02-05 18:04:16',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (27,'2017-02-05 18:04:30',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (28,'2017-02-05 18:05:07',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (29,'2017-02-05 18:05:20',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (30,'2017-02-05 18:05:45',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (31,'2017-02-05 18:06:29',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (32,'2017-02-05 18:07:18',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (33,'2017-02-05 18:17:04',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (34,'2017-02-05 18:18:16',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (35,'2017-02-05 18:19:04',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (36,'2017-02-05 18:21:17',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (37,'2017-02-05 18:21:35',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (38,'2017-02-05 18:23:57',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (39,'2017-02-05 18:24:25',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (40,'2017-02-05 20:57:49',1,'::1','usergroup','Ubah : kontrol akses')");
				$this->dbforge->add_field("INSERT INTO `history`(id, date, userid, ip_address, module, notes) VALUES (41,'2017-02-05 20:58:09',1,'::1','usergroup','Ubah : kontrol akses')");
				
				$this->dbforge->create_table('history');

				/*
				************************************************************
				* Add table inbox
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'from' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'ip_address' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'email' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'receiveDate' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'is_read' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'is_mark' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'content' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				
				$this->dbforge->create_table('inbox');

				/*
				************************************************************
				* Add table ip_filter
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'ip_address' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'reason' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `ip_filter`(id, ip_address, reason) VALUES (1,'164.245.12.31','test')");
				$this->dbforge->add_field("INSERT INTO `ip_filter`(id, ip_address, reason) VALUES (2,'192.168.1.5','sniffing')");
				
				$this->dbforge->create_table('ip_filter');

				/*
				************************************************************
				* Add table javascript
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'sid' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'sname' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'spath' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'cdn' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'integrity' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'crossorigin' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'sgroup' => array(
						'type' => 'VARCHAR',
						'constraint' => '200',
						'null' => TRUE,
						),
				
						'is_active' => array(
						'type' => 'CHAR',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'order_idx' => array(
						'type' => 'INT',
						'constraint' => '11',
						'default' => '1',
						'null' => TRUE,
						),
				
						'global' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'default' => '1',
						'null' => TRUE,
						),
				
						'include_in' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('sid', TRUE);
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (1,'jquery','jquery/2.2.3/jquery.min.js','https://code.jquery.com/jquery-2.2.4.min.js','sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=','anonymous','admin,public,login',1,1,1,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (4,'select2','select2/js/select2.min.js',NULL,NULL,NULL,'admin,public',1,99,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (5,'bootstrap','bootstrap/3.3.4/js/bootstrap.min.js','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js','sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa','anonymous','admin,public,login',1,3,1,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (6,'datatable','datatable/js/jquery.dataTables.min.js','https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js',NULL,NULL,'admin',1,5,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (8,'jqueryui','jquery-ui/1.11.4/jquery-ui.min.js','http://code.jquery.com/ui/1.12.1/jquery-ui.min.js','sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=','anonymous','admin,public',1,2,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (9,'ckeditor','ckeditor/ckeditor.js','http://cdn.ckeditor.com/4.6.1/standard/ckeditor.js',NULL,NULL,'admin',0,8,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (11,'select2_lang_id','select2/js/i18n/id.js',NULL,NULL,NULL,'admin,public',1,10,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (13,'autonumeric','currency/autoNumeric.js','https://cdnjs.cloudflare.com/ajax/libs/autonumeric/1.9.46/autoNumeric.min.js',NULL,NULL,'admin,public',1,11,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (14,'swal','swal/sweetalert.min.js','https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js',NULL,NULL,'admin,public,login',1,12,1,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (15,'highcart','highcart/js/highcharts.js','https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.4/highcharts.js',NULL,NULL,'admin',1,13,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (16,'content','themes/default/js/content.js',NULL,NULL,NULL,'public',1,100,1,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (18,'bootstrap_datable','datatable/js/dataTables.bootstrap.min.js','https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js',NULL,NULL,'admin',1,9,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (20,'fiquery','js/fiquery.js',NULL,NULL,NULL,'admin',1,102,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (21,'form','themes/default/js/form.js',NULL,NULL,NULL,'admin',1,103,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (22,'sbadmin2','sbadmin2/js/sb-admin-2.js',NULL,NULL,NULL,'admin',1,7,1,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (23,'metismenu','sbadmin2/js/metisMenu.min.js',NULL,NULL,NULL,'admin',1,6,1,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (24,'jqueryte','jqueryte/jquery-te-1.4.0.min.js',NULL,NULL,NULL,'admin',1,8,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (26,'masonry','js/masonry.js','https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.1/masonry.pkgd.js',NULL,NULL,'admin',1,14,0,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (27,'jquerylazy','lazy/jquery.lazy.min.js','http://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.4/jquery.lazy.min.js',NULL,NULL,'admin,public',1,15,1,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (28,'jquerylazyplugin','lazy/jquery.lazy.plugins.min.js','http://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.4/jquery.lazy.plugins.min.js',NULL,NULL,'admin,public',1,16,1,NULL)");
				$this->dbforge->add_field("INSERT INTO `javascript`(sid, sname, spath, cdn, integrity, crossorigin, sgroup, is_active, order_idx, global, include_in) VALUES (29,'dashboard','js/dashboard.js',NULL,NULL,NULL,'admin',1,104,0,NULL)");
				
				$this->dbforge->create_table('javascript');

				/*
				************************************************************
				* Add table member_network
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'parentid' => array(
						'type' => 'INT',
						'constraint' => '11',
						'null' => TRUE,
						),
				
						'ewallet' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'ecash' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'ereward' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'epoint' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				
				$this->dbforge->create_table('member_network');

				/*
				************************************************************
				* Add table membermaster
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'group' => array(
						'type' => 'INT',
						'constraint' => '11',
						'null' => TRUE,
						),
				
						'sponsor_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '30',
						'null' => TRUE,
						),
				
						'username' => array(
						'type' => 'VARCHAR',
						'constraint' => '30',
						'null' => TRUE,
						),
				
						'name' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'jk' => array(
						'type' => 'CHAR',
						'constraint' => '1',
						'default' => 'L',
						'null' => TRUE,
						),
				
						'tempatlahir' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'tgllahir' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'alamat1' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'alamat2' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'alamat3' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'kota' => array(
						'type' => 'VARCHAR',
						'constraint' => '30',
						'null' => TRUE,
						),
				
						'propinsi' => array(
						'type' => 'VARCHAR',
						'constraint' => '30',
						'null' => TRUE,
						),
				
						'negara' => array(
						'type' => 'VARCHAR',
						'constraint' => '5',
						'null' => TRUE,
						),
				
						'hp' => array(
						'type' => 'VARCHAR',
						'constraint' => '15',
						'null' => TRUE,
						),
				
						'telp_kantor' => array(
						'type' => 'VARCHAR',
						'constraint' => '15',
						'null' => TRUE,
						),
				
						'telp_rumah' => array(
						'type' => 'VARCHAR',
						'constraint' => '15',
						'null' => TRUE,
						),
				
						'kodepos' => array(
						'type' => 'VARCHAR',
						'constraint' => '6',
						'null' => TRUE,
						),
				
						'ahliwaris' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'noktp' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'email' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'epoin' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'ewallet' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'ecash' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'ereward' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'tglaplikasi' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'photo' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'secpin' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'membership_id' => array(
						'type' => 'TINYINT',
						'constraint' => '11',
						'null' => TRUE,
						),
				
						'password' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'secpassword' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						'null' => TRUE,
						),
				
						'status' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'group_id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'null' => TRUE,
						),
				
						'last_login' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'autotransfer' => array(
						'type' => 'CHAR',
						'constraint' => '1',
						'default' => 'N',
						'null' => TRUE,
						),
				
						'created' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'createdby' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'updated' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'updatedby' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `membermaster`(id, group, sponsor_name, username, name, jk, tempatlahir, tgllahir, alamat1, alamat2, alamat3, kota, propinsi, negara, hp, telp_kantor, telp_rumah, kodepos, ahliwaris, noktp, email, epoin, ewallet, ecash, ereward, tglaplikasi, photo, secpin, membership_id, password, secpassword, status, group_id, last_login, autotransfer, created, createdby, updated, updatedby) VALUES (1,1,NULL,'STOCKPUSAT','STOCKIST KANTOR PUSAT','L',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'N',NULL,NULL,NULL,NULL)");
				$this->dbforge->add_field("INSERT INTO `membermaster`(id, group, sponsor_name, username, name, jk, tempatlahir, tgllahir, alamat1, alamat2, alamat3, kota, propinsi, negara, hp, telp_kantor, telp_rumah, kodepos, ahliwaris, noktp, email, epoin, ewallet, ecash, ereward, tglaplikasi, photo, secpin, membership_id, password, secpassword, status, group_id, last_login, autotransfer, created, createdby, updated, updatedby) VALUES (2,2,'STOCKPUSAT','LUTH55891979f50c62','Luthfi Satria Ramdhani','L',NULL,'1986-05-17',NULL,NULL,NULL,NULL,NULL,NULL,2147483647,NULL,NULL,NULL,NULL,NULL,'luthfi_its@yahoo.com',NULL,NULL,NULL,NULL,NULL,'member/3b2cbfad4597001dd55b7a2fa59ef962.jpg',NULL,NULL,NULL,NULL,0,NULL,NULL,'N',NULL,NULL,NULL,NULL)");
				$this->dbforge->add_field("INSERT INTO `membermaster`(id, group, sponsor_name, username, name, jk, tempatlahir, tgllahir, alamat1, alamat2, alamat3, kota, propinsi, negara, hp, telp_kantor, telp_rumah, kodepos, ahliwaris, noktp, email, epoin, ewallet, ecash, ereward, tglaplikasi, photo, secpin, membership_id, password, secpassword, status, group_id, last_login, autotransfer, created, createdby, updated, updatedby) VALUES (4,11,'STOCKPUSATsssssss','BERT6723','bertrand','L',NULL,'2017-02-09',NULL,NULL,NULL,NULL,NULL,NULL,556787848,NULL,NULL,NULL,NULL,NULL,'bryan@mail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,'N',NULL,NULL,NULL,NULL)");
				
				$this->dbforge->add_field('ALTER TABLE `membermaster` ADD CONSTRAINT `groupid_fk` FOREIGN KEY (`group_id`) REFERENCES `usergroup` (`groupid`) ON DELETE CASCADE ON UPDATE CASCADE');
				
				$this->dbforge->create_table('membermaster');

				/*
				************************************************************
				* Add table menus
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'pid' => array(
						'type' => 'INT',
						'constraint' => '11',
						'null' => TRUE,
						),
				
						'label' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'icon' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'slug' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'folder' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'controllers' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'display' => array(
						'type' => 'INT',
						'constraint' => '11',
						'default' => '1',
						'null' => TRUE,
						),
				
						'urutan' => array(
						'type' => 'INT',
						'constraint' => '11',
						'default' => '1',
						'null' => TRUE,
						),
				
						'posisi' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'default' => 'left',
						'null' => TRUE,
						),
				
						'target' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'must_login' => array(
						'type' => 'CHAR',
						'constraint' => '1',
						'default' => 'y',
						'null' => TRUE,
						),
				
						'action_case' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (1,NULL,'dashboard','desktop',NULL,NULL,'dashboard',1,1,'left',NULL,1,'view')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (2,NULL,'logout','sign-out','logout','admin','logout',1,100,'top',NULL,1,'view')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (3,NULL,'sistem','desktop','sistem','admin','sistem',1,99,'left',NULL,1,'view')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (4,NULL,'navigasi','navicon','navigasi','admin','navigasi',1,98,'left',NULL,1,'view,update')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (5,NULL,'manajemen','desktop','manajemen','admin','manajemen',1,96,'left',NULL,1,'view')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (6,NULL,'alat','cogs','alat','admin','alat',0,100,'left',NULL,1,'view')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (7,NULL,'localization','map-signs','localization','admin','localization',1,97,'left',NULL,1,'view')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (8,7,'language','bold','localization/language','admin','bahasa',1,1,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (9,3,'konfigurasi','cogs','sistem/konfigurasi','admin','konfigurasi',1,1,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (10,9,'setup','book','sistem/konfigurasi/setup','admin','setup',1,1,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (11,NULL,'login','sign-in','login','admin','login',1,101,'top',NULL,0,'view')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (12,9,'stylesheet','css3','sistem/konfigurasi/stylesheet','admin','css',1,2,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (13,9,'javascript','code','sistem/konfigurasi/javascript','admin','script',1,3,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (14,7,'provinsi','map','localization/provinsi','admin','provinsi',1,3,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (15,7,'kota','map-marker','localization/kota','admin','kota',1,4,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (16,7,'negara','map-o','localization/negara','admin','country',1,2,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (17,3,'ip filter','share-alt','sistem/ip-filter','admin','ipfilter',1,2,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (18,3,'log','hourglass','sistem/log','admin','history',1,3,'left',NULL,1,'view,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (19,3,'backup','database','sistem/backup','admin','basisdata',1,4,'left',NULL,1,'view,printed')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (21,3,'restore','cloud-upload','sistem/restore','admin','basisdata/import',1,5,'left',NULL,1,'view,insert,update')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (22,4,'top menu','sort-alpha-asc','navigasi/top-menu','admin','navigasi/top',1,1,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (23,4,'left menu','sort-alpha-desc','navigasi/left-menu','admin','navigasi',1,1,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (24,5,'usergroup','star','manajemen/usergroup','admin','usergroup',1,100,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (25,5,'users','users','manajemen/user','admin','userlist',1,99,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (26,NULL,'administrator','object-group','administrator','admin','administrator',1,2,'left',NULL,1,'view')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (29,3,'announcement','bullhorn','sistem/announcement','admin','pengumuman',1,1,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (30,NULL,'profil','user-secret','profil','admin','userprofil',1,1,'top',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (31,30,'ganti password','key','profil/ganti-password','admin','userprofil/password',1,1,'top',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (32,30,'ganti avatar','photo','profil/ganti-avatar','admin','userprofil/avatar',1,2,'top',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (33,30,'pengaturan','cogs','profil/pengaturan','admin','userprofil',1,3,'top',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (34,5,'bank','bank','manajemen/bank','admin','bank',1,1,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (35,26,'banner','file-image-o','administrator/banner','admin','banner',1,2,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (36,5,'tipe stockies','flag','manajemen/tipe-stockies','admin','stockies_type',1,98,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (37,26,'stockist','flag-checkered','administrator/stockist','admin','stockies',1,2,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (38,26,'member','users','administrator/member','admin','member',1,3,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (39,5,'produk','coffee','manajemen/produk','admin','produk',1,2,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (40,5,'produk kategori','diamond','manajemen/produk-kategori','admin','produk_category',1,5,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (41,5,'tipe produk','flag','manajemen/tipe-produk','admin','produk_type',1,96,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (42,5,'paket group','flag-checkered','manajemen/paket-group','admin','paket_group',1,95,'left',NULL,1,'view,insert,update,delete')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (43,NULL,'transaction','dollar','transaction','admin','transaction',1,97,'left',NULL,1,'view')");
				$this->dbforge->add_field("INSERT INTO `menus`(id, pid, label, icon, slug, folder, controllers, display, urutan, posisi, target, must_login, action_case) VALUES (44,43,'purchase code','bitcoin','transaction/purchase-code','admin','purchase_code',1,1,'left',NULL,1,'view,insert,update,delete')");
				
				$this->dbforge->create_table('menus');

				/*
				************************************************************
				* Add table migrations
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'version' => array(
						'type' => 'BIGINT',
						'constraint' => '20',
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `migrations`(version) VALUES (0)");
				
				$this->dbforge->create_table('migrations');

				/*
				************************************************************
				* Add table paket
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'group_paket_id' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'name' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'description' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						),
				
						'price' => array(
						'type' => 'DOUBLE',
						),
				
						'titik' => array(
						'type' => 'TINYINT',
						'constraint' => '4',
						),
				
						'jmltitik' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'bonus_sponsor' => array(
						'type' => 'DOUBLE',
						),
				
						'bonus_pairing' => array(
						'type' => 'DOUBLE',
						),
				
						'max_pairing' => array(
						'type' => 'DOUBLE',
						),
				
						'promopoin' => array(
						'type' => 'DOUBLE',
						),
				
						'bvindex' => array(
						'type' => 'DECIMAL',
						'constraint' => '14',
						),
				
						'rewardpoin' => array(
						'type' => 'DOUBLE',
						),
				
						'status' => array(
						'type' => 'ENUM',
						'default' => 'active',
						),
				
						'remark' => array(
						'type' => 'VARCHAR',
						'constraint' => '255',
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (1,1,'EKONOMI A','PAKET EKONOMI A',2700000,1,1,300000,150000,10000000,0,1000,0,'active','1 BRAIN PC CAP')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (2,2,'BISNIS A','PAKET BISNIS A',5400000,1,2,800000,150000,30000000,0,2000,0,'active','2 BRAIN PC CAP ')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (3,2,'BISNIS C','PAKET BISNIS C',5900000,1,2,800000,150000,30000000,0,2000,0,'active','1 SPECTRUM SCARF 2ND GENERATION (ORANGE) ')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (4,1,'EKONOMI B','PAKET EKONOMI B',2700000,1,1,300000,150000,10000000,0,1000,0,'active','1 SPECTRUM SOCKS (BLACK) + 1 EYESHADE')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (5,1,'EKONOMI C','PAKET EKONOMI C',2700000,1,1,300000,150000,10000000,0,1000,0,'active','1 SPECTRUM SOCKS (SKIN) + 1 EYESHADE')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (6,1,'EKONOMI D','PAKET EKONOMI D',3300000,1,1,300000,150000,10000000,0,1000,0,'active','1 ELEGANT BRA (BLACK)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (7,1,'EKONOMI E','PAKET EKONOMI E',3300000,1,1,300000,150000,10000000,0,1000,0,'active','1 MINI BOTTOM/3\" PANTS (BLACK)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (10,2,'BISNIS E','PAKET BISNIS E',5900000,1,2,800000,150000,30000000,0,2000,0,'active','1 LONG SLEEVES BRALETTE (BLACK)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (8,2,'BISNIS B','PAKET BISNIS B',5400000,1,2,800000,150000,30000000,0,2000,0,'active','1 9\" TIGHT PANTS (BLACK)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (9,2,'BISNIS D','PAKET BISNIS D',5900000,1,2,800000,150000,30000000,0,2000,0,'active','1 SPECTRUM SCARF 2ND GENERATION (BLACK) ')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (11,2,'BISNIS F','PAKET BISNIS F',5900000,1,2,800000,150000,30000000,0,2000,0,'active','1 MINI BOTTOM/3\" PANTS (SKIN)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (12,2,'BISNIS G','PAKET BISNIS G',6200000,1,2,800000,150000,30000000,0,2000,0,'active','1 MEN\'S WAIST GIRDLE (BLACK)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (13,2,'BISNIS H','PAKET BISNIS H',6200000,1,2,800000,150000,30000000,0,2000,0,'active','2 ELEGANT BRA (BLACK)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (14,3,'EKSEKUTIF A','PAKET EKSEKUTIF A',7200000,1,2,900000,150000,30000000,0,3000,0,'active','1 SPECTRUM BRALETTE (SKIN) ')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (15,3,'EKSEKUTIF B','PAKET EKSEKUTIF B',7200000,1,2,900000,150000,30000000,0,3000,0,'active','1 TUMMY TRIM SHORT/5\" PANTS (SKIN)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (16,3,'EKSEKUTIF C','PAKET EKSEKUTIF C',7200000,1,2,900000,150000,30000000,0,3000,0,'active','1 ECOLOGICAL HEALTH PANTS + 1 EYESHADE')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (17,3,'EKSEKUTIF D','PAKET EKSEKUTIF D',8200000,1,2,1000000,150000,30000000,0,3500,0,'active','1 9\" TIGHT PANTS (SKIN)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (18,3,'EKSEKUTIF E','PAKET EKSEKUTIF E',8200000,1,2,1000000,150000,30000000,0,3500,0,'active','1 MEN WAIST GIRDLE (SKIN)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (19,3,'EKSEKUTIF F','PAKET EKSEKUTIF F',8200000,1,2,1000000,150000,30000000,0,3500,0,'active','1 MID SLEEVES BRALETTE (SKIN)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (20,2,'BISNIS I','PAKET BISNIS I',5900000,1,2,800000,150000,30000000,0,2000,0,'active','1 MID SLEEVES BRALETTE (BLACK)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (21,2,'BISNIS J','PAKET BISNIS J',5400000,1,2,800000,150000,30000000,0,2000,0,'active','1 SPECTRUM BRALETTE/SLEEVELESS (BLACK) + 1 EYESHADE')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (22,2,'BISNIS K','PAKET BISNIS K',5400000,1,2,800000,150000,30000000,0,2000,0,'active','1 TUMMY TRIM SHORT 5\" PANTS (BLACK) + 1 EYESHADE')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (23,2,'BISNIS L','PAKET BISNIS L',5400000,1,2,800000,150000,30000000,0,2000,0,'active','1 SPECTRUM TUMMY 7\" PANTS (BLACK)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (24,3,'EKSEKUTIF G','PAKET EKSEKUTIF G',8400000,1,2,1000000,150000,30000000,0,3500,0,'active','1 SPECTRUM LONG SLEEVELESS BRALETE (SKIN)')");
				$this->dbforge->add_field("INSERT INTO `paket`(id, group_paket_id, name, description, price, titik, jmltitik, bonus_sponsor, bonus_pairing, max_pairing, promopoin, bvindex, rewardpoin, status, remark) VALUES (25,4,'FIRST CLASS','PAKET FIRST CLASS',16800000,3,3,2500000,150000,30000000,0,6500,0,'active','1 PACKAGE FIRST CLASS')");
				
				$this->dbforge->create_table('paket');

				/*
				************************************************************
				* Add table paket_group
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'pg_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'pg_description' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'created_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'created_by' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'updated_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'updated_by' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `paket_group`(id, pg_name, pg_description, created_at, created_by, updated_at, updated_by) VALUES (1,'Ekonomi','','2017-02-03 16:19:07','admin',NULL,NULL)");
				$this->dbforge->add_field("INSERT INTO `paket_group`(id, pg_name, pg_description, created_at, created_by, updated_at, updated_by) VALUES (2,'Bisnis','','2017-02-03 16:19:30','admin','2017-02-04 15:03:49','admin')");
				$this->dbforge->add_field("INSERT INTO `paket_group`(id, pg_name, pg_description, created_at, created_by, updated_at, updated_by) VALUES (3,'Eksekutif','','2017-02-03 16:19:43','admin',NULL,NULL)");
				$this->dbforge->add_field("INSERT INTO `paket_group`(id, pg_name, pg_description, created_at, created_by, updated_at, updated_by) VALUES (4,'First Class','','2017-02-03 16:19:52','admin',NULL,NULL)");
				
				$this->dbforge->create_table('paket_group');

				/*
				************************************************************
				* Add table pengumuman
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'judul' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'isi' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'created_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'priority' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'isUnlimited' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'expire' => array(
						'type' => 'VARCHAR',
						'constraint' => '10',
						'null' => TRUE,
						),
				
						'expire_time' => array(
						'type' => 'VARCHAR',
						'constraint' => '10',
						'null' => TRUE,
						),
				
						'redaktur' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'isPublish' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `pengumuman`(id, judul, isi, created_at, priority, isUnlimited, expire, expire_time, redaktur, isPublish) VALUES (1,'On progress','Website ini masih dalam tahap pengembangan.<p>Segala gambar terkecuali logo website bukan merupakan hak milik maupun hak cipta dari kami.</p>','2016-12-02 17:49:30',2,0,'2017-12-31','23:00:00',1,1)");
				
				$this->dbforge->create_table('pengumuman');

				/*
				************************************************************
				* Add table product
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'id_category' => array(
						'type' => 'INT',
						'constraint' => '11',
						'null' => TRUE,
						),
				
						'produk_type' => array(
						'type' => 'INT',
						'constraint' => '11',
						'null' => TRUE,
						),
				
						'code' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'name' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'deskripsi' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'sales' => array(
						'type' => 'CHAR',
						'constraint' => '3',
						'null' => TRUE,
						),
				
						'manufacture' => array(
						'type' => 'CHAR',
						'constraint' => '3',
						'null' => TRUE,
						),
				
						'slug' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'price' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'price_cust' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'price_register' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'cashback' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'pv' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'bv' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'diskon' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'image_local' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'image_url' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'weight' => array(
						'type' => 'FLOAT',
						'null' => TRUE,
						),
				
						'created_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'created_by' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'updated_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'updated_by' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (1,1,2,'F01B-70CC','SPECTRUM BRA (BLACK) 70CC',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-09-25 14:13:13','GNT911','2015-11-10 18:22:01','GNT911')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (2,1,2,'F01B-70C','SPECTRUM BRA (BLACK) 70C',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-09-25 14:13:59','GNT911','2015-11-10 18:21:38','GNT911')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (3,1,2,'F01B-70D','SPECTRUM BRA (BLACK) 70D',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-09-25 14:15:52','GNT911','2015-11-10 18:22:28','GNT911')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (4,1,2,'F01B-70B','SPECTRUM BRA (BLACK) 70B',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-09-25 14:18:19','GNT911','2016-07-01 11:34:42','GNT911')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (5,1,1,'PCCAP(XS)+EYE','BRAIN PC CAP (XS) + EYESHADE',NULL,'NO','YES',NULL,2700000,0,2700000,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-27 13:47:40','CANAI01','2015-11-18 13:36:55','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (6,1,1,'BISNIS(S)+EYE','PAKET BISNIS SIZE S PROMO FREE EYESHADE',NULL,'NO','YES',NULL,5400000,0,5400000,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-27 13:53:15','CANAI01','2015-11-15 13:33:41','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (7,1,1,'PCCAP(S)+EYE','BRAIN PC CAP (S) + EYESHADE',NULL,'NO','YES',NULL,2700000,0,0,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-27 15:44:48','CANAI01','2015-11-17 11:08:39','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (8,1,1,'PCCAP(L)+EYE','BRAIN PC CAP (L) + EYESHADE',NULL,'NO','YES',NULL,2700000,0,0,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-27 15:48:28','CANAI01','2015-11-16 18:25:36','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (9,1,1,'BISNIS(XS)+EYE','PAKET BISNIS SIZE XS PROMO FREE EYESHADE',NULL,'NO','YES',NULL,5400000,0,0,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-27 15:56:05','CANAI01','2015-11-15 13:34:04','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (10,1,1,'BISNIS(L)+EYE','PAKET BISNIS SIZE L PROMO FREE EYESHADE',NULL,'NO','YES',NULL,5400000,0,0,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-27 15:56:52','CANAI01','2015-11-15 13:33:19','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (11,1,2,'M01B-064','MEN WAIST GIRDLE (BLACK) 64',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:00:42','CANAI03','2015-11-16 18:18:46','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (12,1,2,'M01B-070','MEN WAIST GIRDLE (BLACK) 70',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:03:51','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (13,1,2,'M01B-076','MEN WAIST GIRDLE (BLACK) 76',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:04:37','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (14,1,2,'M01B-082','MEN WAIST GIRDLE (BLACK) 82',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:05:21','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (15,1,2,'M01B-090','MEN WAIST GIRDLE (BLACK) 90',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:06:18','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (16,1,2,'M01B-098','MEN WAIST GIRDLE (BLACK) 98',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:07:27','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (17,1,2,'M01B-106','MEN WAIST GIRDLE (BLACK) 106',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:08:05','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (18,1,2,'M01B-114','MEN WAIST GIRDLE (BLACK) 114',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:09:32','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (19,1,2,'M01B-122','MEN WAIST GIRDLE (BLACK) 122',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:10:08','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (20,1,2,'F06B-064','SPECTRUM MINI BOTTOM 3 INCI (BLACK) 64',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 16:10:46','CANAI02','2015-11-10 19:03:32','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (21,1,2,'M01B-130','MEN WAIST GIRDLE (BLACK) 130',NULL,'YES','NO',NULL,6120000,6200000,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 16:11:18','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (22,1,2,'F06B-070','SPECTRUM MINI BOTTOM 3 INCI (BLACK) 70',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 16:11:45','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (23,1,2,'M04B-064','ECOLOGICAL HEALTH PANTS (BLACK) 64',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:16:09','CANAI03','2015-11-16 18:20:04','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (24,1,2,'M04B-070','ECOLOGICAL HEALTH PANTS (BLACK) 70',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:17:39','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (25,1,2,'M04B-076','ECOLOGICAL HEALTH PANTS (BLACK) 76',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:18:41','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (26,1,2,'M04B-082','ECOLOGICAL HEALTH PANTS (BLACK) 82',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:19:57','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (27,1,2,'M04B-090','ECOLOGICAL HEALTH PANTS (BLACK) 90',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:20:37','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (28,1,2,'M04B-098','ECOLOGICAL HEALTH PANTS (BLACK) 98',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:21:13','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (29,1,2,'F06B-076','SPECTRUM MINI BOTTOM 3 INCI (BLACK) 76',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 16:21:33','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (30,1,2,'M04B-106','ECOLOGICAL HEALTH PANTS (BLACK) 106',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:21:57','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (31,1,2,'F06B-082','SPECTRUM MINI BOTTOM 3 (BLACK) 82',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 16:22:50','CANAI02','2016-07-01 10:06:28','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (32,1,2,'M04B-114','ECOLOGICAL HEALTH PANTS (BLACK) 114',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:22:53','CANAI03','2015-11-18 13:40:39','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (33,1,2,'F06B-090','SPECTRUM MINI BOTTOM 3 INCI (BLACK) 90',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 16:23:13','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (34,1,2,'M04B-122','ECOLOGICAL HEALTH PANTS (BLACK) 122',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:23:37','CANAI03','2015-11-18 13:39:26','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (35,1,2,'F06B-098','SPECTRUM MINI BOTTOM 3 INCI (BLACK) 98',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 16:23:56','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (36,1,2,'M04B-130','ECOLOGICAL HEALTH PANTS (BLACK) 130',NULL,'YES','NO',NULL,6960000,7200000,7800000,NULL,3480,0,NULL,NULL,NULL,NULL,'2015-10-28 16:24:20','CANAI03','2015-11-18 13:37:43','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (37,1,2,'M10S-076','MEN LUXURY WAIST GIRDLE (SKIN) 76',NULL,'YES','NO',NULL,8160000,8200000,9000000,NULL,4080,0,NULL,NULL,NULL,NULL,'2015-10-28 16:35:17','CANAI03','2015-11-16 18:20:55','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (38,1,2,'M10S-082','MEN LUXURY WAIST GIRDLE (SKIN) 82',NULL,'YES','NO',NULL,8160000,8200000,9000000,NULL,4080,0,NULL,NULL,NULL,NULL,'2015-10-28 16:36:23','CANAI03','2015-11-18 13:22:27','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (39,1,2,'M10S-090','MEN LUXURY WAIST GIRDLE (SKIN) 90',NULL,'YES','NO',NULL,8160000,8200000,9000000,NULL,4080,0,NULL,NULL,NULL,NULL,'2015-10-28 16:36:59','CANAI03','2015-11-18 13:23:37','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (40,1,2,'M10S-098','MEN LUXURY WAIST GIRDLE (SKIN) 98',NULL,'YES','NO',NULL,8160000,8200000,9000000,NULL,4080,0,NULL,NULL,NULL,NULL,'2015-10-28 16:37:36','CANAI03','2015-11-18 13:27:19','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (41,1,2,'F07B-064','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK)',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:38:36','CANAI02','2015-11-10 19:07:04','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (42,1,2,'M10S-106','MEN LUXURY WAIST GIRDLE (SKIN) 106',NULL,'YES','NO',NULL,8160000,8200000,9000000,NULL,4080,0,NULL,NULL,NULL,NULL,'2015-10-28 16:39:41','CANAI03','2015-11-18 13:29:32','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (43,1,2,'F07B-070','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK) 70',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:39:45','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (44,1,2,'F07B-076','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK) 76',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:40:12','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (45,1,2,'F07B-082','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK) 82',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:44:37','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (46,1,2,'F07B-090','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK) 90',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:44:59','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (47,1,2,'F07B-098','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK) 98',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:45:48','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (48,1,2,'F07B-106','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK) 106',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:46:42','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (49,1,2,'M10S-114','MEN LUXURY WAIST GIRDLE (SKIN) 114',NULL,'YES','NO',NULL,8160000,8200000,9000000,NULL,4080,0,NULL,NULL,NULL,NULL,'2015-10-28 16:47:05','CANAI03','2015-11-18 13:28:13','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (50,1,2,'F07B-114','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK) 114',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:47:16','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (51,1,2,'F07B-122','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK) 122',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:47:43','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (52,1,2,'F07B-SO','SPECTRUM TUMMY TRIM SHORT 5 INCI (BLACK) SPECIAL O',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-28 16:48:13','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (53,1,2,'M10S-122','MEN LUXURY WAIST GIRDLE (SKIN) 122',NULL,'YES','NO',NULL,8160000,8200000,9000000,NULL,4080,0,NULL,NULL,NULL,NULL,'2015-10-28 16:48:27','CANAI03','2015-11-18 13:30:13','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (54,1,2,'M10S-130','MEN LUXURY WAIST GIRDLE (SKIN) 130',NULL,'YES','NO',NULL,8160000,8200000,9000000,NULL,4080,0,NULL,NULL,NULL,NULL,'2015-10-28 16:49:10','CANAI03','2015-11-18 13:30:46','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (55,1,2,'F08B-064','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) 64',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 16:51:35','CANAI02','2015-11-10 19:10:14','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (56,1,2,'F08B-070','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) 70',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 16:52:10','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (57,1,2,'F08B-076','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) 76',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 16:52:56','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (58,1,2,'F08B-082','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) 82',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 16:53:42','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (59,1,2,'SO4B-070','SPECTRUM MINI BOTTOM 4 INCI (BLACK) 70',NULL,'YES','NO',NULL,3960000,0,4800000,NULL,1980,0,NULL,NULL,NULL,NULL,'2015-10-28 16:57:25','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (60,1,2,'F08B-090','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) 90',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 16:58:52','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (61,1,2,'SO4B-076','SPECTRUM MINI BOTTOM 4 INCI (BLACK) 76',NULL,'YES','NO',NULL,3960000,0,4800000,NULL,1980,0,NULL,NULL,NULL,NULL,'2015-10-28 17:00:11','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (62,1,2,'F08B-106','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) 106',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 17:01:31','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (63,1,2,'SO4B-082','SPECTRUM MINI BOTTOM 4 INCI (BLACK) 82',NULL,'YES','NO',NULL,3960000,0,4800000,NULL,1980,0,NULL,NULL,NULL,NULL,'2015-10-28 17:01:42','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (64,1,2,'F08B-098','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) 98',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 17:02:20','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (65,1,2,'SO4B-090','SPECTRUM MINI BOTTOM 4 INCI (BLACK) 90',NULL,'YES','NO',NULL,3960000,0,4800000,NULL,1980,0,NULL,NULL,NULL,NULL,'2015-10-28 17:02:26','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (66,1,2,'F08B-114','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) 114',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 17:02:57','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (67,1,2,'SO4B-098','SPECTRUM MINI BOTTOM 4 INCI (BLACK) 98',NULL,'YES','NO',NULL,3960000,0,4800000,NULL,1980,0,NULL,NULL,NULL,NULL,'2015-10-28 17:03:01','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (68,1,2,'F08B-122','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) 122',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 17:03:19','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (69,1,2,'F08B-SO','SPECTRUM TUMMY LONG PANT 7 INCI (BLACK) SPECIAL OR',NULL,'YES','NO',NULL,5160000,5400000,6000000,NULL,2580,0,NULL,NULL,NULL,NULL,'2015-10-28 17:04:29','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (70,1,2,'SO4B-106','SPECTRUM MINI BOTTOM 4 INCI (BLACK) 106',NULL,'YES','NO',NULL,3960000,0,4800000,NULL,1980,0,NULL,NULL,NULL,NULL,'2015-10-28 17:04:32','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (71,1,2,'SO4B-114','SPECTRUM MINI BOTTOM 4 INCI (BLACK) 114',NULL,'YES','NO',NULL,3960000,0,4800000,NULL,1980,0,NULL,NULL,NULL,NULL,'2015-10-28 17:05:10','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (72,1,2,'F09B-064','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) 64',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:05:59','CANAI02','2015-11-10 19:19:41','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (73,1,2,'SO4B-122','SPECTRUM MINI BOTTOM 4 INCI (BLACK) 122',NULL,'YES','NO',NULL,3960000,0,4800000,NULL,1980,0,NULL,NULL,NULL,NULL,'2015-10-28 17:06:00','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (74,1,2,'F09B-070','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) 70',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:06:27','CANAI02','2015-10-28 22:00:57','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (75,1,2,'F09B-076','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) 76',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:06:57','CANAI02','2015-10-28 22:01:20','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (76,1,2,'F09B-082','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) 82',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:11:19','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (77,1,2,'F09B-090','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) 90',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:16:48','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (78,1,2,'F09B-098','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) 98',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:17:11','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (79,1,2,'F09B-106','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) 106',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:25:58','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (80,1,2,'F09B-114','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) 114',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:26:23','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (81,1,2,'F09B-122','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) 122',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:26:49','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (82,1,2,'F09B-SO','SPECTRUM TUMMY LONG PANT 9 INCI (BLACK) SPECIAL OR',NULL,'YES','NO',NULL,5400000,5400000,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-28 17:28:11','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (83,1,2,'F10S-70B','SPECTRUM BRA (SKIN) 70B',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:28:47','CANAI02','2015-11-10 19:20:39','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (84,1,2,'F10S-70C','SPECTRUM BRA (SKIN) 70C',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:29:11','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (85,1,2,'F10S-70D','SPECTRUM BRA (SKIN) 70D',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:29:57','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (86,1,2,'F10S-75B','SPECTRUM BRA (SKIN) 75B',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:30:53','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (87,1,2,'F10S-75C','SPECTRUM BRA (SKIN) 75C',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:31:24','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (88,1,2,'F10S-75CC','SPECTRUM BRA (SKIN) 75CC',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:32:09','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (89,1,2,'F10S-75D','SPECTRUM BRA (SKIN) 75D',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:32:45','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (90,1,2,'F10S-75E','SPECTRUM BRA (SKIN) 75E',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:33:36','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (91,1,2,'F10S-80B','SPECTRUM BRA (SKIN) 80B',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:34:54','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (92,1,2,'F10S-80C','SPECTRUM BRA (SKIN) 80C',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:35:29','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (93,1,2,'F10S-80CC','SPECTRUM BRA (SKIN) 80CC',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:35:56','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (94,1,2,'F10S-80D','SPECTRUM BRA (SKIN) 80D',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:36:52','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (95,1,2,'F10S-80E','SPECTRUM BRA (SKIN) 80E',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:37:15','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (96,1,2,'F10S-80F','SPECTRUM BRA (SKIN) 80F',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:38:06','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (97,1,2,'F10S-85B','SPECTRUM BRA (SKIN) 85B',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:38:31','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (98,1,2,'F10S-85C','SPECTRUM BRA (SKIN) 85C',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:38:54','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (99,1,2,'F10S-85CC','SPECTRUM BRA (SKIN) 85CC',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:39:19','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (100,1,2,'F10S-85D','SPECTRUM BRA (SKIN) 85D',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:39:50','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (101,1,2,'M01W-064','MEN WAIST GIRDLE (WHITE) 64',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:43:42','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (102,1,2,'F10S-85E','SPECTRUM BRA (SKIN) 85E',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:43:44','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (103,1,2,'F10S-85F','SPECTRUM BRA (SKIN) 85F',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:44:44','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (104,1,2,'M01W-070','MEN WAIST GIRDLE (WHITE) 70',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:45:08','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (105,1,2,'F10S-90B','SPECTRUM BRA (SKIN) 90B',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:45:51','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (106,1,2,'F10S-90C','SPECTRUM BRA (SKIN) 90C',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:46:09','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (107,1,2,'F10S-90D','SPECTRUM BRA (SKIN) 90D',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:46:27','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (108,1,2,'M01W-076','MEN WAIST GIRDLE (WHITE) 76',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:46:28','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (109,1,2,'F10S-90E','SPECTRUM BRA (SKIN) 90E',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:46:54','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (110,1,2,'M01W-082','MEN WAIST GIRDLE (WHITE) 82',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:47:09','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (111,1,2,'F10S-90F','SPECTRUM BRA (SKIN) 90F',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:47:47','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (112,1,2,'M01W-090','MEN WAIST GIRDLE (WHITE) 90',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:47:53','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (113,1,2,'F10S-95B','SPECTRUM BRA (SKIN) 95B',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:48:26','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (114,1,2,'F10S-95C','SPECTRUM BRA (SKIN) 95C',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:48:49','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (115,1,2,'M01W-098','MEN WAIST GIRDLE (WHITE) 98',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:48:53','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (116,1,2,'F10S-95D','SPECTRUM BRA (SKIN) 95D',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:49:11','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (117,1,2,'M01W-106','MEN WAIST GIRDLE (WHITE) 106',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:49:27','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (118,1,2,'F10S-95E','SPECTRUM BRA (SKIN) 95E',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:50:29','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (119,1,2,'M01W-114','MEN WAIST GIRDLE (WHITE) 114',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:50:41','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (120,1,2,'F10S-95F','SPECTRUM BRA (SKIN) 95F',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:50:49','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (121,1,2,'M01W-122','MEN WAIST GIRDLE (WHITE) 122',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:51:10','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (122,1,2,'F10S-SO','SPECTRUM BRA (SKIN) SPECIAL ORDER',NULL,'YES','NO',NULL,4800000,0,5600000,NULL,2400,0,NULL,NULL,NULL,NULL,'2015-10-28 17:51:24','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (123,1,2,'M01W-130','MEN WAIST GIRDLE (WHITE) 130',NULL,'YES','NO',NULL,6120000,0,7000000,NULL,3060,0,NULL,NULL,NULL,NULL,'2015-10-28 17:51:48','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (124,1,2,'F06W-064','SPECTRUM MINI BOTTOM 3 INCI (WHITE) 64',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 17:53:53','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (125,1,2,'F06W-070','SPECTRUM MINI BOTTOM 3 INCI (WHITE) 70',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 17:54:59','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (126,1,2,'F06W-076','SPECTRUM MINI BOTTOM 3 INCI (WHITE) 76',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 17:55:21','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (127,1,2,'F06W-082','SPECTRUM MINI BOTTOM 3 INCI (WHITE) 82',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 17:56:35','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (128,1,2,'F06W-090','SPECTRUM MINI BOTTOM 3 INCI (WHITE) 90',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 17:57:04','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (129,1,2,'F06W-098','SPECTRUM MINI BOTTOM 3 INCI (WHITE) 98',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-28 17:58:03','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (130,1,2,'F07W-064','SPECTRUM TUMMY TRIM SHORT 5 INCI (WHITE) 64',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 11:28:17','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (131,1,2,'F07W-070','SPECTRUM TUMMY TRIM SHORT 5 INCI (WHITE) 70',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 11:28:43','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (132,1,2,'F07W-076','SPECTRUM TUMMY TRIM SHORT 5 INCI (WHITE) 76',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 11:29:04','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (133,1,2,'F07W-082','SPECTRUM TUMMY TRIM SHORT 5 INCI (WHITE) 82',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 11:29:23','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (134,1,2,'F07W-090','SPECTRUM TUMMY TRIM SHORT 5 INCI (WHITE) 90',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 11:30:23','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (135,1,2,'F07W-098','SPECTRUM TUMMY TRIM SHORT 5 INCI (WHITE) 98',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 11:30:47','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (136,1,2,'F07W-106','SPECTRUM TUMMY TRIM SHORT 5 INCI (WHITE) 106',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 11:31:27','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (137,1,2,'F07W-114','SPECTRUM TUMMY TRIM SHORT 5 INCI (WHITE) 114',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 11:31:47','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (138,1,2,'F07W-122','SPECTRUM TUMMY TRIM SHORT 5 INCI (WHITE) 122',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 11:32:06','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (139,1,2,'AS01B-064','MEN HEALTH CARE UNDERWEAR (BLACK) 64',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 11:44:56','CANAI02','2015-11-11 01:18:22','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (140,1,2,'AS01B-070','MEN HEALTH CARE UNDERWEAR (BLACK) 70',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 11:45:14','CANAI02','2015-11-10 13:20:28','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (141,1,2,'AS01B-076','MEN HEALTH CARE UNDERWEAR (BLACK) 76',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 11:45:38','CANAI02','2015-11-10 13:20:47','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (142,1,2,'AS01B-082','MEN HEALTH CARE UNDERWEAR (BLACK) 82',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 11:45:56','CANAI02','2015-11-10 13:21:09','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (143,1,2,'AS01B-090','MEN HEALTH CARE UNDERWEAR (BLACK) 90',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 11:46:15','CANAI02','2015-11-10 13:19:46','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (144,1,2,'AS01B-098','MEN HEALTH CARE UNDERWEAR (BLACK) 98',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 11:46:36','CANAI02','2015-11-10 13:21:32','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (145,1,2,'AS01B-106','MEN HEALTH CARE UNDERWEAR (BLACK) 106',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 11:46:55','CANAI02','2015-11-10 13:21:48','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (146,1,2,'AS01B-114','MEN HEALTH CARE UNDERWEAR (BLACK) 114',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 11:47:55','CANAI02','2015-11-10 13:22:09','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (147,1,2,'AS01B-122','MEN HEALTH CARE UNDERWEAR (BLACK) 122',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 11:48:20','CANAI02','2015-11-10 13:22:30','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (148,1,2,'AS05B-076','SPECTRUM MEN HEALTHY  UNDERWEAR (BLACK) 76',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 11:51:45','CANAI02','2015-11-10 16:24:49','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (149,1,2,'AS05B-082','SPECTRUM MEN HEALTHY  UNDERWEAR (BLACK) 82',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 11:54:46','CANAI02','2015-11-10 16:26:13','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (150,1,2,'AS05B-090','SPECTRUM MEN HEALTHY  UNDERWEAR (BLACK) 90',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 11:55:05','CANAI02','2015-11-10 16:27:21','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (151,1,2,'AS01R-064','MEN HEALTH CARE UNDERWEAR (RED) 64',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:53:09','CANAI02','2015-11-10 13:22:55','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (152,1,2,'AS01R-070','MEN HEALTH CARE UNDERWEAR (RED) 70',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:53:38','CANAI02','2015-11-10 15:03:55','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (153,1,2,'AS01R-076','MEN HEALTH CARE UNDERWEAR (RED) 76',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:54:05','CANAI02','2015-11-10 15:03:01','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (154,1,2,'AS01R-082','MEN HEALTH CARE UNDERWEAR (RED) 82',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:54:23','CANAI02','2015-11-10 15:05:41','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (155,1,2,'AS01R-090','MEN HEALTH CARE UNDERWEAR (RED) 90',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:54:41','CANAI02','2015-11-10 15:06:52','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (156,1,2,'AS01R-098','MEN HEALTH CARE UNDERWEAR (RED) 98',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:55:47','CANAI02','2015-11-10 15:07:46','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (157,1,2,'AS01R-106','MEN HEALTH CARE UNDERWEAR (RED) 106',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:56:15','CANAI02','2015-11-10 15:08:29','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (158,1,2,'AS01R-114','MEN HEALTH CARE UNDERWEAR (RED) 114',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:56:50','CANAI02','2015-11-10 15:23:02','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (159,1,2,'AS01R-122','MEN HEALTH CARE UNDERWEAR (RED) 122',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:57:33','CANAI02','2015-11-10 15:23:49','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (160,1,2,'AS01W-082','MEN HEALTH CARE UNDERWEAR (WHITE) 82',NULL,'YES','NO',NULL,1020000,0,1450000,NULL,510,0,NULL,NULL,NULL,NULL,'2015-10-29 12:58:27','CANAI02','2015-11-18 15:27:34','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (161,1,2,'AS05B-098','SPECTRUM MEN HEALTHY UNDERWEAR (BLACK) 98',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 12:59:44','CANAI02','2015-11-10 16:29:41','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (162,1,2,'AS05B-106','SPECTRUM MEN HEALTHY UNDERWEAR (BLACK) 106',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 13:00:02','CANAI02','2015-11-10 16:30:41','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (163,1,2,'AS05B-114','SPECTRUM MEN HEALTHY UNDERWEAR (BLACK) 114',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 13:00:28','CANAI02','2015-11-10 16:31:34','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (164,1,2,'F01B-70E','SPECTRUM BRA (BLACK) 70E',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:00:37','CANAI03','2015-11-10 18:30:24','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (165,1,2,'AS05B-122','SPECTRUM MEN HEALTHY UNDERWEAR (BLACK) 122',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 13:01:05','CANAI02','2015-11-10 16:38:10','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (166,1,2,'AS05S-076','SPECTRUM MEN HEALTHY UNDERWEAR (SKIN) 76',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 13:01:36','CANAI02','2015-11-10 16:39:18','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (167,1,2,'F01B-70F','SPECTRUM BRA (BLACK) 70F',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:01:39','CANAI03','2015-11-10 18:30:48','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (168,1,2,'F01B-70G','SPECTRUM BRA (BLACK) 70G',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:02:27','CANAI03','2015-11-10 18:35:44','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (169,1,2,'AS05S-082','SPECTRUM MEN HEALTHY UNDERWEAR (SKIN) 82',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 13:03:16','CANAI02','2015-11-10 16:41:11','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (170,1,2,'AS05S-090','SPECTRUM MEN HEALTHY UNDERWEAR (SKIN) 90',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 13:03:36','CANAI02','2015-11-10 16:41:53','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (171,1,2,'F01B-75B','SPECTRUM BRA (BLACK) 75B',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:07:07','CANAI03','2015-11-10 18:36:11','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (172,1,2,'AS05S-098','SPECTRUM MEN HEALTHY UNDERWEAR (SKIN) 98',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 13:07:19','CANAI02','2015-11-10 16:42:39','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (173,1,2,'AS05S-106','SPECTRUM MEN HEALTHY UNDERWEAR (SKIN) 106',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 13:07:49','CANAI02','2015-11-10 16:43:22','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (174,1,2,'F01B-75C','SPECTRUM BRA (BLACK) 75C',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:07:51','CANAI03','2015-11-10 18:36:35','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (175,1,2,'AS05S-114','SPECTRUM MEN HEALTHY UNDERWEAR (SKIN) 114',NULL,'YES','NO',NULL,1272000,0,1800000,NULL,636,0,NULL,NULL,NULL,NULL,'2015-10-29 13:08:07','CANAI02','2015-11-10 16:45:04','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (176,1,2,'AS02B-064','FEMALE NEGATIVE IONS PANTY (BLACK) 64',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:09:33','CANAI02','2015-11-10 15:26:05','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (177,1,2,'AS02B-070','FEMALE NEGATIVE IONS PANTY (BLACK) 70',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:10:01','CANAI02','2015-11-10 15:27:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (178,1,2,'AS02B-076','FEMALE NEGATIVE IONS PANTY (BLACK) 76',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:10:22','CANAI02','2015-11-10 15:28:17','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (179,1,2,'F01B-75CC','SPECTRUM BRA (BLACK) 75CC',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:10:38','CANAI03','2015-11-10 18:36:54','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (180,1,2,'AS02B-082','FEMALE NEGATIVE IONS PANTY (BLACK) 82',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:10:51','CANAI02','2015-11-10 16:01:28','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (181,1,2,'AS02B-090','FEMALE NEGATIVE IONS PANTY (BLACK) 90',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:11:13','CANAI02','2015-11-10 16:02:18','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (182,1,2,'F01B-75D','SPECTRUM BRA (BLACK) 75D',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:11:24','CANAI03','2015-11-10 18:37:15','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (183,1,2,'AS02B-098','FEMALE NEGATIVE IONS PANTY (BLACK) 98',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:11:31','CANAI02','2015-11-10 16:03:16','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (184,1,2,'F01B-75E','SPECTRUM BRA (BLACK) 75E',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:11:56','CANAI03','2015-11-10 18:37:39','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (185,1,2,'AS02R-064','FEMALE NEGATIVE IONS PANTY (RED) 64',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:12:07','CANAI02','2015-11-10 16:05:49','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (186,1,2,'AS02R-070','FEMALE NEGATIVE IONS PANTY (RED) 70',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:12:49','CANAI02','2015-11-10 16:11:48','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (187,1,2,'AS02R-076','FEMALE NEGATIVE IONS PANTY (RED) 76',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:13:09','CANAI02','2015-11-10 16:13:03','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (188,1,2,'AS02R-082','FEMALE NEGATIVE IONS PANTY (RED) 82',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:13:32','CANAI02','2015-11-10 16:15:52','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (189,1,2,'AS02R-090','FEMALE NEGATIVE IONS PANTY (RED) 90',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:13:51','CANAI02','2015-11-10 16:16:31','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (190,1,2,'AS02R-098','FEMALE NEGATIVE IONS PANTY (RED) 98',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:14:13','CANAI02','2015-11-10 16:17:07','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (191,1,2,'F01B-75F','SPECTRUM BRA (BLACK) 75F',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:14:36','CANAI03','2015-11-10 18:39:12','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (192,1,2,'AS02W-064','FEMALE NEGATIVE IONS PANTY (WHITE) 64',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:15:02','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (193,1,2,'F01B-75G','SPECTRUM BRA (BLACK) 75G',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:15:22','CANAI03','2015-11-10 18:39:56','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (194,1,2,'AS02W-070','FEMALE NEGATIVE IONS PANTY (WHITE) 70',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:15:35','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (195,1,2,'AS02W-076','FEMALE NEGATIVE IONS PANTY (WHITE) 76',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:16:08','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (196,1,2,'AS02W-082','FEMALE NEGATIVE IONS PANTY (WHITE) 82',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:16:34','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (197,1,2,'F01B-80B','SPECTRUM BRA (BLACK) 80B',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:16:45','CANAI03','2015-11-10 18:40:23','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (198,1,2,'AS02W-090','FEMALE NEGATIVE IONS PANTY (WHITE) 90',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:17:22','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (199,1,2,'F01B-80C','SPECTRUM BRA (BLACK) 80C',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:17:38','CANAI03','2015-11-10 18:40:45','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (200,1,2,'AS02W-098','FEMALE NEGATIVE IONS PANTY (WHITE) 98',NULL,'YES','NO',NULL,828000,0,1300000,NULL,414,0,NULL,NULL,NULL,NULL,'2015-10-29 13:17:48','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (201,1,2,'F01B-80CC','SPECTRUM BRA (BLACK) 80CC',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:18:36','CANAI03','2015-11-10 18:41:08','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (202,1,2,'F01B-80D','SPECTRUM BRA (BLACK) 80D',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:19:32','CANAI03','2015-11-10 18:41:44','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (203,1,2,'F01B-80E','SPECTRUM BRA (BLACK) 80E',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:20:36','CANAI03','2015-11-10 18:42:05','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (204,1,2,'AS03B-M','SPECTRUM SOCKS BLACK (1 BOX = 2 SET) M',NULL,'YES','NO',NULL,2160000,2700000,2800000,NULL,1080,0,NULL,NULL,NULL,NULL,'2015-10-29 13:20:51','CANAI02','2016-07-01 14:23:20','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (205,1,2,'F01B-80F','SPECTRUM BRA (BLACK) 80F',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:21:13','CANAI03','2015-11-10 18:42:27','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (206,1,2,'AS03B-L','SPECTRUM SOCKS BLACK (1 BOX = 2 SET) L',NULL,'YES','NO',NULL,2160000,0,2800000,NULL,1080,0,NULL,NULL,NULL,NULL,'2015-10-29 13:21:21','CANAI02','2016-08-29 11:02:11','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (207,1,2,'AS03B-XL','SPECTRUM SOCKS BLACK (1 BOX = 2 SET) XL',NULL,'YES','NO',NULL,2160000,2700000,2800000,NULL,1080,0,NULL,NULL,NULL,NULL,'2015-10-29 13:21:55','CANAI02','2016-07-01 14:23:34','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (208,1,2,'F01B-80G','SPECTRUM BRA (BLACK) 80G',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:22:07','CANAI03','2015-11-10 18:42:47','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (209,1,2,'AS03S-M','SPECTRUM SOCKS SKIN (1 BOX = 2 SET) M',NULL,'YES','NO',NULL,2160000,2700000,2800000,NULL,1080,0,NULL,NULL,NULL,NULL,'2015-10-29 13:22:23','CANAI02','2015-11-10 16:22:50','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (210,1,2,'F01B-85B','SPECTRUM BRA (BLACK) 85B',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:22:58','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (211,1,2,'AS03S-L','SPECTRUM SOCKS SKIN (1 BOX = 2 SET) L',NULL,'YES','NO',NULL,2160000,0,2800000,NULL,1080,0,NULL,NULL,NULL,NULL,'2015-10-29 13:23:01','CANAI02','2016-08-29 11:02:43','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (212,1,2,'F01B-85C','SPECTRUM BRA (BLACK) 85C',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:23:31','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (213,1,2,'F01B-85CC','SPECTRUM BRA (BLACK) 85CC',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:25:02','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (214,1,2,'F01B-85D','SPECTRUM BRA (BLACK) 85D',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 13:25:53','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (215,1,2,'AS04BL-076','HEALTH LUXURIOUS NIGHTDRESS (BLUE) 76',NULL,'YES','NO',NULL,2580000,0,0,NULL,1290,0,NULL,NULL,NULL,NULL,'2015-10-29 13:30:49','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (216,1,2,'AS04BL-082','HEALTH LUXURIOUS NIGHTDRESS (BLUE) 82',NULL,'YES','NO',NULL,2580000,0,0,NULL,1290,0,NULL,NULL,NULL,NULL,'2015-10-29 13:31:09','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (217,1,2,'AS04BL-090','HEALTH LUXURIOUS NIGHTDRESS (BLUE) 90',NULL,'YES','NO',NULL,2580000,0,0,NULL,1290,0,NULL,NULL,NULL,NULL,'2015-10-29 13:31:34','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (218,1,2,'AS04PI-076','HEALTH LUXURIOUS NIGHTDRESS (PINK) 76',NULL,'YES','NO',NULL,2580000,0,0,NULL,1290,0,NULL,NULL,NULL,NULL,'2015-10-29 13:32:18','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (219,1,2,'AS04PI-082','HEALTH LUXURIOUS NIGHTDRESS (PINK) 82',NULL,'YES','NO',NULL,2580000,0,0,NULL,1290,0,NULL,NULL,NULL,NULL,'2015-10-29 13:32:38','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (220,1,2,'AS04PI-090','HEALTH LUXURIOUS NIGHTDRESS (PINK) 90',NULL,'YES','NO',NULL,2580000,0,0,NULL,1290,0,NULL,NULL,NULL,NULL,'2015-10-29 13:33:06','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (221,1,2,'AS06B-070','SPECTRUM ELEGANT FEMALE UNDERWEAR (BLACK) 70',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:34:21','CANAI02','2015-11-10 16:53:10','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (222,1,2,'AS06B-076','SPECTRUM ELEGANT FEMALE UNDERWEAR (BLACK) 76',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:34:39','CANAI02','2015-11-10 16:52:48','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (223,1,2,'AS06B-082','SPECTRUM ELEGANT FEMALE UNDERWEAR (BLACK) 82',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:34:55','CANAI02','2015-11-10 17:46:49','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (224,1,2,'AS06B-090','SPECTRUM ELEGANT FEMALE UNDERWEAR (BLACK) 90',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:35:19','CANAI02','2015-11-10 17:54:29','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (225,1,2,'AS06B-098','SPECTRUM ELEGANT FEMALE UNDERWEAR (BLACK) 98',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:35:40','CANAI02','2015-11-10 18:03:27','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (226,1,2,'AS06S-070','SPECTRUM ELEGANT FEMALE UNDERWEAR (SKIN) 70',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:36:04','CANAI02','2015-11-10 18:03:47','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (227,1,2,'AS06S-076','SPECTRUM ELEGANT FEMALE UNDERWEAR (SKIN) 76',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:36:24','CANAI02','2015-11-10 18:04:05','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (228,1,2,'AS06S-082','SPECTRUM ELEGANT FEMALE UNDERWEAR (SKIN) 82',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:36:41','CANAI02','2015-11-10 18:04:25','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (229,1,2,'AS06S-090','SPECTRUM ELEGANT FEMALE UNDERWEAR (SKIN) 90',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:37:03','CANAI02','2015-11-10 18:04:53','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (230,1,2,'AS06S-098','SPECTRUM ELEGANT FEMALE UNDERWEAR (SKIN) 98',NULL,'YES','NO',NULL,936000,0,1350000,NULL,468,0,NULL,NULL,NULL,NULL,'2015-10-29 13:37:18','CANAI02','2015-11-10 18:05:27','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (231,1,2,'AS07B','SPECTRUM EYESHADE (BLACK) - ALL SIZE',NULL,'YES','NO',NULL,456000,0,700000,NULL,228,0,NULL,NULL,NULL,NULL,'2015-10-29 13:38:17','CANAI02','2016-07-01 14:20:18','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (232,1,2,'AS08B-L','SPECTRUM ICY ARM SLIMMING OVERSLEEVE (BLACK) L',NULL,'YES','NO',NULL,1176000,0,1700000,NULL,588,0,NULL,NULL,NULL,NULL,'2015-10-29 13:39:26','CANAI02','2015-11-10 18:13:32','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (233,1,2,'AS08B-M','SPECTRUM ICY ARM SLIMMING OVERSLEEVE (BLACK) M',NULL,'YES','NO',NULL,1176000,0,1700000,NULL,588,0,NULL,NULL,NULL,NULL,'2015-10-29 13:39:49','CANAI02','2015-11-10 18:13:49','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (234,1,2,'AS08B-S','SPECTRUM ICY ARM SLIMMING OVERSLEEVE (BLACK) S',NULL,'YES','NO',NULL,1176000,0,1700000,NULL,588,0,NULL,NULL,NULL,NULL,'2015-10-29 13:40:10','CANAI02','2015-11-10 18:14:09','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (235,1,2,'AS08S-L','SPECTRUM ICY ARM SLIMMING OVERSLEEVE (SKIN) L',NULL,'YES','NO',NULL,1176000,0,1700000,NULL,588,0,NULL,NULL,NULL,NULL,'2015-10-29 13:40:34','CANAI02','2015-11-10 18:14:32','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (236,1,2,'AS08S-M','SPECTRUM ICY ARM SLIMMING OVERSLEEVE (SKIN) M',NULL,'YES','NO',NULL,1176000,0,1700000,NULL,588,0,NULL,NULL,NULL,NULL,'2015-10-29 13:45:57','CANAI02','2015-11-10 18:14:50','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (237,1,2,'AS08S-S','SPECTRUM ICY ARM SLIMMING OVERSLEEVE (SKIN) S',NULL,'YES','NO',NULL,1176000,0,1700000,NULL,588,0,NULL,NULL,NULL,NULL,'2015-10-29 13:48:30','CANAI02','2015-11-10 18:15:10','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (238,1,1,'AS09B','SPECTRUM SCARF (BLACK)',NULL,'YES','NO',NULL,5400000,0,6700000,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-29 14:02:24','CANAI02','2016-01-30 11:15:08','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (239,1,2,'AS10B-L','SPECTRUM SEDATIVE NIGHT CAP (BLACK) L',NULL,'YES','NO',NULL,1440000,0,2100000,NULL,720,0,NULL,NULL,NULL,NULL,'2015-10-29 14:04:23','CANAI02','2015-11-10 18:20:09','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (240,1,2,'AS10B-S','SPECTRUM SEDATIVE NIGHT CAP (BLACK) S',NULL,'YES','NO',NULL,1440000,0,2100000,NULL,720,0,NULL,NULL,NULL,NULL,'2015-10-29 14:04:43','CANAI02','2015-11-10 18:20:33','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (241,1,2,'M02P-076','SPECTRUM POLO T-SHIRT (PURPLE) 76',NULL,'YES','NO',NULL,6600000,0,0,NULL,3300,0,NULL,NULL,NULL,NULL,'2015-10-29 14:05:43','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (242,1,2,'M02P-082','SPECTRUM POLO T-SHIRT (PURPLE) 82',NULL,'YES','NO',NULL,6600000,0,0,NULL,3300,0,NULL,NULL,NULL,NULL,'2015-10-29 14:06:26','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (243,1,2,'M02P-090','SPECTRUM POLO T-SHIRT (PURPLE) 90',NULL,'YES','NO',NULL,6600000,0,0,NULL,3300,0,NULL,NULL,NULL,NULL,'2015-10-29 14:06:49','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (244,1,2,'M02P-098','SPECTRUM POLO T-SHIRT (PURPLE) 98',NULL,'YES','NO',NULL,6600000,0,0,NULL,3300,0,NULL,NULL,NULL,NULL,'2015-10-29 14:08:46','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (245,1,2,'M02P-106','SPECTRUM POLO T-SHIRT (PURPLE) 106',NULL,'YES','NO',NULL,6600000,0,0,NULL,3300,0,NULL,NULL,NULL,NULL,'2015-10-29 14:09:18','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (246,1,2,'M02P-114','SPECTRUM POLO T-SHIRT (PURPLE) 114',NULL,'YES','NO',NULL,6600000,0,0,NULL,3300,0,NULL,NULL,NULL,NULL,'2015-10-29 14:09:57','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (247,1,2,'M02P-122','SPECTRUM POLO T-SHIRT (PURPLE) 122',NULL,'YES','NO',NULL,6600000,0,0,NULL,3300,0,NULL,NULL,NULL,NULL,'2015-10-29 14:10:37','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (248,1,2,'M03P-076','SPECTRUM GOLF LONG-SLEEVES (PURPLE) 76',NULL,'YES','NO',NULL,7320000,0,0,NULL,3660,0,NULL,NULL,NULL,NULL,'2015-10-29 14:23:24','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (249,1,2,'M03P-082','SPECTRUM GOLF LONG-SLEEVES (PURPLE) 82',NULL,'YES','NO',NULL,7320000,0,0,NULL,3660,0,NULL,NULL,NULL,NULL,'2015-10-29 14:24:05','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (250,1,2,'M03P-090','SPECTRUM GOLF LONG-SLEEVES (PURPLE) 90',NULL,'YES','NO',NULL,7320000,0,0,NULL,3660,0,NULL,NULL,NULL,NULL,'2015-10-29 14:24:25','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (251,1,2,'M03P-098','SPECTRUM GOLF LONG-SLEEVES (PURPLE) 98',NULL,'YES','NO',NULL,7320000,0,0,NULL,3660,0,NULL,NULL,NULL,NULL,'2015-10-29 14:24:41','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (252,1,2,'M03P-106','SPECTRUM GOLF LONG-SLEEVES (PURPLE) 106',NULL,'YES','NO',NULL,7320000,0,0,NULL,3660,0,NULL,NULL,NULL,NULL,'2015-10-29 14:25:03','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (253,1,2,'M03P-114','SPECTRUM GOLF LONG-SLEEVES (PURPLE) 114',NULL,'YES','NO',NULL,7320000,0,0,NULL,3660,0,NULL,NULL,NULL,NULL,'2015-10-29 14:25:20','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (254,1,2,'M03P-122','SPECTRUM GOLF LONG-SLEEVES (PURPLE) 122',NULL,'YES','NO',NULL,7320000,0,0,NULL,3660,0,NULL,NULL,NULL,NULL,'2015-10-29 14:28:57','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (255,1,2,'M05PI-090','SPECTRUM MEN POLO T-SHIRT (PINK) 90',NULL,'YES','NO',NULL,7080000,0,0,NULL,3540,0,NULL,NULL,NULL,NULL,'2015-10-29 14:41:42','CANAI02','2015-11-18 13:15:08','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (256,1,2,'M05PI-098','SPECTRUM MEN POLO T-SHIRT (PINK) 98',NULL,'YES','NO',NULL,7080000,0,0,NULL,3540,0,NULL,NULL,NULL,NULL,'2015-10-29 14:42:16','CANAI02','2015-11-18 13:15:42','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (257,1,2,'M05PI-106','SPECTRUM MEN POLO T-SHIRT (PINK) 106',NULL,'YES','NO',NULL,7080000,0,0,NULL,3540,0,NULL,NULL,NULL,NULL,'2015-10-29 14:42:54','CANAI02','2015-11-18 13:16:29','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (258,1,2,'M05PI-114','SPECTRUM MEN POLO T-SHIRT (PINK) 114',NULL,'YES','NO',NULL,7080000,0,0,NULL,3540,0,NULL,NULL,NULL,NULL,'2015-10-29 14:44:43','CANAI02','2015-11-18 13:17:31','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (259,1,2,'M06PI-090','SPECTRUM MEN LONG-SLEEVES T-SHIRT (PINK) 90',NULL,'YES','NO',NULL,8016000,0,0,NULL,4008,0,NULL,NULL,NULL,NULL,'2015-10-29 14:45:24','CANAI02','2015-11-18 13:18:59','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (260,1,2,'F01B-85E','SPECTRUM BRA (BLACK) 85E',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 14:49:45','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (261,1,2,'F01B-85F','SPECTRUM BRA (BLACK) 85F',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 14:53:07','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (262,1,2,'F01B-85G','SPECTRUM BRA (BLACK) 85G',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 14:53:48','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (263,1,2,'F01B-90B','SPECTRUM BRA (BLACK) 90B',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:04:24','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (264,1,2,'F01B-90C','SPECTRUM BRA (BLACK) 90C',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:05:06','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (265,1,2,'F01B-90D','SPECTRUM BRA (BLACK) 90D',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:05:34','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (266,1,2,'F01B-90E','SPECTRUM BRA (BLACK) 90E',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:06:10','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (267,1,2,'F01B-90F','SPECTRUM BRA (BLACK) 90F',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:06:48','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (268,1,2,'F01B-90G','SPECTRUM BRA (BLACK) 90G',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:07:57','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (269,1,2,'F01B-95B','SPECTRUM BRA (BLACK) 95B',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:08:49','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (270,1,2,'F01B-95C','SPECTRUM BRA (BLACK) 95C',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:09:28','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (271,1,2,'F01B-95D','SPECTRUM BRA (BLACK) 95D',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:10:02','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (272,1,2,'F01B-95E','SPECTRUM BRA (BLACK) 95E',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:10:43','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (273,1,2,'F01B-95F','SPECTRUM BRA (BLACK) 95F',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:11:22','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (274,1,2,'F01B-95G','SPECTRUM BRA (BLACK) 95G',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:11:51','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (275,1,2,'F01B-SO','SPECTRUM BRA (BLACK) SPECIAL ORDER',NULL,'YES','NO',NULL,3360000,3300000,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:14:02','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (276,1,2,'F11S-070','SPECTRUM SLEEVELESS BRALETTE (SKIN) 70',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:14:40','CANAI02','2015-11-16 17:15:43','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (277,1,2,'F01W-70B','SPECTRUM BRA (WHITE) 70B',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:15:09','CANAI03','2015-11-18 15:29:06','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (278,1,2,'F11S-076','SPECTRUM SLEEVELESS BRALETTE (SKIN) 76',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:15:13','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (279,1,2,'F11S-082','SPECTRUM SLEEVELESS BRALETTE (SKIN) 82',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:15:34','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (280,1,2,'F01W-70C','SPECTRUM BRA (WHITE) 70C',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:15:58','CANAI03','2015-11-18 15:30:39','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (281,1,2,'F11S-090','SPECTRUM SLEEVELESS BRALETTE (SKIN) 90',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:16:08','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (282,1,2,'F01W-70CC','SPECTRUM BRA (WHITE) 75CC',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:16:44','CANAI03','2015-11-18 15:31:43','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (283,1,2,'F11S-098','SPECTRUM SLEEVELESS BRALETTE (SKIN) 98',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:17:04','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (284,1,2,'F11S-106','SPECTRUM SLEEVELESS BRALETTE (SKIN) 106',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:17:28','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (285,1,2,'F01W-70D','SPECTRUM BRA (WHITE) 70D ',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:17:42','CANAI03','2015-11-18 15:33:13','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (286,1,2,'F11S-114','SPECTRUM SLEEVELESS BRALETTE (SKIN) 114',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:18:05','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (287,1,2,'F01W-70E','SPECTRUM BRA (WHITE) 70E',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:18:15','CANAI03','2015-11-18 15:34:33','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (288,1,2,'F11S-122','SPECTRUM SLEEVELESS BRALETTE (SKIN) 122',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:18:40','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (289,1,2,'F01W-70F','SPECTRUM BRA (WHITE) 75F',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:18:53','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (290,1,2,'F01W-70G','SPECTRUM BRA (WHITE) 70G',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:19:33','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (291,1,2,'F01W-75B','SPECTRUM BRA (WHITE) 75B',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:20:47','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (292,1,2,'F01W-75C','SPECTRUM BRA (WHITE) 75C',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:21:24','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (293,1,2,'F01W-75CC','SPECTRUM BRA (WHITE) 75CC',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:21:57','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (294,1,2,'F12S-070','SPECTRUM MID-SLEEVES BRALETTE (SKIN) 70',NULL,'YES','NO',NULL,7920000,8200000,8800000,NULL,3960,0,NULL,NULL,NULL,NULL,'2015-10-29 15:22:21','CANAI02','2015-11-16 17:16:31','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (295,1,2,'F12S-076','SPECTRUM MID-SLEEVES BRALETTE (SKIN) 76',NULL,'YES','NO',NULL,7920000,8200000,8800000,NULL,3960,0,NULL,NULL,NULL,NULL,'2015-10-29 15:22:39','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (296,1,2,'F12S-082','SPECTRUM MID-SLEEVES BRALETTE (SKIN) 82',NULL,'YES','NO',NULL,7920000,8200000,8800000,NULL,3960,0,NULL,NULL,NULL,NULL,'2015-10-29 15:23:28','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (297,1,2,'F12S-090','SPECTRUM MID-SLEEVES BRALETTE (SKIN) 90',NULL,'YES','NO',NULL,7920000,8200000,8800000,NULL,3960,0,NULL,NULL,NULL,NULL,'2015-10-29 15:23:54','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (298,1,2,'F12S-098','SPECTRUM MID-SLEEVES BRALETTE (SKIN) 98',NULL,'YES','NO',NULL,7920000,8200000,8800000,NULL,3960,0,NULL,NULL,NULL,NULL,'2015-10-29 15:24:19','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (299,1,2,'F01W-75D','SPECTRUM BRA (WHITE) 75D',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:24:32','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (300,1,2,'F01W-75E','SPECTRUM BRA (WHITE) 75E',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:25:12','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (301,1,2,'F12S-106','SPECTRUM MID-SLEEVES BRALETTE (SKIN) 106',NULL,'YES','NO',NULL,7920000,8200000,8800000,NULL,3960,0,NULL,NULL,NULL,NULL,'2015-10-29 15:25:28','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (302,1,2,'F12S-114','SPECTRUM MID-SLEEVES BRALETTE (SKIN) 114',NULL,'YES','NO',NULL,7920000,8200000,8800000,NULL,3960,0,NULL,NULL,NULL,NULL,'2015-10-29 15:25:46','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (303,1,2,'F01W-75F','SPECTRUM BRA (WHITE) 75F',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:25:51','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (304,1,2,'F12S-122','SPECTRUM MID-SLEEVES BRALETTE (SKIN) 122',NULL,'YES','NO',NULL,7920000,8200000,8800000,NULL,3960,0,NULL,NULL,NULL,NULL,'2015-10-29 15:26:15','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (305,1,2,'F01W-75G','SPECTRUM BRA (WHITE) 75G',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:26:39','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (306,1,2,'F12S-SO','SPECTRUM MID-SLEEVES BRALETTE (SKIN) SPECIAL ORDER',NULL,'YES','NO',NULL,7920000,8200000,8800000,NULL,3960,0,NULL,NULL,NULL,NULL,'2015-10-29 15:27:34','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (307,1,2,'F01W-80B','SPECTRUM BRA (WHITE) 80B',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 15:27:36','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (308,1,2,'F13S-070','SPECTRUM LONG-SLEEVES BRALETTE (SKIN) 70',NULL,'YES','NO',NULL,8400000,8400000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:28:17','CANAI02','2015-11-16 17:49:02','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (309,1,2,'F13S-076','SPECTRUM LONG-SLEEVES BRALETTE (SKIN) 76',NULL,'YES','NO',NULL,8400000,8400000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:28:47','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (310,1,2,'F13S-082','SPECTRUM LONG-SLEEVES BRALETTE (SKIN) 82',NULL,'YES','NO',NULL,8400000,8400000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:29:07','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (311,1,2,'F13S-090','SPECTRUM LONG-SLEEVES BRALETTE (SKIN) 90',NULL,'YES','NO',NULL,8400000,8400000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:29:31','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (312,1,2,'F13S-098','SPECTRUM LONG-SLEEVES BRALETTE (SKIN) 98',NULL,'YES','NO',NULL,8400000,8400000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:29:52','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (313,1,2,'F13S-106','SPECTRUM LONG-SLEEVES BRALETTE (SKIN) 106',NULL,'YES','NO',NULL,8400000,8400000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:30:10','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (314,1,2,'F13S-114','SPECTRUM LONG-SLEEVES BRALETTE (SKIN) 114',NULL,'YES','NO',NULL,8400000,8400000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:30:32','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (315,1,2,'F13S-122','SPECTRUM LONG-SLEEVES BRALETTE (SKIN) 122',NULL,'YES','NO',NULL,8400000,8400000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:30:52','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (316,1,2,'F21S-070','SPECTRUM TUMMY TRIM SHORT 5 INCI (SKIN) 70',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:33:26','CANAI02','2015-11-16 17:50:04','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (317,1,2,'F21S-076','SPECTRUM TUMMY TRIM SHORT 5 INCI (SKIN) 76',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:33:48','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (318,1,2,'F21S-082','SPECTRUM TUMMY TRIM SHORT 5 INCI (SKIN) 82',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:34:10','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (319,1,2,'F21S-090','SPECTRUM TUMMY TRIM SHORT 5 INCI (SKIN) 90',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:34:33','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (320,1,2,'F21S-098','SPECTRUM TUMMY TRIM SHORT 5 INCI (SKIN) 98',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:35:06','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (321,1,2,'F21S-106','SPECTRUM TUMMY TRIM SHORT 5 INCI (SKIN) 106',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:35:27','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (322,1,2,'F21S-114','SPECTRUM TUMMY TRIM SHORT 5 INCI (SKIN) 114',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:35:48','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (323,1,2,'F21S-122','SPECTRUM TUMMY TRIM SHORT 5 INCI (SKIN) 122',NULL,'YES','NO',NULL,7200000,7200000,8000000,NULL,3600,0,NULL,NULL,NULL,NULL,'2015-10-29 15:36:20','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (324,1,2,'F22S-076','SPECTRUM TUMMY LONG PANT 9 INCI (SKIN) 76',NULL,'YES','NO',NULL,8400000,8200000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:40:44','CANAI02','2015-11-16 17:50:39','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (325,1,2,'F22S-082','SPECTRUM TUMMY LONG PANT 9 INCI (SKIN) 82',NULL,'YES','NO',NULL,8400000,8200000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:41:08','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (326,1,2,'F22S-090','SPECTRUM TUMMY LONG PANT 9 INCI (SKIN) 90',NULL,'YES','NO',NULL,8400000,8200000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:41:33','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (327,1,2,'F22S-098','SPECTRUM TUMMY LONG PANT 9 INCI (SKIN) 98',NULL,'YES','NO',NULL,8400000,8200000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:42:07','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (328,1,2,'F22S-106','SPECTRUM TUMMY LONG PANT 9 INCI (SKIN) 106',NULL,'YES','NO',NULL,8400000,8200000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:42:41','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (329,1,2,'F22S-114','SPECTRUM TUMMY LONG PANT 9 INCI (SKIN) 114',NULL,'YES','NO',NULL,8400000,8200000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:43:02','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (330,1,2,'F22S-122','SPECTRUM TUMMY LONG PANT 9 INCI (SKIN) 122',NULL,'YES','NO',NULL,8400000,8200000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:43:22','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (331,1,2,'F22S-SO','SPECTRUM TUMMY LONG PANT 9 INCI (SKIN) SPECIAL ORD',NULL,'YES','NO',NULL,8400000,8200000,9200000,NULL,4200,0,NULL,NULL,NULL,NULL,'2015-10-29 15:43:49','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (332,1,2,'F31S-070','SPECTRUM SLIM & LIFT SHAPING PANTS (SKIN) 70',NULL,'YES','NO',NULL,8640000,0,9500000,NULL,4320,0,NULL,NULL,NULL,NULL,'2015-10-29 15:57:02','CANAI02','2015-11-16 17:56:43','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (333,1,2,'F31S-076','SPECTRUM SLIM & LIFT SHAPING PANTS (SKIN) 76',NULL,'YES','NO',NULL,8640000,0,9500000,NULL,4320,0,NULL,NULL,NULL,NULL,'2015-10-29 16:16:43','CANAI02','2015-11-18 12:56:32','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (334,1,2,'F31S-082','SPECTRUM SLIM & LIFT SHAPING PANTS (SKIN) 82',NULL,'YES','NO',NULL,8640000,0,9500000,NULL,4320,0,NULL,NULL,NULL,NULL,'2015-10-29 16:17:11','CANAI02','2015-11-18 12:57:31','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (335,1,2,'F31S-090','SPECTRUM SLIM & LIFT SHAPING PANTS (SKIN) 90',NULL,'YES','NO',NULL,8640000,0,9500000,NULL,4320,0,NULL,NULL,NULL,NULL,'2015-10-29 16:19:26','CANAI02','2015-11-18 12:58:13','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (336,1,2,'F31S-098','SPECTRUM SLIM & LIFT SHAPING PANTS (SKIN) 98',NULL,'YES','NO',NULL,8640000,0,9500000,NULL,4320,0,NULL,NULL,NULL,NULL,'2015-10-29 16:20:16','CANAI02','2015-11-18 13:09:59','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (337,1,2,'F32S-082','SPECTRUM WAIST GIRDLE (SKIN) 82',NULL,'YES','NO',NULL,5400000,0,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-29 16:21:11','CANAI02','2015-11-16 18:01:21','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (338,1,2,'F32S-090','SPECTRUM WAIST GIRDLE (SKIN) 90',NULL,'YES','NO',NULL,5400000,0,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-29 16:21:35','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (339,1,2,'F32S-098','SPECTRUM WAIST GIRDLE (SKIN) 98',NULL,'YES','NO',NULL,5400000,0,6200000,NULL,2700,0,NULL,NULL,NULL,NULL,'2015-10-29 16:22:01','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (340,1,2,'F33S-070','SPECTRUM MINI BOTTOM 3 INCI (SKIN) 70',NULL,'YES','NO',NULL,6000000,5900000,6800000,NULL,3000,0,NULL,NULL,NULL,NULL,'2015-10-29 16:23:47','CANAI02','2015-11-16 18:10:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (341,1,2,'F33S-076','SPECTRUM MINI BOTTOM 3 INCI (SKIN) 76',NULL,'YES','NO',NULL,6000000,5900000,6800000,NULL,3000,0,NULL,NULL,NULL,NULL,'2015-10-29 16:24:20','CANAI02','2015-11-18 12:53:39','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (342,1,2,'F33S-082','SPECTRUM MINI BOTTOM 3 INCI (SKIN) 82',NULL,'YES','NO',NULL,6000000,5900000,6800000,NULL,3000,0,NULL,NULL,NULL,NULL,'2015-10-29 16:24:40','CANAI02','2015-11-18 12:54:24','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (343,1,2,'F33S-090','SPECTRUM MINI BOTTOM 3 INCI (SKIN) 90',NULL,'YES','NO',NULL,6000000,5900000,6800000,NULL,3000,0,NULL,NULL,NULL,NULL,'2015-10-29 16:24:57','CANAI02','2015-11-18 12:55:19','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (344,1,2,'F33S-098','SPECTRUM MINI BOTTOM 3 INCI (SKIN) 98',NULL,'YES','NO',NULL,6000000,5900000,6800000,NULL,3000,0,NULL,NULL,NULL,NULL,'2015-10-29 16:25:18','CANAI02','2015-11-18 12:55:55','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (345,1,2,'F01W-80C','SPECTRUM BRA (WHITE) 80C',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 16:51:57','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (346,1,2,'F01W-80CC','SPECTRUM BRA (WHITE) 80CC',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 16:53:30','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (347,1,2,'F01W-80D','SPECTRUM BRA (WHITE) 80D',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 16:53:59','CANAI03','2015-10-29 17:44:34','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (348,1,2,'F01W-80E','SPECTRUM BRA (WHITE) 80E',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 16:57:35','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (349,1,2,'F01W-80F','SPECTRUM BRA (WHITE) 80F',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 16:58:25','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (350,1,2,'F01W-80G','SPECTRUM BRA (WHITE) 80G',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 16:59:36','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (351,1,2,'F01W-85B','SPECTRUM BRA (WHITE) 85B',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:03:14','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (352,1,2,'F01W-85C','SPECTRUM BRA (WHITE) 85C',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:04:03','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (353,1,2,'F01W-85CC','SPECTRUM BRA (WHITE) 85CC',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:05:26','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (354,1,2,'F01W-85D','SPECTRUM BRA (WHITE) 85D',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:06:05','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (355,1,2,'F01W-85E','SPECTRUM BRA (WHITE) 85E',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:06:33','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (356,1,2,'F01W-85F','SPECTRUM BRA (WHITE) 85F',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:07:26','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (357,1,2,'F01W-85G','SPECTRUM BRA (WHITE) 85G',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:08:03','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (358,1,2,'F01W-90B','SPECTRUM BRA (WHITE) 90B',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:08:49','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (359,1,2,'F01W-90C','SPECTRUM BRA (WHITE) 90C',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:09:22','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (360,1,2,'F01W-90D','SPECTRUM BRA (WHITE) 90D',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:09:59','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (361,1,2,'F01W-90E','SPECTRUM BRA (WHITE) 90E',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:10:30','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (362,1,2,'F01W-90F','SPECTRUM BRA (WHITE) 90F',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:11:03','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (363,1,2,'F01W-90G','SPECTRUM BRA (WHITE) 90G',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:11:32','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (364,1,2,'F01W-95B','SPECTRUM BRA (WHITE) 95B',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:12:31','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (365,1,2,'F01W-95C','SPECTRUM BRA (WHITE) 95C',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:13:09','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (366,1,2,'F01W-95D','SPECTRUM BRA (WHITE) 95D',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:13:51','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (367,1,2,'F01W-95E','SPECTRUM BRA (WHITE) 95E',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:14:21','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (368,1,2,'F01W-95F','SPECTRUM BRA (WHITE) 85F',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:15:01','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (369,1,2,'F01W-95G','SPECTRUM BRA (WHITE) 95G',NULL,'YES','NO',NULL,3360000,0,4200000,NULL,1680,0,NULL,NULL,NULL,NULL,'2015-10-29 17:15:33','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (370,1,2,'F02B-064','SPECTRUM WAIST GIRDLE (BLACK) 64',NULL,'YES','NO',NULL,3600000,0,4400000,NULL,1800,0,NULL,NULL,NULL,NULL,'2015-10-29 17:18:06','CANAI03','2015-11-10 18:45:58','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (371,1,2,'F02B-070','SPECTRUM WAIST GIRDLE (BLACK) 70',NULL,'YES','NO',NULL,3600000,0,4400000,NULL,1800,0,NULL,NULL,NULL,NULL,'2015-10-29 17:22:06','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (372,1,2,'F02B-076','SPECTRUM WAIST GIRDLE (BLACK) 76',NULL,'YES','NO',NULL,3600000,0,4400000,NULL,1800,0,NULL,NULL,NULL,NULL,'2015-10-29 17:22:52','CANAI03','2015-11-10 18:46:30','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (373,1,2,'F02B-082','SPECTRUM WAIST GIRDLE (BLACK) 82',NULL,'YES','NO',NULL,3600000,0,4400000,NULL,1800,0,NULL,NULL,NULL,NULL,'2015-10-29 17:23:23','CANAI03','2015-11-10 18:46:47','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (374,1,2,'F02B-090','SPECTRUM WAIST GIRDLE (BLACK) 90',NULL,'YES','NO',NULL,3600000,0,4400000,NULL,1800,0,NULL,NULL,NULL,NULL,'2015-10-29 17:23:58','CANAI03','2015-11-10 18:47:06','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (375,1,2,'F02B-098','SPECTRUM WAIST GIRDLE (BLACK) 98',NULL,'YES','NO',NULL,3600000,0,4400000,NULL,1800,0,NULL,NULL,NULL,NULL,'2015-10-29 17:24:31','CANAI03','2015-11-10 18:47:23','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (376,1,2,'F03B-064','SPECTRUM SLEEVELESS BRALETTE (BLACK) 64',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:27:53','CANAI03','2015-11-10 18:47:54','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (377,1,2,'F03B-070','SPECTRUM SLEEVELESS BRALETTE (BLACK) 70',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:28:49','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (378,1,2,'F03B-076','SPECTRUM SLEEVELESS BRALETTE (BLACK) 076',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:29:44','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (379,1,2,'F03B-082','SPECTRUM SLEEVELESS BRALETTE (BLACK) 82',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:30:18','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (380,1,2,'F03B-090','SPECTRUM SLEEVELESS BRALETTE (BLACK) 90',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:30:50','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (381,1,2,'F03B-098','SPECTRUM SLEEVELESS BRALETTE (BLACK) 98',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:31:26','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (382,1,2,'F03B-106','SPECTRUM SLEEVELESS BRALETTE (BLACK) 106',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:32:10','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (383,1,2,'F03B-114','SPECTRUM SLEEVELESS BRALETTE (BLACK) 114',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:32:43','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (384,1,2,'F03B-122','SPECTRUM SLEEVELESS BRALETTE (BLACK) 122',NULL,'YES','NO',NULL,4560000,5400000,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:33:21','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (385,1,2,'F03W-064','SPECTRUM SLEEVELESS BRALETTE (WHITE) 64',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:34:36','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (386,1,2,'F03W-070','SPECTRUM SLEEVELESS BRALETTE (WHITE) 70',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:35:05','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (387,1,2,'F03W-076','SPECTRUM SLEEVELESS BRALETTE (WHITE) 76',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:35:40','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (388,1,2,'F03W-082','SPECTRUM SLEEVELESS BRALETTE (WHITE) 82',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:36:16','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (389,1,2,'F03W-090','SPECTRUM SLEEVELESS BRALETTE (WHITE) 90',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:36:50','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (390,1,2,'F03W-098','SPECTRUM SLEEVELESS BRALETTE (WHITE) 98',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:37:22','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (391,1,2,'F03W-106','SPECTRUM SLEEVELESS BRALETTE (WHITE) 106',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:37:56','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (392,1,2,'F03W-114','SPECTRUM SLEEVELESS BRALETTE (WHITE) 114',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:38:26','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (393,1,2,'F03W-122','SPECTRUM SLEEVELESS BRALETTE (WHITE) 122',NULL,'YES','NO',NULL,4560000,0,5400000,NULL,2280,0,NULL,NULL,NULL,NULL,'2015-10-29 17:38:59','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (394,1,2,'F04B-064','SPECTRUM MID-SLEEVES BRALETTE (BLACK) 64',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:41:11','CANAI03','2015-11-10 18:57:36','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (395,1,2,'F04B-070','SPECTRUM MID-SLEEVES BRALETTE (BLACK) 70',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:41:55','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (396,1,2,'F04B-076','SPECTRUM MID-SLEEVES BRALETTE (BLACK) 76',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:42:27','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (397,1,2,'F04B-082','SPECTRUM MID-SLEEVES BRALETTE (BLACK) 82',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:43:09','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (398,1,2,'F04B-090','SPECTRUM MID-SLEEVES BRALETTE (BLACK) 90',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:43:47','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (399,1,2,'F04B-098','SPECTRUM MID-SLEEVES BRALETTE (BLACK) 98',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:44:14','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (400,1,2,'F04B-106','SPECTRUM MID-SLEEVES BRALETTE (BLACK) 106',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:44:48','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (401,1,2,'F04B-114','SPECTRUM MID-SLEEVES BRALETTE (BLACK) 114',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:45:19','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (402,1,2,'F04B-122','SPECTRUM MID-SLEEVES BRALETTE (BLACK) 122',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:45:52','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (403,1,2,'F04B-SO','SPECTRUM MID-SLEEVES BRALETTE (BLACK) SPECIAL ORDE',NULL,'YES','NO',NULL,5760000,5900000,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:47:05','CANAI03','2015-11-10 19:15:10','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (404,1,2,'F04W-064','SPECTRUM MID-SLEEVES BRALETTE (WHITE) 064 ',NULL,'YES','NO',NULL,5760000,0,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:48:06','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (405,1,2,'F04W-070','SPECTRUM MID-SLEEVES BRALETTE (WHITE) 70',NULL,'YES','NO',NULL,5760000,0,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:48:33','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (406,1,2,'F04W-076','SPECTRUM MID-SLEEVES BRALETTE (WHITE) 76',NULL,'YES','NO',NULL,5760000,0,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:49:09','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (407,1,2,'F04W-082','SPECTRUM MID-SLEEVES BRALETTE (WHITE) 82',NULL,'YES','NO',NULL,5760000,0,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:49:48','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (408,1,2,'F04W-090','SPECTRUM MID-SLEEVES BRALETTE (WHITE) 90',NULL,'YES','NO',NULL,5760000,0,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:50:23','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (409,1,2,'F04W-098','SPECTRUM MID-SLEEVES BRALETTE (WHITE) 98',NULL,'YES','NO',NULL,5760000,0,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:50:59','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (410,1,2,'F04W-106','SPECTRUM MID-SLEEVES BRALETTE (WHITE) 106',NULL,'YES','NO',NULL,5760000,0,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:51:39','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (411,1,2,'F04W-114','SPECTRUM MID-SLEEVES BRALETTE (WHITE) 114',NULL,'YES','NO',NULL,5760000,0,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:52:28','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (412,1,2,'F04W-122','SPECTRUM MID-SLEEVES BRALETTE (WHITE) 122',NULL,'YES','NO',NULL,5760000,0,6600000,NULL,2880,0,NULL,NULL,NULL,NULL,'2015-10-29 17:53:01','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (413,1,2,'F05B-064','SPECTRUM LONG-SLEEVES BRALETTE (BLACK) 64',NULL,'YES','NO',NULL,5880000,5900000,6700000,NULL,2940,0,NULL,NULL,NULL,NULL,'2015-10-29 17:55:36','CANAI03','2015-11-10 19:02:35','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (414,1,2,'F05B-070','SPECTRUM LONG-SLEEVES BRALETTE (BLACK) 70',NULL,'YES','NO',NULL,5880000,5900000,6700000,NULL,2940,0,NULL,NULL,NULL,NULL,'2015-10-29 17:56:34','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (415,1,2,'F05B-076','SPECTRUM LONG-SLEEVES BRALETTE (BLACK) 76',NULL,'YES','NO',NULL,5880000,5900000,6700000,NULL,2940,0,NULL,NULL,NULL,NULL,'2015-10-29 17:57:51','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (416,1,2,'F05B-082','SPECTRUM LONG-SLEEVES BRALETTE (BLACK) 82',NULL,'YES','NO',NULL,5880000,5900000,6700000,NULL,2940,0,NULL,NULL,NULL,NULL,'2015-10-29 17:58:25','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (417,1,2,'F05B-090','SPECTRUM LONG-SLEEVES BRALETTE (BLACK) 90',NULL,'YES','NO',NULL,5880000,5900000,6700000,NULL,2940,0,NULL,NULL,NULL,NULL,'2015-10-29 17:58:58','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (418,1,2,'F05B-098','SPECTRUM LONG-SLEEVES BRALETTE (BLACK) 98',NULL,'YES','NO',NULL,5880000,5900000,6700000,NULL,2940,0,NULL,NULL,NULL,NULL,'2015-10-29 17:59:23','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (419,1,2,'F05B-106','SPECTRUM LONG-SLEEVES BRALETTE (BLACK) 106',NULL,'YES','NO',NULL,5880000,5900000,6700000,NULL,2940,0,NULL,NULL,NULL,NULL,'2015-10-29 17:59:48','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (420,1,2,'F05B-114','SPECTRUM LONG-SLEEVES BRALETTE (BLACK) 114',NULL,'YES','NO',NULL,5880000,5900000,6700000,NULL,2940,0,NULL,NULL,NULL,NULL,'2015-10-29 18:00:19','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (421,1,2,'F05B-122','SPECTRUM LONG-SLEEVES BRALETTE (BLACK) 122',NULL,'YES','NO',NULL,5880000,5900000,6700000,NULL,2940,0,NULL,NULL,NULL,NULL,'2015-10-29 18:00:47','CANAI03','0000-00-00 00:00:00','CANAI03')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (422,1,2,'AS10PCB-XS','SPECTRUM BRAIN PC CAP (BLACK) - XS',NULL,'YES','NO',NULL,0,0,0,NULL,0,0,NULL,NULL,NULL,NULL,'2015-11-15 12:52:12','CANAI01','2016-08-29 11:05:26','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (423,1,2,'AS10PCB-S','SPECTRUM BRAIN PC CAP (BLACK) - S',NULL,'YES','NO',NULL,0,0,0,NULL,0,0,NULL,NULL,NULL,NULL,'2015-11-15 12:53:04','CANAI01','2016-08-29 11:04:40','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (424,1,2,'AS10PCB-L','SPECTRUM BRAIN PC CAP (BLACK) - L',NULL,'YES','NO',NULL,0,0,0,NULL,0,0,NULL,NULL,NULL,NULL,'2015-11-15 12:54:02','CANAI01','2016-08-29 11:03:56','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (425,1,1,'EYE','EYESHADE',NULL,'YES','NO',NULL,0,0,0,NULL,0,0,NULL,NULL,NULL,NULL,'2016-03-05 09:43:15','CANAI911','0000-00-00 00:00:00','CANAI911')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (426,1,1,'PCCAP-XS','BRAIN PC CAP (XS)',NULL,'YES','YES',NULL,2700000,2700000,2700000,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-27 13:47:40','CANAI01','0000-00-00 00:00:00','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (427,1,1,'PCCAP-S','BRAIN PC CAP (S)',NULL,'YES','YES',NULL,2700000,2700000,0,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-27 15:44:48','CANAI01','0000-00-00 00:00:00','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (428,1,1,'PCCAP-L','BRAIN PC CAP (L)',NULL,'YES','YES',NULL,2700000,2700000,0,NULL,0,0,NULL,NULL,NULL,NULL,'2015-10-27 15:48:28','CANAI01','0000-00-00 00:00:00','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (429,1,1,'AS09YB','SPECTRUM ELEGANT SCARF GENERATION 2 (BLACK)',NULL,'YES','NO',NULL,5900000,0,5900000,NULL,0,0,NULL,NULL,NULL,NULL,'2016-04-04 10:07:29','CANAI01','2016-08-29 11:03:10','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (430,1,1,'AS09YO','SPECTRUM ELEGANT SCARF GENERATION 2 (ORANGE)',NULL,'YES','NO',NULL,5900000,0,5900000,NULL,0,0,NULL,NULL,NULL,NULL,'2016-04-04 10:08:58','CANAI01','2016-08-29 11:03:32','CANAI01')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (431,1,2,'AS09(YB)','SPECTRUM ELEGANT SCARF GENERATION 2 (BLACK)',NULL,'YES','NO',NULL,6200000,0,7100000,NULL,0,0,NULL,NULL,NULL,NULL,'2016-06-02 10:25:33','CANAI02','0000-00-00 00:00:00','CANAI02')");
				$this->dbforge->add_field("INSERT INTO `product`(id, id_category, produk_type, code, name, deskripsi, sales, manufacture, slug, price, price_cust, price_register, cashback, pv, bv, diskon, image_local, image_url, weight, created_at, created_by, updated_at, updated_by) VALUES (432,1,2,'AS09(YO)','SPECTRUM ELEGANT SCARF GENERATION 2 (ORANGE)',NULL,'YES','NO',NULL,6200000,0,7100000,NULL,0,0,NULL,NULL,NULL,NULL,'2016-06-02 10:26:03','CANAI02','0000-00-00 00:00:00','CANAI02')");
				
				$this->dbforge->add_field('ALTER TABLE `product` ADD CONSTRAINT `id_category_fk` FOREIGN KEY (`id_category`) REFERENCES `product_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE');
				$this->dbforge->add_field('ALTER TABLE `product` ADD CONSTRAINT `produk_type` FOREIGN KEY (`produk_type`) REFERENCES `product_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE');
				
				$this->dbforge->create_table('product');

				/*
				************************************************************
				* Add table product_category
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'name' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `product_category`(id, name) VALUES (1,'Cosmeceutical Product')");
				$this->dbforge->add_field("INSERT INTO `product_category`(id, name) VALUES (2,'Nutraceutical Product')");
				$this->dbforge->add_field("INSERT INTO `product_category`(id, name) VALUES (3,'Phytonutraceutical Product')");
				
				$this->dbforge->create_table('product_category');

				/*
				************************************************************
				* Add table product_type
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'type_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `product_type`(id, type_name) VALUES (1,'Starter Kit')");
				$this->dbforge->add_field("INSERT INTO `product_type`(id, type_name) VALUES (2,'Product')");
				$this->dbforge->add_field("INSERT INTO `product_type`(id, type_name) VALUES (3,'Advertising')");
				
				$this->dbforge->create_table('product_type');

				/*
				************************************************************
				* Add table province
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'province_id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'province_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'country' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				));
				$this->dbforge->add_key('province_id', TRUE);
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (1,'Bali',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (2,'Bangka Belitung',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (3,'Banten',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (4,'Bengkulu',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (5,'DI Yogyakarta',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (6,'DKI Jakarta',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (7,'Gorontalo',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (8,'Jambi',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (9,'Jawa Barat',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (10,'Jawa Tengah',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (11,'Jawa Timur',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (12,'Kalimantan Barat',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (13,'Kalimantan Selatan',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (14,'Kalimantan Tengah',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (15,'Kalimantan Timur',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (16,'Kalimantan Utara',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (17,'Kepulauan Riau',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (18,'Lampung',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (19,'Maluku',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (20,'Maluku Utara',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (21,'Nanggroe Aceh Darussalam (NAD)',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (22,'Nusa Tenggara Barat (NTB)',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (23,'Nusa Tenggara Timur (NTT)',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (24,'Papua',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (25,'Papua Barat',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (26,'Riau',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (27,'Sulawesi Barat',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (28,'Sulawesi Selatan',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (29,'Sulawesi Tengah',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (30,'Sulawesi Tenggara',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (31,'Sulawesi Utara',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (32,'Sumatera Barat',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (33,'Sumatera Selatan',107)");
				$this->dbforge->add_field("INSERT INTO `province`(province_id, province_name, country) VALUES (34,'Sumatera Utara',107)");
				
				$this->dbforge->create_table('province');

				/*
				************************************************************
				* Add table seo
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'meta_name' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'meta_content' => array(
						'type' => 'VARCHAR',
						'constraint' => '150',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `seo`(id, meta_name, meta_content) VALUES (1,'content-type','text/html')");
				$this->dbforge->add_field("INSERT INTO `seo`(id, meta_name, meta_content) VALUES (2,'description','website jual beli peralatan laboratorium kimia termurah dan terlengkap')");
				$this->dbforge->add_field("INSERT INTO `seo`(id, meta_name, meta_content) VALUES (4,'author','lusara project')");
				$this->dbforge->add_field("INSERT INTO `seo`(id, meta_name, meta_content) VALUES (5,'language','id')");
				$this->dbforge->add_field("INSERT INTO `seo`(id, meta_name, meta_content) VALUES (6,'geo.placename','Indonesia')");
				$this->dbforge->add_field("INSERT INTO `seo`(id, meta_name, meta_content) VALUES (7,'audience','all')");
				$this->dbforge->add_field("INSERT INTO `seo`(id, meta_name, meta_content) VALUES (8,'rating','general')");
				$this->dbforge->add_field("INSERT INTO `seo`(id, meta_name, meta_content) VALUES (9,'refresh',10)");
				$this->dbforge->add_field("INSERT INTO `seo`(id, meta_name, meta_content) VALUES (10,'revisit-after','1 days')");
				
				$this->dbforge->create_table('seo');

				/*
				************************************************************
				* Add table stockies_type
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'type' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'deskripsi' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `stockies_type`(id, type, deskripsi) VALUES (1,'eMobile','')");
				$this->dbforge->add_field("INSERT INTO `stockies_type`(id, type, deskripsi) VALUES (2,'Stockist','')");
				$this->dbforge->add_field("INSERT INTO `stockies_type`(id, type, deskripsi) VALUES (3,'Stockist Klinik','')");
				
				$this->dbforge->create_table('stockies_type');

				/*
				************************************************************
				* Add table stockiest
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'id' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'sponsor_id' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'group_id' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'warehouse_id' => array(
						'type' => 'INT',
						'constraint' => '2',
						),
				
						'name' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						),
				
						'no_stc' => array(
						'type' => 'VARCHAR',
						'constraint' => '30',
						),
				
						'ewallet' => array(
						'type' => 'FLOAT',
						),
				
						'type' => array(
						'type' => 'TINYINT',
						'constraint' => '4',
						),
				
						'status' => array(
						'type' => 'VARCHAR',
						'constraint' => '10',
						),
				
						'messenger' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						),
				
						'alamat' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						),
				
						'kota' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						),
				
						'telp' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'hp' => array(
						'type' => 'VARCHAR',
						'constraint' => '50',
						),
				
						'last_login' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'cashback' => array(
						'type' => 'FLOAT',
						),
				
						'zona_id' => array(
						'type' => 'TINYINT',
						'constraint' => '4',
						'default' => '1',
						),
				
						'created' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'createdby' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'updated' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				
						'updatedby' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						),
				));
				$this->dbforge->add_key('id', TRUE);
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (1,0,103,1,'ANDRIES','ANDRIE0101',0,103,'active','','Surabaya','SURABAYA','',2147483647,'0000-00-00 00:00:00',0,1,'2014-10-24 10:33:21','ERANANTA','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (2,0,102,1,'RASTIAN','RASTIA2707',0,102,'active','','Sukapura-Cilincing','Jakarta Utara','',2147483647,'0000-00-00 00:00:00',0,1,'2014-10-24 10:36:47','ERANANTA','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (3,0,0,1,'ROBERT DB','ROBERT0906',0,102,'inactive','','Cibubur','Kab.Bogor','',2147483647,'0000-00-00 00:00:00',0,1,'2014-10-24 10:39:43','ERANANTA','2015-12-28 17:35:41','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (4,0,0,1,'HARIADI LIM','HARIAD2504',0,103,'inactive','','Makassar','Sulawesi Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2014-10-24 10:44:15','ERANANTA','2014-12-10 17:29:20','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (5,0,102,1,'BUDI MULIA GINTING','BUDIMU1405',0,102,'active','','Padang Bulan','MEDAN','',2147483647,'0000-00-00 00:00:00',0,1,'2014-10-24 10:47:20','ERANANTA','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (6,0,102,1,'DAVID ODANG','DAVIDO0170',0,102,'active','','Kelapa Gading','Jakarta Utara','',2147483647,'0000-00-00 00:00:00',0,1,'2014-10-24 10:48:22','ERANANTA','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (7,0,0,1,'DELLA MORITA','DELLAM2267',0,102,'inactive','','Bekasi','Kab.Bekasi','',2147483647,'0000-00-00 00:00:00',0,1,'2014-10-24 10:50:07','ERANANTA','2014-12-10 17:28:48','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (8,0,102,1,'LIANA DEWI N.','LIANAD1411',0,102,'active','','Denpasar','BALI','',2147483647,'0000-00-00 00:00:00',0,1,'2014-10-24 10:51:03','ERANANTA','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (9,0,0,1,'RUSTAM BUDIMAN','RUSTAM0501',0,102,'inactive','','Makassar','Sulawesi Selatan','',811411687,'0000-00-00 00:00:00',0,1,'2014-11-04 16:46:16','ERANANTA','2014-12-10 17:29:05','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (10,0,0,1,'VONNY FONNARDY','VONNYF2302',0,102,'inactive','','Makassar','Sulawesi Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2014-11-17 12:42:45','SINTESA01','2014-12-10 17:28:12','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (11,0,0,1,'WAYAN HARIMAWAN, MPH, SP.GK/ FRANSISKA LAMATOA','DRAGUS1312',0,103,'active','','Sanglah - Denpasar','BALI','',2147483647,'0000-00-00 00:00:00',0,1,'2014-11-24 10:35:55','SINTESA01','2015-06-29 18:23:57','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (12,0,103,1,'  MARDIANA','MARDIA0306',0,103,'active','','Bekasi','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2014-11-25 15:42:22','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (13,0,0,1,'ERLY MARLINA','ERLYMA2403',0,102,'inactive','','Bekasi','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2014-11-25 15:43:34','SINTESA01','2015-03-02 15:25:00','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (14,0,102,1,'AZIZA NUR','AZIZAN0101',0,102,'active','','Bintaro Jaya','Jakarta Selatan','',816941590,'0000-00-00 00:00:00',0,1,'2014-12-09 14:56:40','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (15,0,0,1,'DRA.MURNIATI, MPD','DRAMUR1102',0,103,'active','','Purwakarta','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2014-12-09 14:58:14','SINTESA01','2014-12-22 14:50:02','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (16,0,102,1,'LENNY LOLONGAN, DR.','LENNYL1911',0,102,'active','','Makassar','Ujung Pandang - Sulawesi Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2014-12-09 15:06:09','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (17,0,103,1,'HARIADI LIM','HARIAD0464',0,103,'active','','Makassar','Ujung Pandang Sul-Sel','',2147483647,'0000-00-00 00:00:00',0,1,'2014-12-10 17:31:46','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (18,0,102,1,'DELLA MORITA','DELLAM1967',0,102,'active','','Bekasi','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2014-12-10 17:33:11','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (19,0,102,1,'VONNY FONNARDY','VONNYF2365',0,102,'active','','Makassar','Ujung Pandang Sul-Sel','',2147483647,'0000-00-00 00:00:00',0,1,'2014-12-10 17:34:30','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (20,0,0,1,'RUSTAM BUDIMAN','RUSTAM0152',0,102,'inactive','','Makassar','Ujung Pandang Sul-Sel','',811411687,'0000-00-00 00:00:00',0,1,'2014-12-10 17:35:37','SINTESA01','2015-12-28 17:36:17','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (21,0,102,1,'I WAYAN RAMLER','IWAYAN2905',0,102,'active','','Gianyar','BALI','',2147483647,'0000-00-00 00:00:00',0,1,'2015-01-08 13:07:07','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (22,0,103,1,'AGUS S.KAIDUN','AGUSSE3008',0,103,'active','','BANDUNG','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-01-08 13:08:53','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (23,0,0,1,'ERLY MARLINA','ERLYMA2403',0,102,'inactive','','Bekasi Timur','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-01-08 13:09:59','SINTESA01','2015-12-28 17:42:56','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (24,0,102,1,'LILIS SURYATI, SE','LILISS2006',0,102,'active','','Sumedang','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-01-08 13:11:01','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (25,0,102,1,'IRMA P HIPPY','IRMAPH3001',0,102,'active','','HR Rasuna Said','Jakarta Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-01-23 17:51:08','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (26,0,102,1,'GAMA KURNIAWAN','GAMATR2304',0,102,'active','','Jatirawamangun','Jakarta Timur','',2147483647,'0000-00-00 00:00:00',0,1,'2015-01-23 17:53:54','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (27,0,0,1,'AGUNG ESTI T.','AGUNGE0705',0,103,'active','','Bandung Barat','Jawa Barat','',817213885,'0000-00-00 00:00:00',0,1,'2015-01-23 17:55:43','SINTESA01','2015-05-05 14:41:32','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (28,0,103,1,'TOTO FM','TOTOFM2012',0,103,'active','','Medan','Sumatera Utara','',2147483647,'0000-00-00 00:00:00',0,1,'2015-01-23 17:56:51','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (29,0,0,1,'PARASIAN TOBING','PARASI0507',0,102,'inactive','','Jl. Jamin Ginting - Medan','Sumatera Utara','',2147483647,'0000-00-00 00:00:00',0,1,'2015-01-23 18:01:55','SINTESA01','2015-03-04 10:02:06','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (30,0,103,1,'CIPTO','NJAUWT0809',0,103,'active','','Pasar BSD K-156','Tangerang','',817828303,'0000-00-00 00:00:00',0,1,'2015-01-27 14:01:46','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (31,0,102,1,'NENDEN KRISTIANI','NENDEN0479',0,102,'active','','JL.DR.Djunjunan NO.126-128 Pasteur','Bandung','',2147483647,'0000-00-00 00:00:00',0,1,'2015-02-13 11:25:57','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (32,0,0,1,'AZHARI TASTIARA','AZHARI2309',0,102,'active','','Depok','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-02-13 11:26:51','SINTESA01','2015-12-28 17:00:32','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (33,0,0,1,'YULITA','YULITA0601',0,102,'active','','Jakarta Timur','DKI Jakarta','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-02 15:36:47','SINTESA01','2015-10-26 15:29:14','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (34,0,0,1,'VICTOR M. PANJAITAN','VICTOR1206',0,102,'active','','Cengkareng','Jakarta Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-03 12:13:21','SINTESA01','2015-03-03 12:15:39','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (35,0,102,1,'IDA BAGUS KETUT MANTRA','IDABAG0101',0,102,'active','','Denpasar','Bali','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-03 12:19:50','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (36,0,102,1,'ISWANTO','ISWANT2567',0,102,'active','','Depok','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-04 10:02:51','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (37,0,102,1,'JAMALUDDIN','JAMALU3006',0,102,'active','','Padang','Sumatera Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-10 15:22:20','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (38,0,103,1,'ALAMSYAH','ALAMSY1308',0,103,'active','','Pulogadung','Jakarta Timur','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-17 16:14:00','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (39,0,0,1,'AMINAH PAPEO','AMINAH1708',0,103,'active','','Jl. Una-Una no.15','Palu - Sulawesi Tengah','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-17 16:16:03','SINTESA01','2015-03-17 16:18:12','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (40,0,102,1,'ABDUL RACHIM','ABDULR1002',0,102,'active','','Denpasar','Bali','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-17 16:17:43','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (41,0,102,1,'FERRY N.SIAGIAN','FERRYN1610',0,102,'active','','Lubuk Linggau','Sumatera Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-18 14:09:05','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (42,0,102,1,'ERNY MARGARETA','ERNYMA0803',0,102,'active','','SAMARINDA','Kalimantan Timur','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-20 17:55:47','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (43,0,102,1,'RAKHMANIAR','RAKHMA1612',0,102,'active','','Lowa Bakung - Samarinda','Kalimantan Timur','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-30 19:03:11','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (44,0,103,1,'HJ IDA ZAIRITA','HJIDAZ2809',0,103,'active','','Jl.Brigjen Katamso - Samarinda','Kalimantan Timur','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-30 19:06:45','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (45,0,103,1,'HERU SUPRAYITNO','HERUSU0203',0,103,'active','','Villa Tanjung Harapan - Palembang','Sumatera Selatan','',811784539,'0000-00-00 00:00:00',0,1,'2015-03-30 19:09:02','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (46,0,102,1,'HASNIAHSYAH.DR','HASNIA2702',0,102,'active','','Tanjung Satu - Kota Palu','Sulawesi Tengah','',2147483647,'0000-00-00 00:00:00',0,1,'2015-03-30 19:11:27','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (47,0,102,1,'ROSITA','ROSITA0202',0,102,'active','','Bogor','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-04-01 17:42:31','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (48,0,102,1,'RASYID SUROTO','RASYID0511',0,102,'active','','Yogyakarta','Daerah Istimewa Yogyakarta','',2147483647,'0000-00-00 00:00:00',0,1,'2015-04-01 17:46:58','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (49,0,0,1,'NENDY','NENDEN1379',0,103,'active','','Bogor','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-04-01 17:49:27','SINTESA01','2015-05-25 14:22:55','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (50,0,102,1,'FIRZA','FIRZAN0107',0,102,'active','','Tomang','Jakarta Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-04-01 17:50:35','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (51,0,102,1,'FADIL ANSYAR','FADILA2303',0,102,'active','','Komp.Pusri Selayur','Palembang','',2147483647,'0000-00-00 00:00:00',0,1,'2015-04-01 17:51:51','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (52,0,102,1,'ENDANG SRI RUMIATI','ENDANG2508',0,102,'active','','Perum Griya Asri','Samarinda','',2147483647,'0000-00-00 00:00:00',0,1,'2015-04-01 17:53:06','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (53,0,102,1,'ULINNOHA, DR.','DRULIN0503',0,102,'active','','Pringsewu','Lampung','',2147483647,'0000-00-00 00:00:00',0,1,'2015-04-01 17:54:35','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (54,0,103,1,'YANSURNI','YANSUR0501',0,103,'active','','Jl. Suntai 1, Labuh Baru, Payung Sekaki','Pekanbaru - RIAU','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-05 14:43:59','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (55,0,103,1,'DEWI HARTINI','DEWIHA3006',0,103,'active','','Batam','KEPRI','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-05 14:47:58','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (56,0,102,1,'LANGGENG INDRAWATI','LANGGE0511',0,102,'active','','Surabaya','Jawa Timur','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-05 18:08:49','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (57,0,102,1,'ELMO MOELJANTO','ELMOMO0603',0,102,'active','','Tebet','Jakarta Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-05 18:09:45','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (58,0,102,1,'YUNATA D.CAHYONO','YUNATA3006',0,102,'active','','Jl. Petitenget No.2A','Denpasar - Bali','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-05 18:11:03','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (59,0,0,1,'NUNUNG N. ADNAN','NUNUNG0708',0,102,'inactive','','Jl. Jakarta No.37','Samarinda - Kaltim','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-05 18:12:21','SINTESA01','2015-06-11 18:28:05','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (60,0,103,1,'RIZKAN RAHMADI','RIZKAN0711',0,103,'active','','Jl. Rungkut Mapan Tengah I','Surabaya - Jawa Timur','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-05 18:14:18','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (61,0,103,1,'RA DEWI TD TJONDROKUSUMANINGRUM','RADEWI0210',0,103,'active','','Jl. Cik Di Tiro ','Yogyakarta (DIY)','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-05 18:42:55','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (62,0,102,1,'FEKY KARWUR','FEKYKA2811',0,102,'active','','Jl. Pramuka Semanda','Banjarmasin - Kalsel','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-05 18:54:55','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (63,0,102,1,'ROSALIN SILITONGA','ROSALI1302',0,102,'active','','Jl Cemara I','Jakarta','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-08 18:37:12','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (64,0,103,1,'CALVIONITA STEVANIE A.S','CALVIO2308',0,103,'active','','Kutai Kertanegara','Tenggarong - Kalimantan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-25 14:26:31','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (65,0,103,1,'HJ IDA ZAIRITA','HJIDAZ2809',0,103,'active','','Jl.Brigjen Katamso','Samarinda - Kalimantan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-25 14:27:39','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (66,0,103,1,'NUNUNG NOOR ADNAN','NUNUNG0708',0,103,'active','','Jl. Wildansari','Banjarmasin - Kalimantan Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-25 14:29:40','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (67,0,103,1,'RETNO INDRARTI BAMBANG','RETNOI0304',0,103,'active','','Balikpapan','Kalimantan Timur','',2147483647,'0000-00-00 00:00:00',0,1,'2015-05-25 14:32:21','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (68,0,0,1,'INGE KAWIHARDJA','INGEKA1001',0,103,'active','','Kelapa Gading','Jakarta Utara','','082114083326 / 087880066047','0000-00-00 00:00:00',0,1,'2015-05-29 17:51:40','SINTESA01','2015-05-29 18:01:07','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (69,0,102,1,'ERNIK TRESNAWATI','ERNIKT1106',0,102,'active','','Tengarong','Kutai Kertanegara - Kalimantan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-06-11 18:07:06','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (70,0,103,1,'ELIZA DESMITA','ELIZAD0412',0,103,'active','','Jl. Raya Griya','Lampung','',2147483647,'0000-00-00 00:00:00',0,1,'2015-06-22 13:05:12','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (71,0,102,1,'JOICE YULIANA ANITA','JOICEY1168',0,102,'active','','Jl Barkah','Jakarta Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-06-22 13:06:34','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (72,0,102,1,'AGNES ANITA PALEBANGAN','AGNESA2404',0,102,'active','','Kota Luwuk','Sulawesi Tengah','',2147483647,'0000-00-00 00:00:00',0,1,'2015-06-29 17:55:43','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (73,0,103,1,'EVITA SULISTIANA','EVITAS2603',0,103,'active','','Pasar Minggu','Jakarta Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-08-24 18:17:03','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (74,0,103,1,'DESIANA MASYHUR','DESIAN0312',0,103,'active','','Palembang','Sumatera Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-08-24 18:18:15','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (75,0,102,1,'DR HJ.NILAWATI','DRHJNI3010',0,102,'active','','Cirebon','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2015-09-16 18:44:03','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (76,0,102,1,'LENY MARLENA','LENYMA1212',0,102,'active','','Jl. Pancur Mas','Bengkulu','',2147483647,'0000-00-00 00:00:00',0,1,'2015-10-26 15:30:55','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (77,0,102,1,'LIA CHARWITA','LIACHA3112',0,102,'active','','Casablanca Mansion','Jakarta Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2015-10-26 15:37:54','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (78,0,102,1,'AHMAD KOSASIH','AHMADK1107',0,102,'active','','Jakarta Selatan','DKI','',2147483647,'0000-00-00 00:00:00',0,1,'2015-10-26 15:39:12','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (79,0,103,1,'RISMAWATI AS','RISMAW0108',0,103,'active','','Palangkaraya','Kalimantan Tengah','',2147483647,'0000-00-00 00:00:00',0,1,'2015-11-10 17:31:43','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (80,0,103,1,'HARUN SUHARYONO','HARUNS0307',0,103,'active','','Catur Tunggal Depok','Yogyakarta','',2147483647,'0000-00-00 00:00:00',0,1,'2015-12-28 17:00:00','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (81,0,102,1,'KRISNA ADITIA ARISTIAN','KRISNA2404',0,102,'active','','DKI','Jakarta Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2016-01-04 08:13:12','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (82,0,103,1,'MALYANI','MAYLYA2707',0,103,'active','','Ciputat ','Jakarta Selatan','',2147483647,'0000-00-00 00:00:00',0,1,'2016-03-02 19:03:37','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (83,0,0,1,'DR A.R.ADJI HOESODO','ADJIHO2210',0,103,'inactive','','STOCKIST KLINIK - Depok','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2016-05-12 17:05:09','SINTESA01','2016-07-15 11:47:19','SINTESA01')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (84,0,102,1,'DR. PHILLIP HASANUDIN','PHILIP1004',0,102,'active','','Pondok Indah ','Jakarta Selatan',2147483647,2147483647,'0000-00-00 00:00:00',0,1,'2016-06-17 16:44:55','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (85,0,102,1,'SUNARJO','SUNARJ0501',0,102,'active','','Pademangan','Jakarta Utara','',2147483647,'0000-00-00 00:00:00',0,1,'2016-06-21 16:40:40','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (86,0,102,1,'GERHARD MAHA','GERHAR1311',0,102,'active','','Cibubur','Jakarta Timur','',2147483647,'0000-00-00 00:00:00',0,1,'2016-06-21 17:08:12','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (87,0,104,1,' DR A.R.ADJI HOESODO ','ADJIHO2210',0,104,'active','','Depok ','Jawa Barat','',2147483647,'0000-00-00 00:00:00',0,1,'2016-07-15 11:45:13','SINTESA01','0000-00-00 00:00:00','')");
				$this->dbforge->add_field("INSERT INTO `stockiest`(id, sponsor_id, group_id, warehouse_id, name, no_stc, ewallet, type, status, messenger, alamat, kota, telp, hp, last_login, cashback, zona_id, created, createdby, updated, updatedby) VALUES (88,0,104,1,'DRA. RATNA SYLVIA','RATNAS0406',0,104,'active','','Medan','Sumatera Utara','',811641367,'0000-00-00 00:00:00',0,1,'2016-07-15 11:49:20','SINTESA01','0000-00-00 00:00:00','')");
				
				$this->dbforge->create_table('stockiest');

				/*
				************************************************************
				* Add table user
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'userid' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'username' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'is_master' => array(
						'type' => 'CHAR',
						'constraint' => '1',
						'default' => 'N',
						'null' => TRUE,
						),
				
						'group' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'rpassword' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						),
				
						'password' => array(
						'type' => 'VARCHAR',
						'constraint' => '200',
						),
				
						'fullname' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'nickname' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'gender' => array(
						'type' => 'VARCHAR',
						'constraint' => '10',
						'null' => TRUE,
						),
				
						'DOB' => array(
						'type' => 'VARCHAR',
						'constraint' => '10',
						'null' => TRUE,
						),
				
						'address' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'city' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'postcode' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'province' => array(
						'type' => 'VARCHAR',
						'constraint' => '100',
						'null' => TRUE,
						),
				
						'country' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'mobile' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'email' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'avatar' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'activation_key' => array(
						'type' => 'TEXT',
						'null' => TRUE,
						),
				
						'is_active' => array(
						'type' => 'CHAR',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'token' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'created_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'updated_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				
						'deleted_at' => array(
						'type' => 'VARCHAR',
						'constraint' => '20',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('userid', TRUE);
				$this->dbforge->add_field("INSERT INTO `user`(userid, username, is_master, group, rpassword, password, fullname, nickname, gender, DOB, address, city, postcode, province, country, mobile, email, avatar, activation_key, is_active, token, created_at, updated_at, deleted_at) VALUES (1,'admin','Y',1,'admin','76aa4af9309a8aee2d9366838f4aa176cf1138cf','administrator','Bertrand','male','1986-05-15','perum griya kencana blok A',79,16161,9,107,789458445,'admin@mail.com','avatar/admin/cee0f7d6df640d0ef8dd6f9bee31928f.jpg',NULL,1,'ec1a8561a95af04021431e67470210f4','2017-01-26 11:51:30','2017-01-05 14:43:35',NULL)");
				$this->dbforge->add_field("INSERT INTO `user`(userid, username, is_master, group, rpassword, password, fullname, nickname, gender, DOB, address, city, postcode, province, country, mobile, email, avatar, activation_key, is_active, token, created_at, updated_at, deleted_at) VALUES (13,'anonymous','N',2,'anonymous','1c5f61032f4d6bd5ca8ad7852656e40d4921e12d',NULL,NULL,'male',NULL,NULL,1,NULL,21,107,NULL,'anonymous@mail.com','image/image/maleava.png',NULL,1,'7c7074ca4477c1457ce93248b22c49b6',NULL,NULL,NULL)");
				$this->dbforge->add_field("INSERT INTO `user`(userid, username, is_master, group, rpassword, password, fullname, nickname, gender, DOB, address, city, postcode, province, country, mobile, email, avatar, activation_key, is_active, token, created_at, updated_at, deleted_at) VALUES (14,'titotest','N',2,'titotest','3b94b5383c1106070f9dcec8a740a00623555790',NULL,NULL,'male',NULL,NULL,1,NULL,21,107,NULL,'tito@mail.com','image/image/maleava.png',NULL,1,'1df1704c168986c96c8235b1a6b42a5d',NULL,NULL,NULL)");
				
				$this->dbforge->add_field('ALTER TABLE `user` ADD CONSTRAINT `fk_user_group` FOREIGN KEY (`group`) REFERENCES `usergroup` (`groupid`) ON DELETE CASCADE ON UPDATE CASCADE');
				
				$this->dbforge->create_table('user');

				/*
				************************************************************
				* Add table usergroup
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'groupid' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'groupname' => array(
						'type' => 'VARCHAR',
						'constraint' => '45',
						'null' => TRUE,
						),
				
						'set_as_default' => array(
						'type' => 'CHAR',
						'constraint' => '1',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('groupid', TRUE);
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (1,'administrator',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (2,'Finance & Accounting',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (3,'Warehouse',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (4,'Direktur',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (5,'Marketing Relation',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (6,'Data Entry CS',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (7,'Customer Service',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (8,'Ethical-Admin',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (9,'Ethical-Warehouse',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (10,'Member',1)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (11,'eMobile',0)");
				$this->dbforge->add_field("INSERT INTO `usergroup`(groupid, groupname, set_as_default) VALUES (12,'Stockist',0)");
				
				$this->dbforge->create_table('usergroup');

				/*
				************************************************************
				* Add table usergroup_access
				************************************************************
				*/

				$this->dbforge->add_field(array(
						'accessid' => array(
						'type' => 'INT',
						'constraint' => '11',
						'unsigned' => TRUE,
						'auto_increment' => TRUE,
						),
				
						'groupid' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'menusid' => array(
						'type' => 'INT',
						'constraint' => '11',
						),
				
						'inserting' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'updating' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'deleting' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'viewing' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				
						'printing' => array(
						'type' => 'TINYINT',
						'constraint' => '1',
						'null' => TRUE,
						),
				));
				$this->dbforge->add_key('accessid', TRUE);
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (1,1,24,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (2,1,1,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (3,1,2,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (4,1,3,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (5,1,9,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (6,1,10,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (7,1,12,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (8,1,13,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (9,1,17,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (10,1,18,0,0,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (11,1,19,0,0,0,1,1)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (12,1,21,1,1,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (13,1,29,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (14,1,4,0,1,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (15,1,22,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (16,1,23,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (17,1,5,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (18,1,25,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (19,1,34,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (20,1,36,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (21,1,39,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (22,1,40,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (23,1,41,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (24,1,42,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (25,1,7,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (26,1,8,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (27,1,14,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (28,1,15,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (29,1,16,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (30,1,26,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (31,1,35,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (32,1,37,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (33,1,38,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (34,1,30,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (35,1,31,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (36,1,32,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (37,1,33,1,1,1,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (38,7,1,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (39,7,2,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (40,7,3,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (41,7,9,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (42,7,10,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (43,7,12,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (44,7,13,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (45,7,17,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (46,7,18,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (47,7,19,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (48,7,21,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (49,7,29,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (50,7,4,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (51,7,22,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (52,7,23,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (53,7,5,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (54,7,24,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (55,7,25,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (56,7,34,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (57,7,36,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (58,7,39,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (59,7,40,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (60,7,41,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (61,7,42,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (62,7,7,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (63,7,8,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (64,7,14,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (65,7,15,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (66,7,16,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (67,7,26,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (68,7,35,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (69,7,37,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (70,7,38,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (71,7,30,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (72,7,31,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (73,7,32,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (74,7,33,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (75,6,1,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (76,6,2,0,0,0,1,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (77,6,3,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (78,6,9,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (79,6,10,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (80,6,12,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (81,6,13,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (82,6,17,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (83,6,18,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (84,6,19,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (85,6,21,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (86,6,29,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (87,6,4,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (88,6,22,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (89,6,23,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (90,6,5,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (91,6,24,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (92,6,25,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (93,6,34,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (94,6,36,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (95,6,39,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (96,6,40,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (97,6,41,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (98,6,42,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (99,6,7,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (100,6,8,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (101,6,14,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (102,6,15,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (103,6,16,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (104,6,26,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (105,6,35,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (106,6,37,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (107,6,38,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (108,6,30,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (109,6,31,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (110,6,32,0,0,0,0,0)");
				$this->dbforge->add_field("INSERT INTO `usergroup_access`(accessid, groupid, menusid, inserting, updating, deleting, viewing, printing) VALUES (111,6,33,0,0,0,0,0)");
				
				$this->dbforge->add_field('ALTER TABLE `usergroup_access` ADD CONSTRAINT `menusid_fk` FOREIGN KEY (`menusid`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE');
				$this->dbforge->add_field('ALTER TABLE `usergroup_access` ADD CONSTRAINT `uaid_fk` FOREIGN KEY (`groupid`) REFERENCES `usergroup` (`groupid`) ON DELETE CASCADE ON UPDATE CASCADE');
				
				$this->dbforge->create_table('usergroup_access');

		}

		public function down(){
				// lakukan function down secara manual
		}
}