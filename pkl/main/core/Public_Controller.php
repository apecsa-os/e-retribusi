<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Public_Controller extends FI_Controller
{

    function __construct()
    {
        parent:: __construct();
        if($this->session->has_userdata(DEF_APP.'_'.SESS_PUB.'token') == null && $this->session->has_userdata(DEF_APP.'_'.SESS_PUB.'loggedin') == null ){
            redirect('login');
        }
        else{
            $this->load->helper('cookie');
            $this->load->model(array('public/main_model'));
            self::getting_cookies();
            self::get_visitors();
            $matches                        = $this->uri->segment_array();
            $pops                           = !empty($matches) && is_numeric(end($matches)) ? array_pop($matches) : '';
            $this->template                 = 'public/';

            $this->vars['_template']        = $this->template;        
            $this->vars['page_title']       = !empty($matches) && count($matches) > 1 ? end($matches) : '';
            $this->vars['_CACHE_DISABLED']  = true;
            $this->vars['_HOOK_EXPIRES']    = '-10 years';      

            if($this->input->post('lang')){
                $this->session->set_userdata(DEF_APP.'_'.SESS_PUB.'language',$this->input->post('lang'));
            }
        }        
    }

    function getting_cookies(){
        if($this->session->userdata(DEF_APP.'_'.SESS_PUB.'loggedin') == null && get_cookie("SID_".sha1('username'.DEF_APP.ENCFK)) != null && get_cookie("SID_".sha1('password'.DEF_APP.ENCFK)) != null){
            $c['username']     = get_cookie("SID_".sha1('username'.DEF_APP.ENCFK));
            $c['password']   = get_cookie("SID_".sha1('password'.DEF_APP.ENCFK));
            $this->login_model->interpreter($c);
        }        
    }

    function get_visitors(){
        $this->resources_model->visitor();
    }
    
    function login_checker(){
        if($this->session->userdata(DEF_APP.'_'.SESS_PUB.'loggedin') == null){
            redirect('login');
        }
    }

    function pageview($page = '', $default = true){
        if($this->vars['_PREF_MODE'] == true )
        {
            self::login_checker();
            if($_SERVER['REMOTE_ADDR'] == '::1'){
                $profiler = true;
            }
            else{
                $profiler = false;
            }
            self::load_config('public');

            // $menu                         = $this->main_model->menu(1);
            // if(array_key_exists('bahasa', $this->vars) == false){
            //     $this->vars['bahasa']     = $this->resources_model->translation($this->language);
            // }
            // $this->vars['_MENU_TOP']      = $this->main_model->tree_top($menu[0], $menu[1]);
            if(array_key_exists('_META', $this->vars) == false){
                $this->vars['_META']      = $this->resources_model->web_seo();
            }

            $this->vars['_PROFILER']      = $profiler;        
            $this->vars['_BREADCRUMB']    = $this->template_model->breadcrumb('vave');
            $this->vars['_FLASH']         = self::flash();

            if(empty($this->vars['_snipped']) || $this->vars['_snipped'] == 'enabled'){
                $this->vars['new_member']       = $this->main_model->new_member();
                $this->vars['top_recruiter']    = $this->main_model->top_recruiter();
                $this->vars['top_shopper']      = $this->main_model->top_shopper();
            }

            $this->vars['_CONTAINER']     = $page != '' ? $this->load->view($this->template.$page, $this->vars, true) : '';
            $this->vars['_INDEX_BODY']    = $this->load->view($this->template.'body',$this->vars,true);
            $menu = '';
        }
        else{
            $this->vars['_INDEX_BODY']    = $this->load->view('errors/maintenance',$this->vars,true);
        }       
        self::master_page();            
    }

    function transaction_off(){
        self::load_config('public');
        $this->vars['_snipped'] = 'disabled';

        $this->vars['_FLASH']         = self::flash();
       
        $this->vars['_CONTAINER']     = $this->load->view('errors/transaction_offline',$this->vars,true);
        $this->vars['_INDEX_BODY']    = $this->load->view($this->template.'body',$this->vars,true);

        self::master_page();            
    }
    
    function login_page($page = null){
        if($this->vars['_PREF_MODE'] == true )
        {        
            if($_SERVER['REMOTE_ADDR'] == '::1'){
                $profiler = FALSE;
            }
            else{
                $profiler = FALSE;
            }
            self::load_config('login');
            $this->vars['_META']            = '';        
            $this->vars['_PROFILER']        = $profiler;        
            $this->vars['_FLASH']           = self::flash();
            $this->vars['_INDEX_BODY']      = $page != '' ? $this->load->view($page, $this->vars, true) : '';
        }
        else{
            $this->vars['_INDEX_BODY']    = $this->load->view('errors/maintenance',$this->vars,true);
        }        
        self::master_page();         
    }

    /* ------------------------------------------------------------------------------------------------------
    ** TEMPLATE INDEX
    ** ------------------------------------------------------------------------------------------------------
    */
    function master_page($hooking = true){

        if(!isset($this->vars['_TITLE'])){
            $this->vars['_TITLE'] = ucwords($this->vars['_INFO_TITLE']);
        }

        $page = $this->load->view($this->template.'index',$this->vars,$hooking);
        
        if($hooking == true){
            self::outputs($page);    
        }
    }
    function outputs($page, $mime = 'text/html', $buffer = true){
        $this->myhook
             ->mime($mime)
             ->charset('utf-8')
             ->status(200)
             ->expires('-10 year')
             ->cache(0)
             ->header("Accept-Ranges: bytes")
             ->header("HTTP/1.0 200 OK")
             ->header("HTTP/1.1 200 OK")
             ->header("Cache-Control: no-cache, no-store, must-revalidate", false)
             ->header("Cache-Control: post-check=0, pre-check=0", false)
             ->header('Access-Control-Request-Headers: X-Requested-With, accept, content-type')
             // ->header('Access-Control-Allow-Methods: GET, POST, OPTIONS')
             // ->header('Access-Control-Allow-Origin: *')
             // ->header('Access-Control-Allow-Origin: http://vave.co.id')
             ->header('Access-Control-Allow-Headers: Api-Key')             
             ->header("Pragma: no-cache")
             ->header("X-Content-Type-Options: nosniff")
             ->header("X-XSS-Protection: 1; mode=block")
             ->header("X-Frame-Options: SAMEORIGIN")
             ->output($page)
             ->buffering()
             ->display();        
    }        
}