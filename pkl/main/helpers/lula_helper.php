<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

    function menu_tree($data, $segment = array(), $parent = 0){
        array_walk($data, function($items) use($data, $parent, $segment){
            if($items['categoryParents'] == $parent){
                $active  = !empty($segment) && in_array(seo_url($items['categoryName']), $segment, true) ? 'class="open"': '' ;              
                $search_child = array_search($items['categoryId'], array_column($data, 'categoryParents'));
                // $url    .= seo_url($items['categoryName']).'/'.$items['categoryId'].'/';
                echo '<li '.$active.'>';
                    $attribute  = ' href="'.site_url('produk/'.$items['slug']).'"';
                    $caret      = '';
                    if($search_child == true && $items['categorydisplayon'] == 1 ){
                        $attribute  = ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"';
                        $caret      = ' <span class="caret"></span>';
                    }
                    elseif($search_child == true && $items['categorydisplayon'] == 0 ){
                        $icons      = ($active != '') ? 'fa-caret-down' : 'fa-caret-right';
                        $caret      = '<div class="btn btn-default btn-xs fa '.$icons.' fa-lg" data-toggle="nav" role="button" aria-expanded="false"></div>';                        
                    }
                    echo '<a'.$attribute.'>';
                        echo ucwords($items['categoryName']);
                    echo $caret;
                    echo '</a>';
                    if($search_child == true){
                        if($items['categorydisplayon'] == 1){
                            echo '<ul class="dropdown-menu" role="menu">';                            
                        }
                        else{
                            echo '<ul class="nav children" role="menu">';
                        }
                        // $url = preg_replace('/(\/[0-9]+)/', '', $url);
                            menu_tree($data, $segment, $items['categoryId']);
                        echo '</ul>';
                    }
                echo '</li>';
            }
        });
    }

    function send_sms($mobilenumbers,$message){
         //Please Enter Your Details
         $user          = SMS_USERNAME; //your username
         $password      = SMS_PASSWORD; //your password
            
         //$mobilenumbers=$_POST['msisdn']; //enter Mobile numbers comma seperated
         
         $senderid      = SMS_SENDERS; //Your senderid
         $messagetype   = "N"; //Type Of Your Message
         
         //domain name: Domain name Replace With Your Domain  
         $url           = SMS_DOMAIN;

         $urlmessage = urlencode($message);
         
         $ch = curl_init(); 
         if (!$ch){die("Couldn't initialize a cURL handle");}
         $ret = curl_setopt($ch, CURLOPT_URL,$url);
         #curl_setopt($ch, CURLOPT_PORT, 8085);
         curl_setopt ($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);          
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
         #curl_setopt ($ch, CURLOPT_POSTFIELDS, "username=$user&password=$password&type=0&dlr=1&destination=$mobilenumbers&source=$senderid&message=$urlmessage");
         curl_setopt ($ch, CURLOPT_POSTFIELDS, "u=$user&p=$password&d=$mobilenumbers&m=$urlmessage");
         $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

         $curlresponse = curl_exec($ch); // execute
         if(curl_errno($ch)){
            return array('error' => true, 'msg' => 'curl error : '. curl_error($ch)) ;
         }

         if(empty($ret)) {
            // some kind of an error happened
            die(curl_error($ch));
            curl_close($ch); // close cURL handler
         } 
         else {
            $info = curl_getinfo($ch);
            curl_close($ch); // close cURL handler
            return $info;
         }
    }    