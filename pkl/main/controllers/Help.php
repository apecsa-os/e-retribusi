<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Help extends Public_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
		$this->pageview('help');
	}
}