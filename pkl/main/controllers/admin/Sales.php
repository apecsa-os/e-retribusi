<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Sales extends Admin_Controller
{
	var $bahasa;
	protected $validback;

	function __construct(){
		parent::__construct();
		$model = array(
			"admin/sales_model",
			"admin/member_model"
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form','jqueryui');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','jqueryui');
		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->sales_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->sales_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function find_member(){
		$term = $this->input->get('term', true);
		$sponsor = $this->member_model->autocomplete_data($term);

		$this->outputs($sponsor, 'application/json');
	}

	function view($id = null, $name = null){
		$this->post_check();
		if($this->vars['_VIEWING'] == true){
			$detail['data'] 			= $this->sales_model->details($id);
			$detail['purchase_detail'] 	= $this->sales_model->purchase_detail($id);			
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Detail PO',
					"body"		=> $this->load->view('admin/detail_po', $detail,true),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Add PO',
					"body"		=> $this->sales_model->form(),
				),
			);
			$data['modal']['body'] .= '<script>'.file_get_contents('assets/js/produk_ctrl.js').'</script>';

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->sales_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Edit '.$name,
					"body"		=> $this->sales_model->form($detail,'edit'),
				),
			);
			$data['modal']['body'] .= '<script>'.file_get_contents('assets/js/produk_ctrl.js').'</script>';

			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		$post = $this->input->post();

		if($this->validation() == true  && $this->input->post('token') != null){	
			$po_data = array(
				'token'   			=> $post['token'],
				'nomor_po' 			=> $post['nomor_po'],
				'member_id' 		=> $this->validback['member']['id'],
				'member_username' 	=> $post['member_username'],
				'purchase_date' 	=> $post['purchase_date'],
				'po_status'			=> $post['po_status'],
				'grand_total' 		=> $post['grand_total'],
				'remark'			=> $post['remark'],
			);


			if(array_key_exists('id', $post) == false){
				$po_data['created_by'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$sql = 1;
				$sql = $this->sales_model->insert($po_data, true);
				$po_detail = array();
				if(!empty($post['item'])){
					foreach ($post['item'] as $key => $value) {
						if($value['item'] >= 0){
							$po_detail[$value['item']] = array(
								'po_id' => $sql,
								'id_produk' => $value['item'],
								'qty' => !empty($po_detail) && array_key_exists($value['item'], $po_detail) ? $po_detail[$value['item']]['qty'] + $value['qty'] : intval($value['qty']),
								'price' => $value['price'],
								'subtotal' => !empty($po_detail) && array_key_exists($value['item'], $po_detail) ? $po_detail[$value['item']]['subtotal'] + $value['subtotal'] : floatval($value['subtotal']),
							);	
						}
					}
					$sql = $this->db->insert_batch('purchaseorder_detail', array_values($po_detail));
				}
	
				$action = true;
			}
			else{
				$po_data['updated_by'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$cond 	= array('id' => $post['id']);
				$this->sales_model->update($po_data, $cond, true);
				$po_detail = array();
				if(!empty($post['item'])){
					foreach ($post['item'] as $key => $value) {
						if($value['item'] >= 0){
							$po_detail[$value['item']] = array(
								'id'	=> $value['id'],
								'id_produk' => $value['item'],
								'qty' => !empty($po_detail) && array_key_exists($value['item'], $po_detail) ? $po_detail[$value['item']]['qty'] + $value['qty'] : intval($value['qty']),
								'subtotal' => !empty($po_detail) && array_key_exists($value['item'], $po_detail) ? $po_detail[$value['item']]['subtotal'] + $value['subtotal'] : floatval($value['subtotal']),
							);	
						}
					}
					$sql = $this->db->update_batch('purchaseorder_detail', array_values($po_detail), 'id');
				}				
				$action = true;
			}

			// hitung bonus jika status payment telah paid
			$bonus = '';
			if($po_data['po_status'] == 'paid' && !empty($po_detail)){
				// load bonus model dan produk model
				$this->load->model(array('mlm/bonus_model','admin/produk_model'));
				
				// ID member dari form validasi
				$member = $this->validback['member']['id'];

				// slot number
				$slot 	= $this->validback['network']['slotnumb'];
				
				// Pilah ID dari purchase detail
				$product = array_values($po_detail);
				$product = array_column($product, 'id_produk');

				// Ambil data produk dari table produk
				$data_product = $this->produk_model->list_detail($product);
				
				// Passing data produk untuk memproses point member
				$bonus = $this->bonus_model->add_point_bonus($member, $slot, $data_product, $po_detail);
			}

			if($action == true){
				$flash = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=> self::set_flash('success', '', false),
				);				
			}
			else{
				$flash = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=>self::set_flash('error', 'An error occur, please contact administrator immediately', false),
				);				
			}
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}

	private function validation(){
		$data = array(
			array("name"=>"nomor_po","message"=>'Purchase number',"valid_type"=>"required"),
			array("name"=>"member_username","message"=>'Member ID',"valid_type"=>"required|callback_member_verification"),
			array("name"=>"purchase_date","message"=>'Purchase date',"valid_type"=>"required"),
		);
		return $this->validate($data);		
	}

	// fungsi callback untuk memverifikasi apakah username sponsor yang dikirim oleh server ada di database
	// jika tidak ada maka kembalikan sebagai nilai FALSE
	function member_verification($string){
		$verify = $this->member_model->details(null, $string);
		if(!empty($verify)){
			$this->load->model('admin/network_model');
			$this->validback['member'] = $verify;
			$this->validback['network'] = $this->network_model->member_slot($verify['id']);
			return true;
		}
		else{
			$this->form_validation->set_message('member_verification','Invalid {field} id');
			return false;
		}
	}
	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->sales_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? 'Deleted' : 'Failed',
		);
		$this->outputs($flash, 'application/json');
	}

	function remove_item(){
		$this->post_check();

		$itemid = $this->input->post('itemid', true);

		// Hapus table purchaseorder_detail dimana produk = $itemid
		$destroy = $this->sales_model->delete_item(array('id' => $itemid));
		// $destroy = true;
		if($destroy == true){
			$output = array(
				'success' => true,
				'modal'	  => true,				
				'title'	  => 'Done',
				'type'	 => 'success',				
				'message' => 'Item has been removed'
			);				
		}
		else{
			$output = array(
				'success' => false,
				'modal'	  => true,				
				'title'	  => 'Failed',
				'type'	 => 'error',				
				'message' => 'Cannot removed item from purchase order'
			);				
		}

		$this->outputs($output, 'application/json');

	}

	function cetak($id, $nomor_po){
		if($this->vars['_PRINTING'] == true && $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token') != null){
			$this->vars['_CACHE_DISABLED'] 	= true;
			$detail['data'] 			= $this->sales_model->details($id);
			if(!empty($detail['data'])){
				$detail['purchase_detail'] 	= $this->sales_model->purchase_detail($id);			
				$detail['stylesheet']	= '<style type="text/css">'.file_get_contents('assets/stylesheet/print.css').'</style>';
				$this->load->library('dompdf_library');
				$output = $this->load->view('admin/detail_po', $detail, true);
				$excel = $this->dompdf_library
							  ->set_filename()
							  ->set_papersize('A4','portrait')
							  ->render($output);

			}
			else{
				$this->outputs('Data tidak ditemukan');
			}		
		}		
	}

	function print_excel(){
		$this->post_check();
		if($this->vars['_PRINTING'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Print Sales Data',
					"body"		=> $this->sales_model->print_form(),
				),
			);

			$this->outputs($data, 'application/json');
		}		
	}

	function excel(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$month = $this->input->post('month', true) + 1;
			$year = $this->input->post('year', true);
			$data = $this->sales_model->print_data($month, $year);
			if(!empty($data)){
				$this->load->library('excelio', $this->vars);
				$excel = $this->excelio
							  ->create_objdrawing()
							  ->filename('Sales Report '.$month.'-'.$year)
							  ->create_sheet('Report', $data)
							  ->auto_gen();				
			}
			else{
				$this->outputs('Oops, Data not found!');
			}
		}
		else{
			redirect(404);
		}
	}						
}