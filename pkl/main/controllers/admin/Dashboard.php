<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Dashboard extends Admin_Controller
{
	var $bahasa;	
	function __construct()
	{
		parent::__construct();
		$model = array(
			"admin/dashboard_model",
			// 'public/main_model'
		);
		$this->load->model($model);
		$this->load->helper('text');
	}

	function index(){
		$this->js = array('fiquery');

		if($this->input->get('maps') == true){
			self::data_processing();
		}
		else{
			$this->vars['_JS_ADDON'] = '<script async defer src="https://maps.googleapis.com/maps/api/js?key='.GMAPKEY.'&language=id&region=ID&&callback=getLocation"></script>';
			$this->vars['_JS_ADDON'] .= '<script src="'.base_url('assets/js/dashboard.js').'" type="text/javascript"></script>';
			$this->pageview('admin/dashboard');						
		}
	}

	function data_processing(){
		$data = $this->dashboard_model->data_pedagang();
		if(!empty($data)){
			$output = array(
				'success' => true,
				'data'	=> $data 
			);
		}
		else{
			$output = array(
				'success' => false,
				'msg'	=> "Map tidak dapat ditampilkan"
			);
		}
		$this->outputs($output, 'application/json');
	}

}