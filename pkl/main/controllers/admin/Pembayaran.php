<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Pembayaran extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/pembayaran_model",
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','autonumeric');

		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->pembayaran_model->table();
		$this->pageview('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->pembayaran_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> 'Form Pembayaran',
					"body"			=> $this->pembayaran_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->pembayaran_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Form Edit Pembayaran',
					"body"		=> $this->pembayaran_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		$post = $this->input->post();

		if($this->validation() == true  && $this->input->post('token') != null){	
			$post['jumlah']			= currency_reverse($post['jumlah']);
			if(array_key_exists('id', $post) == false){
				$post['kode_transaksi'] = date('Ymd').sprintf('%04d',mt_rand(1, 9999));
				$post['tgl_bayar']		= date('Y-m-d H:i:s');
				$post['petugas']		= $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');

				$this->pembayaran_model->insert($post, true);
				$action = true;
			}
			else{
				$cond 	= array('id' => $post['id']);
				$this->pembayaran_model->update($post, $cond, true);
				$action = true;
			}

			if($action == true){
				$flash = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=>self::set_flash('Data transaksi berhasil disimpan', '', false),
				);				
			}
			else{
				$flash = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=>self::set_flash('error', 'Terjadi kesalahan', false),
				);				
			}
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->pembayaran_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? 'Transaksi berhasil ditambahkan' : 'Transaksi Gagal',
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"kode_pedagang","message"=>'Kode Pedagang',"valid_type"=>"required|callback_check_pedagang"),
			array("name"=>"jumlah","message"=>'Jumlah pembayaran',"valid_type"=>"required"),
		);
		return $this->validate($data);		
	}

	function check_pedagang($kode_pedagang){
		$this->load->model('admin/pedagang_model');
		$pedagang = $this->pedagang_model->details($kode_pedagang, 'unique_merchant');
		if(!empty($pedagang)){
			return true;
		}
		else{
			$this->form_validation->set_message('check_pedagang','ID pedagang tidak ditemukan');
			return false;
		}
	}					
}