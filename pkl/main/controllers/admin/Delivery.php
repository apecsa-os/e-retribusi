<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Delivery extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/delivery_model",
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form','jqueryui');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','jqueryui');
		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->delivery_model->table();
		$this->pageview('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->delivery_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function find_po(){
		if($this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$term = $this->input->get('term', true);
			$purcorder = $this->delivery_model->autocomplete_data($term);
		}
		else{
			$purcorder = array('success' => 'false', 'msg' => 'Restriction access');	
		}
		$this->outputs($purcorder, 'application/json');
	}

	function get_product_order(){
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$output = $this->delivery_model->purchase_detail($this->input->post('salesid', true));
		}
		else{
			$output = array('success' => false, 'msg' => 'Restriction page'); 
		}

		$this->outputs($output, 'application/json');		
	}

	function select_sn(){
		if($this->input->get('pid') != null && $this->input->get('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$id = $this->input->get('pid', true);
			$data = $this->delivery_model->select_serial_number($id);
			$this->load_config('admin');
			$this->vars['data'] 	  = $data;
			$this->vars['pagination'] = $this->query_model
						   				 ->initialize($data['page'], $data['total'], $data['limit'], $data['offset'])
						   				 ->page_info()
						   				 ->pagination();
			$output = $this->load->view('form/delivery_serial_number_form', $this->vars);
		}
		else{
			$output = '<div class="alert alert-danger">Invalid Request</div>'; 
		}

		// $this->outputs($output, 'application/json');		
	}

	function view($id = null, $name = null){
		$this->post_check();
		if($this->vars['_VIEWING'] == true){
			$detail['data'] 	= $this->delivery_model->details($id);
			$detail['detail'] 	= $this->delivery_model->delivery_detail($id);			
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Detail DO',
					"body"		=> $this->load->view('admin/detail_delivery', $detail,true),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'New Delivery Order',
					"body"		=> $this->delivery_model->form(),
				),
			);
			$data['modal']['body'] .= '<script>'.file_get_contents('assets/js/delivery_order.js').'</script>';
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		// 1. Pastikan http method adalah POST
		$this->post_check();
		// 2. Lakukan validasi data
		if($this->validation() == TRUE && $this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$postdata = $this->input->post();
			
			// 3.a Update purchase order
			$this->db
				 ->set('po_status', 'delivered')
				 ->where('id', $this->input->post('id_po', true))
				 ->update('purchase_order');

			// 3.b Inisialisasi data delivery order yg akan di insert ke dalam table delivery_order
			$deliv = array(
				'token'		=> $this->input->post('token', true),
				'id_po' 	=> $this->input->post('id_po', true),
				'nomor_po'	=> $this->input->post('nomor_po', true),
				'status'	=> 'delivered',
				'delivery_date' => $this->input->post('delivery_date', true),
				'remark'		=> $this->input->post('remark', true),
				'created_by'	=> $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username')
			);

			// 4. Insert data delivery order
			$sql = true;
			$sql = $this->delivery_model->insert($deliv, true);

			// 5. Jika return value dari insert data berupa nilai ID dan bukan nilai false maka lakukan batching insert delivery detail
			if($sql != false){

				// 6.a definisikan serial number yg dipilih
				$serial = $this->input->post('serial_number', true);

				$deliv_detail = array();

				// 6.b lakukan looping untuk mendapatkan ID serial yg dipilih
				foreach ($this->input->post('serial_id') as $key => $value) {

					// 6.c Masukan masing-masing data detail ke dalam array
					$deliv_detail[] = array(
						'delivery_id' 	=> $sql,
						'sn_id'			=> $value,
						'sn_number'		=> $serial[$key],
					);
				}

				// 6.d lakukan batching insert jika detail array tidak kosong
				if(!empty($deliv_detail)){
					$sql = $this->db->insert_batch('delivery_order_detail', $deliv_detail);
					
					// 7. Lakukan updating data untuk serial number tersebut
					if($sql != false){
						// 7.a insert into voucher logging
						$this->load->model('mlm/log_model');
						$log_model = $this->log_model->insert('serial_id', 'member');
						
						// 7.b update serial number
						$this->db
							 ->set('status', 'sold')
							 ->set('owner', $this->input->post('member', true))
							 ->set('buy_date', $this->input->post('sales_date', true))
							 ->set('ledger_code', 'RO'.$deliv['nomor_po'])
							 ->where_in('id' , $this->input->post('serial_id'))
							 ->or_where_in('bundling_num', $this->input->post('serial_id'))
							 ->update('product_sn');

					}

					// 8. Insert ke dalam tabel product ledger
					$ledger = array();
					$qty = $this->input->post('produk_qty', true);
					foreach ($this->input->post('id_produk') as $key => $value) {
						$ledger[] = array(
							'doc_no' 	 => 'RO_'.$deliv['nomor_po'],
							'produkid' 	 => $value,
							'qty_out'	 => $qty[$key],
							'remarks'	 => 'Product RO member @'.$this->input->post('member', true),
							'tanggal_keluar' => date('Y-m-d H:i:s'),
							'created_by'	 => $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username'),
							'status'		 => 'success',
						);
						
					}

					$this->db->insert_batch('product_ledger', $ledger);
				}

			}

			$flash = array(
				'success' => true,
				'modal'   => true,
				"msg"	  => self::set_flash('Delivery order has been created', '', false),	
			);			
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $this->input->post(), false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);			
		}
		$this->outputs($flash, 'application/json');						
	}

	private function validation(){
		$data = array(
			array("name"=>"nomor_po","message"=>'Sales number',"valid_type"=>"required"),
			array("name"=>"delivery_date","message"=>'Delivery date',"valid_type"=>"required"),
		);

		if($this->input->post('serial_id') == null){
			$data[] = array('name' => 'serial_id', 'message' => 'Each product require serial number', 'valid_type' => 'required');
		}

		return $this->validate($data);		
	}

	function cetak($id, $nomor_po){
		if($this->vars['_PRINTING'] == true && $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token') != null){
			$this->vars['_CACHE_DISABLED'] 	= true;
			$detail['data'] 			= $this->delivery_model->details($id);
			if(!empty($detail['data'])){
				$detail['detail'] 		= $this->delivery_model->delivery_detail($id);			
				$detail['stylesheet']	= '<style type="text/css">'.file_get_contents('assets/stylesheet/print.css').'</style>';
				$this->load->library('dompdf_library');
				$output = $this->load->view('admin/detail_delivery', $detail, true);
				$excel = $this->dompdf_library
							  ->set_filename()
							  ->set_papersize('A4','portrait')
							  ->render($output);

			}
			else{
				$this->outputs('Data tidak ditemukan');
			}		
		}		
	}

	function print_excel(){
		$this->post_check();
		if($this->vars['_PRINTING'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Print Delivery Data',
					"body"		=> $this->delivery_model->print_form(),
				),
			);

			$this->outputs($data, 'application/json');
		}		
	}

	function excel(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$month = $this->input->post('month', true) + 1;
			$year = $this->input->post('year', true);
			$data = $this->delivery_model->print_data($month, $year);
			if(!empty($data)){
				$this->load->library('excelio', $this->vars);
				$excel = $this->excelio
							  ->create_objdrawing()
							  ->filename('Sales Report '.$month.'-'.$year)
							  ->create_sheet('Report', $data)
							  ->auto_gen();				
			}
			else{
				$this->outputs('Oops, Data not found!');
			}
		}
		else{
			redirect(404);
		}
	}			
}