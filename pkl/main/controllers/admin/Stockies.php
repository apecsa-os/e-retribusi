<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Stockies extends Admin_Controller
{
	var $bahasa;
	var $callback;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/stockies_model",
			"admin/member_model"
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form','jqueryui');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','jqueryui');
		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->stockies_model->table();
		$this->pageview('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->stockies_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	// find member data especially for finding sponsor ID
	function find_member(){
		$this->vars['_CACHE_DISABLED'] 	= true;
		$term = $this->input->get('term', true);
		$sponsor = $this->member_model->autocomplete_data($term);
		$result = array(
			'success' 	=> true,
			'result'		=> $sponsor
		);

		$this->outputs($sponsor, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Add',
					"body"		=> $this->stockies_model->form(),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->stockies_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Edit '.$name,
					"body"		=> $this->stockies_model->form($detail,'edit'),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		$post = $this->input->post();

		if($this->validation() == true  && $this->input->post('token') != null){	
			$postdata = array(
				'token'			  => $this->input->post('token', true),
				'parent_stockist' => !empty($this->callback) && !empty($this->callback['stockist']) ? $this->callback['stockist']['id'] : null,
				'member_id'		  => !empty($this->callback) && !empty($this->callback['member']) ? $this->callback['member']['id'] : 1,
				'stockist_type'	  => $this->input->post('stockist_type', true),
				'status'		  => $this->input->post('status', true),
			);
			if($postdata['parent_stockist'] == null){
				unset($postdata['parent_stockist']);
			}

			if(array_key_exists('id', $post) == false){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['created_by'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$this->stockies_model->insert($postdata);
				$action = true;
			}
			else{
				$postdata['updated_at'] = date('Y-m-d H:i:s');
				$postdata['updated_by'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');

				$cond 	= array('id' => $post['id']);
				$this->stockies_model->update($postdata, $cond);
				$action = true;
			}

			if($action == true){
				$flash = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=>self::set_flash('success', '', false),
				);				
			}
			else{
				$flash = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=>self::set_flash('error', 'An error occur, Please report to administrator immediately', false),
				);				
			}
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}

	private function validation(){
		$data = array(
			array("name"=>"parent_stockist","message"=>'Member ID',"valid_type"=>"callback_parental_stc"),
			array("name"=>"member_id","message"=>'Member ID',"valid_type"=>"required|callback_member_verification"),
		);
		return $this->validate($data);		
	}

	// fungsi callback untuk memverifikasi apakah username stockist master yang dikirim oleh server ada di database
	// jika tidak ada maka kembalikan sebagai nilai FALSE
	function parental_stc($string){
		if(!empty($string)){
			$verify = $this->stockies_model->stockist_verify($string);
			if(!empty($verify)){
				$this->callback['stockist'] = $verify;
				return true;
			}
			else{
				$this->form_validation->set_message('parental_stc', 'Sorry, Unknown stockist master');
				return false;
			}
		}
		else{
			return true;
		}
	}

	// fungsi callback untuk memverifikasi apakah username member yang dikirim oleh server ada di database
	// jika tidak ada maka kembalikan sebagai nilai FALSE
	function member_verification($string){
		$verify = $this->member_model->details(null, $string);
		if(!empty($verify)){
			$this->callback['member'] = $verify;
			return true;
		}
		else{
			$this->form_validation->set_message('member_verification','Invalid {field} id');
			return false;
		}
	}
	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->stockies_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? 'Deleted' : 'Failed',
		);
		$this->outputs($flash, 'application/json');
	}					
}