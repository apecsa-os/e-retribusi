<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Pegawai extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/pegawai_model",
			"admin/log_model"
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');
		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->pegawai_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->pegawai_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Tambah Pegawai',
					"body"		=> $this->pegawai_model->form(),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->pegawai_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Edit Pegawai '.$name,
					"body"		=> $this->pegawai_model->form($detail,'edit'),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		
		$post = $this->input->post();
		if($this->validation() == true){
			if(array_key_exists('id', $post) == false){
				$action = $this->pegawai_model->insert($post);
				$logs 	= $this->log_model->insert_log('insert',$post['name']);	
			}
			else{
				$id 	= $this->input->post('id', true);
				
				$action = $this->pegawai_model->update($post,array("id"=>$id));
				$logs 	= $this->log_model->insert_log('Ubah',$post['name']);				
			}

			if($action){
				$flash = array(
					'success' 		=> true, 
					'modal'			=> true, 
					'msg' 			=> self::set_flash('success', '', false),
					'reload'		=> true
				);			
			}
		}
		else{
			$flash = array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		$this->outputs($flash, 'application/json');		
	}

	private function validation(){
		$data = array(
			array("name"=>"name","message"=>"Name","valid_type"=>"required|max_length[45]"),
			array("name"=>"address","message"=>"Address","valid_type"=>"required"),
		);
		return $this->validate($data);		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->pegawai_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? 'Data berhasil dihapus' : 'Data gagal dihapus',
			'reload'	=> true
		);
		$this->outputs($flash, 'application/json');
	}					
}