<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Report_bonus extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/report_bonus_model",
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form','jqueryui');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','jqueryui');
		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->report_bonus_model->table();
		$this->pageview('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->report_bonus_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function view($id = null, $name = null){
		$this->post_check();
		if($this->vars['_VIEWING'] == true){
			$detail['data'] = $this->report_bonus_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Detail Serial',
					"body"		=> $this->load->view('admin/report_bonus', $detail,true),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}
	
	function print_excel(){
		$this->post_check();
		if($this->vars['_PRINTING'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Print Bonus Statement',
					"body"		=> $this->report_bonus_model->print_form(),
				),
			);

			$this->outputs($data, 'application/json');
		}		
	}

	function excel(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$date = $this->input->post('date', true) + 1;		
			$month = $this->input->post('month', true) + 1;
			$year = $this->input->post('year', true);
			$date = sprintf('%02s', $date);
			$month = sprintf('%02s', $month);			
			$data = $this->report_bonus_model->print_data($date, $month, $year);
			if(!empty($data)){
				$this->load->library('excelio', $this->vars);
				$excel = $this->excelio
							  ->create_objdrawing()
							  ->filename('Bonus Report '.$month.'-'.$year)
							  ->create_sheet('Report', $data)
							  ->auto_gen();				
			}
			else{
				$this->outputs('Oops, Data not found!');
			}
		}
		else{
			redirect(404);
		}
	}
}