<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Pedagang extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/pedagang_model",
			"admin/log_model"
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');
		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->pedagang_model->table();
		$this->vars['_JS_ADDON'] 		= '<script async defer src="https://maps.googleapis.com/maps/api/js?key='.GMAPKEY.'&language=id&region=ID"></script>';

		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->pedagang_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> 'Tambah baru',
					"body"			=> $this->pedagang_model->form(),
				),
			);
			$data['modal']['body'] .= '<script>'.file_get_contents('assets/js/map_marker.js').'</script>';
			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->pedagang_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Ubah '.$name,
					"body"		=> $this->pedagang_model->form($detail),
				),
			);
			$data['modal']['body'] .= '<script>'.file_get_contents('assets/js/map_marker.js').'</script>';

			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		$post = $this->input->post();

		if($this->validation() == true  && $this->input->post('token') != null){
			$post['open_hour'] 		= sprintf('%02s', $post['open_hour']).':'.sprintf('%02s', $post['open_minute']);
			$post['closing_hour'] 	= sprintf('%02s', $post['closing_hour']).':'.sprintf('%02s', $post['closing_minute']);
			unset($post['open_minute'], $post['closing_minute']);

			if($_FILES && !empty($_FILES['image']['name'][0])){
				if(array_key_exists('curimg', $post) && file_exists('assets/uploads/'.$post['curimg'])){
					unlink('assets/uploads/'.$post['curimg']);
				}
				$this->load->library('uploader');
				$upload_file = $this->uploader->upload_foto('image',$_FILES, 'uploads/pedagang/');
				$image = array();
				foreach ($upload_file as $key => $value) 
				{
					if(empty($value['error'])){
						array_push($image, 'uploads/pedagang/'.$value['data']['raw_name'].$value['data']['file_ext']);						
					}
				}
				$post['image'] = implode(',',$image);
			}

			if(array_key_exists('id', $post) == false){
				$post['created_by'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$this->pedagang_model->insert($post, true);
				$action = true;
			}
			else{
				unset($post['curimg']);
				$post['updated_by'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$cond 	= array('id' => $post['id']);
				$this->pedagang_model->update($post, $cond, true);
				$action = true;
			}

			if($action == true){
				$flash = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=>self::set_flash('data telah disimpan', '', false),
				);				
			}
			else{
				if(array_key_exists('image', $post)){
					unlink('assets/uploads/'.$post['image']);
				}
				$flash = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=> self::set_flash('error', 'Terjadi kesalahan', false),
				);				
			}
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->pedagang_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? 'Data berhasil dihapus' : 'Data gagal dihapus',
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"name","message"=> 'Nama toko',"valid_type"=>"required"),
			array("name"=>"address","message"=> 'Alamat toko',"valid_type"=>"required"),
			array("name"=>"category","message"=> 'Jenis usaha',"valid_type"=>"required"),
			array("name"=>"kecamatan","message"=> 'Kecamatan toko',"valid_type"=>"required"),
			array("name"=>"kelurahan","message"=> 'Kelurahan toko',"valid_type"=>"required"),
			array("name"=>"longitude","message"=> 'Posisi longitude toko',"valid_type"=>"required"),
			array("name"=>"latitude","message"=> 'Posisi latitude toko',"valid_type"=>"required"),
			array("name"=>"petugas","message"=> 'Nama petugas',"valid_type"=>"required"),
		);
		return $this->validate($data);		
	}

	function galeri($id, $kode){
		$this->post_check();
		if($this->vars['_VIEWING'] == true){
			$this->load->helper('directory');
			$pedagang['id']	  = $id;
			$pedagang['form'] = $this->pedagang_model->form_galery($id);
			$pedagang['galery'] = false;
			if(is_dir(UPLOAD_PATH.'/uploads/media/foto/'.$id) == true){
				$pedagang['galery'] = directory_map(UPLOAD_PATH.'/uploads/media/foto/'.$id);
			}
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> 'Galeri foto pedagang '.$kode,
					"body"			=> $this->load->view('admin/galeri_foto', $pedagang, true),
				),
			);
		}
		else{
			$data = array(
				'success' => false,
				'msg'		=> 'Akses ditolak'
			);
		}
		$this->outputs($data, 'application/json');
	}

	function upload_foto(){
		$this->post_check();
		$post = $this->input->post();

		if($this->input->post('token') != null){
			$upload_file = false;
			$id = $this->input->post('id', true);
			if($_FILES && !empty($_FILES['image']['name'][0])){
				$this->load->library('uploader');
				$upload_file = $this->uploader->upload_foto('image',$_FILES, 'uploads/media/foto/'.$id);
			}

			if(empty($upload_file['error'])){
				$flash = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=> self::set_flash('Gambar berhasil disimpan', '', false),
				);				
			}
			else{
				$flash = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=> self::set_flash('error', implode("\n", array_column($upload_file, 'error')), false),
				);				
			}
		}
		else{
			$flash = array(
				'success' 	=> false,
				"modal"		=> true,
				"msg"		=> self::set_flash('error', 'Invalid token!', false),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}					
}