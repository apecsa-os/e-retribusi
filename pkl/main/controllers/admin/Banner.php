<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Banner extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/banner_model",
			"admin/log_model"
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','group','delete_success', 'delete_failed','name');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form');

		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->banner_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->banner_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->banner_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->banner_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->banner_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		$post = $this->input->post();

		if($this->validation() == true  && $this->input->post('token') != null){	
			if($_FILES && !empty($_FILES['image']['name'][0])){
				if(array_key_exists('curimg', $post) && file_exists('assets/uploads/'.$post['curimg'])){
					unlink('assets/uploads/'.$post['curimg']);
				}
				$this->load->library('uploader');
				$upload_file = $this->uploader->upload_foto('image',$_FILES, 'uploads/pamflet/');
				$image = array();
				foreach ($upload_file as $key => $value) 
				{
					if(empty($value['error'])){
						array_push($image, 'pamflet/'.$value['data']['raw_name'].$value['data']['file_ext']);						
					}
				}
				$post['image'] = implode(',',$image);
			}

			if(array_key_exists('id', $post) == false){
				$this->banner_model->insert($post, true);
				$action = true;
			}
			else{
				// if(file_exists('assets/uploads/'.$post['curimg'])){
				// 	unlink('assets/uploads/'.$post['curimg']);
				// }
				unset($post['curimg']);
				$cond 	= array('id' => $post['id']);
				$this->banner_model->update($post, $cond, true);
				$action = true;
			}

			if($action == true){
				$flash = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=>self::set_flash('banner has been save', '', false),
				);				
			}
			else{
				if(array_key_exists('image', $post)){
					unlink('assets/uploads/'.$post['image']);
				}
				$flash = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=>self::set_flash('error', 'Something error occur', false),
				);				
			}
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->banner_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"title","message"=>$this->bahasa['name'],"valid_type"=>"required"),
		);
		return $this->validate($data);		
	}					
}