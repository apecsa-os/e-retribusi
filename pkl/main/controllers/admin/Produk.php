<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Produk extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/produk_model",
		);
		$this->load->model($model);

		$key 	= array('insert', 'edit', 'delete','form','group','delete_success', 'delete_failed','name',
					'kategori', 'brand', 'price', 'description','discount','buy'
				);
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form','select2');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','select2','autonumeric');

		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->produk_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->produk_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function view($id = null, $name = null){
		$this->post_check();
		if($this->vars['_VIEWING'] == true){
			$detail['data'] 		= $this->produk_model->details($id);
			$detail['produk_list']	= $this->produk_model->package_item($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Detail produk',
					"body"		=> $this->load->view('admin/detail_produk', $detail,true),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				"success" 		=> true,
				"modal"			=> array(
					"title"			=> $this->bahasa['insert'].' '.$this->vars['_TITLE'],
					"body"			=> $this->produk_model->form(),
				),
			);

			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->produk_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> $this->bahasa['edit'].' '.$name,
					"body"		=> $this->produk_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function assembling($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$this->load->model('admin/produk_assembling_model');
			$detail['pid']	  = $id;
			$detail['url']	  = $this->vars['_ROUTED'];
			$detail['list']	  = $this->produk_model->produk_list();
			// $detail['laststock'] = $this->produk_ledger_model->get_items(array($id));
			$detail['composite'] = $this->produk_assembling_model->assembling_data($id);
			$detail['methods'] = 'assemblers_item';
			$detail['produk'] = $this->produk_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> !empty($detail['composite']) ? 'Modify package' : 'Add package',
					"body"		=> $this->load->view('form/produk_assembler_form', $detail, true),
				),
			);
			$data['modal']['body'] .= '<script>'.file_get_contents('assets/js/produk_ctrl.js').'</script>';
			$this->outputs($data, 'application/json');
		}
	}	

	function save(){
		$this->post_check();
		$post = $this->input->post();

		if($this->validation() == true  && $this->input->post('token') != null){
			// cek apakah ada file image ?
			if($_FILES && !empty($_FILES['image']['name'][0])){
				$this->load->library('uploader');
				$upload_file = $this->uploader->upload_foto('image',$_FILES, 'uploads/produk/');
				$image = array();
				foreach ($upload_file as $key => $value) 
				{
					if(empty($value['error'])){
						array_push($image, 'produk/'.$value['data']['raw_name'].$value['data']['file_ext']);						
					}
				}
				$post['image'] = implode(',',$image);
			}

			$post['price'] 			= !empty($post['price']) ? currency_reverse($post['price']) : 0;
			$post['diskon'] 		= !empty($post['diskon']) ? currency_reverse($post['diskon']) : 0;									
			$post['price_cust'] 	= !empty($post['price_cust']) ? currency_reverse($post['price_cust']) : 0;
			$post['price_register'] = !empty($post['price_register']) ? currency_reverse($post['price_register']) : 0;			
			$post['cashback'] 		= !empty($post['cashback']) ? currency_reverse($post['cashback']) : 0;			
			$post['slug'] 			= empty($post['slug']) ? seo_url($post['name']) : seo_url($post['name']);
			$point 					= $post['point'];
			$point 					= array_map('currency_reverse', $point); 
			$post['point']			= serialize($point);
			unset($point);

			if(array_key_exists('id', $post) == false){
				$post['created_by']		= $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$last_insert_id 		= $this->produk_model->insert($post, true);
				$action = true;
			}
			else{
				if(file_exists('assets/uploads/'.$post['curimg']) && !empty($post['image'])){
					unlink('assets/uploads/'.$post['curimg']);
				}
				unset($post['curimg']);

				$post['updated_by']		= $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$cond 	= array('id' => $post['id']);
				$this->produk_model->update($post, $cond, true);
				$action = true;
			}

			if($action == true){
				$flash = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=>self::set_flash('success', '', false),
				);				
			}
			else{
				if(array_key_exists('image', $post)){
					unlink('assets/uploads/'.$post['image']);
				}				
				$flash = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=>self::set_flash('error', 'Something error occur', false),
				);				
			}
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->produk_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	private function validation(){
		$data = array(
			array("name"=>"name","message"=>'Nama produk',"valid_type"=>"required|max_length[100]"),
			array("name"=>"code","message"=>'Kode produk',"valid_type"=>"required|min_length[3]|max_length[20]"),
		);
		return $this->validate($data);		
	}

	function assemblers_item(){
		$this->post_check();
		$post = $this->input->post();
			
		if(!empty($post['item_code']) && array_sum($post['item_code']) > 0){
			$product_assembler =  array();
			foreach ($post['item_code'] as $key => $value) {
				if(!empty($value)){
					$qty = (array_key_exists($value, $product_assembler)) ? ($product_assembler[$value]['qty'] + $post['item_qty'][$key]) : intval($post['item_qty'][$key]);

					if(array_key_exists('assembler_id', $post) && array_key_exists($key, $post['assembler_id'])){
						$product_assembler['update'][$value] = array(
							"id" => $post['assembler_id'][$key],
							"produk" => $post['produkid'],
							"produk_item_id" => $value,
							"qty" => $qty,
							"assembler" => $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username')
						);						
					}
					else{
						$product_assembler['insert'][$value] = array(
							"produk" => $post['produkid'],
							"produk_item_id" => $value,
							"qty" => $qty,
							"assembler" => $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username')
						);
					}
				}
			}

			if(array_key_exists('insert', $product_assembler) && !empty($product_assembler['insert'])){
				// insert into table product_assembler if insert data has exist
				$assembling = $this->db->insert_batch('produk_assembling', array_values($product_assembler['insert']));
			}

			if(array_key_exists('update', $product_assembler) && !empty($product_assembler['update'])){
				// update into table product_assembler if update data has exist
				$assembling = $this->db->update_batch('produk_assembling', array_values($product_assembler['update']), 'id');
			}

				$assembling = true;
				if($assembling == true){
					$output = array(
						'success' 	=> true,
						"modal"		=> true,
						"msg"		=> self::set_flash('success', 'Assembling product has been success ', false),
					);
				}
				else{
					$output = array(
						'success' 	=> false,
						"modal"		=> true,
						"msg"		=> self::set_flash('error', $status_error_id, false),
					);				
					
				}

		}
		else{
			$output = array(
				'success' 	=> true,
				"modal"		=> true,
			);			
		}

		$this->outputs($output, 'application/json');
	}

	function remove_item(){
		$this->post_check();

		$itemid = $this->input->post('itemid', true);
		$this->load->model('admin/produk_assembling_model');
		// Hapus table produk_assembling dimana produk = $itemid
		$destroy = $this->produk_assembling_model->delete(array('id' => $itemid));
		// $destroy = true;
		if($destroy == true){
			$output = array(
				'success' => true,
				'modal'	  => true,				
				'title'	  => 'Done',
				'type'	 => 'success',				
				'message' => 'Item has been dissamblered'
			);				
		}
		else{
			$output = array(
				'success' => false,
				'modal'	  => true,				
				'title'	  => 'Failed',
				'type'	 => 'error',				
				'message' => 'Cannot dissambler item from package'
			);				
		}

		$this->outputs($output, 'application/json');

	}

	function print_excel(){
		$this->post_check();
		if($this->vars['_PRINTING'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Print Product',
					"body"		=> $this->produk_model->print_form(),
				),
			);

			$this->outputs($data, 'application/json');
		}		
	}

	function excel(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$status = $this->input->post('status', true);			
			$data = $this->produk_model->print_data($status);
			if(!empty($data)){
				$this->load->library('excelio', $this->vars);
				$excel = $this->excelio
							  ->create_objdrawing()
							  ->filename('Product Report')
							  ->create_sheet('Report', $data)
							  ->auto_gen();				
			}
			else{
				$this->outputs('Oops, Data not found!');
			}
		}
		else{
			redirect(404);
		}
	}												
}