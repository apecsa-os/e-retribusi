<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Bank extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/bank_model",
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form','jqueryui');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','jqueryui');
		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->bank_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->bank_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function view($id = null, $name = null){
		$this->post_check();
		if($this->vars['_VIEWING'] == true){
			$detail['data'] = $this->bank_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Detail Bank',
					"body"		=> $this->load->view('admin/detail_bank', $detail,true),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Add',
					"body"		=> $this->bank_model->form(),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->bank_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Edit '.$name,
					"body"		=> $this->bank_model->form($detail,'edit'),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		$post = $this->input->post();

		if($this->validation() == true  && $this->input->post('token') != null){	
			if(array_key_exists('id', $post) == false){
				$post['createdby'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$this->bank_model->insert($post);
				$action = true;
			}
			else{
				$post['updatedby'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$cond 	= array('id' => $post['id']);
				$this->bank_model->update($post, $cond);
				$action = true;
			}

			if($action == true){
				$flash = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=>self::set_flash('success', '', false),
				);				
			}
			else{
				$flash = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=>self::set_flash('error', 'Terjadi kesalahan, laporkan kepada administrator', false),
				);				
			}
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $post, false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}

	private function validation(){
		$data = array(
			array("name"=>"name","message"=>'Bank Name',"valid_type"=>"required"),
		);
		return $this->validate($data);		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->bank_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? 'Deleted' : 'Failed',
		);
		$this->outputs($flash, 'application/json');
	}					
}