<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Production extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/production_model",
			"admin/produk_ledger_model",
			"admin/inventori_model",			
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form','jqueryui','select2');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','jqueryui','select2');
		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->production_model->table();
		$this->pageView('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->production_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function view($id = null, $name = null){
		$this->post_check();
		if($this->vars['_VIEWING'] == true){
			$detail['data'] = $this->production_model->details($id);
			$detail['item_list'] = $this->produk_ledger_model->ledger_in($detail['data']['kode_produksi']);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Detail batch '.$name,
					"body"		=> $this->load->view('admin/detail_production', $detail,true),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function insert(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Add Production Batch',
					"body"		=> $this->production_model->form(),
				),
			);
			$data['modal']['body'] .= '<script>'.file_get_contents('assets/js/produk_ctrl.js').'</script>';
			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->production_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Edit '.$name,
					"body"		=> $this->production_model->form($detail,'edit'),
				),
			);
			$data['modal']['body'] .= '<script>'.file_get_contents('assets/js/produk_ctrl.js').'</script>';
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		if($this->validation() == TRUE && $this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$postdata = $this->input->post();
			$post_detail = array_key_exists('details', $postdata) ? $postdata['details'] : '';
			$actor 		 = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');

			// batch variable
			$batch = array(
				'token'			=> $this->input->post('token', true),
				'kode_produksi' 	=> $postdata['kode_produksi'],
				'tgl_masuk'		=> $postdata['tgl_masuk'],
				'remark'		=> $postdata['remark'],
			);

			// product ledger
			$ledger = array();
			if(!empty($post_detail['item_code'])){
				foreach ($post_detail['item_code'] as $key => $value) {
					if(!empty($value)){
						$ledger[$value] = array(
							'doc_no' 	=> $this->input->post('kode_produksi', true),
							'produkid' 	=> $value,
							'qty_in'	=> !empty($ledger[$value]) ? $ledger[$value]['qty_in'] + $post_detail['item_qty'][$key] : $post_detail['item_qty'][$key],
							'remarks'	=> 'Adjustment stock from production batch @'.$this->input->post('kode_produksi'),
							'tanggal_masuk' => date('Y-m-d H:i:s'),
							'status'		=> 'success'
						);
						if(array_key_exists('id', $post_detail) && !empty($post_detail['id'])){
							$ledger[$value]['id'] = $post_detail['id'][$key];
							$ledger[$value]['updated_by'] = $actor;
							$ledger[$value]['updated_at'] = date('Y-m-d H:i:s');
						}
						else{
							$ledger[$value]['created_by'] = $actor;
						}
					}
				}
			}

			// Insert statement
			$produk_sn = array();
			if(array_key_exists('id', $postdata) == FALSE && !empty($ledger)){
				$batch['created_by'] = $actor;
				// insert transaksi baru ke dalam table inventori
				$sql = $this->production_model->insert($batch, true); // nilai return berupa id terakhir yang berhasil di insert

				// insert batching ledger ke dalam table product_ledger
				$sql = $this->db->insert_batch('product_ledger',array_values($ledger));
				
				// cek apakah produk id tersebut dapat diinput serial number
				$group_id 		= array_keys($ledger);
				$status 		= $this->production_model->product_list($group_id);
				$product_stat 	= array_filter($status, function($items){
					if($items['has_code'] == 'Y'){
						return $items;
					}
				});
				$unique_id    	= strtotime(date('Y-m-d H:i:s'));

				// insert batching product serial number ke dalam table produk_sn

				foreach ($product_stat as $key => $value) {
					$unique = 1;
					if(array_key_exists($value['id'], $ledger)){
						for ($i=1; $i <= $ledger[$value['id']]['qty_in'] ; $i++) { 
							$produk_sn[] = array(
									'unique_identifier' => $unique_id.$value['id'].sprintf('%04s',$unique),
									'doc_no' 			=> $ledger[$value['id']]['doc_no'],
									'product_name'		=> $value['name'],
									'product_id' 		=> $ledger[$value['id']]['produkid'],
									'status'			=> 'available',
									'created_at'		=> date('Y-m-d H:i:s'),
									'created_by'		=> $actor
								);
							$unique++;
						}
					}
				}

				if(!empty($produk_sn)){
					$sql = $this->db->insert_batch('product_sn', $produk_sn);
				}
			}
			// update statement
			elseif(array_key_exists('id', $postdata)){
				$batch['updated_by'] = $actor;

				if(empty($ledger)){
					// jika tidak ada item, maka hapus data invoice
					$sql = $this->production_model->delete(array('id' => $postdata['id']));

				}
				else{
					// jika terdapat perubahan pada item, lakukan update table production dan product_ledger
					$sql = $this->production_model->update($batch, true);

					// update item pada table product_ledger
					$sql = ($sql == true) ? $this->db->update_batch('product_ledger', array_values($ledger),'id') : false;
				}
			}
			else{
				$sql = FALSE;
			}

			if($sql == TRUE){
				$flash = array(
					'success' => true,
					'modal'	  => true,
					'msg'	  => 'Inventory data has been added',
				);	
			}
			else{
				$flash = array(
					'success' => false,
					'modal'	  => true,
					'msg'	  => self::set_flash('error','Cannot add inventory data, Please choose the item.',false),
				);
			}
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $this->input->post(), false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}

	function remove_item(){
		$this->post_check();

		$itemid = $this->input->post('itemid', true);

		// Hapus table produk_assembling dimana produk = $itemid
		$destroy = $this->production_model->change_ledger($itemid);
		// $destroy = true;
		if($destroy == true){
			$output = array(
				'success' => true,
				'modal'	  => true,				
				'title'	  => 'Done',
				'type'	 => 'success',				
				'message' => 'Item has been canceled'
			);				
		}
		else{
			$output = array(
				'success' => false,
				'modal'	  => true,				
				'title'	  => 'Failed',
				'type'	 => 'error',				
				'message' => 'Cannot canceled item from invoice'
			);				
		}

		$this->outputs($output, 'application/json');

	}
	private function validation(){
		$data = array(
			array("name"=>"kode_produksi","message"=>'Invoice number',"valid_type"=>"required"),
			array("name"=>"tgl_masuk","message"=>'Invoice date',"valid_type"=>"required"),
		);
		return $this->validate($data);		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->production_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? 'Deleted' : 'Failed',
		);
		$this->outputs($flash, 'application/json');
	}

	/*
	* ######################################################################################
	* FUNCTION UNTUK EXPORT SERIAL NUMBER KE DALAM FORMAT EXCEL
	* ######################################################################################
	*/

	function export($inv){
		if($this->vars['_PRINTING'] == true && $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token') != null){
			$this->vars['_CACHE_DISABLED'] 	= true;
			$check_invoice = $this->production_model->details($inv, 'kode_produksi');
			if(!empty($check_invoice)){
				$list = $this->inventori_model->export_data($inv);

				$this->load->library('excelio', $this->vars);

				$excel = $this->excelio
							  ->create_objdrawing()
							  ->filename('BATCH_'.$inv)
							  ->create_sheet($inv, $list)
							  ->auto_gen();
				// $this->outputs($list, 'application/json');
			}
			else{
				$this->outputs('Data tidak ditemukan');
			}		
		}
	}						
}