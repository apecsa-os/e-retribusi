<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Inventori extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/inventori_model",
			'admin/produk_assembling_model',
		);
		$this->load->model($model);
	}

	function index(){
		$this->css 						= array('form','jqueryui');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','jqueryui');
		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->inventori_model->table();
		$this->pageview('output');		
	}

	function renders(){
		$this->post_check();
		$data = $this->inventori_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}

	function view($id = null, $name = null){
		$this->post_check();
		if($this->vars['_VIEWING'] == true){
			$detail['data'] = $this->inventori_model->details($id);
			$detail['bundled'] = $this->inventori_model->bundled_detail($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Detail Serial',
					"body"		=> $this->load->view('admin/detail_product_sn', $detail,true),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->inventori_model->details($id);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Edit '.$name,
					"body"		=> $this->inventori_model->form($detail,'edit'),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function save(){
		$this->post_check();
		if($this->validation() == true  && $this->input->post('token') != null){
			$action = false;	
			if(array_key_exists('id', $this->input->post()) == true){
				$cond 	= array('id' => $this->input->post('id', true));
				$update_data = array(
					'token'			=> $this->input->post('token', true),
					'serial_number' => $this->input->post('serial_number', true)
				);
				$action = $this->inventori_model->update($update_data, $cond, true);
			}

			if($action == true){
				$flash = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=> self::set_flash('success', 'Serial number has been settled', false),
				);				
			}
			else{
				$flash = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=>self::set_flash('error', 'Something error occur', false),
				);				
			}
		}
		else{
			$flash 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $this->input->post(), false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);
		}		
		
		$this->outputs($flash, 'application/json');		
	}

	private function validation(){
		$data = array(
			array("name"=>"serial_number","message"=>'Serial number',"valid_type"=>"required"),
		);
		return $this->validate($data);		
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->inventori_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? 'Deleted' : 'Failed',
		);
		$this->outputs($flash, 'application/json');
	}

	/*
	* ######################################################################################
	* FUNCTION UNTUK BUNDLING PAKET VOUCHER VIA AJAX
	* ######################################################################################
	*/

	function bundling($id){
		$this->post_check();
		$bundling['url']			 = $this->vars['_ROUTED'];
		$bundling['sn_no']			 = $id;
		$bundling['detail'] 		 = $this->inventori_model->details($id);
		$bundling['assembling_data'] = $this->produk_assembling_model->assembling_data($bundling['detail']['product_id']);
		$bundling['serialdata']		 = $this->inventori_model->serial_list(array_column($bundling['assembling_data'], 'produk_item_id'), $id);
		$form			 			 = $this->load->view('form/sn_bundling_form', $bundling, true);
		$output = array(
			'success'   => true,
			'modal' 	=> array(
				'title' => 'Serial numbers packaging',
				'body'	=> $form,			
			)
		);
		$this->outputs($output, 'application/json');
	}

	function open_serial(){
		if($this->input->get('token', true) == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$this->vars['_CACHE_DISABLED'] 	= true;

			$item = $this->input->get('item', true);
			$tabledata['list'] = $this->inventori_model->get_serialdata($item);
			$table  = $this->load->view('form/sn_bundling_selection_form', $tabledata, true);
			$this->outputs($table);
		}
	}

	function bundling_sn(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$user = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
			$serialdata = $this->input->post('serialist', true);
			$serialdata = json_decode($serialdata);
			$serial_number = $this->input->post('serialnumber', true);

			$update_data = array();
			foreach ($serialdata as $key => $value) {
				$update_data[] = array(
					'id' 				=> $value->unique,
					'bundling_num'		=> $serial_number,
					'status'			=> 'unavailable',
					'updated_at'		=> date('Y-m-d H:i:s'),
					'updated_by'		=> $user
				);
			}

			$sql = true;
			
			// update status paket yg telah dibuild
			$sql = $this->db
						->set('status','packed')
						->from('product_sn')
						->where('id', $serial_number)
						->update();

			// jika paket serial sebelumnya telah ada di table product_sn maka update
			$sql = $this->db
				 ->set('bundling_num', null)
				 ->set('status','available')
				 ->set('updated_at', date('Y-m-d H:i:s'))
				 ->set('updated_by', $user)
				 ->from('product_sn')
				 ->where('bundling_num', $serial_number)
				 ->update();

			// new update batch
			$sql = ($sql == true) ? $this->db->update_batch('product_sn', $update_data, 'id') : false;

			if($sql == true){
	 			$output = array(
					'success' 	=> true,
					"modal"		=> true,
					"msg"		=> self::set_flash('success', 'Product has been packaged', false),
				);
			}
			else{
				$output = array(
					'success' 	=> false,
					"modal"		=> true,
					"msg"		=> self::set_flash('error', 'Something error occur', false),				
				);
			}

			$this->outputs($output, 'application/json');
		}
	}

	function import(){
		$this->post_check();
		if($this->vars['_INSERT'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Import serial number',
					"body"		=> $this->inventori_model->import_form(),
				),
			);
			$this->outputs($data, 'application/json');
		}		
	}

	function force_upload(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			if($_FILES && !empty($_FILES['fupload']['name'][0])){
				$filetype = $_FILES['fupload']['type'][0];
				
				$mime_allowed = array('application/vnd.ms-excel', 'application/msexcel', 'application/x-msexcel', 'application/x-ms-excel', 'application/x-excel', 'application/x-dos_ms_excel', 'application/xls', 'application/x-xls', 'application/excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel');
				
				if(in_array($filetype, $mime_allowed) == true){
					$status = 'OK';
				}
				else{
					$status = 'FAIL';
				}
			}
			else{
				$status = 'FAIL';
			}

			if($status == 'OK'){
				$user = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
				$this->load->library('excelio', $this->vars);
				$path = $_FILES['fupload']['tmp_name'][0];
				$phpexcel = $this->excelio
								 ->create_objreader($path)
								 ->activesheet(0)
								 ->change_array_keys('unique_identifier','serial_number')
								 ->add_column('updated_at', date('Y-m-d H:i:s'))
								 ->add_column('updated_by', $user)
								 ->row_index(1)
								 ->start_index(2)
								 ->start_column('A')
								 ->read();
				if(!empty($phpexcel)){
					$this->db->update_batch('product_sn',$phpexcel,'unique_identifier');
					$output = array(
						'success' 	=> true,
						'modal'		=> true,
						'msg'		=> 'File has been uploaded',
					);
				}
				else{
					$output = array(
						'success' 	=> false,
						'modal'		=> true,
						'msg'		=> self::set_flash('error','there\' was an error occur')
					);
				}
			}
			else{
				$output = array(
					'success' 	=> false,
					'modal'		=> true,
					'msg'	 	=> self::set_flash('error','Please choose correct excel files to be execute', false),
					'upload'	=> $_FILES,
					'ext'		=> $filetype
				);
			}

			$this->outputs($output, 'application/json');
		}
	}
	function print_excel(){
		$this->post_check();
		if($this->vars['_PRINTING'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Print Stock',
					"body"		=> $this->inventori_model->print_form(),
				),
			);

			$this->outputs($data, 'application/json');
		}		
	}

	function excel(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$month = $this->input->post('month', true) + 1;
			$year = $this->input->post('year', true);
			$month = sprintf('%02s', $month);			
			$data = $this->inventori_model->print_data($month, $year);
			if(!empty($data)){
				$this->load->library('excelio', $this->vars);
				$excel = $this->excelio
							  ->create_objdrawing()
							  ->filename('Stock Report '.$month.'-'.$year)
							  ->create_sheet('Report', $data)
							  ->auto_gen();				
			}
			else{
				$this->outputs('Oops, Data not found!');
			}
		}
		else{
			redirect(404);
		}
	}
}