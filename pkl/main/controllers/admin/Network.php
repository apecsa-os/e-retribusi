<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Network extends Admin_Controller
{
	var $bahasa;
	function __construct(){
		parent::__construct();
		$model = array(
			"admin/network_model",
		);
		$this->load->model($model);
	}

	function index(){
		$this->css = array('jqueryui');
		$this->js 						= array('fiquery','jqueryui');

		$this->vars['url']				= $this->vars['_ROUTED'];
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['placement_tree'] 	= $this->network_model->genealogy_tree();
		$this->vars['last_query']		= $this->network_model->last_query();
		$this->vars['stylesheet']		= '<style type="text/css">'.file_get_contents('assets/stylesheet/genealogy_tree2.css').'</style>';
		$this->vars['_JS_ADDON']		= $this->load->view('genealogy_tree_js','',true);
		$this->pageview('form/genealogy_tree');		
	}

	/*
	* ##############################################################################################################
	* THIS FUNCTION GROUP IS USED FOR ADDED NEW MEMBER
	* ##############################################################################################################	
	*/

	// find member data especially for finding sponsor ID
	function find_member(){
		$this->vars['_CACHE_DISABLED'] 	= true;
		$term = $this->input->get('term', true);
		$sponsor = $this->network_model->autocomplete_data($term);
		$result = array(
			'success' 	=> true,
			'result'		=> $sponsor
		);

		$this->outputs($sponsor, 'application/json');
	}

	function tree(){
		$this->post_check();
		$this->vars['_CACHE_DISABLED'] = true;
		$data['tree'] = $this->network_model->genealogy_tree();
		$pohon = $this->load->view('tree/pohon_jaringan', $data, true);
		$this->outputs($data, 'application/json');
	}	
	/*
	* LOAD GENEALOGY TREE
	*/
	function load_trees(){
		$this->post_check();
		$this->vars['_CACHE_DISABLED'] 	= true;
		if($this->vars['_VIEWING'] == true && $this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			
			$tree = $this->network_model->genealogy_tree();
			// if($tree != FALSE){
				$data['url']			= $this->vars['_ROUTED'];
				$data['ajax']			= true;
				$data['placement_tree'] = $tree;
				$data['member']		 	= $this->input->post('member');
				$data['slotnumber']		= $this->input->post('slotnumber');
				$data['add_member_url']	= BASE_PANEL.'administrator/member/insert';
				$data['stylesheet']		= '<style type="text/css">'.file_get_contents('assets/stylesheet/genealogy_tree2.css').'</style>';
				$output = array(
					'success' => true, 
					'content' => array(
						"body"		=> $this->load->view('form/genealogy_tree', $data, true),
					),
				);			
			// }
			// else{
			// 	$output = array(
			// 		'success' => false,
			// 		'type' => 'alerts',
			// 		'message' => array(
			// 			'title' => 'Info',
			// 			'type' => 'warning',
			// 			'text' => 'Member not found',
			// 			'showCancelButton' => false,
			// 			'closeOnConfirm' => true
			// 		)
			// 	);
			// }
		}		
		else{
			$output = array(
				'success' => false,
				'type' => 'alerts',
				'message' => array(
					'title' => 'Info',
					'type' => 'warning',
					'text' => 'Unauthenticated user has detected',
					'showCancelButton' => false,
					'closeOnConfirm' => true
				)
			);
		}
		$this->outputs($output, 'application/json');
	}

	function network_profile(){
		$this->post_check();
		$this->vars['_CACHE_DISABLED'] 	= true;
		if($this->vars['_VIEWING'] == true && $this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$data['member']		 	= $this->input->post('member');
			$data['slotnumber']		= $this->input->post('slotnumber');

			$data['network'] 		= $this->network_model->detail_network($data);
			$data['network_group']  = $this->network_model->network_group($data['slotnumber']);
			$data['point']			= $this->network_model->network_point($data['network'], $data['slotnumber']);
			$data['summary_point']	= $this->network_model->summary_network_point($data['network'], $data['slotnumber']);
			
			$output = array(
				'success' => true, 
				'content' => array(
					"body"		=> $this->load->view('admin/network_tree_detail', $data, true),
				),
			);
		}		
		else{
			$output = array(
				'success' => false,
				'type' => 'alerts',
				'message' => array(
					'title' => 'Info',
					'type' => 'warning',
					'text' => 'Unauthenticated user has detected',
					'showCancelButton' => false,
					'closeOnConfirm' => true
				)
			);
		}
		$this->outputs($output, 'application/json');		
	}
}