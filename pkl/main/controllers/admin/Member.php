<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Member extends Admin_Controller
{
	var $bahasa;
	var $validback; // returning validation data into this variable

	function __construct(){
		parent::__construct();
		// load semua resources model yang diperlukan
		$model = array(
			"admin/member_model",
			// "mlm/mlm_model",
			"admin/network_model",
		);
		$this->load->model($model);

		// define kata kunci bahasa yang digunakan
		$key 	= array('insert', 'edit', 'delete','form','group','delete_success', 'delete_failed','name');
		$this->bahasa = $this->resources_model->translation($key, 0);
	}

	function index(){
		$this->css 						= array('form', 'jqueryui','select2');
		$this->js 						= array('datatable','bootstrap_datable','fiquery','form','jqueryui','select2');

		
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->vars['OUTPUT'] 			= $this->member_model->table();	
		$this->pageview('output');		
	}
	/*
	* ##############################################################################################################
	* THIS FUNCTION IS USED FOR RENDERING DATATABLE
	* ##############################################################################################################	
	*/
	function renders(){
		$this->post_check();
		$data = $this->member_model->json_data();
		
		$this->vars['_HOOK_EXPIRES'] 	= '-1 month';
		$this->vars['_CACHE_DISABLED'] 	= true;
		$this->outputs($data, 'application/json');
	}
	/*
	* ##############################################################################################################
	* THIS FUNCTION USED FOR VIEW MEMBER PROFILE
	* ##############################################################################################################	
	*/
	function view($id = null, $name = null){
		$this->post_check();
		if($this->vars['_VIEWING'] == true){
			$detail['data'] = $this->member_model->details($id, $name);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Member Detail',
					"body"		=> $this->load->view('admin/detail_member', $detail,true),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	/*
	* ##############################################################################################################
	* THIS FUNCTION GROUP IS USED FOR ADDED NEW MEMBER
	* ##############################################################################################################	
	*/

	// find member data especially for finding sponsor ID
	function find_member(){
		$this->vars['_CACHE_DISABLED'] 	= true;
		$term = $this->input->get('term', true);
		$sponsor = $this->network_model->autocomplete_data($term);
		$result = array(
			'success' 	=> true,
			'result'		=> $sponsor
		);

		$this->outputs($sponsor, 'application/json');
	}

	// check input member ID
	function check_member_id(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			if(preg_match('/[\W]+/', $this->input->post('member')) == false){
				$config = $this->query_model->config(5)->get_config_data();
				if(strlen($this->input->post('member', true)) >= $config['memberid_length'] && strlen($this->input->post('member', true)) <= 10){
					$data = $this->member_model->details(null, $this->input->post('member', true));
					if(!empty($data)){
						$output = array(
							'success' => true,
							'colors' => '#963737',
							'message' => 'Member id already exist, please try another ID'
						);
					}
					else{
						$output = array(
							'success' => true,
							'colors' => '#19981e',
							'message' => 'Member ID available'
						);
					}
				}
				else{
					$output = array(
							'success' => true,
							'colors' => '#963737',
							'message' => 'Member ID must have minimum length '.$config['memberid_length'].' and maximum length 10 of characters'
					);
				}
			}
			else{
				$output = array(
					'success' => true,
					'colors' => '#963737',
					'message' => 'Member ID does not allowed special characters'
				);
			}
			
		}
		else{
			$output = array(
				'success' => false,
				'type' => 'alerts',
				'message' => array(
					'title' => 'Info',
					'type' => 'warning',
					'text' => 'You dont have restriction access to this page',
					'showCancelButton' => false,
					'closeOnConfirm' => true
				)
			);
		}
		$this->outputs($output, 'application/json');
	}

	// load all active package 
	function get_package(){
		$this->post_check();
		$token = $this->input->post('token', true);
		$member = $this->input->post('member', true);
		if($token == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$config = $this->query_model->config(5)->get_config_data();
			$package['list'] = $this->member_model->get_package_list($member, $config);
			$result = array(
				'success' 	=> true,
				'content'	=> $this->load->view('form/option_package_list', $package, true),
				// 'paket' 	=> $package
			);
		}
		else{
			$result = array(
				'success' => false,
				'message' => 'You doesn\'t have priviledge to do this method'
			);
		}
		$this->outputs($result, 'application/json');
	}
	/*
	* LOAD GENEALOGY TREE
	*/
	function load_trees(){
		$this->post_check();
		$this->vars['_CACHE_DISABLED'] 	= true;
		if($this->vars['_VIEWING'] == true && $this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){

			$tree = $this->network_model->genealogy_tree();
			// if($tree != FALSE){
				$data['url']			= $this->vars['_ROUTED'];
				$data['ajax']			= true;
				$data['placement_tree'] = $tree;
				$data['tree_class']		= 'vertical ';
				$data['stylesheet']		= '<style type="text/css">'.file_get_contents('assets/stylesheet/genealogy_tree2.css').'</style>';
				$data['stylesheet']		.= $this->load->view('genealogy_tree_js','',true);				
				$output = array(
					'success' => true, 
					'modal' => array(
						"title" 	=> 'Select network position',
						"body"		=> $this->load->view('form/genealogy_tree', $data, true),
					),
				);			
			// }
			// else{
			// 	$output = array(
			// 		'success' => false,
			// 		'type' => 'alerts',
			// 		'message' => array(
			// 			'title' => 'Info',
			// 			'type' => 'warning',
			// 			'text' => 'Member not found',
			// 			'showCancelButton' => false,
			// 			'closeOnConfirm' => true
			// 		)
			// 	);
			// }
		}		
		else{
			$output = array(
				'success' => false,
				'type' => 'alerts',
				'message' => array(
					'title' => 'Info',
					'type' => 'warning',
					'text' => 'Unauthenticated user has detected',
					'showCancelButton' => false,
					'closeOnConfirm' => true
				)
			);
		}
		$this->outputs($output, 'application/json');
	}

	/*
	* ##############################################################################################################	
	* END OF ADDED NEW MEMBER FUNCTION GROUP
	* ##############################################################################################################	
	*/

	// function insert digunakan untuk menampilkan formulir insert member / add new member
	function insert(){
		$this->css 						= array('form', 'jqueryui');
		$this->js 						= array('fiquery','form','jqueryui');

		$this->vars['_CACHE_DISABLED'] 	= true;
		$data['url']				= $this->vars['_ROUTED'];
		$data['token']				= $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token');
		$data['bank']				= $this->member_model->bank_data();
		$data['nid']				= $this->network_model->get_netwid();
		$this->vars['OUTPUT'] 		= $this->load->view('form/member_registration_form',$data, true);	
		$this->pageview('output');
	}

	function edit($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$detail = $this->member_model->details($id, $name);
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Edit ID ' . $name,
					"body"		=> $this->member_model->form($detail),
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	function upgrade($id = null, $name = null){
		$this->post_check();
		if($this->vars['_UPDATE'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Upgrade ID ' . $name,
					"body"		=> $this->member_model->upgrade_form($id, $name)
				),
			);
			$this->outputs($data, 'application/json');
		}
	}

	// function register digunakan untuk memproses data dari form tambah member atau insert new member
	function register(){
		// 1. Check metode http request, jika post maka eksekusi proses registrasi, jika tidak lempar error 404
		$this->post_check();
		// 2. lakukan validasi form
		if(self::validation(1) == true && $this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			// 3.1 jika telah lolos validasi
			// 4. ambil default member group pada table usergroup
			$member_group = $this->member_model->default_group();

			// 5. Load model terpilih untuk eksekusi fungsi MLM untuk menyimpan data member ke dalam tabel membermaster
			$reg_method  = 'registration_'.strtolower(REGMETHOD).'_model';
			$this->load->model('mlm/'.$reg_method);

			// 6. Predefined variable membermaster
			$membermaster = array(
				'group' 		=> $member_group['groupid'],
				'produkid' 		=> $this->input->post('method') == 0 ? 1 : $this->validback['acticode']['product_id'],
				'has_upgrade' 	=> $this->input->post('method') == 1 && !empty($this->validback['acticode']) ? 'Y' : 'N',
				'name'			=> $this->input->post('fullname'),
				'jk'			=> $this->input->post('jk'),
				'tgllahir' 		=> $this->input->post('tgllahir'),
				'bank_name' 	=> $this->input->post('bank_name'),
				'account_name' 	=> $this->input->post('account_name'),
				'account_number' 	=> $this->input->post('account_number'),
				'noktp'			=> $this->input->post('noktp'),
				'alamat'		=> $this->input->post('alamat'),
				'npwp'			=> $this->input->post('npwp'),
				'hp'			=> $this->input->post('hp'),
				'email'			=> $this->input->post('email'),
				'ahliwaris'		=> $this->input->post('ahliwaris'),
				'username'		=> $this->input->post('member_id'),
				'password'		=> sha1(md5(strtolower($this->input->post('password').$this->config->item('encryption_key')))),
				'secpassword'	=> sha1(md5(strtolower($this->input->post('hp').$this->config->item('encryption_key')))),
				'created'		=> date('Y-m-d H:i:s'),
				'createdby'		=> $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username')
			);
			// 7. Eksekusi methode mlm dan simpan member ke dalam table membermaster			
			$execute = $this->$reg_method->registration($membermaster);

			// 8. Predefined member network
			$process_date = date('Y-m-d H:i:s');
			$networking = array(
				'networks_id'  	=> $this->input->post('netwid', true),
				'member_id'		=> $execute,
				'sponsor_id'	=> $this->validback['sponsor']['id'],
				'prefix'		=> 'A',
				'slotnumb'		=> $this->input->post('placement_number', true),
				'created_at'	=> $process_date
			);

			// 9. Eksekusi member network dan simpan networking member ke dalam table member networks
			$execute = $this->$reg_method->networking($networking);

			// 10.a Jika Upgraded member maka berikan bonus kepada sponsor
			if($execute == TRUE && $membermaster['has_upgrade'] == 'Y'){
				$this->load->model('mlm/bonus_model');
				$sponsor_position = $this->network_model->member_slot($networking['sponsor_id']);
				$execute = $this->bonus_model->add_sponsorship_bonus($networking['sponsor_id'], $sponsor_position['slotnumb'], $this->validback['acticode']);	
				
				// 11. Insert pengeluaran barang pada tabel product ledger
				$code = 'UPG'.date('ymd').strtoupper($membermaster['username']);
				$this->load->model('admin/produk_ledger_model');
				$product_ledger = array(
					'doc_no' => $code,
					'produkid' => $membermaster['produkid'],
					'qty_out' => 1,
					'remarks' => 'Upgrading member ID @'.$membermaster['username'],
					'tanggal_keluar' => $process_date,
					'created_by' => 'system',
					'status' => 'success'
				);
				$this->produk_ledger_model->insert($product_ledger);
				
				// 12. Update inventori serial number
				$this->load->model('admin/inventori_model');
				$update_serial = array(
					'token' => $this->input->post('token'),
					'status' => 'used',
					'owner'	=> $membermaster['username'],
					'used_at' => $process_date,
					'ledger_code' => $code,
				);

				$this->inventori_model->update($update_serial, array('unique_identifier' => $this->input->post('activation_code', true)));

				// 13. SMS notifikasi kepada member (pending)
				// $this->load->model('mlm/sms_gateway_model');
				// $execute = ($execute == TRUE) ? $this->sms_gateway_model->send();
			}

			if($execute == true){
				self::set_flash('Registered', 'Congratulations ! Your Member Has Been Activated.');
			}
			// sebaliknya jika gagal, tampilkan pesan error
			else{
				self::set_flash('Error', 'There was an error occured, Please call system administrator immediately');
			}			
		}
		else{
			// 3.2 lempar kesalahan error ke dalam form
			self::set_flash('validation error',$this->input->post(), true);
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	function save(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			if($this->validation(2) == true){
				$data = array(
					"token" 		=> $this->input->post('token', true),
					"username" 		=> $this->input->post('username', true),
					"name" 			=> $this->input->post('name', true),
					"jk"			=> $this->input->post('jk', true),
					"tempatlahir" 	=> $this->input->post('tempatlahir', true),
					"tgllahir"		=> $this->input->post('tgllahir', true),
					"alamat"		=> $this->input->post('alamat', true),
					"hp"			=> $this->input->post('hp', true),
					"status" 		=> $this->input->post('status', true),
					"kodepos" 		=> $this->input->post('kodepos', true),
					"noktp" 		=> $this->input->post('noktp', true),
					"autotransfer"	=> $this->input->post('autotransfer', true),
					"updatedby"		=> $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username')
				);
				$where = array(
					"id"	=> $this->input->post('id', true)
				);
				$insert = $this->member_model->update($data, $where);
				if($insert == true){
					$output = array(
						"success" 	=> true,
						"modal"		=> true,
						"msg"		=> self::set_flash('Data has been updated', '', false),
					);
				}
				else{
					$output = array(
						"success" 	=> false,
						"modal"		=> true,
						"msg"		=> self::set_flash('error', 'Error updating data', false),
					);					
				}
			}
			else{
				$output 	= array(
					'success' => false, 
					'modal'	  => true,
					'msg' 	  => self::set_flash('validation error', $this->input->post(), false),
					'error'	  => array_keys($this->form_validation->error_array()),
				);				
			}
		}
		else{
			$output = array(
				"success" 	=> false,
				"modal"		=> true,
				"msg"		=> self::set_flash('You don\'t have any access to this page', false),
			);		
		}
		$this->outputs($output, 'application/json');
	}

	function delete(){
		$this->post_check();
		$where = array(
			'id' => $this->input->post('stamp', true)
		);
		$attemp = $this->member_model->delete($where);

		$flash = array(
			'success' 	=> $attemp != false ? true : false,
			'title'		=> $this->input->post('title'),
			'subtitle'	=> $attemp != false ? $this->bahasa['delete_success'] : $this->bahasa['delete_failed'],
		);
		$this->outputs($flash, 'application/json');
	}

	// function validation digunakan untuk menyediakan parameter validasi terhadap post request dari server
	private function validation($type){
		$data = array(						
			array("name"=>"hp","message"=>'Mobile number',"valid_type"=>"required|numeric")			
		);

		if($type == 1){
			$data[] = array("name"=>"sponsor","message"=>'Nama sponsor',"valid_type"=>"required|callback_sponsor_verification");
			$data[] = array("name"=>"placement_number","message"=>'Placement number',"valid_type"=>"required");
			$data[] = array("name"=>"member_id","message"=>'Username',"valid_type"=>"required|min_length[6]|max_length[12]");
			$data[] = array("name"=>"fullname","message"=>'Member fullname',"valid_type"=>"required");
			$data[] = array("name"=>"tgllahir","message"=>'Date of Birth',"valid_type"=>"required");
			$data[] = array("name"=>"email","message"=>'Email address',"valid_type"=>"required|valid_email");
			$data[] = array("name"=>"password","message"=>'Password',"valid_type"=>"required|min_length[4]|max_length[8]|alpha_numeric");
			$data[] = array("name"=>"repass","message"=>'Retype Password',"valid_type"=>"required|matches[password]");
			$data[] = array("name"=>"bank_name","message"=>'Bank Name',"valid_type"=>"required");
			$data[] = array("name"=>"account_name","message"=>'Account name',"valid_type"=>"required");
			$data[] = array("name"=>"account_number","message"=>'Account Number',"valid_type"=>"required|numeric");
			$data[] = array("name"=>"noktp","message"=>'ID Card Number',"valid_type"=>"required|numeric");
			$data[] = array("name"=>"npwp","message"=>'NPWP',"valid_type"=>"required|numeric");
			
			if($this->input->post('method') == 1){
				$data[] = array("name"=>"activation_code","message"=>'Activation code',"valid_type"=>"required|callback_activation_code");
			}
		}
		elseif ($type == 2) {
			$data[] = array("name"=>"username","message"=>'Username',"valid_type"=>"required");			
			$data[] = array("name"=>"name","message"=>'Member name',"valid_type"=>"required");
		}
		elseif($type == 3){
			$data = array();
			$data[] = array("name"=>"package","message"=>'Upgrade code',"valid_type"=>"required|callback_activation_code");
		}
		// panggil fungsi validate (referensi : core/FI_Controller.php chapter validate function)
		return $this->validate($data);		
	}					

	// fungsi callback untuk memverifikasi apakah username sponsor yang dikirim oleh server ada di database
	// jika tidak ada maka kembalikan sebagai nilai FALSE
	function sponsor_verification($string){
		$verify = $this->member_model->details(null, $string);
		if(!empty($verify)){
			$this->validback['sponsor'] = $verify;
			$this->validback['sponsor_level'] = $this->member_model->get_member_placement('member_id',$verify['id']);
			return true;
		}
		else{
			$this->form_validation->set_message('sponsor_verification','Invalid {field} id');
			return false;
		}
	}

	// fungsi callback untuk mengecek activation code. jika sudah digunakan maka kembalikan nilai sebagai FALSE
	function activation_code($string){
		if(empty($string) || $string == -1){
			$this->form_validation->set_message('activation_code','Activation code wajib dipilih!');
			return false;
		}
		else{
			$verify = $this->member_model->identify_package($string);
			if(!empty($verify)){
				$this->validback['acticode'] = $verify;
				return true;
			}
			else{
				$this->form_validation->set_message('activation_code','Invalid activation code');
				return false;
			}			
		}
	}

	function network_profile(){
		$this->post_check();
		$this->vars['_CACHE_DISABLED'] 	= true;
		if($this->vars['_VIEWING'] == true && $this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$data['member']		 	= $this->input->post('member');
			$data['slotnumber']		= $this->input->post('slotnumber');

			$data['network'] 		= $this->network_model->detail_network($data);
			$data['bonus']			= 
			$output = array(
				'success' => true, 
				'content' => array(
					"body"		=> $this->load->view('admin/network_tree_detail', $data, true),
				),
			);
		}		
		else{
			$output = array(
				'success' => false,
				'type' => 'alerts',
				'message' => array(
					'title' => 'Info',
					'type' => 'warning',
					'text' => 'Unauthenticated user has detected',
					'showCancelButton' => false,
					'closeOnConfirm' => true
				)
			);
		}
		$this->outputs($output, 'application/json');		
	}

	function upgrade_member(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token') && $this->validation(3) == true){
			// 1. Inisialisasi upgrade member
			$member_id 		= $this->input->post('id', true);
			$username 		= $this->input->post('username', true);
			$produk_unique 	= $this->input->post('package', true);
			$process_date 	= date('Y-m-d H:i:s');
			$sponsor_id 	= $this->input->post('sponsor_id', true);
			$sponsor_slot 	= $this->network_model->member_slot($sponsor_id);
			
			$data = array(
				"token" 		=> $this->input->post('token', true),
				'produkid'		=> $this->validback['acticode']['product_id'],
				"has_upgrade" 	=> 'Y',
				"upgrade_at"	=> $process_date,
				"upgrade_by"	=> $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username')
			);

			$where = array(
				"id"	=> $this->input->post('id', true)
			);

			// 2. Update data membermaster
			$execute = $this->member_model->update($data, $where);

			// 3 Jika Upgraded member maka berikan bonus kepada sponsor
			if($execute == TRUE && $data['has_upgrade'] == 'Y'){
				$this->load->model('mlm/bonus_model');
				$execute = $this->bonus_model->add_sponsorship_bonus($sponsor_id, $sponsor_slot['slotnumb'], $this->validback['acticode'], 'username');	
					
				// 11. Insert pengeluaran barang pada tabel product ledger
				$code = 'UPG'.date('ymd').strtoupper($username);
				$this->load->model('admin/produk_ledger_model');
				$product_ledger = array(
					'doc_no' => $code,
					'produkid' => $this->validback['acticode']['product_id'],
					'qty_out' => 1,
					'remarks' => 'Upgrading member ID @'.$username,
					'tanggal_keluar' => $process_date,
					'created_by' => 'system',
					'status' => 'success'
				);
				$this->produk_ledger_model->insert($product_ledger);
					
				// 12. Update inventori serial number
				$this->load->model('admin/inventori_model');
				$update_serial = array(
					'token' => $this->input->post('token'),
					'status' => 'used',
					'owner'	=> $username,
					'used_at' => $process_date,
					'ledger_code' => $code,
				);

				$this->inventori_model->update($update_serial, array('unique_identifier' => $produk_unique));
			}


			if($execute == true){				
				$output = array(
					"success" 	=> true,
					"modal"		=> true,
					"msg"		=> self::set_flash('Member has been upgraded!', '', false),
				);
			}
			else{
				$output = array(
					"success" 	=> false,
					"modal"		=> true,
					"msg"		=> self::set_flash('error', 'Error upgrading member', false),
				);					
			}
		}
		else{
			$output 	= array(
				'success' => false, 
				'modal'	  => true,
				'msg' 	  => self::set_flash('validation error', $this->input->post(), false),
				'error'	  => array_keys($this->form_validation->error_array()),
			);		
		}
		$this->outputs($output, 'application/json');
	}

	function print_excel(){
		$this->post_check();
		if($this->vars['_PRINTING'] == true){
			$data = array(
				'success' => true, 
				'modal' => array(
					"title" 	=> 'Print Member Report',
					"body"		=> $this->member_model->print_form(),
				),
			);

			$this->outputs($data, 'application/json');
		}		
	}

	function excel(){
		$this->post_check();
		if($this->input->post('token') == $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token')){
			$data = $this->member_model->print_data();
			if(!empty($data)){
				$this->load->library('excelio', $this->vars);
				$excel = $this->excelio
							  ->create_objdrawing()
							  ->filename('Member Report')
							  ->create_sheet('Report', $data)
							  ->auto_gen();				
			}
			else{
				// $this->outputs('Oops, Data not found!');
				$output = array(
					'post' => $this->input->post(),
					'query' => $this->db->last_query()
				);
				$this->outputs($output, 'application/json');
			}
		}
		else{
			redirect(404);
		}
	}			
}