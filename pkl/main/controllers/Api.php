<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Api extends Api_Controller
{
	private $member_check;

	function __construct()
	{
		parent::__construct();
		$model = array(
			'api_model',
		);

		$this->load->model($model);
		$this->vars['_CACHE_DISABLED'] 	= true;
	}

	function initialize(){
		$whitelist  = array();
		$httpheader = getallheaders();

		$key = array_keys($httpheader);
		if(array_key_exists('Api-Key', $httpheader) == false && $httpheader['Api-Key'] !== API){
			$output = array(
				'code'	  => 406,
				'description' => 'Invalid API key',
			);
			
			$this->outputs($output, 'application/json');			
		}
		return true;
	}

	function token_checker(){
		$username 	= $this->input->post('username', true); 
		$token 		= $this->input->post('token', true);
		$user_exist = $this->api_model->petugas($username, $token);
		if(empty($user_exist)){
			$output = array(
				'code' => 500,
				'description' => 'Anda tidak memiliki akses ke halaman ini'
			);
			$this->outputs($output, 'application/json');
		}
	}

	function login(){
		self::initialize();
		if(self::validation(2) == true){
			$this->load->model('login_model');
			$username = strtolower($this->input->post('username',true));
			$password = sha1(md5(strtolower($this->input->post('password',true)).$this->config->item('encryption_key')));
			$result = $this->login_model->verification($username, $password);
			if(!empty($result)){
				$output = array(
					'code' 	=> 200,
					'msg'	=> 'OK',
					'petugas' => $result
				);				
			}
			else{
				$output = array(
					'code' 	=> 400,
					'msg'	=> self::set_flash('error', 'Username dan password tidak sesuai'),
				);
			}			
		}
		else{
			$output = array(
				'code' 	=> 400,
				'msg'	=> 'Format username atau password salah',
				'status' => $this->form_validation->error_array(),
			);
		}
		$this->outputs($output, 'application/json');
	}

	function registration(){
		self::initialize();
		self::token_checker();
		if(self::validation(1) == true){
			$data['token']			= $this->input->post('token', true);
			$data['unique_merchant'] 	= $this->input->post('kode_pedagang', true);
			$data['category'] 		= $this->input->post('bidang_usaha', true);
			$data['name']			= $this->input->post('jenis_usaha', true);
			$data['owner']			= $this->input->post('nama_pemilik', true);
			$data['owner_phone']	= $this->input->post('telp_pemilik', true);
			$data['no_ktp']			= $this->input->post('ktp', true);
			$data['jenis_kelamin'] 	= $this->input->post('jenis_kelamin', true);
			$data['address']		= $this->input->post('alamat', true);
			$data['kelurahan']		= $this->input->post('kelurahan', true);
			$data['kecamatan']		= $this->input->post('kecamatan', true);
			$data['description']	= $this->input->post('keterangan', true);
			$data['petugas']		= $this->input->post('nama_petugas', true);
			$data['petugas_phone']	= $this->input->post('telp_petugas', true);
			$data['latitude']		= $this->input->post('latitude', true);
			$data['longitude']		= $this->input->post('longitude', true);
			$data['open_hour']		= $this->input->post('jam_buka', true);
			$data['closing_hour']	= $this->input->post('jam_tutup', true);
			$data['status']			= 1;
			$data['created_by']		= $this->input->post('username', true);

			$action = $this->api_model->add_new_register($data);
			if($action != false){
				$output = array(
					'code' => 200,
					'message' => "Pendaftaran berhasil, silahkan melakukan pembayaran pertama",
				);
			}
			else{
				$output = array(
					'code' 		=> 400,
					'message' 	=> 'Pengisian formulir belum benar atau anda memerlukan akses login terlebih dahulu',
				);
			}
		}
		else{
			$output = array(
				'code' 		=> 400,
				'message'	=> $this->form_validation->error_array()
			);
		}

		$this->outputs($output, 'application/json');
	}

	function cek_tagihan(){
		self::initialize();
		self::token_checker();
		if($this->validation(4) == true){
			$idpedagang = $this->input->post('id_pedagang', true);
			$status = $this->api_model->status_tagihan($idpedagang);
			if(!empty($status)){
				$output = array(
					'code' => 200,
					'status' => 'OK',
					'tagihan' => $status
				);
			}
			else{
				$output = array(
					'code' => 400,
					'status' => 'Gagal menampilkan data tagihan'
				);
			}
		}
		else{
			$output = array(
				'code' 		=> 400,
				'message'	=> $this->form_validation->error_array()
			);			
		}
		$this->outputs($output, 'application/json');
	}

	function pembayaran(){
		self::initialize();
		self::token_checker();
		if($this->validation(3) == true){
			$status = $this->api_model->pembayaran();
			if($status == true){
				$output = array(
					'code' => 200,
					'message' => 'transaksi pembayaran sukses dilakukan'
				);
			}
			else{
				$output = array(
					'code' => 400,
					'message' => 'Gagal melakukan transaksi'
				);
			}
		}
		else{
			$output = array(
				'code' 		=> 400,
				'message'	=> $this->form_validation->error_array()
			);			
		}
		$this->outputs($output, 'application/json');
	}

	function validation($type){
		$data = array();
		if($type == 1){
			$data[] = array('name' => 'bidang_usaha', 'message' => 'Bidang usaha', 'valid_type' => 'required|min_length[4]|max_length[45]');
			$data[] = array('name' => 'jenis_usaha', 'message' => 'Jenis usaha', 'valid_type' => 'required|min_length[4]|max_length[45]');
			$data[] = array('name' => 'jenis_kelamin', 'message' => 'Jenis kelamin', 'valid_type' => 'required|in_list[L,P]');
			$data[] = array('name' => 'alamat', 'message' => 'Alamat lengkap', 'valid_type' => 'required|min_length[10]');
			$data[] = array('name' => 'kecamatan', 'message' => 'Kecamatan tempat berdagang', 'valid_type' => 'required');
			$data[] = array('name' => 'kelurahan', 'message' => 'Kelurahan tempat berdagang', 'valid_type' => 'required');
			$data[] = array('name' => 'ktp', 'message' => 'Nomor KTP', 'valid_type' => 'required|numeric');
			$data[] = array('name' => 'nama_pemilik', 'message' => 'Nama pemilik', 'valid_type' => 'required|min_length[3]');
			$data[] = array('name' => 'telp_pemilik', 'message' => 'Nomor telepon pemilik', 'valid_type' => 'required|numeric');
			$data[] = array('name' => 'nama_petugas', 'message' => 'Nama petugas jaga', 'valid_type' => 'required');
			$data[] = array('name' => 'telp_petugas', 'message' => 'Nomor telepon petugas', 'valid_type' => 'required|numeric');
			$data[] = array('name' => 'latitude', 'message' => 'Posisi latitude tempat usaha', 'valid_type' => 'required');
			$data[] = array('name' => 'longitude', 'message' => 'Posisi longitude tempat usaha', 'valid_type' => 'required');
		}
		elseif ($type == 2) {
			$data[] = array("name"=>"username", "message"=>"username","valid_type"=>"required");
			$data[] = array("name"=>"password", "message"=>"password","valid_type"=>"required|min_length[5]");
		}
		elseif ($type == 3) {
			$data[] = array("name"=>"id_pedagang", "message"=>"ID pedagang","valid_type"=>"required");
			$data[] = array("name"=>"jumlah", "message"=>"jumlah","valid_type"=>"required|numeric");
		}
		elseif ($type == 4) {
			$data[] = array("name"=>"id_pedagang", "message"=>"ID pedagang","valid_type"=>"required");
		}

		return $this->validate($data);		
	}
}