<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Main extends Public_Controller
{
	function __construct()
	{
		parent::__construct();		
		// $model = array(
		// 	"mlm/bonus_model"
		// );
		// $this->load->model($model);
		$this->vars['_HOOK_EXPIRES'] 	= '+3 months';
		$this->vars['_CACHE_DISABLED'] 	= true;
	}

	function index()
	{
		$this->vars['point_achieve']			= $this->main_model->point_achievement();
		$this->vars['monthly_point_achieve']	= $this->main_model->monthly_point_achievement();

		$this->pageview('dashboard');
	}

	// -----------------------------------------------------------------------------------------------
	// LOGGING OUT
	// fungsi untuk keluar dari aplikasi dan menghapus session
	// -----------------------------------------------------------------------------------------------

	function logout(){
		$this->session->sess_destroy();
		redirect();
	}
}