<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['^'.BASE_PANEL.'migrate'] = 'migrate';
/*
* --------------------------------------------------------------------------
* CPANEL ROUTES
* --------------------------------------------------------------------------
*/
$route['default_controller']  = 'main';
$route['^admin$'] = 'not_found';

if($lookup == true){
  $sql = $db->select('slug, folder, controllers')
            ->from('menus')
            ->where('display', 1)
            ->get()
            ->result_array();

  $panel  = !empty(BASE_PANEL) ? preg_replace('/\/$/', '', BASE_PANEL) : '*';
  $route['^'.BASE_PANEL.'clear']    = 'admin/logout/clear_cache';

  array_walk($sql, function($items) use(&$route){
      $redirect = $items['folder'].'/'.$items['controllers'];
      if(!empty($items['controllers'])){
        $route[BASE_PANEL.$items['slug']]         = $redirect;
        $route[BASE_PANEL.$items['slug'].'/(.*)'] = $redirect.'/$1';          
      }
  });
  
  $route = array_reverse($route);
  // $route['^'.$panel.'$']            = 'admin/dashboard/index';

}

/* --------------------------------------------------------------------------
* PUBLIC ROUTES
* --------------------------------------------------------------------------
*/
if($lookup == false){
  $route['^api/(:any)'] = 'public/api/$1';
  $route['^rekapitulasi/(:any)'] = 'public/rekapitulasi/$1';
  $route['^member']    = 'member/profile';
}

