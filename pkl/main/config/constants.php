<?php
defined('BASEPATH') OR exit('No direct script access allowed');

defined('BASE_PANEL') 	OR define('BASE_PANEL', '');
defined('ENCFK') 		OR define('ENCFK', 'Y0u5hOulDK40W5');

defined('TEMPLATE') 	OR define('TEMPLATE', 'gentelella');
defined('UPLOAD_PATH') 	OR define('UPLOAD_PATH', './assets/');
defined('ICON_PATH') 	OR define('ICON_PATH', 'image/icon/');
defined('THUMBNAME') 	OR define('THUMBNAME', '_thumbs');
defined('DEFAULTIMG') 	OR define('DEFAULTIMG', 'image/image/default.png');
defined('UPLOADIMG') 	OR define('UPLOADIMG', 'image/image/uploader.png');
defined('MALEAVA') 	 	OR define('MALEAVA', 'image/image/maleava.png');
defined('FEMALEAVA') 	OR define('FEMALEAVA', 'image/image/femaleava.png');
defined('GMAPKEY') 		OR define('GMAPKEY', 'AIzaSyD1moNAkIldseLoMTY6J-fX04eEFkWnt_8');

defined('API') 			OR define('API', "EreTRibuS1P3mK0tBogOR");
/*
* REGISTRATION METHOD LIST
* first => Beli 1 paket aktifasi mendapatkan akun member yang berbeda sesuai dengan jumlah titik paket yang dibeli
* second =>. Beli 1 paket aktifasi mendapatkan sibling atau kaki jaringan yang berbeda sesuai dengan jumlah titik paket yang dibeli.
*/
// defined('REGMETHOD') 			OR define('REGMETHOD', "first");
// defined('MAX_SLOT') 			OR define('MAX_SLOT', 5);
// defined('MAX_DEPTH_DISPLAYED') 	OR define('MAX_DEPTH_DISPLAYED', 3);

/*
* BONUS INITIALIZATION
*/
// defined('BONUS_LEVEL') 			OR define('BONUS_LEVEL', 5);
// defined('MAX_DEPTH_COUNTING') 	OR define('MAX_DEPTH_COUNTING', 25);

/*
* CONFIGURATION UNTUK SMS GATEWAY
*/
// defined('SMS_PROCEDURE_ACTIVATION') 	OR define('SMS_PROCEDURE_ACTIVATION', 1);
// defined('SMS_USERNAME') 	OR define('SMS_USERNAME', '');
// defined('SMS_PASSWORD') 	OR define('SMS_PASSWORD', '');
// defined('SMS_SENDERS') 		OR define('SMS_SENDERS', '');
// defined('SMS_DOMAIN') 		OR define('SMS_DOMAIN', '');

