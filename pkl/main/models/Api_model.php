<?php

if(!defined('BASEPATH')) exit ('No direct script access allowed');

/**
* 
*/
class Api_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	/*
	* ####################################################################################################################################################################
	* API MEMBER DATA
	* ####################################################################################################################################################################	
	*/

	function petugas($username, $token){
		$data = $this->db
					 ->select('username')
					 ->from('user')
					 ->where('username', $username)
					 ->where('token', $token)
					 ->get()
					 ->row_array();
		return $data;
	}

	function add_new_register($postdata){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('merchant', $postdata);
			$result = $this->db->insert_id();
		}
		else{
			$result = false;
		}
		return $result;
	}

	function pembayaran(){
		$data['kode_transaksi'] = date('Ymd').sprintf('%04d',mt_rand(1, 9999));
		$data['kode_pedagang'] 	= $this->input->post('id_pedagang', true);
		$data['jumlah'] 		= $this->input->post('jumlah', true);
		$data['tgl_bayar']		= date('Y-m-d H:i:s');
		$data['petugas']		= $this->input->post('username');
		$data['created_at']		= date('Y-m-d H:i:s');
		$data['created_by']		= 'System';
		$data = array_map('trim', $data);
		$sql = $this->db->insert('retribusi', $data);
		return $sql;
	}

	function status_tagihan($idpedagang){
		$data = $this->db
					 ->select('tgl_bayar')
					 ->from('retribusi')
					 ->where('kode_pedagang', $idpedagang)
					 ->order_by('id', 'DESC')
					 ->get()
					 ->row_array();
		if(!empty($data)){
			$month = new DateTime($data['tgl_bayar']);
			$now = new DateTime(date('Y-m-d'));
			$diff = (int)$now->diff($month)->format('%r%m');
			$call = array(
				'month' => $month,
				'now'	=> $now,
				'diff' 	=> $diff
			);
			return $call;
		}
		else{
			return false;
		}
	}	
}