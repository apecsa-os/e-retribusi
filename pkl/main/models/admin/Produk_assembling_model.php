<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk_assembling_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
		$this->load->helper('text');
	}

	function assembling_data($id){
		$data = $this->db
					 ->select('a.id, a.produk_item_id, a.qty, b.name, b.price')
					 ->from('produk_assembling a')
					 ->join('product b', 'b.id = a.produk_item_id')
					 ->where('a.produk', $id)
					 ->get()
					 ->result_array();
		return $data;
	}

	function assembling_detail($id){
		$data = $this->db
					 ->select('a.id, a.produk_item_id, a.qty, b.name')
					 ->from('produk_assembling a')
					 ->join('product b', 'b.id = a.produk')
					 ->where('a.id', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function insert($postdata, $datetime = false){
		$postdata = array_map('trim', $postdata);
		$sql = $this->db->insert('produk_assembling', $postdata);
		
		if($sql == true){
			$result = $this->db->insert_id();
		}
		else{
			$result = false;
		}

		return $result;
	}

	function update($postdata, $where){
		$result = $this->db->update('produk_assembling', $postdata, $where);
		return $result;
	}

	function delete($where){
		return $this->db->delete('produk_assembling', $where);
	}
}