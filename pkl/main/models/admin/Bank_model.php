<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Bank_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('name', 'code', 'account no', 'account name', 'account area', 'company', 'action')
		     ->column_width(array(0=>200, 1=>120, 2=>150, 3=>200, 4=>150, 5=>100))
		     ;
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i> Add'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0, 1, 2, 3, 4)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.name, a.bankcode, a.accountno, a.accountname, a.accountarea, a.status, a.id')
					 ->from('bank a')
					 ->set_action(
					 	'',
					 	array('href' => site_url($this->vars['_ROUTED'].'/view/%1$d/%2$s'), 
				 			'class' => 'btn btn-xs btn-success btn-update',
				 			'title' => 'Detail' 
				 		), 
				 		'<i class="fa fa-search"></i>'
					 )
					 ;
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update',
				 		'title' => 'Edit' 				 		 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 )
			 ;
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete',
				 	'title' => 'Delete'
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}
	function details($id = null){
		$src = $this->db
					->select('a.id, a.name, a.bankcode, a.accountno, a.accountname, a.accountarea, a.status, a.created, a.createdby, a.updated, a.updatedby')
					->from('bank a')
					->where('id', $id)
					->get()
					->row_array();	
		return $src;		
	}

	function form($data = null,$action = "tambah"){
		$form = $this->fiform;

		if(!empty($data)){
			$action = "Update";
			$form->hidden('id', $data['id']);
		}

		$name 			= !empty($data) ? $data['name'] 	: '';
		$bankcode 		= !empty($data) ? $data['bankcode'] 	: '';
		$accountno 		= !empty($data) ? $data['accountno'] 	: '';
		$accountname 	= !empty($data) ? $data['accountname'] 	: '';
		$accountarea 	= !empty($data) ? $data['accountarea'] 	: '';
		$status 		= !empty($data) ? $data['status'] 	: 'Y';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('name')
					 ->field(
					 		'text', 
					 		'name', 
					 		array('name' => 'name', "value"=>$name) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'code', 
					 		array('name' => 'bankcode', "value"=>$bankcode) 
					 	)
					 ->new_row()					 
					 ->field(
					 		'text', 
					 		'account no', 
					 		array('name' => 'accountno', "value"=>$accountno) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'account nama', 
					 		array('name' => 'accountname', "value"=>$accountname) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'account area', 
					 		array('name' => 'accountarea', "value"=>$accountarea) 
					 	)
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'bank company', 
					 		array('name' => 'status', "value"=>$status, 'option' => array('Y' => 'Yes', 'N' => 'No'), 'inline' => true) 
					 	)
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('bank', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);						
			$result = $this->db->update('bank', $postdata, $where);			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('bank', $where);
	}	
}