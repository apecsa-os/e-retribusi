<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Inventori_model extends Query_model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
	}

	function table(){
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		'Import', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/import'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-cloud-upload"></i> '
		     	);
		}
		if($this->vars['_PRINTING'] == true){
			$this->fitable
				 ->add_buttons(
		     		'', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/print_excel'), 
		     			'class' => 'btn btn-sm btn-default btn-insert' 
		     		), 
		     		'<i class="fa fa-print"></i> Inventory Report'
		     	);
		}

		$output = $this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/'),
		     		"data-col-order"	=> '1:desc,0:asc'		     		
		     		)
		    	)
		     ->set_column('name', 'date', 'bundled number','serial number', 'batch no', 'status', 'action')
		     ->column_width(array(0=>'20%',1=>'10%', 2=>'15%', 3=>'15%', 4=>'15%', 5=>'15%'))
		     ->filtered_column(0,1,2,3,4)
		     ->filter_type(array(
		     		4 => array('available' => 'Available', 'unavailable' => 'Unavailable','packed' => 'Packed','sold' => 'Sold', 'used' => 'Used')
		     	))
		     ->generate();
		return ($output);
	}

	function json_data(){
		$data = $this->fitable
					 ->select('a.product_name, a.created_at, c.serial_number as bundled, a.serial_number, a.doc_no, a.status, a.id, b.manufacture, c.id as sid')
					 ->from('product_sn a')
					 ->join('product b','b.id = a.product_id')
					 ->join('product_sn c', 'c.id = a.bundling_num')
					 ->data_format('id','conditional_action')
					 ->data_format('bundled','bundled')
					 ->data_format('status','availability')
					 ->aodata();
		return $data;
	}

	function bundled($string, $data){
		return !empty($string) ? '<a href="'.site_url($this->vars['_ROUTED'].'/view/'.$data['sid']).'" class="label label-danger label-big btn-update">'.$string.'</a>' : $string;
	}

	function availability($string){
		if($string == 'available'){
			$label = 'success';
		}
		elseif($string == 'packed'){
			$label = 'primary';
		}
		elseif($string == 'sold'){
			$label = 'danger';
		}
		elseif($string == 'used'){
			$label = 'info';
		}
		else{
			$label = 'warning';
		}

		return '<span class="label label-'.$label.' label-big">'.ucfirst($string).'</span>';		
	}

	function conditional_action($id, $data){
		$action = '';
		if($this->vars['_VIEWING'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/view/'.$data['id']).'" class="btn btn-success btn-xs btn-update" title="View">
			<i class="fa fa-search"></i></a> ';
		}		
		if($this->vars['_UPDATE'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/edit/'.$data['id']).'" class="btn btn-primary btn-xs btn-update" title="Edit">
			<i class="fa fa-edit"></i></a> ';
		}
		if($this->vars['_UPDATE'] == true && $data['manufacture'] == "YES" && !empty($data['serial_number'])){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/bundling/'.$data['id']).'" class="btn btn-warning btn-xs btn-update" title="Assembling">
			<i class="fa fa-dropbox"></i></a> ';
		}
		if($this->vars['_DELETE'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/delete').'" data-stamp="'.$id.'" data-title="'.$data['product_name'].'" title="Delete" class="btn btn-danger btn-xs btn-delete">
				<i class="fa fa-trash"></i></a>';
		}
		return $action;
	}

	function details($id = null){
		$src = $this->db
					->select('a.unique_identifier, a.doc_no, a.product_id, b.name, a.serial_number, a.status, a.created_at, a.created_by, a.updated_at, a.updated_by, a.id, b.price')
					->from('product_sn a')
					->join('product b','b.id = a.product_id')
					->where('a.id', $id)
					->get()
					->row_array();	
		return $src;		
	}

	function bundled_detail($where){
		$data = $this->db
					 ->select('b.name, a.serial_number')
					 ->from('product_sn a')
					 ->join('product b','b.id = a.product_id')
					 ->where('a.bundling_num', $where)
					 ->get()
					 ->result_array();
		return $data;
	}

	function get_all_packs($where){
		$data = $this->db
					 ->select('a.product_id, b.name, a.serial_number, a.id')
					 ->from('product_sn a')
					 ->join('product b','b.id = a.product_id')
					 ->where_in('a.product_id', $where)
					 ->where('a.status','packed')
					 ->get()
					 ->result_array();
		return $data;
	}

	function form($data = null, $action = "Add"){
		$form = $this->fiform;

		if(!empty($data)){
			$action = "Update";
			$form->hidden('id', $data['id']);
		}

		$unique_identifier 		= !empty($data) ? $data['unique_identifier'] 	: '';
		$doc_no 				= !empty($data) ? $data['doc_no'] 	: '';
		$product_id 			= !empty($data) ? $data['product_id'] 	: '';
		$name 					= !empty($data) ? $data['name'] 	: '';
		$serial_number 			= !empty($data) ? $data['serial_number'] 	: '';
		$status 				= !empty($data) ? $data['status'] 	: 'Y';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->hidden('unique_identifier', $unique_identifier)
					 ->hidden('product_id', $product_id)
					 ->field(
					 		'text', 
					 		'batch number', 
					 		array('name' => 'doc_no', "value"=>$doc_no,'readonly' => true) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'product name', 
					 		array('name' => 'name', "value"=>$name,'readonly' => true) 
					 	)
					 ->new_row()					 
					 ->field(
					 		'text', 
					 		'serial number', 
					 		array('name' => 'serial_number', "value"=>$serial_number) 
					 	)
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'status', 
					 		array('name' => 'status', "value"=>$status, 'option' => array('available' => 'Available', 'unavailable' => 'Unavailable', 'packed' => 'Packed', 'sold' => 'Sold'), 'inline' => true) 
					 	)
					 ->new_row()
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
				$postdata['updated_by'] = $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);						
			$result = $this->db->update('product_sn', $postdata, $where);			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('product_sn', $where);
		return true;
	}

	function serial_list($where, $id = null){
		$data = $this->db
					 ->select('serial_number, unique_identifier, product_id, id, bundling_num, doc_no')
					 ->from('product_sn')
					 ->where_in('product_id', $where)
					 ->where('serial_number !=', null)
					 ->group_start()
					 ->where('bundling_num', $id)
					 ->or_where('status', 'available')
					 ->group_end()
					 ->get()
					 ->result_array();
		$result = array();
		foreach ($data as $key => $value) {
			$result[$value['product_id']][] = $value;
		}
		unset($data);
		return $result;
	}

	function import_form(){
		$form = $this->fiform
					 ->create($this->vars['_ROUTED'].'/force_upload', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->field(
					 		'file', 
					 		'Excel files', 
					 		array('name' => 'fupload', "value"=>'') 
					 	)
					 ->new_row()
					 ->btn('Import', array('type' => 'button','class' => 'btn btn-primary btn-form'), '<i class="fa fa-cloud-upload"></i>')
					 ->retrieve()
					 ;
		return $form;		
	}

	function export_data($inv){
		$data = $this->db
					 ->select('unique_identifier, serial_number')
					 ->from('product_sn')
					 ->where('doc_no', $inv)
					 ->get()
					 ->result_array();
		return $data;
	}

	function print_form(){
		$form = $this->fiform;
		$form = $form->create($this->vars['_ROUTED'].'/excel', array("class" => 'form-horizontal', 'target' => '_blank'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('date','month','year')					 
					 ->field(
					 		'dropdown', 
					 		'Month', 
					 		array('name' => 'month', "value"=>'', 'option' => range(1, 12)) 
					 	)
					 ->field(
					 		'text', 
					 		'Year', 
					 		array('name' => 'year', "value"=>'') 
					 	)
					 ->new_row()
					 ->field(
					 		'checkbox',
					 		'status',
					 		array('name' => 'status[]', 'value' => '', 'option' => array('available' => 'Available', 'unavailable' => 'Unavailable', 'packed' => 'Packed', 'sold' => 'Sold'))
					 	)	 						 
					 ->btn('Print', array('type' => 'submit','class' => 'btn btn-primary'))
					 ->retrieve()
					 ;
		return $form;		
	}

	function print_data($month, $year){
		$type = $this->input->post('status', true);
		$type = !empty($type) ? $type: array('available','unavailable','packed','sold');		
		$data = $this->db
					 ->select('a.unique_identifier, a.doc_no, b.name, a.serial_number, b.price, a.status, a.created_at, a.created_by, a.updated_at, a.updated_by,a.owner')
					 ->from('product_sn a')
					 ->join('product b','b.id = a.product_id')
					 ->where('MONTH(a.created_at)', $month)
					 ->where('YEAR(a.created_at)', $year)
					 ->where_in('a.status', $type)
					 ->get()
					 ->result_array();
		return $data;
	}			
}