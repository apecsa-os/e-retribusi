<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Production_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('batch', 'date', 'remark', 'creator', 'action')
		     ->column_width(array(0 => '20%', 1 => '15%' , 2 => '20%', 3 => '10%'))
		     ;
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i> Add'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0, 1, 2, 3)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.kode_produksi, a.tgl_masuk,  a.remark, a.created_by, a.id')
					 ->from('production a')
					 ->set_action(
					 	'',
					 	array('href' => site_url($this->vars['_ROUTED'].'/view/%1$d/%2$s'), 
				 			'class' => 'btn btn-xs btn-success btn-update',
				 			'title' => 'Detail' 
				 		), 
				 		'<i class="fa fa-search"></i>'
					 )
					 ;
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update',
				 		'title' => 'Edit' 				 		 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 )
			 ;
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete',
				 	'title' => 'Delete'
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}

		if($this->vars['_PRINTING'] == true){
			$this->fitable
				 ->set_action(
		     		'Export Serial', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/export/%2$s'), 
		     			'class' => 'btn btn-sm btn-default btn-xs',
		     			'target' => '_blank' 
		     		), 
		     		'<i class="fa fa-cloud-download"></i> '
		     	);
		}

		$data = $this->fitable->aodata();

		return $data;
	}
	function details($id = null, $flag = 'id'){
		$src = $this->db
					->select('a.id, a.kode_produksi, a.tgl_masuk, a.remark, a.created_at, a.created_by, a.updated_at, a.updated_by')
					->from('production a')
					->where($flag, $id)
					->get()
					->row_array();	
		return $src;		
	}

	function production_item($invoice){
		$data = $this->db
					 ->select('a.id, a.qty_in, a.remarks, a.produkid, b.name')
					 ->from('product_ledger a')
					 ->join('product b','b.id = a.produkid')
					 ->where('a.doc_no', $invoice)
					 ->where('status','success')
					 ->get()
					 ->result_array();
		return $data;
	}

	function product_list($where_in = null){
		if(!empty($where_in)){
			$this->db->where_in('a.id', $where_in);
		}
		$data = $this->db
					 ->select('a.id, a.name, a.has_code')
					 ->from('product a')
					 ->where('a.has_code','Y')
					 ->get()
					 ->result_array();
		return $data;
	}

	function form($data = null,$action = "tambah"){
		$form = $this->fiform;
		$details = array();
		$details['list'] = self::product_list();
		$disabled = array();
		if(!empty($data)){
			$action = "Update";
			$details['composite'] = self::production_item($data['kode_produksi']);
			$disabled = array('disabled' => true);
			$form->hidden('id', $data['id']);
		}

		$kode_produksi 	= !empty($data) ? $data['kode_produksi'] 	: '';
		$tgl_masuk 		= !empty($data) ? $data['tgl_masuk'] 	: '';
		$remark 		= !empty($data) ? $data['remark'] 	: '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('kode_produksi','tgl_masuk')
					 ->field(
					 		'text', 
					 		'batching code', 
					 		array_merge(array('name' => 'kode_produksi', "value"=>$kode_produksi), $disabled) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'batch date', 
					 		array('name' => 'tgl_masuk', "value"=>$tgl_masuk,'class' => 'datepicker') 
					 	)
					 ->new_row()					 
					 ->field(
					 		'text', 
					 		'remark', 
					 		array('name' => 'remark', "value"=>$remark) 
					 	)
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->form_addon($this->load->view('form/production_form', $details, true))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('production', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);						
			$result = $this->db->update('production', $postdata, $where);			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('production', $where);
	}

	function change_ledger($id){
		$sql = $this->db
				    ->set('status', 'canceled')
				    ->set('tanggal_keluar', date('Y-m-d H:i:s'))
				    ->set('updated_by', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username'))
				    ->set('updated_at', date('Y-m-d H:i:s'))
				    ->from('product_ledger')
				    ->where('id', $id)
				    ->update();
		return $sql;
	}	

	function upgrade_package_list(){
		$data = $this->db
					 ->select('a.id, a.unique_identifier, a.serial_number, a.product_name')
					 ->from('product_sn a')
					 // ->join('product b','b.id = a.product_id')
					 // ->join('product_type c','c.id = b.produk_type')
					 ->where('a.status','available')
					 ->where('a.product_id', 10)
					 ->where('a.owner', 'company')
					 ->get()
					 ->result_array();
		return $data;
	}
}