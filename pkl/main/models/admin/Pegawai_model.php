<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Pegawai_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
		$this->load->helper('text');
	}

	function table(){
		$this->fitable
		     ->attribute(array(
					"title" => $this->vars['_TITLE']." data", 
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('nama','alamat', 'action')
		     ;
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i> Tambah'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0, 1)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.name, a.address, a.id')
					 ->from('employee a')				
					 ;
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 )
			 ;
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function ellipsis($string){
		return ellipsize($string, 25, 1, '...');
	}

	function konversi($number){
		return sprintf('%02s', $number);
	}

	function details($id = null){
		$src = $this->db
					->select('a.name, a.address, a.id')
					->from('employee a')
					->where('id', $id)
					->get()
					->row_array();	
		return $src;		
	}

	function form($data = null){
		$form = $this->fiform;

		$action = "tambah";
		if(!empty($data)){
			$action = "Update";
			$form->hidden('id', $data['id']);
		}

		$name 			= !empty($data) ? $data['name'] 	: '';
		$address 		= !empty($data) ? $data['address'] 	: '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('name','address')
					 ->field(
					 		'text', 
					 		'name', 
					 		array('name' => 'name', "placeholder" => 'name', "value"=>$name) 
					 	)					 						 
					 ->new_row()
					 ->field(
					 		'textarea', 
					 		'address', 
					 		array('name' => 'address', "value"=>$address, "placeholder" => $address )
					 	)					 					 
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('employee', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);						
			$result = $this->db->update('employee', $postdata, $where);			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('employee', $where);
	}	
}