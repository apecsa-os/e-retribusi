<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('expire', 'title', 'description', 'status','image', 'action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0)
		     			->column_width(array(0 => 100, 1 => 120 , 2 => 400))
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.expdate, a.title, a.description, a.is_publish, a.image, a.id')
					 ->from('banner a')
					 ->data_format('image', 'img_converter')
					 ->data_format('is_publish', 'publishing');
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 );
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function img_converter($string){
		$string = !empty($string) ? $string : DEFAULTIMG;
		return '<img src="'.base_url(BASE_PANEL.MINIFIER.$string).'" class="img-thumbnail" width="100" height="100">';
	}

	function publishing($number){
		return $number == 1 ? '<span class="label label-primary">Active</label>' : '<span class="label label-default">Inactive</label>';
	}

	function details($id = null){
		$data = $this->db
					 ->select('title, subtitle, image, is_publish, description, label, id')
					 ->from('banner')
					 ->where('id', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = $this->bahasa['insert'];
		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['id']);
			$form->hidden('curimg', $data['image']);
		}

		$title 		= !empty($data) ? $data['title'] : '';
		$subtitle 	= !empty($data) ? $data['subtitle'] : '';
		$description = !empty($data) ? $data['description'] : '';
		$label 		= !empty($data) ? $data['label'] : '';
		$image 		= !empty($data) ? BASE_PANEL.MINIFIER.$data['image'] : DEFAULTIMG;
		$is_publish = !empty($data) ? $data['is_publish'] : 1;

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('title')
					 ->field('text', 'title', array('name' => 'title', "placeholder"=>'title', "value"=>$title) )
					 ->new_row()
					 ->field('text', 'subtitle', array('name' => 'subtitle', "placeholder"=>'subtitle', "value"=>$subtitle) )
					 ->new_row()
					 ->field('text', 'description', array('name' => 'description', "placeholder"=>'description', "value"=>$description) )
					 ->new_row()
					 ->field('text', 'label', array('name' => 'label', "placeholder"=>'label', "value"=>$label) )
					 ->new_row()					 					 					 
					 ->field('radio', 'publish', array('name' => 'is_publish', "option"=>array($this->bahasa['hide'],$this->bahasa['show']), "value"=>$is_publish, "inline" => true) )
					 ->new_row()
					 ->field('filesimage','Logo', array('name' => 'image', 'value' =>$image) )
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('banner', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('banner', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('banner', $where);
	}
	
}