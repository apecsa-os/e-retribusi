<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Network_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$ci = &get_instance();
		$ci->load->model('admin/member_model');
	}

	function last_query(){
		return $this->db->last_query();
	}

	function detail_network($data, $prefix = 'A'){
		$slotnumber = 1;
		$prefix = $this->input->post('prefix') != null ? $this->input->post('prefix', true) : $prefix;

		$result = $this->db
					 ->select('a.member_id, c.username as sponsor, d.username as upline, a.created_at')
					 ->from('member_networks a')
					 ->join('member_networks b','b.slotnumb = a.upline_id')
					 ->join('membermaster c','c.id = a.sponsor_id')
					 ->join('membermaster d','d.id = b.member_id')
					 ->group_start()
					 	->where('a.prefix', $prefix)
					 	->or_where('a.prefix', null)
					 ->group_end()
					 ->where('a.slotnumb',$data['slotnumber'])
					 ->get()
					 ->row_array();
		return $result;
	}

	function network_group($slotnumb){
		$prefix = $this->input->post('prefix') != null ? $this->input->post('prefix', true) : 'A';

		$data = $this->db
					 ->select('COUNT(member_id) - 1 as group_member')
					 ->from('member_networks')
					 ->where('prefix', $prefix)
					 ->like('slotnumb', $slotnumb, 'after')
					 ->get()
					 ->row_array();
		return $data;
	}

	function tipe_poin(){
		$data = $this->db
					 ->select('id, type_poin, multilevel')
					 ->from('type_poin')
					 ->where('status', 1)
					 ->get()
					 ->result_array();
		return $data;
	}

	function network_point($member = array(), $slotnumb = null ){
		$point_list			= self::tipe_poin();

		$date_from 			= date('Y-m-01');
		$day_of_this_month 	= cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
		$date_to 			= date('Y-m-'.sprintf('%02s',$day_of_this_month));

		// point
		$result = array();
		foreach ($point_list as $key => $value) {
			if($value['multilevel'] == 1){
				$this->db->like('a.slotnumb', $slotnumb);
			}
			else{
				$this->db->where('a.id_member', $member['member_id']);
			}

			$poin = $this->db
						 ->select('SUM(a.point) as poin')
						 ->from('rekapitulasi_poin a')
						 ->where('a.point_flag', $value['id'])
						 ->where('DATE(a.get_date) >=', $date_from)
						 ->where('DATE(a.get_date) <=', $date_to)
						 ->get()
						 ->row_array();
			$result[$value['type_poin']] = !empty($poin['poin']) ? $poin['poin'] : 0;
		}
		return $result;
	}

	function summary_network_point($member = array(), $slotnumb = null ){
		$point_list			= self::tipe_poin();

		// point
		$result = array();
		foreach ($point_list as $key => $value) {
			if($value['multilevel'] == 1){
				$this->db->like('a.slotnumb', $slotnumb);
			}
			else{
				$this->db->where('a.id_member', $member['member_id']);
			}

			$poin = $this->db
						 ->select('SUM(a.point) as poin')
						 ->from('rekapitulasi_poin a')
						 ->where('a.point_flag', $value['id'])
						 ->get()
						 ->row_array();
			$result[$value['type_poin']] = !empty($poin['poin']) ? $poin['poin'] : 0;
		}
		return $result;
	}

	function bonus_sponsor($id){
		$date_from 			= date('Y-m-01');
		$day_of_this_month 	= cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
		$date_to 			= date('Y-m-'.sprintf('%02s',$day_of_this_month));		
		$data = $this->db
					 ->select('SUM(point) as poin, point_type')
					 ->from('rekapitulasi_poin')
					 ->where('id_member', $id)
					 ->where('status', 1)
					 ->where('point_type', 'BS')
					 ->where('get_date >=', $date_from)
					 ->where('get_date <=', $date_to)
					 ->get()
					 ->row_array();
		return $data;		
	}

	function genealogy_tree($slotnumber = ''){
		$networks_id = !empty($slotnumber) ? $slotnumber : ( $this->input->post('slotnumber', true) != null ? $this->input->post('slotnumber', true) : '');
		$networks_id = !empty($networks_id) ? $networks_id : 1;

		$limit =  (pow(MAX_SLOT, MAX_DEPTH_DISPLAYED) - 1) / (MAX_SLOT - 1);
		// if($this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'loggedin') == null && $this->session->userdata(DEF_APP.'_'.SESS_PUB.'level') != null){
		// 	$this->db->where('a.level >=', $this->session->userdata(DEF_APP.'_'.SESS_PUB.'level') );
		// 	$this->db->like('a.slotnumb', $this->session->userdata(DEF_APP.'_'.SESS_PUB.'slotnumb'), 'AFTER' );
		// }
		$data = $this->db
					 ->select('
					 	a.id, a.networks_id, a.member_id, a.placement_number, a.upline_id, a.slotnumb, a.prefix, a.level,
					 	b.username, b.epoin, b.ewallet, b.ecash, b.status, b.photo, b.status,
					 	c.username as sponsor
					 	')
					 ->from('member_networks a')
					 ->join('membermaster b','b.id = a.member_id')
					 ->join('membermaster c','c.id = a.sponsor_id')
					 ->like('a.slotnumb', $networks_id, 'after')
					 ->order_by('a.level')
					 ->limit($limit)
					 ->get()
					 ->result_array();
		$result = array();
		foreach ($data as $key => $value) {
			$result[$value['slotnumb']] = $value;
		}
		return $result;
	}

	// fungsi autocomplete_data digunakan untuk menampilkan data member berdasarkan ajax request pada form add member
	function autocomplete_data($id){
		if($this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'loggedin') == null && $this->session->userdata(DEF_APP.'_'.SESS_PUB.'level') != null){
			$this->db->where('b.level >=', $this->session->userdata(DEF_APP.'_'.SESS_PUB.'level') );
			$this->db->like('b.slotnumb', $this->session->userdata(DEF_APP.'_'.SESS_PUB.'slotnumb'), 'AFTER' );
		}		
		$data = $this->db
					 ->select('a.id, b.networks_id, a.username, b.level, b.prefix, b.slotnumb,a.photo, a.jk')
					 ->from('membermaster a')
					 ->join('member_networks b','b.member_id = a.id')
					 ->like('a.username', $id)
					 ->get()
					 ->result_array();
		$result = array();
		foreach ($data as $key => $value) {
			$pict = !empty($value['photo']) ? BASE_PANEL.MINIFIER.$value['photo'] : ($value['jk'] == 'L' ? BASE_PANEL.MINIFIER.MALEAVA : BASE_PANEL.MINIFIER.FEMALEAVA);
			$result[] = array(
				"attribute" => array(
					'data-value' 	=> $value['id'], 
					'data-name' 	=> $value['username'], 
					'data-slot'		=> $value['slotnumb'],
					'data-level' 	=> $value['level'],
					'onclick'		=> '_fill_slot(\''.$value['slotnumb'].'\')' 
				),
				"value"		=> $value['username'],
				"label"		=> $value['username'],
				"img"		=> array("src" => site_url($pict), 'width' => 50, 'alt' => $value['username'], 'style' => 'margin-right:5px;')
			);
		}
		return $result;
	}

	function member_slot($id){
		$data = $this->db
					 ->select('slotnumb')
					 ->from('member_networks')
					 ->where('member_id', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function get_netwid($full_fetch = true){
		if($this->input->get('pos') != null){
			$slot = array();
			$pos = $this->input->get('pos');
			$pos = explode('-', $pos);
			if($full_fetch == true){
				foreach ($pos as $key => $value) {
					$slot[] = implode('-', array_merge($slot, array($value)));
				}
			}

			$removed = array_pop($pos);
			$pos = implode('-', $pos);
			$netwid = $this->db
						 ->select('id')
						 ->from('member_networks')
						 ->where('slotnumb', $pos)
						 ->get()
						 ->row_array();
			$data['netwid'] = $netwid['id'];
			
			if($full_fetch == true){
				$data['list_sponsor'] = $this->db
											 ->select('a.username, concat(a.username, " (", a.name, ")") as name')
											 ->from('membermaster a')
											 ->join('member_networks b', 'b.member_id = a.id')
											 ->where_in('b.slotnumb', $slot)
											 ->get()
											 ->result_array();				
			}

			return $data;
		}
		else{
			return false;
		}
	}			
}