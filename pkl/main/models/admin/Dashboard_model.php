<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function data_pedagang(){
		$data = $this->db
					 ->select('name, latitude, longitude, kecamatan, kelurahan, description, open_hour, closing_hour')
					 ->from('merchant')
					 ->where('status', 1)
					 ->get()
					 ->result_array();
		return $data;
	}		
}