<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Delivery_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('Sales no', 'status', 'date', 'create', 'by', 'action')
		     ->column_width(array(0=>200, 1=>200, 2=>200, 3=>200, 4=>150))
		     ;
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		'&nbsp;', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i> New DO'
		     	);
		}
		if($this->vars['_PRINTING'] == true){
			$this->fitable
				 ->add_buttons(
		     		'', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/print_excel'), 
		     			'class' => 'btn btn-sm btn-default btn-insert' 
		     		), 
		     		'<i class="fa fa-print"></i> Delvery Report'
		     	);
		}		
		$output = $this ->fitable
		     			->filtered_column(0, 4)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.nomor_po, a.status, a.delivery_date, a.created_at, a.created_by, a.id')
					 ->from('delivery_order a')
					 ->data_format('id','table_action')
					 ;
		$data = $this->fitable->aodata();

		return $data;
	}

	function table_action($id, $data){
		$action = '';
		if($this->vars['_VIEWING'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/view/'.$id.'/'.seo_url($data['nomor_po'])).'" class="btn btn-success btn-xs btn-update" title="View">
			<i class="fa fa-search"></i></a> ';
		}		
		// if($this->vars['_UPDATE'] == true){
		// 	$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/edit/'.$id.'/'.seo_url($data['nomor_po'])).'" class="btn btn-primary btn-xs btn-update" title="Edit">
		// 	<i class="fa fa-edit"></i></a> ';
		// }
		if($this->vars['_DELETE'] == true){
			$action .= '<a data-url="'.$this->vars['_ROUTED'].'/delete'.'" data-stamp="'.$id.'" data-title="'.$data['nomor_po'].'" title="Delete" class="btn btn-danger btn-xs btn-delete">
				<i class="fa fa-trash"></i></a> ';
		}
		if($this->vars['_PRINTING'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/cetak/'.$id.'/'.seo_url($data['nomor_po'])).'" title="Print" class="btn btn-default btn-xs" target="_blank">
				<i class="fa fa-print"></i></a>';
		}		
		return $action;
	}

	function currencies($string, $data){
		return currency($string);
	}

	function details($id = null){
		$src = $this->db
					->select(
						'a.id, a.nomor_po, a.status, a.delivery_date, a.remark, 
						b.member_username, b.purchase_date, b.grand_total, 
					 ')
					->from('delivery_order a')
					->join('purchase_order b', 'b.id = a.id_po')
					->where('a.id', $id)
					->get()
					->row_array();	
		return $src;		
	}

	function delivery_detail($id = null){
		$src = $this->db
					->select(
						'a.sn_id, a.sn_number, 
						b.unique_identifier, b.product_name 
					 ')
					->from('delivery_order_detail a')
					->join('product_sn b', 'b.id = a.sn_id')
					->where('a.delivery_id', $id)
					->get()
					->result_array();	
		return $src;		
	}

	function form($data = null, $action = "Add"){
		$form = $this->fiform;

		$id_po 				= !empty($data) && array_key_exists('id_po', $data) 	? $data['id_po'] 		: '';
		$nomor_po 			= !empty($data) && array_key_exists('nomor_po', $data) 	? $data['nomor_po'] 	: '';
		$status 			= !empty($data) && array_key_exists('status', $data) 	? $data['status'] 		: 'pending';
		$delivery_date 		= !empty($data) && array_key_exists('delivery_date', $data) ? $data['delivery_date'] 	: date('Y-m-d');
		$remark 			= !empty($data) && array_key_exists('remark', $data) 	? $data['remark'] 		: '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting', 'id'=>'delivform'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->hidden('id_po', $id_po)
					 ->required('nomor_po','delivery_date')
					 ->field(
					 		'text', 
					 		'sales number', 
					 		array('name' => 'nomor_po', "value"=>$nomor_po, 'class' => 'autocomplete', 'data-url' => 'find_po', 'onfocus'=>'_autocomplete(this)') 
					 	)
					 ->field(
					 		'text', 
					 		'delivery date', 
					 		array('name' => 'delivery_date', "value"=>$delivery_date, 'class'=>'datepicker') 
					 	)
					 ->new_row()
					 // ->field(
					 // 		'radio', 
					 // 		'status', 
					 // 		array('name' => 'status', "value"=>$status, 'inline' => true, 'option' => array('pending' => 'Pending', 'success' => 'Success','failed' => 'Failed')) 
					 // 	)
					 // ->new_row()
					 ->field(
					 		'text', 
					 		'remark', 
					 		array('name' => 'remark', "value"=>$remark) 
					 	)				 						 
					 ->btn($action, array('type' => 'submit','class' => 'btn btn-primary btn-form disabled'))
					 ->form_addon($this->load->view('form/delivery_order_form', $data, true))					 
					 ->retrieve()
					 ;
		return $form;
	}

	function autocomplete_data($id){
		$data = $this->db
					 ->select('a.id, a.nomor_po, a.member_username, a.grand_total, a.purchase_date')
					 ->from('purchase_order a')
					 ->where('a.po_status', 'paid')
					 ->like('a.nomor_po', $id)
					 ->get()
					 ->result_array();
		$result = array();
		foreach ($data as $key => $value) {
			$result[] = array(
				"attribute" => array(
					'data-value' => $value['nomor_po'], 
					'data-id' => $value['id'], 
					'data-buyer' => $value['member_username'], 
					'data-total'=> $value['grand_total'],
					'data-sales-date' => $value['purchase_date'],
					'onclick' => 'fetch_form(this, \''.$this->vars['_ROUTED'].'/get_item\')',
					'onkeypress' => 'fetch_form(this, \''.$this->vars['_ROUTED'].'/get_item\')',
				),
				"value"		=> $value['nomor_po'],
				"label"		=> $value['nomor_po'],
			);
		}
		return $result;
	}

	function purchase_detail($id){
		$data = $this->db
					 ->select('a.id, a.id_produk, a.qty, a.price, a.subtotal, b.name, b.product_fisik, b.manufacture')
					 ->from('purchaseorder_detail a')
					 ->join('product b','b.id = a.id_produk')
					 ->where('a.po_id', $id)
					 ->get()
					 ->result_array();
		return $data;
	}

	function select_serial_number($id){
		$page = $this->input->get('page') ? $this->input->get('page') : 1;
		$manufacture = $this->input->get('manufacture') == 'YES' ? 'packed' : 'available';
		$limit = 10;
		$offset = ($page - 1) * $limit;

		$this->db->start_cache();
		$this->db
			 ->select('id,unique_identifier,product_name,serial_number')
			 ->from('product_sn')
			 ->where('owner', 'company')
			 ->where('serial_number !=', null)
			 ->where('product_id', $id)
			 ->where('status', $manufacture);
			 // ->where_in('status', array('available','packed'));
		$this->db->stop_cache();
		$total = $this->db->count_all_results();

		$data = $this->db	
					 ->limit($limit, $offset)
			 		 ->get()
			 		 ->result_array();
		$this->db->flush_cache();
		return array('total' => $total, 'data' => $data, 'offset' => $offset, 'limit' => $limit, 'page' => $page);
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('delivery_order', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function print_form(){
		$form = $this->fiform;
		$form = $form->create($this->vars['_ROUTED'].'/excel', array("class" => 'form-horizontal', 'target' => '_blank'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('month','year')
					 ->field(
					 		'dropdown', 
					 		'Month', 
					 		array('name' => 'month', "value"=>'', 'option' => range(1, 12)) 
					 	)
					 ->field(
					 		'text', 
					 		'Year', 
					 		array('name' => 'year', "value"=>'') 
					 	)	 						 
					 ->btn('Print', array('type' => 'submit','class' => 'btn btn-primary'))
					 ->retrieve()
					 ;
		return $form;		
	}

	function print_data($month, $year){
		$data = $this->db
					 ->select('a.nomor_po, a.status, a.delivery_date, a.created_at, a.created_by')
					 ->from('delivery_order a')
					 ->where('MONTH(a.delivery_date)', $month)
					 ->where('YEAR(a.delivery_date)', $year)
					 ->get()
					 ->result_array();
		return $data;	
	}							
}