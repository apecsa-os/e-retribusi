<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('kode', 'ID pedagang', 'tanggal', 'jumlah', 'petugas', 'action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		'Transaksi', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i> '
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0, 1, 2, 3, 4)
		     			->column_width(array(0 => 100, 1 => 150 , 2 => 200, 3 => 200, 4 => 200))
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.kode_transaksi, a.kode_pedagang, a.tgl_bayar, a.jumlah, a.petugas, a.id')
					 ->from('retribusi a')
					 ->data_format('jumlah', 'currencies')
					 ;
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	'Edit', 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 );
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				'Hapus', 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function currencies($string){
		return currency($string);
	}

	function details($id = null){
		$data = $this->db
					 ->select('kode_transaksi, kode_pedagang, tgl_bayar, jumlah, petugas, id')
					 ->from('retribusi')
					 ->where('id', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = 'Tambah Transaksi';
		if(!empty($data)){
			$action = 'Edit Transaksi';
			$form->hidden('id', $data['id']);
		}

		$kode_pedagang 		= !empty($data) ? $data['kode_pedagang'] : '';
		$jumlah 			= !empty($data) ? $data['jumlah'] : '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('kode_pedagang','jumlah')
					 ->field('text', 'ID pedagang', array('name' => 'kode_pedagang', "placeholder"=>'xxxxxxxxxxxx', "value"=>$kode_pedagang) )
					 ->new_row()
					 ->field('text', 'jumlah', array('name' => 'jumlah', "placeholder"=>'100.000', "value"=>$jumlah, 'class' => 'currency') )
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['created_by'] = 'System';
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('retribusi', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
				$postdata['updated_by']	= $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('retribusi', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('retribusi', $where);
	}
	
}