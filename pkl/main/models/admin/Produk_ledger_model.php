<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk_ledger_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
		$this->load->helper('text');
	}

	function get_items($where_in){
		$data = $this->db
					 ->select('a.produkid, b.name, SUM(a.qty_in) as jml_msk, SUM(a.qty_out) as jml_keluar')
					 ->from('product_ledger a')
					 ->join('product b', 'b.id = a.produkid')
					 ->where_in('a.produkid', $where_in)
					 ->group_by('a.produkid')
					 ->get()
					 ->result_array();
		return $data;
	}

	function ledger_in($doc_no){
		$data = $this->db
					 ->select('a.doc_no, a.qty_in, a.status, b.code, b.name')
					 ->from('product_ledger a')
					 ->join('product b','b.id = a.produkid')
					 ->where('a.doc_no', $doc_no)
					 ->get()
					 ->result_array();
		return $data;
	}

	function insert($postdata, $datetime = false){
		$postdata = array_map('trim', $postdata);
		$sql = $this->db->insert('product_ledger', $postdata);
		
		if($sql == true){
			$result = $this->db->insert_id();
		}
		else{
			$result = false;
		}

		return $result;
	}

	function update($postdata, $where, $datetime = false){
		$postdata = array_map('trim', $postdata);						
		$result = $this->db->update('product_ledger', $postdata, $where);

		if($sql == true){
			$result = $this->db->insert_id();
		}
		else{
			$result = false;
		}

		return $result;
	}

	function delete($where){
		return $this->db->delete('product_ledger', $where);
	}
}