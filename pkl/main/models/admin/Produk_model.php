<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
		$this->load->helper('text');
	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('code', 'name', 'category', 'sales', 'bundling', 'price', 'publish','action')
		     ;
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		if($this->vars['_PRINTING'] == true){
			$this->fitable
				 ->add_buttons(
		     		'Product Report', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/print_excel'), 
		     			'class' => 'btn btn-sm btn-default btn-insert' 
		     		), 
		     		'<i class="fa fa-print"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0, 1, 2, 3, 4)
		     			->filter_type(array(
		     				3 => array('NO' => 'NO', 'YES' => 'YES'),
		     				4 => array('NO' => 'NO', 'YES' => 'YES'),
		     			 ))
		     			->column_width(array(0 => 120, 1 => 200 , 2 => 200, 3 => 200, 4 => 200, 5 => 150, 6 => 150))
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.code, a.name, b.name as category, a.sales, a.manufacture, a.price, a.active, a.id'
					  )
					 ->from('product a')
					 ->join('product_category b','b.id = a.id_category')
					 ->data_format('price', 'currency')
					 ->data_format('price_register', 'currency')
					 ->data_format('active', 'publishing')
					 ->data_format('id','conditional_action')
					 ;

		$data = $this->fitable->aodata();

		return $data;
	}

	function publishing($string){
		if($string == 1){
			$label = 'success';
			$text = 'Publish';
		}
		else{
			$text = 'Draft';
			$label = 'warning';
		}

		return '<span class="label label-'.$label.' label-big">'.ucfirst($text).'</span>';		
	}

	function conditional_action($id, $data){
		$action = '';
		if($this->vars['_VIEWING'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/view/'.$id.'/'.seo_url($data['name'])).'" class="btn btn-success btn-xs btn-update" title="View">
			<i class="fa fa-search"></i></a> ';
		}		
		if($this->vars['_UPDATE'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/edit/'.$id.'/'.seo_url($data['name'])).'" class="btn btn-primary btn-xs btn-update" title="Edit">
			<i class="fa fa-edit"></i></a> ';
		}
		if($this->vars['_UPDATE'] == true && $data['manufacture'] == "YES"){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/assembling/'.$id.'/'.seo_url($data['code'])).'" class="btn btn-warning btn-xs btn-update" title="Assembling">
			<i class="fa fa-dropbox"></i></a> ';
		}
		if($this->vars['_DELETE'] == true){
			$action .= '<a data-url="'.($this->vars['_ROUTED'].'/delete').'" data-stamp="'.$id.'" data-title="'.$data['name'].'" title="Delete" class="btn btn-danger btn-xs btn-delete">
				<i class="fa fa-trash"></i></a>';
		}
		return $action;
	}

	function currency($number){
		return currency($number);
	}

	function details($id = null){
		$data = $this->db
					 ->select('id, id_category, name, code, deskripsi, sales, manufacture, slug, min_stok, active, level_depth, jumlah_titik,
					 	price, price_cust, price_register, cashback, point, diskon, image, image, weight, has_code, product_fisik')
					 ->from('product')
					 ->where('id', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;
		$poin_type = self::point_type();

		$action = $this->bahasa['insert'];
		$category 		= !empty($data) ? $data['id_category'] : '';
		$name 			= !empty($data) ? $data['name'] : '';
		$code 			= !empty($data) ? $data['code'] : '';
		$price 			= !empty($data) ? $data['price'] : 0;
		$description 	= !empty($data) ? $data['deskripsi'] : '';
		$sales 			= !empty($data) ? $data['sales'] : 'NO';
		$manufacture 	= !empty($data) ? $data['manufacture'] : 'NO';
		$slug 			= !empty($data) ? $data['slug'] : '';
		$diskon 		= !empty($data) ? $data['diskon'] : 0;
		$price 			= !empty($data) ? $data['price'] : 0;
		$price_cust 	= !empty($data) ? $data['price_cust'] : 0;
		$price_register = !empty($data) ? $data['price_register'] : 0;
		$cashback 		= !empty($data) ? $data['cashback'] : 0;
		$point_vars 	= !empty($data) ? unserialize($data['point']) : 0;
		$min_stok 		= !empty($data) ? $data['min_stok'] : 1;
		$has_code 		= !empty($data) ? $data['has_code'] : 'N';
		$active 		= !empty($data) ? $data['active'] : '1';
		$level_depth 		= !empty($data) ? $data['level_depth'] : '1';
		$jumlah_titik 		= !empty($data) ? $data['jumlah_titik'] : '1';
		$product_fisik 		= !empty($data) ? $data['product_fisik'] : '1';

		$image 			= !empty($data) && !empty($data['image']) ? BASE_PANEL.MINIFIER.$data['image'] : BASE_PANEL.MINIFIER.DEFAULTIMG;


		if(!empty($data)){
			$action = $this->bahasa['edit'];
			$form->hidden('id', $data['id']);
			$form->hidden('curimg', $data['image']);			
		}


		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('name','code')
					 ->field(
					 	'dropdown',
					 	'Category', 
					 	array(
					 		'name' => 'id_category', "placeholder"=>'Product category', "value"=>$category,
					 		'option' => self::option_category(), 'option_label' => 'name', 'option_value' => 'id'
					 	) 
					  )
					 ->field(
					 	'radio',
					 	'save as', 
					 	array(
					 		'name' => 'active',  "value"=>$active, 'inline' => true,
					 		'option' => array('Draft', 'Publish')
					 	) 
					  )
					 ->new_row()
					 ->field(
					 	'text',
					 	'code', array('name' => 'code', "placeholder"=> 'Product code', "value"=>$code) )
					 ->new_row()
					 ->field(
					 	'text',
					 	'product name', array('name' => 'name', "placeholder"=>'Product name', "value"=>$name) )
					 ->new_row()
					 ->field(
					 	'radio',
					 	 'sales', array('name' => 'sales', "option"=> array('NO' => 'NO', 'YES' => 'YES'), "value"=>$sales, 'inline' => true) )
					 ->new_row()
					 ->field(
					 	'radio',
					 	 'bill of material', array('name' => 'manufacture', 'class' => 'manufacture', "option"=> array('NO' => 'NO', 'YES' => 'YES'), "value"=>$manufacture, 'inline' => true) )
					  ->new_row()
					  ->field(
					 	'radio',
					 	 'Produk fisik', array('name' => 'product_fisik', "option"=> array('Elektronik', 'Fisik'), "value"=>$product_fisik, 'inline' => true) )
					 ->new_row()
					 ->field(
					 	'radio',
					 	 'has serial number', array('name' => 'has_code', "option"=> array('N' => 'NO', 'Y' => 'YES'), "value"=>$has_code, 'inline' => true) )
					 ->new_row()
					 ->field(
					 	'number',
					 	 'Level depth', array('name' => 'level_depth', 'value' => $level_depth,'min'=>'1','max'=>"50"))
					 ->field(
					 	'number',
					 	 'Slot number', array('name' => 'jumlah_titik', 'value' => $jumlah_titik,'min'=>'1','max'=>MAX_SLOT))	 
					 ->new_row()
					 ->field(
					 	'textarea',
					 	 'description', array('name' => 'deskripsi', "placeholder"=> 'Description', "value"=>$description) 
					 	)
					 ->new_row();
			if(empty($data)){
			$form = $form->field(
					 	'number',
					 	 'minimum stock', array('name' => 'min_stok', "placeholder"=>'Stok minimal', "value"=>$min_stok, 'min' => "1") 
					 	 )
					 ->new_row();
			}

			$form = $form->field(
					 	'text',
					 	 'price', array('name' => 'price', "placeholder"=>'Price', "value"=>$price, 'class' => 'currency') 
					 	 )
					 ->new_row()
					 ->field(
					 	'text',
					 	 'customer price', array('name' => 'price_cust', "placeholder"=>'Customer price', "value"=>$price_cust, 'class' => 'currency') )
					 ->new_row()
					 ->field(
					 	'text',
					 	 'registration price', array('name' => 'price_register', "placeholder"=>'Registration price', "value"=>$price_register, 'class' => 'currency') )
					 ->new_row()
					 ->field(
					 	'number',
					 	 'discount', array('name' => 'diskon', "placeholder"=> 'Discount', "value"=>$diskon, 'min' => 0,'suffix' => '%') )					 
					 ->new_row()
					 ->field(
					 	'text',
					 	 'cashback', array('name' => 'cashback', "placeholder"=>'Cashback', "value"=>$cashback, 'class' => 'currency') )
					 ->new_row();
			foreach ($poin_type as $key => $value) {
				$pv = !empty($point_vars) && array_key_exists($value['type_poin'], $point_vars) ? $point_vars[$value['type_poin']] : 0;
				$form =	$form->field(
						'text',
						$value['type_poin'],
						array('name' => 'point['.$value['type_poin'].']', "placeholder" => $value['keterangan'], 'class' => 'currency', 'value' => $pv )
						)
					 ->new_row();					 					 					 					 
			}
			$form = $form ->field(
					 	'filesimage',
					 	 'image', array('name' => 'image', "placeholder"=> 'image', "value"=>$image) )
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	private function option_category(){
		$data = $this->db
					 ->select('id, name')
					 ->from('product_category')
					 ->get()
					 ->result_array();
		uasort($data, function($a, $b){
			if($a['name'] == $b['name']){
				return 0;
			}
			return $a['name'] < $b['name'] ? -1 : 1;
		});
		return $data;
	}
	
	private function option_type(){
		$data = $this->db
					 ->select('id, type_name')
					 ->from('product_type')
					 ->get()
					 ->result_array();
		uasort($data, function($a, $b){
			if($a['type_name'] == $b['type_name']){
				return 0;
			}
			return $a['type_name'] < $b['type_name'] ? -1 : 1;
		});
		return $data;
	}

	private function point_type(){
		$data = $this->db
					 ->select('id, type_poin, keterangan')
					 ->from('type_poin')
					 ->where('status', 1)
					 // ->where('get_on_transaction', 1)
					 ->get()
					 ->result_array();
		return $data;
	}

	function produk_list(){
		$data = $this->db
					 ->select('a.id, a.name')
					 ->from('product a')
					 ->get()
					 ->result_array();
		return $data;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('product', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);						
			$result = $this->db->update('product', $postdata, $where);			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('product', $where);
	}
	
	function package_item($id){
		$data = $this->db
					 ->select('a.qty, b.name, b.code')
					 ->from('produk_assembling a')
					 ->join('product b','b.id = a.produk_item_id')
					 ->where('a.produk', $id)
					 ->get()
					 ->result_array();
		return $data;
	}

	function list_detail($array_product_id){
		$data = $this->db
					 ->select('id, code, point')
					 ->from('product')
					 ->where_in('id', $array_product_id)
					 ->get()
					 ->result_array();
		return $data;
	}

	function print_form(){
		$form = $this->fiform;
		$form = $form->create($this->vars['_ROUTED'].'/excel', array("class" => 'form-horizontal', 'target' => '_blank'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('date','month','year')					 
					 ->field(
					 		'dropdown', 
					 		'Status', 
					 		array('name' => 'status', "value"=>'', 'option' => array('draft','active')) 
					 	)	 						 
					 ->btn('Print', array('type' => 'submit','class' => 'btn btn-primary'))
					 ->retrieve()
					 ;
		return $form;		
	}

	function print_data($status){		
		$data = $this->db
					 ->select('a.code, a.name, b.name as category, a.sales, a.manufacture, a.price, a.active'
					  )
					 ->from('product a')
					 ->join('product_category b','b.id = a.id_category')
					 ->where('a.active', $status)
					 ->get()
					 ->result_array();
		return $data;
	}	
}