<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		// load resources library yang akan digunakan dalam pemrosesan data
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	// fungsi table digunakan untuk menggenerate tabel sebagai datatable snippet
	function table(){
		$this->fitable // class fitable (ref : _application/libraries/Fitable.php)
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('ID', 'name', 'SID', 'sponsor', 'status', 'upgraded','produk','action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		'New member', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary' 
		     		), 
		     		'<i class="fa fa-plus"></i> '
		     	);
		}
		if($this->vars['_PRINTING'] == true){
			$this->fitable
				 ->add_buttons(
		     		'Member Report', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/print_excel'), 
		     			'class' => 'btn btn-sm btn-default btn-insert' 
		     		), 
		     		'<i class="fa fa-print"></i>'
		     	);
		}		
		$output = $this ->fitable
		     			->filtered_column(0,1,2,3,4,5)
		     			->filter_type(array(
					     		4 => array('aktif' => 'Aktif', 'blok' => 'Blok'),
					     		5 => array('N' => 'Standard', 'Y' => 'Upgraded'),
					     	))
		     			->column_width(array(0 => '10%', 1=> '10%', 2 => '10%', 3 => '10%', 4 => '10%', 5=> '10%', 6 => '15%'))
						->generate();
		return ($output);
	}

	// fungsi json_data digunakan untuk meretrieve ajax request dan ditampilkan pada table datatable
	function json_data(){
		$this->fitable
					 ->select('a.username, a.name, d.username as sponsor_id, d.name as sponsor, a.status, a.has_upgrade, c.name as package_name, a.id')
					 ->from('membermaster a')
					 ->join('member_networks b', 'b.member_id = a.id')
					 ->join('product c','c.id = a.produkid')
					 ->join('membermaster d','d.id = b.sponsor_id')
					 ->data_format('id','conditional_action')
					 ->data_format('has_upgrade','upgradation')
					 ->group_by('b.member_id')
					;

		$data = $this->fitable->aodata();

		return $data;
	}

	function upgradation($string){
		if($string == 'Y'){
			$label = '<span class="label label-success">Upgraded</span>';
		}
		else{
			$label = '<span class="label label-default">Standard</span>';
		}
		return $label;
	}

	function conditional_action($id, $data){
		$action = '';
		if($this->vars['_VIEWING'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/view/'.$id.'/'.$data['username']).'" class="btn btn-success btn-xs btn-update" title="View">
			<i class="fa fa-search"></i></a> ';
		}		
		if($this->vars['_UPDATE'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/edit/'.$id.'/'.$data['username']).'" class="btn btn-primary btn-xs btn-update" title="Edit">
			<i class="fa fa-edit"></i></a> ';
		}
		if($this->vars['_UPDATE'] == true && preg_match('/(Standard)/', $data['has_upgrade']) == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/upgrade/'.$id.'/'.$data['username']).'" class="btn btn-warning btn-xs btn-update" title="Upgrade">
			<i class="fa fa-bookmark"></i></a> ';
		}
		if($this->vars['_DELETE'] == true){
			$action .= '<a data-url="'.$this->vars['_ROUTED'].'/delete'.'" data-stamp="'.$id.'" data-title="'.$data['name'].'" title="Delete" class="btn btn-danger btn-xs btn-delete">
				<i class="fa fa-trash"></i></a>';
		}
		return $action;
	}	

	// fungsi detail, digunakan untuk mendapatkan data detail dari member
	function details($id = null, $username = null){
		if(!empty($id)){
			$this->db->where('a.id', $id);
		}
		if(!empty($username)){
			$this->db->like('a.username', $username);
		}
		$data = $this->db
					 ->select('a.id, a.username, a.name, a.jk, a.tempatlahir, a.tgllahir, a.alamat, a.kota, a.hp, a.telp_kantor, a.telp_rumah, a.status,
					 	a.kodepos, a.ahliwaris, a.noktp, a.email, a.npwp, a.ewallet, a.ecash, a.has_upgrade, a.photo, a.autotransfer, a.upgrade_at, a.upgrade_by,
					 	a.propinsi, a.negara, a.group, a.bank_name, a.account_name, a.account_number')
					 ->from('membermaster a')
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data){
		$username 		= !empty($data) ? $data['username'] 	: '';
		$name 			= !empty($data) ? $data['name'] 		: '';
		$jk 			= !empty($data) ? $data['jk'] 			: 'L';
		$tempatlahir 	= !empty($data) ? $data['tempatlahir'] 	: '';
		$tgllahir 		= !empty($data) ? $data['tgllahir'] 	: '';
		$alamat 		= !empty($data) ? $data['alamat'] 		: '';
		$hp 			= !empty($data) ? $data['hp'] 			: '';
		$kantor 		= !empty($data) ? $data['telp_kantor'] 	: '';
		$rumah 			= !empty($data) ? $data['telp_rumah'] 	: '';
		$status 		= !empty($data) ? $data['status'] 		: 'aktif';
		$kodepos 		= !empty($data) ? $data['kodepos'] 		: '';
		$noktp 			= !empty($data) ? $data['noktp'] 		: '';
		$autotransfer	= !empty($data) ? $data['autotransfer']	: '';
		
		$form = $this->fiform
					 ->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->hidden('id', $data['id'])
					 ->required('username','name','hp')
					 ->field(
					 		'text', 
					 		'username', 
					 		array('name' => 'username', "value"=>$username) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'name', 
					 		array('name' => 'name', "value"=>$name) 
					 	)
					 ->new_row()					 
					 ->field(
					 		'radio', 
					 		'gender', 
					 		array('name' => 'jk', "value"=>$jk, 'option' => array("L" => "Male", "F" => "Female"), "inline" => true) 
					 	)
					 ->new_row()
					 ->field(
					 		'dropdown', 
					 		'Place of birth', 
					 		array('name' => 'tempatlahir', "value"=>$tempatlahir, 'class' => 'select-two', 'option' => self::data_city()) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'date of birth', 
					 		array('name' => 'tgllahir', "value"=>$tgllahir, 'class' => 'datepicker', 'data-year-range' => date('Y', strtotime('-60 years')) .':'.date('Y')) 
					 	)
					 ->new_row()
					 ->field(
					 		'textarea',
					 		'address',
					 		array('name' => 'alamat', "value" => $alamat)
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'mobile number', 
					 		array('name' => 'hp', "value"=>$hp) 
					 	)
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'status', 
					 		array('name' => 'status', "value"=>$status, 'option' => array("aktif" => "Active", "blok" => "Blocked"), 'inline' => true) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'postalcode', 
					 		array('name' => 'kodepos', "value"=>$kodepos) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'identification card number', 
					 		array('name' => 'noktp', "value"=>$noktp) 
					 	)
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'auto transfer', 
					 		array('name' => 'autotransfer', "value"=>$autotransfer, 'option' => array("N" => "No", "Y" => "Yes"), 'inline' => true) 
					 	)
					 ->btn('update', array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$result = $this->db->update('membermaster', $postdata, $where);			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		// return $this->db
		// 	 		->set('status', 'deleted')
		// 	 		->set('updated', date('Y-m-d H:i:s'))
		// 	 		->from('membermaster')
		// 	 		->where($where)
		// 	 		->update();

		return $this->db->delete('membermaster', $where);
	}
	
	function default_group(){
		$data = $this->db
					 ->select('groupid,groupname')
					 ->from('usergroup')
					 ->where('set_as_default', 1)
					 ->get()
					 ->row_array();
		return $data;
	}

	// fungsi autocomplete_data digunakan untuk menampilkan data member berdasarkan ajax request pada form add member
	function autocomplete_data($id){
		$data = $this->db
					 ->select('a.username, a.photo, a.name, a.id')
					 ->from('membermaster a')
					 ->where('a.status', 'aktif')
					 ->like('a.username', $id)
					 ->limit(10)
					 ->get()
					 ->result_array();
		$result = array();
		foreach ($data as $key => $value) {
			$pict = !empty($value['photo']) ? BASE_PANEL.MINIFIER.$value['photo'] : BASE_PANEL.MINIFIER.MALEAVA;

			$result[] = array(
				"attribute" => array(
					'data-value' => $value['username'], 
					'data-timestamp' => strtotime('+'.$value['id'].' hour'),
				),
				"value"		=> $value['username'],
				"label"		=> $value['name']. ' (' .$value['username'].')',
				"img"		=> array("src" => site_url($pict), 'width' => 50, 'alt' => $value['name'], 'style' => 'margin-right:5px;')
			);
		}
		return $result;
	}

	function get_package_list($username, $configuration = array()){
		$exp = !empty($configuration) && array_key_exists('expiration_code', $configuration) ? $configuration['expiration_code'] : false;
		if($exp == 'TRUE' && $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'gid') !== '1'){
			$this->db->where(array('a.expire_date >' => date('Y-m-d H:i:s'), 'a.owner' => $username));
		}
		elseif ($this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'gid') == '1') {
			$this->db->where('a.owner', $username);
			$this->db->or_where('a.owner', 'company');
		}
		$data = $this->db
					 ->select('a.unique_identifier, a.serial_number, a.product_name, a.owner')
					 ->from('product_sn a')
					 ->where_in('a.product_id',array(1,2))
					 ->where('a.status', 'available')
					 ->where('a.serial_number !=', null)
					 ->get()
					 ->result_array();
		return $data;
	}

	function get_member_placement($flag, $id){
		$data = $this->db
					 ->select('id, networks_id, member_id, sponsor_id, level')
					 ->from('member_networks')
					 ->where($flag, $id)
					 ->group_by('member_id')
					 ->get()
					 ->row();
		if(!empty($data)){
			$data = $data->level;
		}
		else{
			$data = "FALSE";
		}
		return $data;
	}

	function identify_package($code){
		$data = $this->db
					 ->select('a.unique_identifier, b.jumlah_titik, b.point, a.product_id')
					 ->from('product_sn a')
					 ->join('product b','b.id = a.product_id')
					 ->where_in('a.product_id',array(1,2))
					 ->where('a.status', 'available')
					 ->where('a.serial_number !=', null)
					 ->where('a.unique_identifier', $code)
					 ->get()
					 ->row_array();

		return $data;
	}

	function data_city(){
		$data = $this->db
					 ->select('a.city_name')
					 ->from('city a')
					 ->order_by('a.city_name')
					 ->get()
					 ->result_array();
		$result = array();
		foreach ($data as $key => $value) {
			$result[$value['city_name']] = $value['city_name'];
		}
		return $result;
	}

	function bank_data(){
		$data = $this->db
					 ->select('a.name')
					 ->from('bank a')
					 ->order_by('a.name')
					 ->get()
					 ->result_array();
		return $data;
	}

	function network_detail($id){
		$data = $this->db
					 ->select('sponsor_id')
					 ->from('member_networks')
					 ->where('member_id', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function upgrade_form($id, $name){
		$network = self::network_detail($id);
		$package = self::get_package_list($name);
		$list = array();
		array_map(function($items) use (&$list){
			$list[$items['unique_identifier']] = $items['owner'].' ('.$items['product_name'].')';
		}, $package);

		$form = $this->fiform
					 ->create($this->vars['_ROUTED'].'/upgrade_member', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->hidden('id', $id)
					 ->hidden('sponsor_id', $network['sponsor_id'])
					 ->required('package')
					 ->field(
					 		'text', 
					 		'username', 
					 		array('name' => 'username', "value"=>$name, 'readonly' => 'readonly') 
					 	)
					 ->new_row()
					 ->field(
					 		'dropdown', 
					 		'paket upgrade', 
					 		array('name' => 'package', "option"=>$list) 
					 	)
					 ->btn('upgrade', array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function update_serial(){
		$code  = 'UPG'.$this->input->post('id').date('Ymd');
		$ledger = array(
			'doc_no' 	=> $code,
			'produkid' 	=> 10,
			'qty_out' 	=> 1,
			'remarks' 	=> 'Upgrade member ID @'.$this->input->post('id').' username @'.$this->input->post('username', true),
			'tanggal_keluar' => date('Y-m-d H:i:s'),
			'created_by' 	=> $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'username'),
			'status' 	=>'success'
		);
		$insert_ledger = $this->db->insert('product_ledger', $ledger);
		$update = $this->db
						->set('status','sold')
						->set('owner', $this->input->post('username', true))
						->set('used_at', date('Y-m-d H:i:s'))
						->set('ledger_code', $code)
						->from('product_sn')
						->where('unique_identifier', $this->input->post('package', true))
						->update();	
		return $update;	
	}

	function member_list($limit = 5){
		$page = $this->input->get('page') != null ? $this->input->get('page') : 1;
		$offset = ($page - 1) * $limit;
		$data = $this->db
					 ->select('
					 		a.id, a.group, a.has_upgrade, a.username, a.name, a.created,
					 		b.level, b.slotnumb
					 	')
					 ->from('membermaster a')
					 ->join('member_networks b', 'b.member_id = a.id')
					 ->where('a.status', 'aktif')
					 ->where('a.id !=', 1)
					 ->limit($limit, $offset)
					 ->get()
					 ->result_array();
		return $data;
	}

	function verify_old_password($id, $password){
		$data = $this->db
					 ->select('username')
					 ->from('membermaster')
					 ->where('id', $id)
					 ->where('password', $password)
					 ->get()
					 ->row_array();
		return $data;
	}

	function print_form(){
		$form = $this->fiform;
		$form = $form->create($this->vars['_ROUTED'].'/excel', array("class" => 'form-horizontal', 'target' => '_blank'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->field(
					 		'dropdown', 
					 		'Status', 
					 		array('name' => 'status', "value"=>'', 'option' => array(0 => 'All status', 'aktif' => 'Active','blok' => 'Blocked')) 
					 	)
					 ->new_row()
					 ->field(
					 		'dropdown', 
					 		'Join date', 
					 		array('name' => 'date', "value"=>'', 'option' => array_merge(array(-1 => 'All'),range(1, 31)) ) 
					 	)
					 ->field(
					 		'dropdown', 
					 		'Month', 
					 		array('name' => 'month', "value"=>'', 'option' => array_merge(array(-1 => 'All'), range(1, 12)) )  
					 	)
					 ->field(
					 		'text', 
					 		'Year', 
					 		array('name' => 'year', "value"=>'') 
					 	)	 						 
					 ->btn('Print', array('type' => 'submit','class' => 'btn btn-primary'))
					 ->retrieve()
					 ;
		return $form;		
	}

	function print_data(){
		$status = $this->input->post('status', true);
		$date  	= $this->input->post('date', true);
		$month  = $this->input->post('month', true);
		$year  	= $this->input->post('year', true);
		if(!empty($status)){
			$this->db->where('a.status', $status);
		}
		if(!empty($date)){
			$this->db->where('DAYOFMONTH(a.created)', $date);
		}		
		if(!empty($month)){
			$this->db->where('MONTH(a.created)', $month);
		}
		if(!empty($year)){
			$this->db->where('YEAR(a.created)', $year);
		}
		$data = $this->db
					 ->select('a.username, a.name, a.jk, a.tempatlahir, a.tgllahir, a.alamat, a.kota, a.hp, a.telp_kantor, a.telp_rumah, a.status,
					 	a.kodepos, a.ahliwaris, a.noktp, a.email, a.npwp,
					 	a.propinsi, a.negara, a.bank_name, a.account_name, a.account_number')
					 ->from('membermaster a')
					 ->get()
					 ->result_array();
		return $data;
	}
}