<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Sales_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('code', 'date', 'name', 'status', 'total', 'action')
		     ->column_width(array(0=>200, 1=>120, 2=>200, 3=>200, 4=>150))
		     ;
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i> Add'
		     	);
		}
		if($this->vars['_PRINTING'] == true){
			$this->fitable
				 ->add_buttons(
		     		'', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/print_excel'), 
		     			'class' => 'btn btn-sm btn-default btn-insert' 
		     		), 
		     		'<i class="fa fa-print"></i> Sales Report'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0, 1, 2, 3, 4)
		     			->column_width(array(1=>'100px', 2=>'180px', 3 => '150px'))
		     			->filter_type(array(
		     				3 => array('paid' => 'Paid', 'unpaid' => 'Unpaid', 'cancel' => 'canceled','delivered' => 'Delivered')
		     			))
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.nomor_po, a.purchase_date, a.member_username, a.po_status, a.grand_total, a.id')
					 ->from('purchase_order a')
					 ->data_format('grand_total','currencies')
					 ->data_format('id','table_action');
		$data = $this->fitable->aodata();

		return $data;
	}

	function table_action($id, $data){
		$action = '';
		if($this->vars['_VIEWING'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/view/'.$id.'/'.seo_url($data['nomor_po'])).'" class="btn btn-success btn-xs btn-update" title="View">
			<i class="fa fa-search"></i></a> ';
		}		
		// if($this->vars['_UPDATE'] == true && $data['po_status'] == 'unpaid'){
		if($this->vars['_UPDATE'] == true && $data['po_status'] != 'delivered'){			
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/edit/'.$id.'/'.seo_url($data['nomor_po'])).'" class="btn btn-primary btn-xs btn-update" title="Edit">
			<i class="fa fa-edit"></i></a> ';
		}
		// if($this->vars['_DELETE'] == true && $data['po_status'] == 'unpaid'){
		if($this->vars['_DELETE'] == true && $data['po_status'] != 'delivered'){
			$action .= '<a data-url="'.$this->vars['_ROUTED'].'/delete'.'" data-stamp="'.$id.'" data-title="'.$data['nomor_po'].'" title="Delete" class="btn btn-danger btn-xs btn-delete">
				<i class="fa fa-trash"></i></a> ';
		}
		if($this->vars['_PRINTING'] == true){
			$action .= '<a href="'.site_url($this->vars['_ROUTED'].'/cetak/'.$id.'/'.seo_url($data['nomor_po'])).'" title="Print" class="btn btn-default btn-xs" target="_blank">
				<i class="fa fa-print"></i></a>';
		}		
		return $action;
	}

	function currencies($string, $data){
		return currency($string);
	}

	function details($id = null, $flag = 'id'){
		$src = $this->db
					->select(
						'a.id, a.nomor_po, a.member_id, a.member_username, a.purchase_date, 
						a.po_status, a.grand_total, a.remark, a.created_at, a.created_by, a.updated_at, a.updated_by
					 ')
					->from('purchase_order a')
					->where($flag, $id)
					->get()
					->row_array();	
		return $src;		
	}

	function item_list(){
		$data = $this->db
					 ->select('code, name, price, price_cust, price_register, point, id')
					 ->from('product')
					 ->where('active','1')
					 ->get()
					 ->result_array();
		return $data;
	}

	function purchase_detail($id){
		$data = $this->db
					 ->select('a.id, a.id_produk, a.qty, a.price, a.subtotal, b.name')
					 ->from('purchaseorder_detail a')
					 ->join('product b','b.id = a.id_produk')
					 ->where('a.po_id', $id)
					 ->get()
					 ->result_array();
		return $data;
	}

	function form($data = null,$action = "Add"){
		$form = $this->fiform;

		if(!empty($data)){
			$action = "Update";
			$form->hidden('id', $data['id']);
			$data['purchase_detail'] = self::purchase_detail($data['id']);
		}

		$data['item_list'] = self::item_list();


		$nomor_po 		= !empty($data) && array_key_exists('nomor_po', $data) ? $data['nomor_po'] 	: '';
		$member_username 	= !empty($data) && array_key_exists('member_username', $data) ? $data['member_username'] 	: '';
		$purchase_date 		= !empty($data) && array_key_exists('purchase_date', $data) ? $data['purchase_date'] 	: date('Y-m-d');
		$po_status 		= !empty($data) && array_key_exists('po_status', $data) ? $data['po_status'] 	: 'unpaid';
		$grand_total 	= !empty($data) && array_key_exists('grand_total', $data) ? $data['grand_total'] 	: 0;
		$remark 		= !empty($data) && array_key_exists('remark', $data) ? $data['remark'] 	: '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->hidden('grand_total', $grand_total)
					 ->required('nomor_po','member_username','purchase_date')
					 ->field(
					 		'text', 
					 		'purchase number', 
					 		array('name' => 'nomor_po', "value"=>$nomor_po) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'member id', 
					 		array('name' => 'member_username', "value"=>$member_username, 'class' => 'autocomplete', 'data-url' => 'find_member') 
					 	)
					 ->new_row()					 
					 ->field(
					 		'text', 
					 		'date', 
					 		array('name' => 'purchase_date', "value"=>$purchase_date, 'class'=>'datepicker') 
					 	)
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'status', 
					 		array('name' => 'po_status', "value"=>$po_status, 'inline' => true, 'option' => array('paid' => 'Paid', 'unpaid' => 'Unpaid', 'cancel' => 'canceled','delivered' => 'Delivered')) 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'remark', 
					 		array('name' => 'remark', "value"=>$remark) 
					 	)				 						 
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->form_addon($this->load->view('form/purchase_order_form', $data, true))					 
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('purchase_order', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);						
			$result = $this->db->update('purchase_order', $postdata, $where);			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('purchase_order', $where);
	}

	function delete_item($where){
		return $this->db->delete('purchaseorder_detail', $where);
	}

	// fungsi autocomplete_data digunakan untuk menampilkan data member berdasarkan ajax request pada form add member
	function autocomplete_data($id){
		$data = $this->db
					 ->select('a.id, a.nomor_po, a.member_username, a.grand_total')
					 ->from('purchase_order a')
					 ->where('po_status', 'unpaid')
					 ->get()
					 ->result_array();
		$result = array();
		$timer = strtotime('now');
		$tfirst = substr($timer, 0, 5);
		$tlast  = substr($timer, -5);
		foreach ($data as $key => $value) {
			$result[] = array(
				"attribute" => array(
					'data-value' => $value['nomor_po'], 
					'data-timestamp' => $tfirst.$value['id'].$tlast, 
					'data-buyer' => $value['member_username'], 
					'data-total'=> $value['grand_total'], 
					'onclick' => 'fetch_form(this)',
				),
				"value"		=> $value['nomor_po'],
				"label"		=> $value['nomor_po'],
			);
		}
		return $result;
	}

	function print_form(){
		$form = $this->fiform;
		$form = $form->create($this->vars['_ROUTED'].'/excel', array("class" => 'form-horizontal', 'target' => '_blank'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('month','year')
					 ->field(
					 		'dropdown', 
					 		'Month', 
					 		array('name' => 'month', "value"=>'', 'option' => range(1, 12)) 
					 	)
					 ->field(
					 		'text', 
					 		'Year', 
					 		array('name' => 'year', "value"=>'') 
					 	)	 						 
					 ->btn('Print', array('type' => 'submit','class' => 'btn btn-primary'))
					 ->retrieve()
					 ;
		return $form;		
	}

	function print_data($month, $year){
		$data = $this->db
					 ->select('a.nomor_po, a.purchase_date, a.member_username, a.po_status, a.grand_total')
					 ->from('purchase_order a')
					 ->where('MONTH(a.purchase_date)', $month)
					 ->where('YEAR(a.purchase_date)', $year)
					 ->get()
					 ->result_array();
		return $data;	
	}		
}