<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedagang_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('Nomor Izin', 'bidang','jenis', 'jk', 'alamat','kelurahan', 'kecamatan' , 'status', 'action');
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		'Insert', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i>'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0, 1, 2, 3, 4, 5, 6, 7)
		     			->column_width(array(0 => 100, 1 => 100 , 2 => 200, 3 => 100, 4 => 200, 5 => 150, 6 => 150, 7 => 100))
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('a.unique_merchant, a.category, a.name, a.jenis_kelamin, a.address, a.kelurahan, a.kecamatan, a.status, a.id')
					 ->from('merchant a')
					 // ->data_format('image', 'img_converter')
					 ->data_format('status', 'publishing')
					 ;
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	'Edit', 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update' 
				 	), 
				 	'<i class="fa fa-edit"></i> '
				 );
		}
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	'Galeri', 
				 	array('href' => site_url($this->vars['_ROUTED'].'/galeri/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-warning btn-update' 
				 	), 
				 	'<i class="fa fa-image"></i> '
				 );
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				'Delete', 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete' 
				), 
				'<i class="fa fa-trash"></i> ' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}

	function img_converter($string){
		$string = !empty($string) ? $string : DEFAULTIMG;
		return '<img src="'.base_url(BASE_PANEL.MINIFIER.$string).'" class="img-thumbnail" width="100" height="100">';
	}

	function publishing($number){
		return $number == 1 ? '<span class="label label-primary">Aktif</label>' : '<span class="label label-default">Tidak aktif</label>';
	}

	function details($id = null, $flag = 'id'){
		$data = $this->db
					 ->select('unique_merchant, category, name, jenis_kelamin, address, kecamatan, kelurahan, description, owner, petugas, petugas_phone, 
					 	latitude, longitude, open_hour, closing_hour, image, status, id')
					 ->from('merchant')
					 ->where($flag, $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function form($data = null){
		$form = $this->fiform;

		$action = 'Baru';
		if(!empty($data)){
			$action = 'Ubah';
			$form->hidden('id', $data['id']);
			$form->hidden('curimg', $data['image']);
		}

		$no_izin 		= !empty($data) && !empty($data['unique_merchant']) ? $data['unique_merchant'] : '';
		$name 			= !empty($data) && !empty($data['name']) ? $data['name'] : '';
		$jenis_kelamin 	= !empty($data) && !empty($data['jenis_kelamin']) ? $data['jenis_kelamin'] : 'L';
		$address 		= !empty($data) && !empty($data['address']) ? $data['address'] : '';
		$kecamatan 		= !empty($data) && !empty($data['kecamatan']) ? $data['kecamatan'] : '';
		$kelurahan 		= !empty($data) && !empty($data['kelurahan']) ? $data['kelurahan'] : '';
		$category 		= !empty($data) && !empty($data['category']) ? $data['category'] : '';

		$image 			= !empty($data) && !empty($data['image']) ? BASE_PANEL.MINIFIER.$data['image'] : BASE_PANEL.MINIFIER.DEFAULTIMG;
		$description 	= !empty($data) && !empty($data['description']) ? $data['description'] : '';
		$owner 			= !empty($data) && !empty($data['owner']) ? $data['owner'] : '';
		$petugas 		= !empty($data) && !empty($data['petugas']) ? $data['petugas'] : '';
		$petugas_phone 	= !empty($data) && !empty($data['petugas_phone']) ? $data['petugas_phone'] : '';
		$latitude 		= !empty($data) && !empty($data['latitude']) ? $data['latitude'] : -6.593761;
		$longitude 		= !empty($data) && !empty($data['longitude']) ? $data['longitude'] : 106.793056;
		$lokasi 		= !empty($latitude) && $longitude ? $latitude.', '.$longitude : '';
		$open_hour 		= !empty($data) && !empty($data['open_hour']) ? explode(':', $data['open_hour']) : '';
		$closing_hour 	= !empty($data) && !empty($data['closing_hour'])? explode(':', $data['closing_hour']) : '';
		$open_hour 	 	= !empty($open_hour) && is_array($open_hour) == true ? sprintf('%01d', $open_hour[0]) : '0';
		$open_minute 	= !empty($open_hour) && is_array($open_hour) == true ? sprintf('%01d', $open_hour[1]) : '0'; 
		$closing_hour 	= !empty($closing_hour) && is_array($closing_hour) == true ? sprintf('%01d', $closing_hour[0]) : '0';
		$closing_minute = !empty($closing_hour) && is_array($closing_hour) == true ? sprintf('%01d', $closing_hour[1]) : '0'; 
		$status 		= !empty($data) ? $data['status'] : '';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->hidden('latitude', $latitude)
					 ->hidden('longitude', $longitude)
					 ->required('unique_merchant','category','name', 'owner', 'address', 'kecamatan', 'kelurahan', 'longitude', 'latitude', 'petugas')
					 ->fieldset('Informasi Pemilik')
					 	->field('text', 'nama pemilik', array('name' => 'owner', "placeholder"=>'Nama Pemilik', "value"=>$owner) )
						->new_row()
					 	->field('radio', 'jenis kelamin', array('name' => 'jenis_kelamin', "option"=> array('L' => 'Laki-laki', 'P' => 'Perempuan'), 
					 	"value"=>$jenis_kelamin, 'inline' => true) )
					 ->endfieldset()					 
					 ->fieldset('Informasi Usaha')
					 	->field('text', 'nomor izin', array('name' => 'unique_merchant', 'placeholder' => 'Nomor izin' ,'value' => $no_izin) )
					 	->new_row()					 
					 	->field('text', 'bidang usaha', array('name' => 'category', 'placeholder' => 'Bidang usaha' ,'value' => $category) )
					 	->new_row()
					 	->field('text', 'jenis usaha', array('name' => 'name', "placeholder"=>'Jenis usaha', "value"=>$name) )
					 	->new_row()
					 	->field('text', 'alamat', array('name' => 'address', "placeholder"=>'Alamat', "value"=>$address) )
					 	->new_row()
					 	->field('textarea', 'deskripsi', array('name' => 'description', "placeholder"=>'Keterangan usaha', "value"=>$description) )
					 	->new_row()					 	
					 	->field('text', 'kecamatan', array('name' => 'kecamatan', "placeholder"=>'Kecamatan', "value"=>$kecamatan) )
					 	->field('text', 'kelurahan', array('name' => 'kelurahan', "placeholder"=>'Kelurahan', "value"=>$kelurahan) )
					 	->new_row()
						->field('text', 'latitude', array('name' => 'latitude', "id" => "lat", "placeholder"=>'Latitude', "value"=>$latitude, 'readonly' => true) )
						->field('text', 'longitude', array('name' => 'longitude', "id" => "lng", "placeholder"=>'Longitude', "value"=>$longitude, 'readonly' => true) )
					 	->new_row()
					 	->field('custom','pilih lokasi','<div id="select_area" style="width:100%; height:300px;background-color:#ccc;"></div>')
					 	->new_row()
					 	->field('text', 'petugas', array('name' => 'petugas', "placeholder"=>'Nama Petugas', "value"=>$petugas) )
					 	->field('text', 'telp', array('name' => 'petugas_phone', "placeholder"=>'Nomor telp / HP petugas', "value"=>$petugas_phone) )
					 	->new_row()
					 	->field('number', 'jam buka', array('name' => 'open_hour', "placeholder"=>'Jam', "value"=>$open_hour, 'min' => '0', 'max' => '24') )
					 	->field('number', 'menit', array('name' => 'open_minute', "placeholder"=>'Menit', "value"=>$open_minute, 'min' => '0', 'max' => '59') )
					 	->new_row()
					 	->field('number', 'jam tutup', array('name' => 'closing_hour', "placeholder"=>'Jam Tutup', "value"=>$closing_hour, 'min' => '0', 'max' => '24') )
					 	->field('number', 'menit', array('name' => 'closing_minute', "placeholder"=>'Menit', "value"=>$closing_minute, 'min' => '0', 'max' => '59') )
					 	->new_row()
					 	->field('radio', 'status', array('name' => 'status', "option"=>array('non aktif', 'aktif'), "value"=>$status, 'inline' => true) )
					 	->new_row()
					 	->field('filesimage','Logo', array('name' => 'image', 'value' =>$image) )
					 	->new_row()
					 ->endfieldset()
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('merchant', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('merchant', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('merchant', $where);
	}
	

	function form_galery($id){
		$form = $this->fiform;

		$form = $form->create($this->vars['_ROUTED'].'/upload_foto', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->hidden('id', $id)
					 ->required('multimg')
					 ->field(
					 		'filesimage', 
					 		'unggah foto', 
					 		array('name' => 'image', 'multiple' => 'true') 
					 	)
					 ->btn('Unggah', array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}
}