<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/

class Stockies_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);
	}

	function table(){
		$this->fitable
		     ->attribute(array(
					"title" => $this->vars['_TITLE']." data", 
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/')
		     		)
		    	)
		     ->set_column('memberid', 'name', 'type', 'status', 'action')
		     ->column_width(array(0=>200, 1=>200, 2=>150, 3=>150))		     
		     ;
		if($this->vars['_INSERT'] == true){
			$this->fitable
				 ->add_buttons(
		     		$this->bahasa['insert'], 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/insert'), 
		     			'class' => 'btn btn-sm btn-primary btn-insert' 
		     		), 
		     		'<i class="fa fa-plus"></i> Add'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0,1,2,3)
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('b.username, b.name, a.stockist_type, a.status, a.id')
					 ->from('stockist a')
					 ->join('membermaster b','b.id = a.member_id')
					 ;
		if($this->vars['_UPDATE'] == true){
		$this->fitable
			 ->set_action(
				 	$this->bahasa['edit'], 
				 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
				 		'class' => 'btn btn-xs btn-primary btn-update',
				 		'title' => 'Edit' 				 		 
				 	), 
				 	'<i class="fa fa-edit"></i>'
				 )
			 ;
		}
		if($this->vars['_DELETE'] == true){
		$this->fitable
			 ->set_action(
				$this->bahasa['delete'], 
				array(
					'data-url' => $this->vars['_ROUTED'].'/delete', 
					'data-stamp' => '%1$d', 
					'data-title' => '%2$s', 
					'class' => 'btn btn-xs btn-danger btn-delete',
				 	'title' => 'Delete'
				), 
				'<i class="fa fa-trash"></i>' 
			 )	;				 
		}		
		$data = $this->fitable->aodata();

		return $data;
	}
	function details($id = null){
		$src = $this->db
					->select('a.id, a.parent_stockist, b.username, a.stockist_type, a.status')
					->from('stockist a')
					->join('membermaster b','b.id = a.member_id')
					->where('a.id', $id)
					->get()
					->row_array();	
		return $src;		
	}

	function form($data = null,$action = "tambah"){
		$form = $this->fiform;

		if(!empty($data)){
			$action = "Update";
			$form->hidden('id', $data['id']);
		}

		$parent_stockist 	= !empty($data) ? $data['parent_stockist'] 	: '';
		$member_id 			= !empty($data) ? $data['username'] 		: '';
		$stockist_type 		= !empty($data) ? $data['stockist_type'] 	: 'master';
		$status 			= !empty($data) ? $data['status'] 			: 'aktif';

		$form = $form->create($this->vars['_ROUTED'].'/save', array("class" => 'form-horizontal form-submitting'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('member_id')
					 ->field(
					 		'text', 
					 		'master stockist', 
					 		array('name' => 'parent_stockist', "value"=>$parent_stockist, 'class' => 'autocomplete', 'data-url' => "find_member") 
					 	)
					 ->new_row()
					 ->field(
					 		'text', 
					 		'member id', 
					 		array('name' => 'member_id', "value"=>$member_id, 'class' => 'autocomplete', 'data-url' => "find_member") 
					 	)
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'stockist type', 
					 		array('name' => 'stockist_type', "value"=>$stockist_type, 'option' => array('master' => 'Master Stockist', 'stockist' => 'Stockist', 'agen' => 'Agen'), 'inline' => true) 
					 	)
					 ->new_row()
					 ->field(
					 		'radio', 
					 		'status', 
					 		array('name' => 'status', "value"=>$status, 'option' => array('aktif' => 'Active', 'inactive' => 'Inactive'), 'inline' => true) 
					 	)
					 ->btn($action, array('type' => 'button','class' => 'btn btn-primary btn-form'))
					 ->retrieve()
					 ;
		return $form;
	}

	function stockist_verify($username){
		$data = $this->db
					 ->select('b.username, a.id, a.member_id, a.stockist_type')
					 ->from('stockist a')
					 ->join('membermaster b','b.id = a.member_id')
					 ->where('b.username', $username)
					 ->where('a.stockist_type','master')
					 ->get()
					 ->row_array();
		return $data;
	}

	function insert($postdata, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['created_at'] = date('Y-m-d H:i:s');
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			$postdata = array_map('trim', $postdata);
			$this->db->insert('stockist', $postdata);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);						
			$result = $this->db->update('stockist', $postdata, $where);			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('stockist', $where);
	}	
}