<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_bonus_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$library = array(
			'fitable',
			'fiform'
		);
		$this->load->library($library);

	}

	function table(){
		$this->fitable
		     ->attribute(array(
		     		"data-render" 	=> "datatable", 
		     		"data-sources" 	=> site_url($this->vars['_ROUTED'].'/renders/'),
		     		"data-col-order"	=> '1:desc,0:asc'
		     		)
		    	)
		     ->set_column('username', 'report date', 'status', 'gmp','ecash','sponsor','total', 'tax','transfer')
		     ;
		if($this->vars['_PRINTING'] == true){
			$this->fitable
				 ->add_buttons(
		     		'', 
		     		array(
		     			'href' 	=> site_url($this->vars['_ROUTED'].'/print_excel'), 
		     			'class' => 'btn btn-sm btn-default btn-insert' 
		     		), 
		     		'<i class="fa fa-print"></i> Print Report'
		     	);
		}
		$output = $this ->fitable
		     			->filtered_column(0,1,2)
		     			->filter_type(array(
		     				1 => 'class:form-control column-filter datepicker',
		     				2 => array('0' => 'Pending', '1' => 'Paid')
		     			))
		     			->column_width(array(0 => 170, 1 => 200 , 2 => 150, 3 => 120, 4=> 120, 5=>120, 6=>120, 7=>120, 8=>120))
						->generate();
		return ($output);
	}

	function json_data(){
		$this->fitable
					 ->select('b.username, a.rekap_date, a.status, a.gmp, a.ecash, a.bs, a.total, a.tax, a.transfer, a.id')
					 ->from('bonus_statement a')
					 ->where('a.transfer !=', 0)
					 ->join('membermaster b','b.id = a.member_id')
					 ->data_format('status', 'status')
					 ->data_format('gmp', 'currencies')
					 ->data_format('ecash', 'currencies')
					 ->data_format('bs', 'currencies')
					 ->data_format('total', 'currencies')
					 ->data_format('tax', 'currencies')
					 ->data_format('transfer', 'currencies')
					 ;
		// if($this->vars['_UPDATE'] == true){
		// $this->fitable
		// 	 ->set_action(
		// 		 	$this->bahasa['edit'], 
		// 		 	array('href' => site_url($this->vars['_ROUTED'].'/edit/%1$d/%2$s'), 
		// 		 		'class' => 'btn btn-xs btn-primary btn-update' 
		// 		 	), 
		// 		 	'<i class="fa fa-edit"></i>'
		// 		 );
		// }
		// if($this->vars['_DELETE'] == true){
		// $this->fitable
		// 	 ->set_action(
		// 		$this->bahasa['delete'], 
		// 		array(
		// 			'data-url' => $this->vars['_ROUTED'].'/delete', 
		// 			'data-stamp' => '%1$d', 
		// 			'data-title' => '%2$s', 
		// 			'class' => 'btn btn-xs btn-danger btn-delete' 
		// 		), 
		// 		'<i class="fa fa-trash"></i>' 
		// 	 )	;				 
		// }		
		$data = $this->fitable->aodata();

		return $data;
	}

	function status($string){
		if($string == 0){
			$label = '<span class="label label-default">pending</span>';
		}
		else{
			$label = '<span class="label label-success">paid</span>';
		}
		return $label;
	}

	function currencies($number){
		return currency($number);
	}

	function details($id = null){
		$data = $this->db
					 ->select('member_id, monthly, status, rekap_date, date_from, date_to, total, gmp, ecash, eshop, bs, id')
					 ->from('bonus_statement')
					 ->where('id', $id)
					 ->get()
					 ->row_array();
		return $data;
	}

	function update($postdata, $where, $datetime = false){
		if(array_key_exists('token', $postdata) == true){
			unset($postdata['token']);
			if($datetime == true){
				$postdata['updated_at'] = date('Y-m-d H:i:s');
			}
			unset($postdata['id']);
			$postdata = array_map('trim', $postdata);			
			$this->db->update('bonus_statement', $postdata, $where);
			$result = $this->db->insert_id();			
		}
		else{
			$result = false;
		}
		return $result;
	}

	function delete($where){
		return $this->db->delete('bonus_statement', $where);
	}

	function print_form(){
		$bonus_type = self::bonus_type();
		$form = $this->fiform;
		$form = $form->create($this->vars['_ROUTED'].'/excel', array("class" => 'form-horizontal', 'target' => '_blank'))
					 ->hidden('token', $this->session->userdata(DEF_APP.'_'.SESS_ADMIN.'token'))
					 ->required('date','month','year')
					 ->field(
					 		'dropdown', 
					 		'Date', 
					 		array('name' => 'date', "value"=>'','option' => range(1, 31)) 
					 	)					 
					 ->field(
					 		'dropdown', 
					 		'Month', 
					 		array('name' => 'month', "value"=>'', 'option' => range(1, 12)) 
					 	)
					 ->field(
					 		'text', 
					 		'Year', 
					 		array('name' => 'year', "value"=>'') 
					 	)
					 ->new_row()
					 ->field(
					 		'checkbox',
					 		'Bonus Type',
					 		array('name' => 'type[]', 'value' => array_values($bonus_type), 'option' => $bonus_type)
					 	)	 						 
					 ->btn('Print', array('type' => 'submit','class' => 'btn btn-primary'))
					 ->retrieve()
					 ;
		return $form;		
	}

	function bonus_type(){
		$data = $this->db
					 ->select('lcase(bonus_name) as bonus')
					 ->from('type_bonus')
					 ->where('status', 1)
					 ->where('interval >', 0)
					 ->get()
					 ->result_array();
		$result = array();
		foreach ($data as $key => $value) {
			$result[$value['bonus']] = $value['bonus'];
		}
		return $result;
	}

	function print_data($date, $month, $year){
		$type = $this->input->post('type', true);
		$type = !empty($type) ? 'a.'.implode(', a.', $type): 'a.'.implode(', a.', array_values(self::bonus_type()));		
		$data = $this->db
					 ->select('a.rekap_date, b.username, b.account_name as holder, b.bank_name as bank, b.account_number as norek, 
					 	'.$type.', a.tax, a.total')
					 ->from('bonus_statement a')
					 ->join('membermaster b','b.id = a.member_id')
					 ->where('a.transfer !=', 0)					 
					 ->where('a.rekap_date', $year.'-'.$month.'-'.$date)
					 ->get()
					 ->result_array();
		return $data;	
	}	
}