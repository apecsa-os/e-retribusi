function getLocation(){
  var result = [];
  var content = [];
  var postdata = new FormData();
  postdata.append('token', token);

  var ajaxdata = {
    'url'    : base+'?maps=true',
    'method' : 'POST',
    'type'   : 'json',
    'data'   : postdata
  };

  ajax(ajaxdata).success(function(callback){
    if(callback.success == true){
      $.each(callback.data, function(index, val) {
         result.push([
            val['name']+', '+val['kelurahan']+', '+val['kecamatan'],
            parseFloat(val['latitude']),
            parseFloat(val['longitude']), 
          ]);
         desc = val.description != null ? '<p>'+val.description+'</p>' : '';
         content.push([
            '<div class="info_content">' +
            '<strong>'+val.name+'</strong>' +
            desc+
            '</div>' 
         ]);
      });
      initMap(result, content);
    }
  });

};

function initMap(markers, infoWindowContent){
  var map;  
  var bounds = new google.maps.LatLngBounds();
  var mapOptions = {
      mapTypeId: 'roadmap',
      zoom:14,
      center:{lat:-6.5971,lng:106.8060}
  };

  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
  map.setTilt(45);

  var infoWindow = new google.maps.InfoWindow(), marker, i;

  for( i = 0; i < markers.length; i++ ) {
      var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
      bounds.extend(position);
      marker = new google.maps.Marker({
          position: position,
          map: map,
          title: markers[i][0]
      });
            
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
              infoWindow.setContent(infoWindowContent[i][0]);
              infoWindow.open(map, marker);
          }
      })(marker, i));

      map.fitBounds(bounds);
  }

  var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
      this.setZoom(13);
      google.maps.event.removeListener(boundsListener);
  });             
};

