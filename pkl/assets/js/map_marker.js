var lat = document.getElementById('lat').value;
var lng = document.getElementById('lng').value;

var mapOptions = {
	mapTypeId: 'roadmap',
	zoom:14,
	center:{lat:-6.5971,lng:106.8060}
};
map = new google.maps.Map(document.getElementById("select_area"), mapOptions);

var position = new google.maps.LatLng(lat, lng);

var marker = new google.maps.Marker({
	position: position,
	map: map,
	title: 'Lokasi Pedangan'
});

google.maps.event.addListener(map, 'click', function (event) {
	map.panTo(event.latLng);
	map.setCenter(event.latLng);
	createMarker(event.latLng);
});


function createMarker(latLng) {
	if ( !! marker && !! marker.setMap) marker.setMap(null);
	marker = new google.maps.Marker({
		map: map,
		position: latLng,
		draggable: true
	});

	document.getElementById('lat').value = marker.getPosition().lat().toFixed(6);
	document.getElementById('lng').value = marker.getPosition().lng().toFixed(6);

	google.maps.event.addListener(marker, "dragend", function () {
		document.getElementById('lat').value = marker.getPosition().lat().toFixed(6);
		document.getElementById('lng').value = marker.getPosition().lng().toFixed(6);
	});
}