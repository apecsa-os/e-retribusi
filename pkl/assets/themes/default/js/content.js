var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
            '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
            '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
            '</div>' +
        '</div></div></div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         *                options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *                options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Loading';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null
            }, options);

            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        },
    };

})(jQuery);
$(document).ready(function() {
    function_loader();
});
function function_loader(){
    if(typeof datepick == "function"){
        datepick(); 
    }
    if(typeof texteditor == "function"){
        texteditor();
    }
    if(typeof imageuploads == "function"){
        imageuploads();
    }
    if(typeof currency == "function"){
        currency();
    }
    if(typeof checkbox_image == "function"){
        checkbox_image();
    }

    if(typeof lazy_loader == 'function'){
        lazy_loader();
    }

    if(typeof text_autocomplete == 'function'){
        text_autocomplete();
    }

    if(typeof selectiontwo == 'function'){
        selectiontwo();
    }
}

function lazy_loader(){
    if($.fn.lazyload){
        $(".lazy-loader").lazyload();      
    }   
}
function selectiontwo(){
    if(typeof select2 != undefined && $('.select-two').length > 0){
        $('.select-two').select2({
            language:lang,
        });
    }   
}
function ajax(data){
    return $.ajax({
        url: data.url,
        type: data.method != undefined ? data.method : 'POST',
        dataType: data.type != undefined ? data.type : 'json',
        data: data.data,
        processData:false,
        contentType:false,
        beforeSend:function(){
            waitingDialog.show('Please wait...');
        }        
    }).success(function(){
        waitingDialog.hide();
    });
}
function text_autocomplete(){
    $('.autocomplete').focus(function() {
        var d = $(this);
        _autocomplete(d);
    });
}

function _autocomplete(d){
        var dparent = $(d).parent();
        var resources = $(d).data('url');
        var minLength = $(d).data('min-length') != undefined ? $(d).data('min-length') : 3;
        var autolength = dparent.find('.ui-autocomplete').length;
        if(autolength == 0){
            $(d).autocomplete({
                minLength : minLength,
                appendTo: dparent,
                source : base + '/'+resources,
                focus : function(event, ui){
                    $(d).val(ui.item.label);
                    return false;
                }
            });

            $(d).data('ui-autocomplete')._renderItem = function(ul, item){
                var $li = $('<li>');
                if(item.img != undefined){
                    var $img = $('<img>');
                    $img.attr(item.img);
                    $li.append($img);
                }
                $li.append(item.label);
                $li.attr(item.attribute);
                return $li.appendTo(ul);
            };
        }    
}
function currencies(nStr) {
    nStr += '';
    nStr    = nStr.replace(/,/,'');
    var x   = nStr.split('.');
    var x1  = x[0];
    var x2  = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function currencies_reverse(str){
    return str.replace(/,/g,'');
}
function swalalerts(args, callback){
    var $title  = (args.title != undefined) ? args.title : '';
    var $text   = (args.text != undefined) ? args.text : '';
    var $type   = (args.type != undefined) ? args.type : 'warning';
    var $cancel = (args.showCancelButton != undefined) ? args.showCancelButton : true;
    var $close  = (args.closeOnConfirm != undefined) ? args.closeOnConfirm : false;
    var $abort  = (args.closeOnCancel != undefined) ? args.closeOnCancel : true;

    swal({
        title:$title,
        text:$text,
        type:$type,
        showCancelButton:$cancel,
        confirmButtonColor:"#DD6B55",
        cancelButtonText:"Cancel",
        closeOnConfirm:$close,
        closeOnCancel: $abort,
        allowOutsideClick :true,
       },
       function (isConfirm){
            callback(isConfirm);
        }
    );
}
function load_modal(modaldata){

    var winHeight = $(window).height() * 0.75;

    var $title  = modaldata.title != undefined ? modaldata.title : '';
    var $body   = modaldata.body != undefined ? modaldata.body : '';

    $('#myModal').modal('show');
    $('#myModal').on('shown.bs.modal', function(event){
        var elem = $(event.relatedTarget);
        $('#myModal .modal-body').css('max-height',winHeight+'px');
        $('#myModal').appendTo(elem);
        $('#myModal .modal-title').html($title);
        $('#myModal .modal-body').html($body);
        if(typeof treesbtn == 'function'){
            treesbtn();     
        }
        if(typeof after_modal == 'function'){
            after_modal();  
        }
        if(typeof function_loader == 'function'){
            function_loader();
        }
    });
    $('#myModal').on('hidden.bs.modal', function(event){
        $('body').css('padding-right',0);
    });
}