	$.fn.clickOff = function(callback, selfDestroy) {
	    var clicked = false;
	    var parent = this;
	    var destroy = selfDestroy || true;
	    
	    parent.click(function() {
	        clicked = true;
	    });
	    
	    $(document).click(function(event) { 
	        if (!clicked) {
	            callback(parent, event);
	        }
	        clicked = false;
	    });
	};
$(function(){
	sidebar();
	filtering();
	comments();
	wishlist();
	buy();
	abuse();
	view_cart();
	checkout();
	$('form[method=get]').submit(function() {
		$(this).find(':input[name]').each(function(index, el) {
			if(!$(el).val()){
				$(el).attr('disabled', true);				
			}
		});
	});

	if($.fn.select2){
		$('select').not('.unedited').select2({
			language:lang
		});		
	}				    
});

function post_ajax(data, e){
    return $.ajax({
        url: data.url,
        type: data.method,
        dataType: data.type,
        data: data.data,
        processData:false,
        contentType:false,
        beforeSend:function(){
        	$(e).append('<span class="fa fa-spinner fa-pulse fa-fw"></span>');
        }
    });
}

function sidebar(){
    $('.nav-heading .buttons > .btn').click(function(){
    	$(this).children('.fa').toggleClass('fa-minus fa-plus');
    	$(this).parents('.nav-heading').siblings('li').not('.nav-heading').toggle(400);
    });	
	$('.ddlink').on('click', function() {
		var parent = $(this).parent('li');
		if(parent.hasClass('open') == true){
			$(this).parent('li').removeClass('open');			
		}
		else{
			$(this).parent('li').addClass('open');	
		}
	});
	$('.nav-box div.fa[data-toggle=nav]').on('click', function(event) {
		var parents = $(this).parent().parent();
		event.preventDefault();
		event.stopPropagation();
		$(this).toggleClass('fa-caret-right fa-caret-down'); 
		parents.siblings().removeClass('open');
		parents.toggleClass('open');
		if(parents.hasClass('open') == true){		
			parents.children('.children').css('display','block');
		}
		else{
			parents.children('.children').css('display','none');						
		}
	});	
}

function filtering(){
	$('.multiselect').clickOff(function(){
    	$('.multiselect .combobox').css('display','none');
    });
    $('.combobox-selection.btn').on('click', function() {
    	$(this).next().toggle();
    });
    $('.combobox input').change(function() {
    	var selected = $(this).parents('.combobox').find('input[type=hidden]').val();
    	var induk 	= $(this).parents('.combobox').prev();
    	var labelen = induk.find('.filler-option:not(.combobox-title)').length;
    	var val 	= $(this).attr('value');
    	var slcval 	= $(this).parent().text();
    	var input 	= $(this).parents('.combobox').find('input[type=hidden]');
    	var hdname 	= input.attr('name');

    	if($(this).is(':checked') == true){
    		var nilai 	= [];
    		$(this).parents('.combobox').find(':checked').each(function(index, el) {
    			nilai.push($(el).val());
    		});
    		input.val(nilai.join('+'));
    		induk.find('.combobox-filler').append('<div class="filler-option filler-box" id="'+hdname+'_'+val+'">'+slcval+'</div>');
    	}
    	else{
    		$('.combobox-filler #'+hdname+'_'+val).remove();
    	}

    	if($(this).parents('.combobox').find('input:checked').length > 0){
    		induk.find('.combobox-title').css('display','none');
    	}
    	else{
    		induk.find('.combobox-title').css('display','block');
    	}    	
    });
	$('.btn-confirm').click(function(event){
		event.preventDefault();
		var product_len = $('.checked_product:checked').length;
		if(product_len > 0){
			var idtrans = [];
			$('.checked_product:checked').each(function(index, el) {
				idtrans.push($(el).val());
			});
			$(this).parents('form').find('input[name=codes]').val(idtrans.join('&'));
			$(this).parents('form').submit();
		}
		else{
			swal($(this).attr('err-msg'));
		}
	});
	$('button[type=reset]').click(function() {
		var par = $(this).parents('form');
		par.find('input').val('');
		par.find(':selected').prop('selected',false);
		par.find(':checked').prop('checked',false);
		if(par.find('.filler-box').length > 0){
			par.find('.filler-box').remove();
			par.find('.filler-option.combobox-title').show();
		}
	});    	
}

function wishlist(){
	$('.btn-wishlist').on('click', function() {
		var e    = $(this);				
		var data = [] ;
		var postdata = new FormData();
		postdata.append('codes', $(this).attr('data-produk'));

		data['url'] 	= base + '/' + $(this).attr('data-url');
		data['method']	= 'POST';
		data['type']	= 'json';
		data['data']	= postdata;

		post_ajax(data, e).success(function(callback){
			e.find('span.fa.fa-spinner').remove();
			if(callback.success == true){
				$('.btn-wishlist').find('.fa').toggleClass('fa-heart fa-close');
				$('.btn-wishlist').find('.hidden-xs').html(callback.button);
			}
			else{
				swalalerts(callback, function(confirm){
					if(confirm == true){
						window.location.replace(siteurl+'/authorize/login');
					}
				});
			}
		});
	});
		
}

function buy(){
	$('.btn-buy').on('click', function() {
		var e    = $(this);		
		var data = [] ;
		var postdata = new FormData();
		postdata.append('codes', $(this).attr('data-produk'));

		data['url'] 	= base + $(this).attr('data-url');
		data['method']	= 'POST';
		data['type']	= 'json';
		data['data']	= postdata;

		post_ajax(data, e).success(function(callback){
			e.find('span.fa.fa-spinner').remove();				
			if(callback.success == true){
				load_modal(callback.modal);
			}
			else{
				swalalerts(callback, function(confirm){
					if(confirm == true){
						window.location.replace(siteurl+'/authorize/login');
					}
				});
			}
		});
	});	
}

function abuse(){
	$('.btn-abuse').on('click', function(){
		var e    = $(this);
		var data = [] ;
		var postdata = new FormData();
		postdata.append('codes', $(this).attr('data-id'));
		postdata.append('type', $(this).attr('data-type'));

		data['url'] 	= base + $(this).attr('data-url');
		data['method']	= 'POST';
		data['type']	= 'json';
		data['data']	= postdata;

		post_ajax(data, e).success(function(callback){
			e.find('span.fa.fa-spinner').remove();
			if(callback.success == true){
				load_modal(callback.modal);
			}
			else{
				swalalerts(callback, function(confirm){
					if(confirm == true && callback.status == 1){
						window.location.replace(siteurl+'/authorize/login');
					}
				});
			}
		});		
	});
}

function comments(){
	$('.btn-reply').click(function() {
		if(token == '' || token == null){
			swal('Anda harus login dahulu','You must login first');
		}
		else{
			var lastli = $(this).parents('ul').find('li:last');
			$(this).parents('.row.control').next('.row').toggleClass('hidden');
		}
	});

	$('.reply-btn').click(function() {
		if($(this).prev('.reply-input').find('textarea').val() != ""){
			$(this).parents('form').submit();
		}
	});

	$('.btn-comments').click(function() {
		var btn 	= $(this);
		var par 	= $(this).parents('ul');
		var data = [] ;		
		var postdata = new FormData();
		var page = $(this).attr('data-page');
		if(typeof page == 'undefined'){
			page = 1;
		}
		else{
			page = parseInt(page) + 1;
		}
		postdata.append('codes', $(this).attr('data-id'));
		postdata.append('page', page);

		data['url'] 	= base + $(this).attr('data-url');
		data['method']	= 'POST';
		data['type']	= 'json';
		data['data']	= postdata;

		post_ajax(data, btn).success(function(callback){
			btn.find('span.fa.fa-spinner').remove();			
			if(callback.success == true){
				par.append(callback.retrieve);
				if(callback.page > page){
					btn.attr('data-page', page);	
				}
				else{
					btn.addClass('disabled');
				}
			}
			else{
				btn.remove();
			}
		});
	});
}
function swalalerts(args, callback){
	var $title 	= (args.title != undefined) ? args.title : '';
	var $text 	= (args.text != undefined) ? args.text : '';
	var $type 	= (args.type != undefined) ? args.type : 'warning';
	var $cancel = (args.showCancelButton != undefined) ? args.showCancelButton : true;
	var $close 	= (args.closeOnConfirm != undefined) ? args.closeOnConfirm : false;
	swal({
	   	title:$title,
	   	text:$text,
	   	type:$type,
	   	showCancelButton:$cancel,
	   	confirmButtonColor:"#DD6B55",
	   	cancelButtonText:"Cancel",
	   	closeOnConfirm:$close,
	   	closeOnCancel: true,
	   	allowOutsideClick :true,
	   },
	   function (isConfirm){
	    	callback(isConfirm);
	    }
	);
}

function load_modal(modaldata){
    var $title  = modaldata.title != undefined ? modaldata.title : '';
    var $body   = modaldata.body != undefined ? modaldata.body : '';

    $('#myModal').modal('show');
    $('#myModal').on('shown.bs.modal', function(event){
        var elem = $(event.relatedTarget);
        $('#myModal').appendTo(elem);
        $('#myModal .modal-title').html($title);
        $('#myModal .modal-body').html($body);          
    });
}

function purchase(){
	var counter = 0;
		
	$('.btn-calc').on('click', function(event) {
		counter++;
		event.preventDefault();
		if(counter == 1){
			$('.calc-input').val('');
		}
		var numb = $(this).html();
		var input_cal = $('.calc-input').val();
		input_cal += numb;
		$('.calc-input').val(input_cal.replace(/^0+/,''));
	});

	$('.btn-calc-c').on('click', function(event) {
		event.preventDefault();
		counter = 0;
		$('.calc-input').val(1);
	});

	$('.btn-calc-e').on('click', function(event) {
		event.preventDefault();
		counter = 0;
		var thisval = $('.calc-input.currency').val();
		var initprice = $('#init_price').val();
		var total = parseFloat(thisval * initprice);

		$('.calc-input').autoNumeric('init',{aSep:tsep,aDec:dsep,mDec:ddig});
		$('.calc-input').autoNumeric('set',thisval);
		$('.calc-price').autoNumeric('init',{aSep:tsep,aDec:dsep,mDec:ddig});
		$('.calc-price').autoNumeric('set',total);
		$('#calculator').removeClass('in');
	});	
}

function view_cart(){
	$('.cart-btn').on('click', function() {
		var e    = $(this);
		var data = [] ;
		var postdata = new FormData();
		postdata.append('token', e.attr('data-token'));

		data['url'] 	= siteurl + e.attr('data-url');
		data['method']	= 'POST';
		data['type']	= 'json';
		data['data']	= postdata;

		post_ajax(data, e).success(function(callback){
			e.find('span.fa.fa-spinner').remove();
			if(callback.success == true){
				load_modal(callback.modal);
			}
			else{
				swalalerts(callback, function(confirm){
					if(confirm == true && callback.status == 1){
						window.location.replace(siteurl+'/authorize/login');
					}
				});
			}
		});		
	});
}

function checkout(){

	var subtotal = function(e, qty){
		var $par 		= $(e).parents('.list-group-item');
		var $initprice 	=  $par.find('input.pricing').val();
		var $subtotal 	= parseFloat($initprice * qty);

		$par.find('.price_sub').val($subtotal);
		$par.find('.subtotal').autoNumeric('init', {aSep:tsep,aDec:dsep,mDec:ddig});
		$par.find('.subtotal').autoNumeric('set', $subtotal);
	};

	var grandtotal = function(){
		var $total = 0;
		var $unik  = $('.unique_code').text();
		$('.cart-item').each(function(index, el) {
			if($(el).find('.price_sub').length > 0){
				var sub = $(el).find('.price_sub').val();
				$total += parseFloat(sub);				
			}
		});
		var taxes = $('.taxes').attr('data-real');
		var courier = $('.courier_services').attr('data-real');

		taxes = parseFloat(taxes / 100) * $total;
		$total += parseFloat(taxes);
		$total += parseFloat(courier);
		$total += parseFloat($unik);

		$('.cart-grandtotal, .taxes').autoNumeric('init', {aSep:tsep,aDec:dsep,mDec:ddig});
		$('.cart-grandtotal').autoNumeric('set', $total);		
		$('.taxes').autoNumeric('set',taxes);
	};

	var costs = function(){
		var e    		= $('.courier_option:checked');				
		var $courier 	= e.val();
		var $weight 	= $('.total_weight').text();
		var $dest 		= $('.ro_dest').val();

		var data = [] ;
		var postdata = new FormData();
		postdata.append('token', e.parents('form').find('input[name=token]').val());
		postdata.append('courier', $courier);
		postdata.append('destination', $dest);
		postdata.append('weight', $weight);

		data['url'] 	= siteurl + 'cart/shipping_cost';
		data['method']	= 'POST';
		data['type']	= 'json';
		data['data']	= postdata;

		
		post_ajax(data, e).success(function(callback){
			e.find('span.fa.fa-spinner').remove();
			if(callback.success == true){
				if($('.courier_option').parents('.form-group').next().length == 0){
					$('.courier_option').parents('.form-group').after(callback.response);					
				}
				else{
					$('.courier_option').parents('fieldset').find('.form-group:eq(1)').html('');
					$('.courier_option').parents('.form-group').after(callback.response);
				}

				var $selected = $('.courier_method:checked').val();

				$('.courier_method').on('change', function() {
					$selected = $(this).val();
					method_selection($selected);
					grandtotal();						
				});
				method_selection($selected);
				grandtotal();
			}
		});
	};

	$('.btn-minus').on('click', function() {
		var qty 	= $(this).parent().next('.qty').val();
		var initw 	= $(this).parents('.list-group-item').find('.init-weight').val();
		var totalw 	= $('.total_weight').text();
		if(qty > 1){
			qty--;
			totalw 	= parseFloat(totalw) - parseFloat(initw);
		}
		$(this).parent().next('.qty').val(qty);
		$('.total_weight').html(totalw);
		subtotal(this, qty);
		grandtotal();
	});

	$('.btn-plus').on('click', function() {
		var qty = $(this).parent().prev('.qty').val();
		var initw 	= $(this).parents('.list-group-item').find('.init-weight').val();
		var totalw 	= $('.total_weight').text();

		if(qty >= 1){
			qty++;
			totalw 	= parseFloat(totalw) + parseFloat(initw);

		}
		$(this).parent().prev('.qty').val(qty);
		$('.total_weight').html(totalw);

		subtotal(this, qty);
		grandtotal();
	});

	$('.update-cart').click(function(event) {
		var e    = $(this);		
		var data = [] ;
		var postdata = new FormData();
		$('.cart-item').each(function(index, el) {
			if($(el).find(':input').length > 0){
				$(el).find(':input').each(function(i, e) {
					if($(e).attr('name') != undefined){
						postdata.append($(e).attr('name'), $(e).val() );
					}
				});
				
			}
		});
		/*
		console.log(postdata);
		*/		
		data['url'] 	= siteurl+'cart/update';
		data['method']	= 'POST';
		data['type']	= 'json';
		data['data']	= postdata;

		post_ajax(data, e).success(function(callback){
			e.find('span.fa.fa-spinner').remove();				
			swalalerts(callback, function(confirm){
				if(confirm == true){
					
				}
			});
		});
	});

	$('.btn-del-item').on('click', function(event) {
		var e    = $(this);		
		var data = [] ;
		var postdata = new FormData();
		postdata.append('rowid', e.attr('data-item'));

		data['url'] 	= siteurl+'cart/delete';
		data['method']	= 'POST';
		data['type']	= 'json';
		data['data']	= postdata;

        var $swaldata = {
            title: 'Delete',
            text: e.attr('title'),
            type: 'warning',
            showCancelButton:true,
            closeOnConfirm:true,
        };		
		swalalerts($swaldata, function(response){
			if(response == true){
				post_ajax(data, e).success(function(callback){
					e.find('span.fa.fa-spinner').remove();
					e.parents('.cart-item').remove();
					grandtotal();
				});
			}
		});
	});

	$('.use_default').change(function() {
		var e    		= $(this);
		var data 		= [] ;
		var $default 	= "false";

		if($(this).is(':checked') == true){
			$default = "true";
			/*$(this).parents('form').find('input[type="text"], textarea, select option').prop('disabled', true);*/
			
		}
		else{
			/*$(this).parents('form').find('input[type="text"], textarea, select option').prop('disabled', false);*/	
		}
		var postdata = new FormData();
		postdata.append('token', e.parents('form').find('input[name=token]').val());
		postdata.append('default', $default);

		data['url'] 	= siteurl + 'cart/profile';
		data['method']	= 'POST';
		data['type']	= 'json';
		data['data']	= postdata;
		
		post_ajax(data, e).success(function(callback){
			e.find('span.fa.fa-spinner').remove();
			if(callback.success == true){
				$('input[name=fullname]').val(callback.name);
				$('input[name=email]').val(callback.email);
				$('textarea[name=address]').val(callback.alamat);
				$('select[name=city]').val(callback.kota);
				$('input[name=postcode]').val(callback.kodepos);
				$('input[name=phone_number]').val(callback.telp);
			}
		});
	});

	$('.courier_option').change(function() {
		costs();
	});
}

function method_selection(init){
	$('.courier_services').attr('data-real', init);
	$('.courier_services').autoNumeric('init', {aSep:tsep,aDec:dsep,mDec:ddig});
	$('.courier_services').autoNumeric('set',init);
}