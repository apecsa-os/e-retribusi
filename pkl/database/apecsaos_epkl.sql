-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30 Jul 2017 pada 07.17
-- Versi Server: 5.6.24-log
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `apecsaos_epkl`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahasa`
--

CREATE TABLE IF NOT EXISTS `bahasa` (
  `idbahasa` int(11) NOT NULL,
  `key_bahasa` varchar(45) DEFAULT NULL,
  `indonesia` text,
  `english` text,
  `is_global` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bahasa`
--

INSERT INTO `bahasa` (`idbahasa`, `key_bahasa`, `indonesia`, `english`, `is_global`) VALUES
(2, 'kategori', 'Kategori', 'Category', 1),
(3, 'filter', 'Saring', 'Filter', 1),
(4, 'search', 'Cari', 'Search', 1),
(5, 'sort', 'Urutkan', 'Sort', 1),
(7, 'welcome', 'Selamat datang', 'Welcome', 0),
(8, 'guest', 'Pengunjung', 'Guest', 0),
(10, 'browse', 'Telusur', 'Browse', 1),
(15, 'detail', 'Rincian', 'Detail', 1),
(25, 'description', 'Deskripsi', 'Description', 0),
(28, 'quality', 'Kualitas', 'Quality', 0),
(29, 'accuracy', 'Akurasi', 'Accuration', 0),
(30, 'report', 'Lapor', 'Report', 0),
(33, 'load_more', 'Tampilkan lebih banyak', 'Load more', 0),
(34, 'here', 'Disini', 'Here', 0),
(35, 'sign_in', 'Masuk', 'Sign in', 1),
(36, 'rememberme', 'Ingat saya', 'Remember me', 0),
(37, 'or', 'Atau', 'Or', 0),
(38, 'login_with', 'Masuk dengan', 'Login with', 0),
(39, 'doesnt_have_account', 'Tidak punya akun', 'Doesn\\''t have account', 0),
(40, 'username', 'Nama user', 'Username', 0),
(41, 'password', 'Kata kunci', 'Password', 0),
(42, 'not_match', 'Nama user dan kata kunci tidak sama', 'Username and password not matches', 0),
(43, 'help', 'Bantuan', 'Help', 0),
(44, 'register', 'Daftar', 'Register', 0),
(45, 'profile', 'Profil', 'Profile', 0),
(46, 'sign_out', 'Keluar', 'Logout', 1),
(47, 'fullname', 'Nama lengkap', 'Fullname', 0),
(48, 'retype', 'Ketik ulang', 'Retype', 0),
(49, 'form', 'Formulir', 'form', 0),
(50, 'group', 'Nama grup pengguna', 'User group name', 0),
(51, 'edit', 'Ubah', 'Edit', 0),
(52, 'delete_success', 'Berhasil dihapus', 'Has been deleted', 0),
(53, 'delete_failed', 'Gagal dihapus', 'Delete failed', 0),
(54, 'insert', 'Tambah', 'Add', 0),
(55, 'delete', 'Hapus', 'Delete', 0),
(56, 'access_control', 'Kontrol akses', 'Access control', 0),
(57, 'translation', 'Terjemahan', 'Translation', 0),
(58, 'key_lang', 'Kunci bahasa', 'Language key', 0),
(59, 'download', 'Unduh', 'Download', 0),
(60, 'upload', 'Unggah', 'upload', 0),
(61, 'order_numb', 'Nomor urut', 'Order number', 0),
(62, 'hide', 'Sembunyi', 'Hide', 0),
(63, 'show', 'Tampil', 'Show', 0),
(64, 'truncate', 'Bersihkan', 'Truncate', 0),
(65, 'name', 'Nama', 'Name', 0),
(66, 'phone_code', 'Kode telepon', 'Phone code', 0),
(67, 'province', 'Provinsi', 'District', 0),
(68, 'country', 'Negara', 'Country', 0),
(71, 'city', 'Kota', 'City', 0),
(72, 'error_valid_ux', 'Halaman tempat sumber ini akan diinstal (login/admin/public), pisahkan dengan tanda koma.', 'the page of this resources will be installed (login/admin/public), separate with comma', 0),
(73, 'error_valid_path', 'dokumen root menuju sumber daya ini berikut ekstensi berkas', 'document root path with file extentions', 0),
(74, 'reason', 'Alasan', 'Reason', 0),
(75, 'nickname', 'Panggilan', 'Nickname', 0),
(76, 'birthdate', 'Tanggal lahir', 'Birthdate', 0),
(77, 'gender', 'Jenis kelamin', 'Gender', 0),
(78, 'address', 'Alamat', 'Address', 0),
(79, 'postcode', 'Kode pos', 'Postal Code', 0),
(80, 'male', 'Laki - laki', 'Male', 0),
(81, 'female', 'Perempuan', 'Female', 0),
(83, 'announce', 'Pengumuman', 'Announcement', 0),
(84, 'title', 'Judul', 'Title', 0),
(85, 'content', 'Isi', 'Content', 0),
(86, 'creator', 'Pembuat', 'Creator', 0),
(87, 'priority', 'Prioritas', 'Priority', 0),
(88, 'expire', 'Kadaluarsa', 'Expire', 0),
(89, 'login_match', 'Username dan password tidak sama', 'Username and password not matches', 0),
(90, 'have_account', 'Sudah mempunyai akun', 'Have account', 0),
(91, 'date', 'Tanggal', 'Date', 0),
(92, 'month', 'Bulan', 'Month', 0),
(93, 'year', 'Tahun', 'Year', 0),
(94, 'activate', 'Aktivasi', 'Activate', 0),
(95, 'contact_support', 'Hubungi CS', 'Contact customer support', 0),
(96, 'register_success', 'Pendaftaran berhasil, silahkan cek kotak surat pada email anda dan lakukan aktivasi segera mungkin', 'Registration succeed, please check you mailbox and activate your account immediately', 0),
(97, 'login_required', 'Maaf, anda harus login dahulu', 'Sorry, you must logged in first', 0),
(98, 'quantity', 'Jumlah', 'Quantity', 0),
(99, 'notes', 'Catatan', 'Notes', 0),
(102, 'submitted', 'Terima kasih atas timbal balik yang anda berikan', 'Thank you for your feed back', 0),
(103, 'error_submitted', 'Terjadi kesalahan pemrosesan data', 'Something error occur on data processing', 0),
(110, 'calculate', 'Hitung', 'Calculate', 0),
(114, 'phone_number', 'Nomor telepon', 'Phone number', 0),
(117, 'proceed', 'Proses', 'Proceed', 0),
(121, 'method', 'Metode', 'Method', 0),
(122, 'days', 'Harian', 'Days', 0),
(124, 'shipping', 'Pengiriman', 'Shipping', 1),
(125, 'payout', 'Pembayaran', 'Pembayaran', 1),
(126, 'tracking', 'Pelacakan', 'Tracking', 1),
(127, 'help', 'Bantuan', 'Help', 1),
(130, 'pending', 'Tertunda', 'Pending', 0),
(134, 'success', 'Sukses', 'Success', 0),
(139, 'required', 'harus terisi', 'are required', 1),
(141, 'abort', 'Batalkan', 'Abort', 0),
(142, 'confirm', 'Konfirmasi', 'Confirmation', 0),
(154, 'cache_clears', 'Cache telah dikosongkan', 'Cache has been clear', 0),
(156, 'print', 'Cetak', 'Print', 0),
(168, 'visitor', 'Pengunjung', 'Visitor', 0),
(169, 'lost_password', 'Lupa password', 'Lost password', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(100) DEFAULT NULL,
  `province` int(11) NOT NULL,
  `postal_code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `province`, `postal_code`) VALUES
(1, 'Kab. Aceh Barat', 21, '23681'),
(2, 'Kab. Aceh Barat Daya', 21, '23764'),
(3, 'Kab. Aceh Besar', 21, '23951'),
(4, 'Kab. Aceh Jaya', 21, '23654'),
(5, 'Kab. Aceh Selatan', 21, '23719'),
(6, 'Kab. Aceh Singkil', 21, '24785'),
(7, 'Kab. Aceh Tamiang', 21, '24476'),
(8, 'Kab. Aceh Tengah', 21, '24511'),
(9, 'Kab. Aceh Tenggara', 21, '24611'),
(10, 'Kab. Aceh Timur', 21, '24454'),
(11, 'Kab. Aceh Utara', 21, '24382'),
(12, 'Kab. Agam', 32, '26411'),
(13, 'Kab. Alor', 23, '85811'),
(14, 'Kota Ambon', 19, '97222'),
(15, 'Kab. Asahan', 34, '21214'),
(16, 'Kab. Asmat', 24, '99777'),
(17, 'Kab. Badung', 1, '80351'),
(18, 'Kab. Balangan', 13, '71611'),
(19, 'Kota Balikpapan', 15, '76111'),
(20, 'Kota Banda Aceh', 21, '23238'),
(21, 'Kota Bandar Lampung', 18, '35139'),
(22, 'Kab. Bandung', 9, '40311'),
(23, 'Kota Bandung', 9, '40115'),
(24, 'Kab. Bandung Barat', 9, '40721'),
(25, 'Kab. Banggai', 29, '94711'),
(26, 'Kab. Banggai Kepulauan', 29, '94881'),
(27, 'Kab. Bangka', 2, '33212'),
(28, 'Kab. Bangka Barat', 2, '33315'),
(29, 'Kab. Bangka Selatan', 2, '33719'),
(30, 'Kab. Bangka Tengah', 2, '33613'),
(31, 'Kab. Bangkalan', 11, '69118'),
(32, 'Kab. Bangli', 1, '80619'),
(33, 'Kab. Banjar', 13, '70619'),
(34, 'Kota Banjar', 9, '46311'),
(35, 'Kota Banjarbaru', 13, '70712'),
(36, 'Kota Banjarmasin', 13, '70117'),
(37, 'Kab. Banjarnegara', 10, '53419'),
(38, 'Kab. Bantaeng', 28, '92411'),
(39, 'Kab. Bantul', 5, '55715'),
(40, 'Kab. Banyuasin', 33, '30911'),
(41, 'Kab. Banyumas', 10, '53114'),
(42, 'Kab. Banyuwangi', 11, '68416'),
(43, 'Kab. Barito Kuala', 13, '70511'),
(44, 'Kab. Barito Selatan', 14, '73711'),
(45, 'Kab. Barito Timur', 14, '73671'),
(46, 'Kab. Barito Utara', 14, '73881'),
(47, 'Kab. Barru', 28, '90719'),
(48, 'Kota Batam', 17, '29413'),
(49, 'Kab. Batang', 10, '51211'),
(50, 'Kab. Batang Hari', 8, '36613'),
(51, 'Kota Batu', 11, '65311'),
(52, 'Kab. Batu Bara', 34, '21655'),
(53, 'Kota Bau-Bau', 30, '93719'),
(54, 'Kab. Bekasi', 9, '17837'),
(55, 'Kota Bekasi', 9, '17121'),
(56, 'Kab. Belitung', 2, '33419'),
(57, 'Kab. Belitung Timur', 2, '33519'),
(58, 'Kab. Belu', 23, '85711'),
(59, 'Kab. Bener Meriah', 21, '24581'),
(60, 'Kab. Bengkalis', 26, '28719'),
(61, 'Kab. Bengkayang', 12, '79213'),
(62, 'Kota Bengkulu', 4, '38229'),
(63, 'Kab. Bengkulu Selatan', 4, '38519'),
(64, 'Kab. Bengkulu Tengah', 4, '38319'),
(65, 'Kab. Bengkulu Utara', 4, '38619'),
(66, 'Kab. Berau', 15, '77311'),
(67, 'Kab. Biak Numfor', 24, '98119'),
(68, 'Kab. Bima', 22, '84171'),
(69, 'Kota Bima', 22, '84139'),
(70, 'Kota Binjai', 34, '20712'),
(71, 'Kab. Bintan', 17, '29135'),
(72, 'Kab. Bireuen', 21, '24219'),
(73, 'Kota Bitung', 31, '95512'),
(74, 'Kab. Blitar', 11, '66171'),
(75, 'Kota Blitar', 11, '66124'),
(76, 'Kab. Blora', 10, '58219'),
(77, 'Kab. Boalemo', 7, '96319'),
(78, 'Kab. Bogor', 9, '16911'),
(79, 'Kota Bogor', 9, '16119'),
(80, 'Kab. Bojonegoro', 11, '62119'),
(81, 'Kab. Bolaang Mongondow (Bolmong)', 31, '95755'),
(82, 'Kab. Bolaang Mongondow Selatan', 31, '95774'),
(83, 'Kab. Bolaang Mongondow Timur', 31, '95783'),
(84, 'Kab. Bolaang Mongondow Utara', 31, '95765'),
(85, 'Kab. Bombana', 30, '93771'),
(86, 'Kab. Bondowoso', 11, '68219'),
(87, 'Kab. Bone', 28, '92713'),
(88, 'Kab. Bone Bolango', 7, '96511'),
(89, 'Kota Bontang', 15, '75313'),
(90, 'Kab. Boven Digoel', 24, '99662'),
(91, 'Kab. Boyolali', 10, '57312'),
(92, 'Kab. Brebes', 10, '52212'),
(93, 'Kota Bukittinggi', 32, '26115'),
(94, 'Kab. Buleleng', 1, '81111'),
(95, 'Kab. Bulukumba', 28, '92511'),
(96, 'Kab. Bulungan (Bulongan)', 16, '77211'),
(97, 'Kab. Bungo', 8, '37216'),
(98, 'Kab. Buol', 29, '94564'),
(99, 'Kab. Buru', 19, '97371'),
(100, 'Kab. Buru Selatan', 19, '97351'),
(101, 'Kab. Buton', 30, '93754'),
(102, 'Kab. Buton Utara', 30, '93745'),
(103, 'Kab. Ciamis', 9, '46211'),
(104, 'Kab. Cianjur', 9, '43217'),
(105, 'Kab. Cilacap', 10, '53211'),
(106, 'Kota Cilegon', 3, '42417'),
(107, 'Kota Cimahi', 9, '40512'),
(108, 'Kab. Cirebon', 9, '45611'),
(109, 'Kota Cirebon', 9, '45116'),
(110, 'Kab. Dairi', 34, '22211'),
(111, 'Kab. Deiyai (Deliyai)', 24, '98784'),
(112, 'Kab. Deli Serdang', 34, '20511'),
(113, 'Kab. Demak', 10, '59519'),
(114, 'Kota Denpasar', 1, '80227'),
(115, 'Kota Depok', 9, '16416'),
(116, 'Kab. Dharmasraya', 32, '27612'),
(117, 'Kab. Dogiyai', 24, '98866'),
(118, 'Kab. Dompu', 22, '84217'),
(119, 'Kab. Donggala', 29, '94341'),
(120, 'Kota Dumai', 26, '28811'),
(121, 'Kab. Empat Lawang', 33, '31811'),
(122, 'Kab. Ende', 23, '86351'),
(123, 'Kab. Enrekang', 28, '91719'),
(124, 'Kab. Fakfak', 25, '98651'),
(125, 'Kab. Flores Timur', 23, '86213'),
(126, 'Kab. Garut', 9, '44126'),
(127, 'Kab. Gayo Lues', 21, '24653'),
(128, 'Kab. Gianyar', 1, '80519'),
(129, 'Kab. Gorontalo', 7, '96218'),
(130, 'Kota Gorontalo', 7, '96115'),
(131, 'Kab. Gorontalo Utara', 7, '96611'),
(132, 'Kab. Gowa', 28, '92111'),
(133, 'Kab. Gresik', 11, '61115'),
(134, 'Kab. Grobogan', 10, '58111'),
(135, 'Kab. Gunung Kidul', 5, '55812'),
(136, 'Kab. Gunung Mas', 14, '74511'),
(137, 'Kota Gunungsitoli', 34, '22813'),
(138, 'Kab. Halmahera Barat', 20, '97757'),
(139, 'Kab. Halmahera Selatan', 20, '97911'),
(140, 'Kab. Halmahera Tengah', 20, '97853'),
(141, 'Kab. Halmahera Timur', 20, '97862'),
(142, 'Kab. Halmahera Utara', 20, '97762'),
(143, 'Kab. Hulu Sungai Selatan', 13, '71212'),
(144, 'Kab. Hulu Sungai Tengah', 13, '71313'),
(145, 'Kab. Hulu Sungai Utara', 13, '71419'),
(146, 'Kab. Humbang Hasundutan', 34, '22457'),
(147, 'Kab. Indragiri Hilir', 26, '29212'),
(148, 'Kab. Indragiri Hulu', 26, '29319'),
(149, 'Kab. Indramayu', 9, '45214'),
(150, 'Kab. Intan Jaya', 24, '98771'),
(151, 'Kota Jakarta Barat', 6, '11220'),
(152, 'Kota Jakarta Pusat', 6, '10540'),
(153, 'Kota Jakarta Selatan', 6, '12230'),
(154, 'Kota Jakarta Timur', 6, '13330'),
(155, 'Kota Jakarta Utara', 6, '14140'),
(156, 'Kota Jambi', 8, '36111'),
(157, 'Kab. Jayapura', 24, '99352'),
(158, 'Kota Jayapura', 24, '99114'),
(159, 'Kab. Jayawijaya', 24, '99511'),
(160, 'Kab. Jember', 11, '68113'),
(161, 'Kab. Jembrana', 1, '82251'),
(162, 'Kab. Jeneponto', 28, '92319'),
(163, 'Kab. Jepara', 10, '59419'),
(164, 'Kab. Jombang', 11, '61415'),
(165, 'Kab. Kaimana', 25, '98671'),
(166, 'Kab. Kampar', 26, '28411'),
(167, 'Kab. Kapuas', 14, '73583'),
(168, 'Kab. Kapuas Hulu', 12, '78719'),
(169, 'Kab. Karanganyar', 10, '57718'),
(170, 'Kab. Karangasem', 1, '80819'),
(171, 'Kab. Karawang', 9, '41311'),
(172, 'Kab. Karimun', 17, '29611'),
(173, 'Kab. Karo', 34, '22119'),
(174, 'Kab. Katingan', 14, '74411'),
(175, 'Kab. Kaur', 4, '38911'),
(176, 'Kab. Kayong Utara', 12, '78852'),
(177, 'Kab. Kebumen', 10, '54319'),
(178, 'Kab. Kediri', 11, '64184'),
(179, 'Kota Kediri', 11, '64125'),
(180, 'Kab. Keerom', 24, '99461'),
(181, 'Kab. Kendal', 10, '51314'),
(182, 'Kota Kendari', 30, '93126'),
(183, 'Kab. Kepahiang', 4, '39319'),
(184, 'Kab. Kepulauan Anambas', 17, '29991'),
(185, 'Kab. Kepulauan Aru', 19, '97681'),
(186, 'Kab. Kepulauan Mentawai', 32, '25771'),
(187, 'Kab. Kepulauan Meranti', 26, '28791'),
(188, 'Kab. Kepulauan Sangihe', 31, '95819'),
(189, 'Kab. Kepulauan Seribu', 6, '14550'),
(190, 'Kab. Kepulauan Siau Tagulandang Biaro (Sitaro)', 31, '95862'),
(191, 'Kab. Kepulauan Sula', 20, '97995'),
(192, 'Kab. Kepulauan Talaud', 31, '95885'),
(193, 'Kab. Kepulauan Yapen (Yapen Waropen)', 24, '98211'),
(194, 'Kab. Kerinci', 8, '37167'),
(195, 'Kab. Ketapang', 12, '78874'),
(196, 'Kab. Klaten', 10, '57411'),
(197, 'Kab. Klungkung', 1, '80719'),
(198, 'Kab. Kolaka', 30, '93511'),
(199, 'Kab. Kolaka Utara', 30, '93911'),
(200, 'Kab. Konawe', 30, '93411'),
(201, 'Kab. Konawe Selatan', 30, '93811'),
(202, 'Kab. Konawe Utara', 30, '93311'),
(203, 'Kab. Kotabaru', 13, '72119'),
(204, 'Kota Kotamobagu', 31, '95711'),
(205, 'Kab. Kotawaringin Barat', 14, '74119'),
(206, 'Kab. Kotawaringin Timur', 14, '74364'),
(207, 'Kab. Kuantan Singingi', 26, '29519'),
(208, 'Kab. Kubu Raya', 12, '78311'),
(209, 'Kab. Kudus', 10, '59311'),
(210, 'Kab. Kulon Progo', 5, '55611'),
(211, 'Kab. Kuningan', 9, '45511'),
(212, 'Kab. Kupang', 23, '85362'),
(213, 'Kota Kupang', 23, '85119'),
(214, 'Kab. Kutai Barat', 15, '75711'),
(215, 'Kab. Kutai Kartanegara', 15, '75511'),
(216, 'Kab. Kutai Timur', 15, '75611'),
(217, 'Kab. Labuhan Batu', 34, '21412'),
(218, 'Kab. Labuhan Batu Selatan', 34, '21511'),
(219, 'Kab. Labuhan Batu Utara', 34, '21711'),
(220, 'Kab. Lahat', 33, '31419'),
(221, 'Kab. Lamandau', 14, '74611'),
(222, 'Kab. Lamongan', 11, '64125'),
(223, 'Kab. Lampung Barat', 18, '34814'),
(224, 'Kab. Lampung Selatan', 18, '35511'),
(225, 'Kab. Lampung Tengah', 18, '34212'),
(226, 'Kab. Lampung Timur', 18, '34319'),
(227, 'Kab. Lampung Utara', 18, '34516'),
(228, 'Kab. Landak', 12, '78319'),
(229, 'Kab. Langkat', 34, '20811'),
(230, 'Kota Langsa', 21, '24412'),
(231, 'Kab. Lanny Jaya', 24, '99531'),
(232, 'Kab. Lebak', 3, '42319'),
(233, 'Kab. Lebong', 4, '39264'),
(234, 'Kab. Lembata', 23, '86611'),
(235, 'Kota Lhokseumawe', 21, '24352'),
(236, 'Kab. Lima Puluh Koto/Kota', 32, '26671'),
(237, 'Kab. Lingga', 17, '29811'),
(238, 'Kab. Lombok Barat', 22, '83311'),
(239, 'Kab. Lombok Tengah', 22, '83511'),
(240, 'Kab. Lombok Timur', 22, '83612'),
(241, 'Kab. Lombok Utara', 22, '83711'),
(242, 'Kota Lubuk Linggau', 33, '31614'),
(243, 'Kab. Lumajang', 11, '67319'),
(244, 'Kab. Luwu', 28, '91994'),
(245, 'Kab. Luwu Timur', 28, '92981'),
(246, 'Kab. Luwu Utara', 28, '92911'),
(247, 'Kab. Madiun', 11, '63153'),
(248, 'Kota Madiun', 11, '63122'),
(249, 'Kab. Magelang', 10, '56519'),
(250, 'Kota Magelang', 10, '56133'),
(251, 'Kab. Magetan', 11, '63314'),
(252, 'Kab. Majalengka', 9, '45412'),
(253, 'Kab. Majene', 27, '91411'),
(254, 'Kota Makassar', 28, '90111'),
(255, 'Kab. Malang', 11, '65163'),
(256, 'Kota Malang', 11, '65112'),
(257, 'Kab. Malinau', 16, '77511'),
(258, 'Kab. Maluku Barat Daya', 19, '97451'),
(259, 'Kab. Maluku Tengah', 19, '97513'),
(260, 'Kab. Maluku Tenggara', 19, '97651'),
(261, 'Kab. Maluku Tenggara Barat', 19, '97465'),
(262, 'Kab. Mamasa', 27, '91362'),
(263, 'Kab. Mamberamo Raya', 24, '99381'),
(264, 'Kab. Mamberamo Tengah', 24, '99553'),
(265, 'Kab. Mamuju', 27, '91519'),
(266, 'Kab. Mamuju Utara', 27, '91571'),
(267, 'Kota Manado', 31, '95247'),
(268, 'Kab. Mandailing Natal', 34, '22916'),
(269, 'Kab. Manggarai', 23, '86551'),
(270, 'Kab. Manggarai Barat', 23, '86711'),
(271, 'Kab. Manggarai Timur', 23, '86811'),
(272, 'Kab. Manokwari', 25, '98311'),
(273, 'Kab. Manokwari Selatan', 25, '98355'),
(274, 'Kab. Mappi', 24, '99853'),
(275, 'Kab. Maros', 28, '90511'),
(276, 'Kota Mataram', 22, '83131'),
(277, 'Kab. Maybrat', 25, '98051'),
(278, 'Kota Medan', 34, '20228'),
(279, 'Kab. Melawi', 12, '78619'),
(280, 'Kab. Merangin', 8, '37319'),
(281, 'Kab. Merauke', 24, '99613'),
(282, 'Kab. Mesuji', 18, '34911'),
(283, 'Kota Metro', 18, '34111'),
(284, 'Kab. Mimika', 24, '99962'),
(285, 'Kab. Minahasa', 31, '95614'),
(286, 'Kab. Minahasa Selatan', 31, '95914'),
(287, 'Kab. Minahasa Tenggara', 31, '95995'),
(288, 'Kab. Minahasa Utara', 31, '95316'),
(289, 'Kab. Mojokerto', 11, '61382'),
(290, 'Kota Mojokerto', 11, '61316'),
(291, 'Kab. Morowali', 29, '94911'),
(292, 'Kab. Muara Enim', 33, '31315'),
(293, 'Kab. Muaro Jambi', 8, '36311'),
(294, 'Kab. Muko Muko', 4, '38715'),
(295, 'Kab. Muna', 30, '93611'),
(296, 'Kab. Murung Raya', 14, '73911'),
(297, 'Kab. Musi Banyuasin', 33, '30719'),
(298, 'Kab. Musi Rawas', 33, '31661'),
(299, 'Kab. Nabire', 24, '98816'),
(300, 'Kab. Nagan Raya', 21, '23674'),
(301, 'Kab. Nagekeo', 23, '86911'),
(302, 'Kab. Natuna', 17, '29711'),
(303, 'Kab. Nduga', 24, '99541'),
(304, 'Kab. Ngada', 23, '86413'),
(305, 'Kab. Nganjuk', 11, '64414'),
(306, 'Kab. Ngawi', 11, '63219'),
(307, 'Kab. Nias', 34, '22876'),
(308, 'Kab. Nias Barat', 34, '22895'),
(309, 'Kab. Nias Selatan', 34, '22865'),
(310, 'Kab. Nias Utara', 34, '22856'),
(311, 'Kab. Nunukan', 16, '77421'),
(312, 'Kab. Ogan Ilir', 33, '30811'),
(313, 'Kab. Ogan Komering Ilir', 33, '30618'),
(314, 'Kab. Ogan Komering Ulu', 33, '32112'),
(315, 'Kab. Ogan Komering Ulu Selatan', 33, '32211'),
(316, 'Kab. Ogan Komering Ulu Timur', 33, '32312'),
(317, 'Kab. Pacitan', 11, '63512'),
(318, 'Kota Padang', 32, '25112'),
(319, 'Kab. Padang Lawas', 34, '22763'),
(320, 'Kab. Padang Lawas Utara', 34, '22753'),
(321, 'Kota Padang Panjang', 32, '27122'),
(322, 'Kab. Padang Pariaman', 32, '25583'),
(323, 'Kota Padang Sidempuan', 34, '22727'),
(324, 'Kota Pagar Alam', 33, '31512'),
(325, 'Kab. Pakpak Bharat', 34, '22272'),
(326, 'Kota Palangka Raya', 14, '73112'),
(327, 'Kota Palembang', 33, '31512'),
(328, 'Kota Palopo', 28, '91911'),
(329, 'Kota Palu', 29, '94111'),
(330, 'Kab. Pamekasan', 11, '69319'),
(331, 'Kab. Pandeglang', 3, '42212'),
(332, 'Kab. Pangandaran', 9, '46511'),
(333, 'Kab. Pangkajene Kepulauan', 28, '90611'),
(334, 'Kota Pangkal Pinang', 2, '33115'),
(335, 'Kab. Paniai', 24, '98765'),
(336, 'Kota Parepare', 28, '91123'),
(337, 'Kota Pariaman', 32, '25511'),
(338, 'Kab. Parigi Moutong', 29, '94411'),
(339, 'Kab. Pasaman', 32, '26318'),
(340, 'Kab. Pasaman Barat', 32, '26511'),
(341, 'Kab. Paser', 15, '76211'),
(342, 'Kab. Pasuruan', 11, '67153'),
(343, 'Kota Pasuruan', 11, '67118'),
(344, 'Kab. Pati', 10, '59114'),
(345, 'Kota Payakumbuh', 32, '26213'),
(346, 'Kab. Pegunungan Arfak', 25, '98354'),
(347, 'Kab. Pegunungan Bintang', 24, '99573'),
(348, 'Kab. Pekalongan', 10, '51161'),
(349, 'Kota Pekalongan', 10, '51122'),
(350, 'Kota Pekanbaru', 26, '28112'),
(351, 'Kab. Pelalawan', 26, '28311'),
(352, 'Kab. Pemalang', 10, '52319'),
(353, 'Kota Pematang Siantar', 34, '21126'),
(354, 'Kab. Penajam Paser Utara', 15, '76311'),
(355, 'Kab. Pesawaran', 18, '35312'),
(356, 'Kab. Pesisir Barat', 18, '35974'),
(357, 'Kab. Pesisir Selatan', 32, '25611'),
(358, 'Kab. Pidie', 21, '24116'),
(359, 'Kab. Pidie Jaya', 21, '24186'),
(360, 'Kab. Pinrang', 28, '91251'),
(361, 'Kab. Pohuwato', 7, '96419'),
(362, 'Kab. Polewali Mandar', 27, '91311'),
(363, 'Kab. Ponorogo', 11, '63411'),
(364, 'Kab. Pontianak', 12, '78971'),
(365, 'Kota Pontianak', 12, '78112'),
(366, 'Kab. Poso', 29, '94615'),
(367, 'Kota Prabumulih', 33, '31121'),
(368, 'Kab. Pringsewu', 18, '35719'),
(369, 'Kab. Probolinggo', 11, '67282'),
(370, 'Kota Probolinggo', 11, '67215'),
(371, 'Kab. Pulang Pisau', 14, '74811'),
(372, 'Kab. Pulau Morotai', 20, '97771'),
(373, 'Kab. Puncak', 24, '98981'),
(374, 'Kab. Puncak Jaya', 24, '98979'),
(375, 'Kab. Purbalingga', 10, '53312'),
(376, 'Kab. Purwakarta', 9, '41119'),
(377, 'Kab. Purworejo', 10, '54111'),
(378, 'Kab. Raja Ampat', 25, '98489'),
(379, 'Kab. Rejang Lebong', 4, '39112'),
(380, 'Kab. Rembang', 10, '59219'),
(381, 'Kab. Rokan Hilir', 26, '28992'),
(382, 'Kab. Rokan Hulu', 26, '28511'),
(383, 'Kab. Rote Ndao', 23, '85982'),
(384, 'Kota Sabang', 21, '23512'),
(385, 'Kab. Sabu Raijua', 23, '85391'),
(386, 'Kota Salatiga', 10, '50711'),
(387, 'Kota Samarinda', 15, '75133'),
(388, 'Kab. Sambas', 12, '79453'),
(389, 'Kab. Samosir', 34, '22392'),
(390, 'Kab. Sampang', 11, '69219'),
(391, 'Kab. Sanggau', 12, '78557'),
(392, 'Kab. Sarmi', 24, '99373'),
(393, 'Kab. Sarolangun', 8, '37419'),
(394, 'Kota Sawah Lunto', 32, '27416'),
(395, 'Kab. Sekadau', 12, '79583'),
(396, 'Kab. Selayar (Kepulauan Selayar)', 28, '92812'),
(397, 'Kab. Seluma', 4, '38811'),
(398, 'Kab. Semarang', 10, '50511'),
(399, 'Kota Semarang', 10, '50135'),
(400, 'Kab. Seram Bagian Barat', 19, '97561'),
(401, 'Kab. Seram Bagian Timur', 19, '97581'),
(402, 'Kab. Serang', 3, '42182'),
(403, 'Kota Serang', 3, '42111'),
(404, 'Kab. Serdang Bedagai', 34, '20915'),
(405, 'Kab. Seruyan', 14, '74211'),
(406, 'Kab. Siak', 26, '28623'),
(407, 'Kota Sibolga', 34, '22522'),
(408, 'Kab. Sidenreng Rappang/Rapang', 28, '91613'),
(409, 'Kab. Sidoarjo', 11, '61219'),
(410, 'Kab. Sigi', 29, '94364'),
(411, 'Kab. Sijunjung (Sawah Lunto Sijunjung)', 32, '27511'),
(412, 'Kab. Sikka', 23, '86121'),
(413, 'Kab. Simalungun', 34, '21162'),
(414, 'Kab. Simeulue', 21, '23891'),
(415, 'Kota Singkawang', 12, '79117'),
(416, 'Kab. Sinjai', 28, '92615'),
(417, 'Kab. Sintang', 12, '78619'),
(418, 'Kab. Situbondo', 11, '68316'),
(419, 'Kab. Sleman', 5, '55513'),
(420, 'Kab. Solok', 32, '27365'),
(421, 'Kota Solok', 32, '27315'),
(422, 'Kab. Solok Selatan', 32, '27779'),
(423, 'Kab. Soppeng', 28, '90812'),
(424, 'Kab. Sorong', 25, '98431'),
(425, 'Kota Sorong', 25, '98411'),
(426, 'Kab. Sorong Selatan', 25, '98454'),
(427, 'Kab. Sragen', 10, '57211'),
(428, 'Kab. Subang', 9, '41215'),
(429, 'Kota Subulussalam', 21, '24882'),
(430, 'Kab. Sukabumi', 9, '43311'),
(431, 'Kota Sukabumi', 9, '43114'),
(432, 'Kab. Sukamara', 14, '74712'),
(433, 'Kab. Sukoharjo', 10, '57514'),
(434, 'Kab. Sumba Barat', 23, '87219'),
(435, 'Kab. Sumba Barat Daya', 23, '87453'),
(436, 'Kab. Sumba Tengah', 23, '87358'),
(437, 'Kab. Sumba Timur', 23, '87112'),
(438, 'Kab. Sumbawa', 22, '84315'),
(439, 'Kab. Sumbawa Barat', 22, '84419'),
(440, 'Kab. Sumedang', 9, '45326'),
(441, 'Kab. Sumenep', 11, '69413'),
(442, 'Kota Sungaipenuh', 8, '37113'),
(443, 'Kab. Supiori', 24, '98164'),
(444, 'Kota Surabaya', 11, '60119'),
(445, 'Kota Surakarta (Solo)', 10, '57113'),
(446, 'Kab. Tabalong', 13, '71513'),
(447, 'Kab. Tabanan', 1, '82119'),
(448, 'Kab. Takalar', 28, '92212'),
(449, 'Kab. Tambrauw', 25, '98475'),
(450, 'Kab. Tana Tidung', 16, '77611'),
(451, 'Kab. Tana Toraja', 28, '91819'),
(452, 'Kab. Tanah Bumbu', 13, '72211'),
(453, 'Kab. Tanah Datar', 32, '27211'),
(454, 'Kab. Tanah Laut', 13, '70811'),
(455, 'Kab. Tangerang', 3, '15914'),
(456, 'Kota Tangerang', 3, '15111'),
(457, 'Kota Tangerang Selatan', 3, '15332'),
(458, 'Kab. Tanggamus', 18, '35619'),
(459, 'Kota Tanjung Balai', 34, '21321'),
(460, 'Kab. Tanjung Jabung Barat', 8, '36513'),
(461, 'Kab. Tanjung Jabung Timur', 8, '36719'),
(462, 'Kota Tanjung Pinang', 17, '29111'),
(463, 'Kab. Tapanuli Selatan', 34, '22742'),
(464, 'Kab. Tapanuli Tengah', 34, '22611'),
(465, 'Kab. Tapanuli Utara', 34, '22414'),
(466, 'Kab. Tapin', 13, '71119'),
(467, 'Kota Tarakan', 16, '77114'),
(468, 'Kab. Tasikmalaya', 9, '46411'),
(469, 'Kota Tasikmalaya', 9, '46116'),
(470, 'Kota Tebing Tinggi', 34, '20632'),
(471, 'Kab. Tebo', 8, '37519'),
(472, 'Kab. Tegal', 10, '52419'),
(473, 'Kota Tegal', 10, '52114'),
(474, 'Kab. Teluk Bintuni', 25, '98551'),
(475, 'Kab. Teluk Wondama', 25, '98591'),
(476, 'Kab. Temanggung', 10, '56212'),
(477, 'Kota Ternate', 20, '97714'),
(478, 'Kota Tidore Kepulauan', 20, '97815'),
(479, 'Kab. Timor Tengah Selatan', 23, '85562'),
(480, 'Kab. Timor Tengah Utara', 23, '85612'),
(481, 'Kab. Toba Samosir', 34, '22316'),
(482, 'Kab. Tojo Una-Una', 29, '94683'),
(483, 'Kab. Toli-Toli', 29, '94542'),
(484, 'Kab. Tolikara', 24, '99411'),
(485, 'Kota Tomohon', 31, '95416'),
(486, 'Kab. Toraja Utara', 28, '91831'),
(487, 'Kab. Trenggalek', 11, '66312'),
(488, 'Kota Tual', 19, '97612'),
(489, 'Kab. Tuban', 11, '62319'),
(490, 'Kab. Tulang Bawang', 18, '34613'),
(491, 'Kab. Tulang Bawang Barat', 18, '34419'),
(492, 'Kab. Tulungagung', 11, '66212'),
(493, 'Kab. Wajo', 28, '90911'),
(494, 'Kab. Wakatobi', 30, '93791'),
(495, 'Kab. Waropen', 24, '98269'),
(496, 'Kab. Way Kanan', 18, '34711'),
(497, 'Kab. Wonogiri', 10, '57619'),
(498, 'Kab. Wonosobo', 10, '56311'),
(499, 'Kab. Yahukimo', 24, '99041'),
(500, 'Kab. Yalimo', 24, '99481'),
(501, 'Kota Yogyakarta', 5, '55222');

-- --------------------------------------------------------

--
-- Struktur dari tabel `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `idconfig` int(11) NOT NULL,
  `cgid` int(11) NOT NULL,
  `configname` varchar(100) DEFAULT NULL,
  `configlabel` varchar(45) DEFAULT NULL,
  `configvalue` text,
  `configicon` varchar(45) DEFAULT NULL,
  `configtype` varchar(45) DEFAULT NULL,
  `configoption` text,
  `configoptionvalue` text,
  `configdesc` text,
  `configuseicon` tinyint(1) DEFAULT '0',
  `editable` tinyint(1) DEFAULT '0',
  `deletable` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `configuration`
--

INSERT INTO `configuration` (`idconfig`, `cgid`, `configname`, `configlabel`, `configvalue`, `configicon`, `configtype`, `configoption`, `configoptionvalue`, `configdesc`, `configuseicon`, `editable`, `deletable`) VALUES
(1, 2, 'pref_mode', 'mode', '1', 'tags', 'radio', 'a:2:{i:0;s:11:"maintenance";i:1;s:7:"publish";}', '0,1', 'Aktifkan mode maintenance atau publish website di web', 0, 0, 0),
(2, 1, 'info_name', 'name', 'E- Retribusi', 'font', 'text', NULL, NULL, 'Nama website', 0, 0, 0),
(3, 1, 'info_title', 'title', 'E-Retribusi', 'header', 'text', NULL, NULL, 'Judul website yang akan ditampilkan di website', 0, 0, 0),
(4, 1, 'info_subtitle', 'subtitle', 'EPKL', 'strikethrough', 'text', NULL, NULL, 'Sub judul website yang akan ditampilkan di web', 0, 0, 0),
(5, 1, 'info_desc', 'description', 'E Retribusi Pemerintah Kota Bogor', 'paragraph', 'textarea', NULL, NULL, 'Keterangan mengenai website', 0, 0, 0),
(6, 1, 'info_owner', 'owner', 'Pemerintah Kota Bogor', 'qrcode', 'text', NULL, NULL, 'Pemilik website', 0, 0, 0),
(7, 1, 'info_address', 'address', 'Kota Bogor', 'map-marker', 'text', NULL, NULL, 'Alamat pemilik atau kantor website', 0, 0, 0),
(8, 1, 'info_phone', 'phone', '1234', 'phone', 'text', NULL, NULL, 'No telf pemilik atau kantor website', 0, 0, 0),
(9, 1, 'info_fax', 'fax', '12345465', 'fax', 'text', NULL, NULL, 'No fax kantor bila ada', 0, 0, 0),
(10, 1, 'info_reg', 'reg no', '1234', 'gavel', 'text', NULL, NULL, 'No legalitas perusahaan', 0, 0, 0),
(12, 1, 'info_logo', 'logo', 'image/icon/06234cd6979a4b5bddaf784df8661e8d.png', 'image', 'filesimage', NULL, NULL, 'Logo website, ukuran ( panjang x lebar ) gambar yang disarankan 100 pixel x 100 pixel', 0, 0, 0),
(13, 4, 'social_fb', 'facebook', 'N/A', 'facebook-square', 'text', NULL, NULL, 'Alamat URL sosial media facebook', 1, 0, 0),
(14, 4, 'social_twitter', 'twitter', NULL, 'twitter-square', 'text', NULL, NULL, 'Alamat URL sosial media twitter', 1, 0, 0),
(15, 4, 'social_google', 'google', NULL, 'google-plus', 'text', NULL, NULL, 'Alamat URL sosial media google', 1, 0, 0),
(16, 4, 'social_instagram', 'instagram', NULL, 'instagram', 'text', NULL, NULL, 'Alamat URL sosial media instagram', 1, 0, 0),
(19, 3, 'protocol', 'protocol', 'mail', 'random', 'radio', 'a:3:{s:4:"mail";s:4:"mail";s:8:"sendmail";s:8:"sendmail";s:4:"smtp";s:4:"smtp";}', 'mail,sendmail,smtp', 'Jenis komunikasi data yang digunakan untuk mengirim pesan', 0, 0, 0),
(20, 3, 'smtp_host', 'smtp host', 'smpt_host', 'building', 'text', NULL, NULL, 'Penyedia komunikasi data untuk pengiriman pesan', 0, 0, 0),
(21, 3, 'smtp_port', 'smtp port', '25', 'terminal', 'number', NULL, NULL, 'Nomor port yang disediakan penyedia komunikasi pesan', 0, 0, 0),
(22, 3, 'smtp_user', 'smtp user', 'smpt_username', 'user', 'text', NULL, NULL, 'Nama akun pengguna yang terdaftar di penyedia komunikasi pesan', 0, 0, 0),
(23, 3, 'smtp_pass', 'smtp pass', 'smpt_password', 'unlock', 'password', NULL, NULL, 'Password akun sesuai nama akun yang terdaftar di penyedia komunikasi pesan', 0, 0, 0),
(24, 3, 'smtp_timeout', 'smtp timeout', '5', 'clock-o', 'number', NULL, NULL, 'Batas waktu pengiriman pesan', 0, 0, 0),
(25, 3, 'wordwrap', 'wordwrap', 'TRUE', 'crosshair', 'radio', 'a:2:{s:5:"FALSE";s:2:"NO";s:4:"TRUE";s:3:"YES";}', 'TRUE,FALSE', 'Aktifkan atau non aktifkan baris baru pada pesan', 0, 0, 0),
(26, 3, 'wrapchars', 'wrapchars', '76', 'paw', 'number', NULL, NULL, 'Jumlah karakter setiap baris baru', 0, 0, 0),
(27, 3, 'mailtype', 'mailtype', 'html', 'envelope', 'radio', 'a:3:{s:4:"text";s:4:"Text";i:0;s:4:"html";i:1;s:4:"HTML";}', 'text,html', 'Jenis pesan', 0, 0, 0),
(28, 3, 'charset', 'charset', 'iso-8859-1', 'ticket', 'radio', 'a:2:{s:5:"utf-8";s:5:"utf-8";s:10:"iso-8859-1";s:10:"iso-8859-1";}', 'utf-8,iso-8859-1', 'Charset pengiriman pesan', 0, 0, 0),
(29, 3, 'validate', 'validasi pesan', 'FALSE', 'send-o', 'radio', 'a:2:{s:5:"FALSE";s:2:"NO";s:4:"TRUE";s:3:"YES";}', 'TRUE,FALSE', 'Aktifkan atau non aktifkan validasi pesan', 0, 0, 0),
(30, 3, 'priority', 'prioritas email', '3', 'wheelchair', 'radio', 'a:5:{i:1;s:6:"urgent";i:2;s:4:"high";i:3;s:6:"medium";i:4;s:3:"low";i:5;s:6:"lowest";}', '1,2,3,4,5', 'Jenis prioritas pesan', 0, 0, 0),
(31, 3, 'newline', 'newline', '\\n', 'arrow-circle-o-down', 'text', NULL, NULL, 'Karakter untuk membuat baris baru', 0, 0, 0),
(32, 3, 'bcc_batch_mode', 'enable_bcc', 'FALSE', 'cc', 'radio', 'a:2:{s:5:"FALSE";s:2:"NO";s:4:"TRUE";s:3:"YES";}', 'TRUE,FALSE', 'Aktifkan atau non aktifkan fitur BCC pesan', 0, 0, 0),
(33, 3, 'bcc_batch_size', 'bcc_size', '200', 'circle', 'number', NULL, NULL, 'Jumlah email yang dapat di input pada fitur BCC', 0, 0, 0),
(34, 3, 'mailpath', 'mailpath', '/usr/sbin/sendmail', 'ticket', 'text', NULL, NULL, 'Jalur pesan server', 0, 0, 0),
(35, 3, 'smtp_crypto', 'smpt mode', 'ssl', 'ticket', 'radio', 'a:2:{s:3:"tls";s:3:"TLS";s:3:"ssl";s:3:"SSL";}', 'tls,ssl', 'Mode yang digunakan penyedia komunikasi pesan', 0, 0, 0),
(37, 5, 'captcha', 'captcha security', 'FALSE', 'key', 'radio', 'a:2:{s:5:"FALSE";s:2:"NO";s:4:"TRUE";s:3:"YES";}', 'TRUE,FALSE', 'Create captcha security on login', 0, 0, 0),
(38, 5, 'expiration_code', 'set expiration code', 'FALSE', 'ticket', 'radio', 'a:2:{s:5:"FALSE";s:2:"NO";s:4:"TRUE";s:3:"YES";}', 'TRUE,FALSE', 'Mode kadaluarsa untuk activation code', 0, 1, 0),
(39, 5, 'registration_mode', 'register_mode', '1', 'coffee', 'number', NULL, NULL, 'mode registrasi, mode 1 : setiap register secara automatis memiliki login account yang berbeda, mode 2 : setiap register secara automatis memiliki account alias namun hanya memiliki 1 account untuk login, mode 3 : setiap register hanya mendapatkan 1 account alias dan 1 membership', 0, 1, 0),
(40, 5, 'memberid_length', 'ID length', '7', 'tag', 'number', NULL, NULL, 'batas maksimal ulasan yg akan ditampilkan', 0, 1, 0),
(41, 5, 'max_network_slot', 'Max network slot', '5', 'sitemap', 'number', NULL, NULL, 'Batas maksimal slot pada jaringan', 0, 1, 0),
(42, 6, 'actmail_english', 'mail activation (en)', '<h1>Hello {username}</h1><p>Your account has been registered.</p><p>To activate your account, please click this link below :</p><p>{activation_url}</p><p> </p><p>If url above is not worked, you can visit this alternative url:</p><p>{activation_url_alt}</p><p> </p><p>then, fill the form within username and activation code correctly</p><p>username : {username}</p><p>activation code : {key}</p><p> </p><p>If you have any question about activation account or any other matter, please feel free to contact us at {support_url} or by phone at.</p><p> </p><p>Thank You</p><p>{_INFO_NAME}</p>', 'puzzle-piece', 'editor', NULL, NULL, 'template aktivasi bahasa inggris', 0, 1, 0),
(43, 6, 'actmail_indonesia', 'mail activation (id)', '<h1>Terima kasih {username}</h1><h4>telah memesan aplikasi di lusaraproject.com</h4><p><br></p><p>Untuk harga aplikasi :</p><p>paket beli 2 aplikasi langsung : Rp . 2.000.000</p><hr id="null"><p>Belum mau beli? ga apa apa, donasinya aja seikhlasnya gan. Sebagai penghargaan buat programmer yang sudah susah payah siang dan malem berkutat depan laptop.</p><p>Cuma dapat paket 1 aplikasi aja gan. tapi sudah include segala resources sama librarynya ko. jangan khawatir.</p><hr id="null"><p>Silahkan transfer pembelian / donasi anda ke :</p><p>Rek BCA Syariah</p><p>No Rek : 1020899464</p><p>Atas nama : luthfi satria ramdhani</p><p><br></p><p>Setelah melakukan pembayaran, silahkan klik link berikut :</p><p>{activation_url}</p><p><br></p><p>Jangan lupa sertakan / unggah bukti transfer atau pembelian anda di formulir yang telah disediakan.</p><p><br></p><p>Kami akan memverifikasi pembelian anda terlebih dahulu, setelah kami terima. Kami akan mengirimkan alamat url ke email anda segera mungkin.</p><p>Jika menemui kesulitan silahkan kontak kami atau kirim email kepada kami di</p><p>luthfi_its@yahoo.com</p><p><br></p><p>Terima kasih</p><p></p>', 'puzzle-piece', 'editor', NULL, NULL, 'template aktivasi bahasa indonesia', 0, 1, 0),
(44, 5, 'activation_expiry', 'activation expiry', '5', 'calendar', 'number', NULL, NULL, 'Batas maksimal hari kadaluarsa proses aktivasi', 0, 1, 0),
(45, 8, 'taxes', 'pajak (%)', '5', 'calculator', 'number', NULL, NULL, 'Pajak yang dikenakan kepada pembeli setiap kali bertransaksi ( dalam %)', 1, 0, 0),
(46, 5, 'depth_level_display', 'Depth level display', '3', 'sitemap', 'number', NULL, NULL, 'Batas kedalaman jaringan yang ditampilkan', 1, 0, 0),
(47, 2, 'cursymbol', 'simbol mata uang', 'Rp.', 'dollar', 'text', NULL, NULL, 'Simbol mata uang', 1, 0, 0),
(48, 2, 'thou_sep', 'simbol ribuan', '.', 'dollar', 'text', NULL, NULL, 'Simbol pembatas mata uang untuk ribuan', 1, 0, 0),
(49, 2, 'dec_sep', 'simbol desimal', ',', 'dollar', 'text', NULL, NULL, 'Simbol pembatas mata uang desimal', 1, 0, 0),
(50, 2, 'dec_digit', 'jumlah digit angka desimal', '0', 'dollar', 'number', NULL, NULL, 'Jumlah angka digit desimal', 1, 0, 0),
(51, 2, 'ldate', 'format tanggal', 'd-m-Y', 'calendar', 'radio', 'a:2:{s:5:"d-m-Y";s:10:"dd-mm-yyyy";s:5:"Y-m-d";s:10:"yyyy-mm-dd";}', NULL, 'Format tanggal', 0, 0, 0),
(52, 7, 'api_key', 'API key raja ongkir', 'raja ongkir api key', 'pencil', 'text', NULL, NULL, NULL, 0, 0, 0),
(53, 7, 'origin', 'asal pengiriman', '79', 'pencil', 'text', NULL, NULL, NULL, 0, 0, 0),
(54, 2, 'transc_mode', 'transaction mode', '1', 'tag', 'radio', 'a:2:{i:0;s:3:"OFF";i:1;s:2:"ON";}', NULL, 'Mode transaksi', 0, 0, 0),
(55, 8, 'monthly_tax', 'monthly taxes (%)', '50', 'pencil', 'number', NULL, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `config_groups`
--

CREATE TABLE IF NOT EXISTS `config_groups` (
  `cgid` int(11) NOT NULL,
  `groupname` varchar(45) DEFAULT NULL,
  `groupicon` varchar(45) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `config_groups`
--

INSERT INTO `config_groups` (`cgid`, `groupname`, `groupicon`, `is_active`) VALUES
(1, 'information', 'info-circle', 1),
(2, 'preference', 'puzzle-piece', 1),
(3, 'mail', 'envelope', 0),
(4, 'sosial media', 'link', 0),
(5, 'web config', 'desktop', 0),
(6, 'mail template', 'puzzle-piece', 0),
(7, 'raja ongkir', 'cogs', 0),
(8, 'taxes', 'coin', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(45) DEFAULT NULL,
  `country_code` varchar(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=254 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `country_code`) VALUES
(1, 'Afghanistan', NULL),
(2, 'Akrotiri', NULL),
(3, 'Albania', NULL),
(4, 'Algeria', NULL),
(5, 'American Samoa', NULL),
(6, 'Andorra', NULL),
(7, 'Angola', NULL),
(8, 'Anguilla', NULL),
(9, 'Antigua and Barbuda', NULL),
(10, 'Argentina', NULL),
(11, 'Armenia', NULL),
(12, 'Aruba', NULL),
(13, 'Ashmore and Cartier Islands', NULL),
(14, 'Australia', NULL),
(15, 'Austria', NULL),
(16, 'Azerbaijan', NULL),
(17, 'Bahamas', NULL),
(18, 'Bahrain', NULL),
(19, 'Baker Island', NULL),
(20, 'Bangladesh', NULL),
(21, 'Barbados', NULL),
(22, 'Belarus', NULL),
(23, 'Belgium', NULL),
(24, 'Belize', NULL),
(25, 'Benin', NULL),
(26, 'Bermuda', NULL),
(27, 'Bhutan', NULL),
(28, 'Bolivia', NULL),
(29, 'Bosnia and Herzegovina', NULL),
(30, 'Botswana', NULL),
(31, 'Bouvet Island', NULL),
(32, 'Brazil', NULL),
(33, 'British Indian Ocean Territory', NULL),
(34, 'British Virgin Islands', NULL),
(35, 'Brunei', NULL),
(36, 'Bulgaria', NULL),
(37, 'Burkina Faso', NULL),
(38, 'Burma', NULL),
(39, 'Burundi', NULL),
(40, 'Cambodia', NULL),
(41, 'Cameroon', NULL),
(42, 'Canada', NULL),
(43, 'Cape Verde', NULL),
(44, 'Cayman Islands', NULL),
(45, 'Central African Republic', NULL),
(46, 'Chad', NULL),
(47, 'Chile', NULL),
(48, 'China', NULL),
(49, 'Christmas Island', NULL),
(50, 'Clipperton Island', NULL),
(51, 'Cocos (Keeling) Islands', NULL),
(52, 'Colombia', NULL),
(53, 'Comoros', NULL),
(54, 'Congo', NULL),
(55, 'Cook Islands', NULL),
(56, 'Coral Sea Islands', NULL),
(57, 'Costa Rica', NULL),
(58, 'Cote d Ivoire', NULL),
(59, 'Croatia', NULL),
(60, 'Cuba', NULL),
(61, 'Cyprus', NULL),
(62, 'Czech Republic', NULL),
(63, 'Denmark', NULL),
(64, 'Dhekelia', NULL),
(65, 'Djibouti', NULL),
(66, 'Dominica', NULL),
(67, 'Dominican Republic', NULL),
(68, 'Ecuador flag', NULL),
(69, 'Egypt', NULL),
(70, 'El Salvador', NULL),
(71, 'Equatorial Guinea', NULL),
(72, 'Eritrea', NULL),
(73, 'Estonia', NULL),
(74, 'Ethiopia', NULL),
(75, 'European Union', NULL),
(76, 'Falkland Islands flag', NULL),
(77, 'Faroe Islands', NULL),
(78, 'Fiji', NULL),
(79, 'Finland', NULL),
(80, 'France', NULL),
(81, 'French Polynesia', NULL),
(82, 'French Southern and Antarctic Lands', NULL),
(83, 'Gabon', NULL),
(84, 'Gambia', NULL),
(85, 'Georgia', NULL),
(86, 'Germany', NULL),
(87, 'Ghana', NULL),
(88, 'Gibraltar', NULL),
(89, 'Greece', NULL),
(90, 'Greenland', NULL),
(91, 'Grenada', NULL),
(92, 'Guam', NULL),
(93, 'Guatemala', NULL),
(94, 'Guernsey', NULL),
(95, 'Guinea Bissau', NULL),
(96, 'Guinea', NULL),
(97, 'Guyana', NULL),
(98, 'Haiti', NULL),
(99, 'Heard Island and McDonald Islands', NULL),
(100, 'Holy See', NULL),
(101, 'Honduras', NULL),
(102, 'Hong Kong', NULL),
(103, 'Howland Island', NULL),
(104, 'Hungary', NULL),
(105, 'Iceland', NULL),
(106, 'India', NULL),
(107, 'Indonesia', '62'),
(108, 'Iran', NULL),
(109, 'Iraq', NULL),
(110, 'Ireland', NULL),
(111, 'Isle of Man', NULL),
(112, 'Israel', NULL),
(113, 'Italy', NULL),
(114, 'Jamaica flag', NULL),
(115, 'Jan Mayen', NULL),
(116, 'Japan', NULL),
(117, 'Jarvis Island', NULL),
(118, 'Jersey', NULL),
(119, 'Johnston Atoll', NULL),
(120, 'Jordan', NULL),
(121, 'Kazakhstan', NULL),
(122, 'Kenya', NULL),
(123, 'Kingman Reef', NULL),
(124, 'Kiribati', NULL),
(125, 'Kuwait', NULL),
(126, 'Kyrgyzstan', NULL),
(127, 'Laos', NULL),
(128, 'Latvia', NULL),
(129, 'Lebanon', NULL),
(130, 'Lesotho', NULL),
(131, 'Liberia', NULL),
(132, 'Libya', NULL),
(133, 'Liechtenstein', NULL),
(134, 'Lithuania', NULL),
(135, 'Luxembourg', NULL),
(136, 'Macau', NULL),
(137, 'Macedonia', NULL),
(138, 'Madagascar', NULL),
(139, 'Malawi', NULL),
(140, 'Malaysia', NULL),
(141, 'Maldives', NULL),
(142, 'Mali', NULL),
(143, 'Malta', NULL),
(144, 'Marshall Islands', NULL),
(145, 'Mauritania', NULL),
(146, 'Mauritius', NULL),
(147, 'Mayotte', NULL),
(148, 'Mexico', NULL),
(149, 'Micronesia', NULL),
(150, 'Midway Islands', NULL),
(151, 'Moldova', NULL),
(152, 'Monaco', NULL),
(153, 'Mongolia', NULL),
(154, 'Montenegro', NULL),
(155, 'Montserrat', NULL),
(156, 'Morocco', NULL),
(157, 'Mozambique', NULL),
(158, 'Namibia', NULL),
(159, 'Nauru', NULL),
(160, 'Navassa Island', NULL),
(161, 'Nepal', NULL),
(162, 'Netherlands Antilles', NULL),
(163, 'Netherlands', NULL),
(164, 'New Caledonia', NULL),
(165, 'New Zealand', NULL),
(166, 'Nicaragua', NULL),
(167, 'Niger', NULL),
(168, 'Nigeria', NULL),
(169, 'Niue', NULL),
(170, 'Norfolk Island', NULL),
(171, 'North Korea', NULL),
(172, 'Northern Mariana Islands', NULL),
(173, 'Norway', NULL),
(174, 'Oman', NULL),
(175, 'Pakistan', NULL),
(176, 'Palau', NULL),
(177, 'Palmyra Atoll', NULL),
(178, 'Panama', NULL),
(179, 'Papua New Guinea', NULL),
(180, 'Paraguay', NULL),
(181, 'Peru', NULL),
(182, 'Philippines', NULL),
(183, 'Pitcairn Islands', NULL),
(184, 'Poland', NULL),
(185, 'Portugal', NULL),
(186, 'Puerto Rico', NULL),
(187, 'Qatar', NULL),
(188, 'Romania', NULL),
(189, 'Russia', NULL),
(190, 'Rwanda', NULL),
(191, 'Saint Barthelemy', NULL),
(192, 'Saint Helena', NULL),
(193, 'Saint Kitts and Nevis', NULL),
(194, 'Saint Lucia', NULL),
(195, 'Saint Martin', NULL),
(196, 'Saint Pierre and Miquelon', NULL),
(197, 'Saint Vincent and the Grenadines', NULL),
(198, 'Samoa', NULL),
(199, 'San Marino', NULL),
(200, 'Sao Tome and Principe', NULL),
(201, 'Saudi Arabia', NULL),
(202, 'Senegal', NULL),
(203, 'Serbia', NULL),
(204, 'Seychelles', NULL),
(205, 'Sierra Leone', NULL),
(206, 'Singapore', NULL),
(207, 'Slovakia', NULL),
(208, 'Slovenia', NULL),
(209, 'Solomon Islands', NULL),
(210, 'Somalia', NULL),
(211, 'South Africa', NULL),
(212, 'South Georgia', NULL),
(213, 'South Korea', NULL),
(214, 'Spain', NULL),
(215, 'Sri Lanka', NULL),
(216, 'Sudan', NULL),
(217, 'Suriname', NULL),
(218, 'Svalbard', NULL),
(219, 'Swaziland', NULL),
(220, 'Sweden', NULL),
(221, 'Switzerland', NULL),
(222, 'Syria', NULL),
(223, 'Taiwan', NULL),
(224, 'Tajikistan', NULL),
(225, 'Tanzania', NULL),
(226, 'Thailand', NULL),
(227, 'Timor Leste', NULL),
(228, 'Togo', NULL),
(229, 'Tokelau', NULL),
(230, 'Tonga', NULL),
(231, 'Trinidad and Tobago', NULL),
(232, 'Tunisia', NULL),
(233, 'Turkey', NULL),
(234, 'Turkmenistan', NULL),
(235, 'Turks and Caicos Islands', NULL),
(236, 'Tuvalu', NULL),
(237, 'Uganda', NULL),
(238, 'Ukraine', NULL),
(239, 'United Arab Emirates', NULL),
(240, 'United Kingdom', NULL),
(241, 'United States', NULL),
(242, 'Uruguay', NULL),
(243, 'US Pacific Island Wildlife Refuges', NULL),
(244, 'Uzbekistan', NULL),
(245, 'Vanuatu', NULL),
(246, 'Venezuela', NULL),
(247, 'Vietnam', NULL),
(248, 'Virgin Islands', NULL),
(249, 'Wake Island', NULL),
(250, 'Wallis and Futuna', NULL),
(251, 'Yemen', NULL),
(252, 'Zambia', NULL),
(253, 'Zimbabwe', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `css`
--

CREATE TABLE IF NOT EXISTS `css` (
  `cid` int(11) NOT NULL,
  `cname` varchar(45) DEFAULT NULL,
  `cpath` text,
  `cdn` text,
  `integrity` text,
  `crossorigin` varchar(45) DEFAULT NULL,
  `cgroup` varchar(200) DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL,
  `order_idx` int(11) DEFAULT '1',
  `include_in` text,
  `global` tinyint(1) DEFAULT '1',
  `cdn_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `css`
--

INSERT INTO `css` (`cid`, `cname`, `cpath`, `cdn`, `integrity`, `crossorigin`, `cgroup`, `is_active`, `order_idx`, `include_in`, `global`, `cdn_active`) VALUES
(1, 'bootstrap', 'bootstrap/3.3.4/css/bootstrap.min.css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', 'sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u', 'anonymous', 'admin,public,login', '1', 2, NULL, 1, 1),
(2, 'font_awesome', 'font-awesome/4.5.0/css/font-awesome.css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', 'sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN', 'anonymous', 'admin,public,login', '1', 3, NULL, 1, 1),
(3, 'select2', 'select2/css/select2.min.css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css', NULL, NULL, 'admin,public', '1', 96, NULL, 0, 1),
(4, 'cpanel', 'themes/gentelella/cpanel.css', NULL, NULL, NULL, 'admin', '1', 97, NULL, 1, 1),
(6, 'sbadmin2', 'sbadmin2/css/sb-admin-2.css', NULL, NULL, NULL, 'admin', '0', 7, NULL, 1, 1),
(7, 'login', 'themes/default/login.css', NULL, NULL, NULL, 'login', '1', 99, NULL, 0, 1),
(8, 'jqueryui', 'jquery-ui/1.11.4/jquery-ui.min.css', 'http://code.jquery.com/ui/1.12.1/jquery-ui.min.css', 'sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=', 'anonymous', 'admin,public', '1', 1, NULL, 0, 1),
(9, 'tokenfield', 'tokenfield/bootstrap-tokenfield.min.css', NULL, NULL, NULL, 'admin', '0', 100, NULL, 0, 1),
(10, 'typeahead', 'tokenfield/tokenfield-typeahead.min.css', NULL, NULL, NULL, 'admin', '0', 101, NULL, 0, 1),
(11, 'swal', 'swal/sweetalert.min.css', 'https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css', NULL, NULL, 'admin,public,login', '1', 95, NULL, 1, 1),
(15, 'content', 'themes/default/css/content.css', NULL, NULL, NULL, 'public', '1', 101, NULL, 1, 1),
(21, 'bdatatable', 'datatable/css/dataTables.bootstrap.min.css', 'https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css', NULL, NULL, 'admin', '0', 94, 'usergroup', 0, 1),
(22, 'form', 'themes/default/css/form.css', NULL, NULL, NULL, 'admin,login,public', '1', 107, NULL, 0, 1),
(23, 'jqueryte', 'jqueryte/jquery-te-1.4.0.css', NULL, NULL, NULL, 'admin', '1', 8, NULL, 0, 1),
(24, 'publogin', 'themes/default/css/login.css', NULL, NULL, NULL, 'login', '1', 4, NULL, 0, 1),
(25, 'gentelella', 'themes/gentelella/css/custom.css', 'https://cdnjs.cloudflare.com/ajax/libs/gentelella/1.3.0/css/custom.min.css', NULL, NULL, 'admin', '1', 7, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `golongan`
--

CREATE TABLE IF NOT EXISTS `golongan` (
  `id` int(11) NOT NULL,
  `golongan` varchar(45) DEFAULT NULL,
  `tarif` float DEFAULT NULL,
  `jenis` varchar(45) DEFAULT NULL,
  `char_jenis` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL,
  `date` varchar(20) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `module` varchar(45) DEFAULT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `inbox`
--

CREATE TABLE IF NOT EXISTS `inbox` (
  `id` int(11) NOT NULL,
  `from` varchar(100) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `receiveDate` varchar(20) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT '0',
  `is_mark` tinyint(1) DEFAULT '0',
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_filter`
--

CREATE TABLE IF NOT EXISTS `ip_filter` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `reason` text
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ip_filter`
--

INSERT INTO `ip_filter` (`id`, `ip_address`, `reason`) VALUES
(1, '164.245.12.31', 'test'),
(2, '192.168.1.5', 'sniffing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `javascript`
--

CREATE TABLE IF NOT EXISTS `javascript` (
  `sid` int(11) NOT NULL,
  `sname` varchar(45) DEFAULT NULL,
  `spath` text,
  `cdn` text,
  `integrity` text,
  `crossorigin` varchar(45) DEFAULT NULL,
  `sgroup` varchar(200) DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL,
  `order_idx` int(11) DEFAULT '1',
  `global` tinyint(1) DEFAULT '1',
  `include_in` text,
  `cdn_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `javascript`
--

INSERT INTO `javascript` (`sid`, `sname`, `spath`, `cdn`, `integrity`, `crossorigin`, `sgroup`, `is_active`, `order_idx`, `global`, `include_in`, `cdn_active`) VALUES
(1, 'jquery', 'jquery/2.2.3/jquery.min.js', 'https://code.jquery.com/jquery-2.2.4.min.js', 'sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=', 'anonymous', 'admin,public,login', '1', 1, 1, NULL, 1),
(4, 'select2', 'select2/js/select2.min.js', NULL, NULL, NULL, 'admin,public', '1', 99, 0, NULL, 1),
(5, 'bootstrap', 'bootstrap/3.3.4/js/bootstrap.min.js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', 'sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa', 'anonymous', 'admin,public,login', '1', 3, 1, NULL, 1),
(6, 'datatable', 'datatable/js/jquery.dataTables.min.js', 'https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js', NULL, NULL, 'admin', '1', 5, 0, NULL, 1),
(8, 'jqueryui', 'jquery-ui/1.11.4/jquery-ui.min.js', 'http://code.jquery.com/ui/1.12.1/jquery-ui.min.js', 'sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=', 'anonymous', 'admin,public', '1', 2, 0, NULL, 1),
(9, 'ckeditor', 'ckeditor/ckeditor.js', 'http://cdn.ckeditor.com/4.6.1/standard/ckeditor.js', NULL, NULL, 'admin', '0', 8, 0, NULL, 1),
(11, 'select2_lang_id', 'select2/js/i18n/id.js', NULL, NULL, NULL, 'admin,public', '1', 10, 0, NULL, 1),
(13, 'autonumeric', 'currency/autoNumeric.js', 'https://cdnjs.cloudflare.com/ajax/libs/autonumeric/1.9.46/autoNumeric.min.js', NULL, NULL, 'admin,public', '1', 11, 0, NULL, 1),
(14, 'swal', 'swal/sweetalert.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js', NULL, NULL, 'admin,public,login', '1', 12, 1, NULL, 1),
(15, 'highcart', 'highcart/js/highcharts.js', 'https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.4/highcharts.js', NULL, NULL, 'admin', '1', 13, 0, NULL, 1),
(16, 'content', 'themes/default/js/content.js', NULL, NULL, NULL, 'public', '1', 100, 1, NULL, 1),
(18, 'bootstrap_datable', 'datatable/js/dataTables.bootstrap.min.js', 'https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js', NULL, NULL, 'admin', '0', 9, 0, NULL, 1),
(20, 'fiquery', 'js/fiquery.js', NULL, NULL, NULL, 'admin', '1', 102, 0, NULL, 1),
(21, 'form', 'themes/default/js/form.js', NULL, NULL, NULL, 'admin,public', '1', 103, 0, NULL, 1),
(22, 'sbadmin2', 'sbadmin2/js/sb-admin-2.js', NULL, NULL, NULL, 'admin', '0', 7, 1, NULL, 1),
(23, 'metismenu', 'sbadmin2/js/metisMenu.min.js', NULL, NULL, NULL, 'admin', '0', 6, 1, NULL, 1),
(24, 'jqueryte', 'jqueryte/jquery-te-1.4.0.min.js', NULL, NULL, NULL, 'admin', '1', 8, 0, NULL, 1),
(26, 'masonry', 'js/masonry.js', 'https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.1/masonry.pkgd.js', NULL, NULL, 'admin', '1', 14, 0, NULL, 1),
(27, 'jquerylazy', 'lazy/jquery.lazy.min.js', 'http://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.4/jquery.lazy.min.js', NULL, NULL, 'admin,public', '1', 15, 1, NULL, 1),
(28, 'jquerylazyplugin', 'lazy/jquery.lazy.plugins.min.js', 'http://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.4/jquery.lazy.plugins.min.js', NULL, NULL, 'admin,public', '1', 16, 0, NULL, 1),
(29, 'dashboard', 'js/dashboard.js', NULL, NULL, NULL, 'admin', '1', 104, 0, NULL, 1),
(30, 'gentelella', 'themes/gentelella/js/custom.js', 'https://cdnjs.cloudflare.com/ajax/libs/gentelella/1.3.0/js/custom.min.js', NULL, NULL, 'admin', '1', 7, 1, NULL, 1),
(31, 'adminlte', 'themes/admin_lte/js/adminlte.min.js', NULL, NULL, NULL, 'public', '1', 98, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `label` varchar(45) DEFAULT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `slug` text,
  `folder` varchar(45) DEFAULT NULL,
  `controllers` varchar(45) DEFAULT NULL,
  `display` int(11) DEFAULT '1',
  `urutan` int(11) DEFAULT '1',
  `posisi` varchar(45) DEFAULT 'left',
  `target` varchar(45) DEFAULT NULL,
  `must_login` char(1) DEFAULT 'y',
  `action_case` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menus`
--

INSERT INTO `menus` (`id`, `pid`, `label`, `icon`, `slug`, `folder`, `controllers`, `display`, `urutan`, `posisi`, `target`, `must_login`, `action_case`) VALUES
(1, NULL, 'dashboard', 'desktop', 'dashboard', 'admin', 'dashboard', 1, 1, 'left', NULL, '1', 'view'),
(2, NULL, 'logout', 'sign-out', 'logout', 'admin', 'logout', 1, 100, 'top', NULL, '1', 'view'),
(3, NULL, 'sistem', 'desktop', 'sistem', 'admin', 'sistem', 1, 99, 'left', NULL, '1', 'view'),
(4, NULL, 'navigasi', 'navicon', 'navigasi', 'admin', 'navigasi', 1, 98, 'left', NULL, '1', 'view,update'),
(5, NULL, 'master', 'desktop', 'master', 'admin', 'manajemen', 1, 93, 'left', NULL, '1', 'view'),
(6, NULL, 'alat', 'cogs', 'alat', 'admin', 'alat', 0, 100, 'left', NULL, '1', 'view'),
(7, NULL, 'localization', 'map-signs', 'localization', 'admin', 'localization', 0, 97, 'left', NULL, '1', 'view'),
(8, 5, 'language', 'bold', 'master/language', 'admin', 'bahasa', 0, 1, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(9, 3, 'konfigurasi', 'cogs', 'sistem/konfigurasi', 'admin', 'konfigurasi', 1, 1, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(10, 9, 'setup', 'book', 'sistem/konfigurasi/setup', 'admin', 'setup', 1, 1, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(11, NULL, 'login', 'sign-in', 'login', 'admin', 'login', 1, 101, 'top', NULL, '0', 'view'),
(12, 9, 'stylesheet', 'css3', 'sistem/konfigurasi/stylesheet', 'admin', 'css', 0, 2, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(13, 9, 'javascript', 'code', 'sistem/konfigurasi/javascript', 'admin', 'script', 0, 3, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(14, 5, 'provinsi', 'map', 'master/provinsi', 'admin', 'provinsi', 0, 3, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(15, 5, 'kota', 'map-marker', 'master/kota', 'admin', 'kota', 0, 4, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(16, 5, 'negara', 'map-o', 'master/negara', 'admin', 'country', 0, 2, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(17, 3, 'ip filter', 'share-alt', 'sistem/ip-filter', 'admin', 'ipfilter', 1, 2, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(18, 3, 'log', 'hourglass', 'sistem/log', 'admin', 'history', 1, 3, 'left', NULL, '1', 'view,delete'),
(19, 3, 'backup', 'database', 'sistem/backup', 'admin', 'basisdata', 1, 4, 'left', NULL, '1', 'view,printed'),
(21, 3, 'restore', 'cloud-upload', 'sistem/restore', 'admin', 'basisdata/import', 1, 5, 'left', NULL, '1', 'view,insert,update'),
(22, 4, 'top menu', 'sort-alpha-asc', 'navigasi/top-menu', 'admin', 'navigasi/top', 1, 1, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(23, 4, 'left menu', 'sort-alpha-desc', 'navigasi/left-menu', 'admin', 'navigasi', 1, 1, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(24, 5, 'usergroup', 'star', 'master/usergroup', 'admin', 'usergroup', 1, 100, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(25, 5, 'users', 'users', 'master/user', 'admin', 'userlist', 1, 99, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(26, NULL, 'administrator', 'object-group', 'administratot', 'admin', 'administrator', 1, 2, 'left', NULL, '1', 'view'),
(29, 3, 'announcement', 'bullhorn', 'sistem/announcement', 'admin', 'pengumuman', 1, 1, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(30, NULL, 'profil', 'user-secret', 'profil', 'admin', 'userprofil', 1, 1, 'top', NULL, '1', 'view,insert,update,delete,printed'),
(31, 30, 'ganti password', 'key', 'profil/ganti-password', 'admin', 'userprofil/password', 1, 1, 'top', NULL, '1', 'view,insert,update,delete,printed'),
(32, 30, 'ganti avatar', 'photo', 'profil/ganti-avatar', 'admin', 'userprofil/avatar', 1, 2, 'top', NULL, '1', 'view,insert,update,delete,printed'),
(33, 30, 'pengaturan', 'cogs', 'profil/pengaturan', 'admin', 'userprofil', 1, 3, 'top', NULL, '1', 'view,insert,update,delete,printed'),
(34, 5, 'bank', 'bank', 'master/bank', 'admin', 'bank', 0, 1, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(35, 26, 'pedagang', 'bank', 'administrator/pedagang', 'admin', 'pedagang', 1, 1, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(36, 5, 'kategori produk', 'diamond', 'master/produk-kategori', 'admin', 'produk_category', 0, 2, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(37, 26, 'pembayaran', 'coffee', 'administrator/pembayaran', 'admin', 'pembayaran', 1, 2, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(38, 26, 'inventori batch', 'truck', 'administrator/inventori-batch', 'admin', 'production', 0, 3, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(39, 26, 'inventori', 'tag', 'administrator/inventori', 'admin', 'inventori', 0, 4, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(40, 26, 'member', 'users', 'administrator/member', 'admin', 'member', 0, 5, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(41, NULL, 'transaksi', 'dollar', 'transaksi', 'admin', 'transaksi', 0, 94, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(42, 41, 'sales', 'exchange', 'transaksi/sales', 'admin', 'sales', 0, 1, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(43, NULL, 'network', 'sitemap', 'network', 'admin', 'network', 0, 3, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(44, NULL, 'report', 'file-o', 'report', 'admin', 'report', 0, 95, 'left', NULL, '1', 'view'),
(45, NULL, 'bonus', 'trophy', 'bonus', 'admin', 'report_bonus', 0, 95, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(46, 26, 'stockies', 'truck', 'administrator/stockies', 'admin', 'stockies', 0, 6, 'left', NULL, '1', 'view,insert,update,delete,printed'),
(47, 41, 'delivery', 'fighter-jet', 'transaksi/delivery', 'admin', 'delivery', 0, 2, 'left', NULL, '1', 'view,insert,update,delete,printed');

-- --------------------------------------------------------

--
-- Struktur dari tabel `merchant`
--

CREATE TABLE IF NOT EXISTS `merchant` (
  `id` int(11) NOT NULL,
  `unique_merchant` varchar(45) DEFAULT NULL,
  `golongan` int(11) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `jenis_kelamin` varchar(20) DEFAULT 'laki - laki',
  `address` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(45) DEFAULT NULL,
  `kelurahan` varchar(45) DEFAULT NULL,
  `description` text,
  `owner` varchar(45) DEFAULT NULL,
  `owner_phone` varchar(45) DEFAULT NULL,
  `no_ktp` varchar(45) DEFAULT NULL,
  `petugas` varchar(45) DEFAULT NULL,
  `petugas_phone` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `retribusi` varchar(45) DEFAULT NULL,
  `open_hour` varchar(5) DEFAULT NULL,
  `closing_hour` varchar(5) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `merchant`
--

INSERT INTO `merchant` (`id`, `unique_merchant`, `golongan`, `category`, `name`, `jenis_kelamin`, `address`, `kecamatan`, `kelurahan`, `description`, `owner`, `owner_phone`, `no_ktp`, `petugas`, `petugas_phone`, `latitude`, `longitude`, `retribusi`, `open_hour`, `closing_hour`, `image`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'PKL0001', NULL, 'Minuman', 'Es kelapa muda gula merah', 'L', 'Taman Kencana Bogor', 'Bogor Utara', 'Bantarjati', 'Pedagang Es Kelapa Muda', 'Ramdhani Nur Aspia', NULL, NULL, 'Ramdhani Nur Aspia', '12345678', '-6.588763', '106.801310', NULL, '08:00', '16:00', 'uploads/pedagang/2beda01d765d582522ab3f150fd08e67.jpg', '1', '2017-07-13 14:14:46', 'admin', '2017-07-17 13:16:38', 'admin'),
(2, 'PKL0002', NULL, 'Minuman', 'Es Kantong Ceria', 'P', 'Taman Kencana Bogor', 'Bogor Utara', 'Bantarjati', '', 'Eni Susana', NULL, NULL, 'Eni Susana', '123456789', '-6.588358', '106.801734', NULL, '08:00', '14:00', '', '1', '2017-07-19 14:32:16', 'admin', '2017-07-19 14:40:55', 'admin'),
(3, 'PKL0003', NULL, 'Minuman', 'Kopi dan Minuman Sachet', 'L', 'Taman Kencana Bogor', 'Bogor Utara', 'Bantarjati', '', 'Subeki', NULL, NULL, 'Subeki', '', '-6.588669', '106.801446', NULL, '09:00', '16:00', '', '1', '2017-07-19 14:39:04', 'admin', '2017-07-19 14:39:04', NULL),
(4, 'PKL0004', NULL, 'minuman', 'gerobak minuman', 'L', 'Jl Tata Winata Kedung Badak RT 005 / RW 002', 'Tanah Sareal', 'Kedung Badak', '', 'Sudrajat', '123456789', '3271060603680004', 'Sudrajat', '123456789', '-6.588763', '106.801310', NULL, '08:00', '16:00', NULL, '1', NULL, 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE IF NOT EXISTS `pengumuman` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text,
  `created_at` varchar(20) DEFAULT NULL,
  `priority` tinyint(1) DEFAULT NULL,
  `isUnlimited` tinyint(1) DEFAULT NULL,
  `expire` varchar(10) DEFAULT NULL,
  `expire_time` varchar(10) DEFAULT NULL,
  `redaktur` int(11) NOT NULL,
  `isPublish` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`id`, `judul`, `isi`, `created_at`, `priority`, `isUnlimited`, `expire`, `expire_time`, `redaktur`, `isPublish`) VALUES
(1, 'On progress', 'Website ini masih dalam tahap pengembangan.<p>Segala gambar terkecuali logo website bukan merupakan hak milik maupun hak cipta dari kami.</p>', '2016-12-02 17:49:30', 2, 0, '2017-12-31', '23:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `province`
--

CREATE TABLE IF NOT EXISTS `province` (
  `province_id` int(11) NOT NULL,
  `province_name` varchar(45) DEFAULT NULL,
  `country` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `province`
--

INSERT INTO `province` (`province_id`, `province_name`, `country`) VALUES
(1, 'Bali', 107),
(2, 'Bangka Belitung', 107),
(3, 'Banten', 107),
(4, 'Bengkulu', 107),
(5, 'DI Yogyakarta', 107),
(6, 'DKI Jakarta', 107),
(7, 'Gorontalo', 107),
(8, 'Jambi', 107),
(9, 'Jawa Barat', 107),
(10, 'Jawa Tengah', 107),
(11, 'Jawa Timur', 107),
(12, 'Kalimantan Barat', 107),
(13, 'Kalimantan Selatan', 107),
(14, 'Kalimantan Tengah', 107),
(15, 'Kalimantan Timur', 107),
(16, 'Kalimantan Utara', 107),
(17, 'Kepulauan Riau', 107),
(18, 'Lampung', 107),
(19, 'Maluku', 107),
(20, 'Maluku Utara', 107),
(21, 'Nanggroe Aceh Darussalam (NAD)', 107),
(22, 'Nusa Tenggara Barat (NTB)', 107),
(23, 'Nusa Tenggara Timur (NTT)', 107),
(24, 'Papua', 107),
(25, 'Papua Barat', 107),
(26, 'Riau', 107),
(27, 'Sulawesi Barat', 107),
(28, 'Sulawesi Selatan', 107),
(29, 'Sulawesi Tengah', 107),
(30, 'Sulawesi Tenggara', 107),
(31, 'Sulawesi Utara', 107),
(32, 'Sumatera Barat', 107),
(33, 'Sumatera Selatan', 107),
(34, 'Sumatera Utara', 107);

-- --------------------------------------------------------

--
-- Struktur dari tabel `retribusi`
--

CREATE TABLE IF NOT EXISTS `retribusi` (
  `id` int(11) NOT NULL,
  `kode_transaksi` varchar(45) DEFAULT NULL,
  `kode_pedagang` varchar(45) DEFAULT NULL,
  `tgl_bayar` varchar(20) DEFAULT NULL,
  `jumlah` float DEFAULT NULL,
  `petugas` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `retribusi`
--

INSERT INTO `retribusi` (`id`, `kode_transaksi`, `kode_pedagang`, `tgl_bayar`, `jumlah`, `petugas`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '201707169078', 'PKL0001', '2017-06-16 19:18:47', 100000, 'admin', '2017-07-16 19:18:47', 'System', '2017-07-17 13:43:32', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `seo`
--

CREATE TABLE IF NOT EXISTS `seo` (
  `id` int(11) NOT NULL,
  `meta_name` varchar(45) DEFAULT NULL,
  `meta_content` varchar(150) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `seo`
--

INSERT INTO `seo` (`id`, `meta_name`, `meta_content`) VALUES
(1, 'content-type', 'text/html'),
(2, 'description', 'website jual beli peralatan laboratorium kimia termurah dan terlengkap'),
(4, 'author', 'lusara project'),
(5, 'language', 'id'),
(6, 'geo.placename', 'Indonesia'),
(7, 'audience', 'all'),
(8, 'rating', 'general'),
(9, 'refresh', '10'),
(10, 'revisit-after', '1 days');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userid` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `is_master` char(1) DEFAULT 'N',
  `group` int(11) NOT NULL,
  `rpassword` varchar(45) NOT NULL,
  `password` varchar(200) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `nickname` varchar(45) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `DOB` varchar(10) DEFAULT NULL,
  `address` text,
  `city` varchar(100) DEFAULT NULL,
  `postcode` varchar(45) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `avatar` text,
  `activation_key` text,
  `is_active` char(1) DEFAULT '0',
  `token` varchar(45) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL,
  `deleted_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`userid`, `username`, `is_master`, `group`, `rpassword`, `password`, `fullname`, `nickname`, `gender`, `DOB`, `address`, `city`, `postcode`, `province`, `country`, `mobile`, `email`, `avatar`, `activation_key`, `is_active`, `token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'Y', 1, 'admin', '3011fff11a322a21921ffda006991a5de5fbd05d', 'administrator', 'Bertrand', 'male', '1986-05-15', 'perum griya kencana blok A', '79', '16161', '9', '107', '789458445', 'admin@mail.com', 'avatar/admin/b2899cacdff922ba1f25c3c03b142cc1.png', NULL, '1', '8a876d5edb445186f2171e2e34f02768', '2017-07-06 13:29:11', '2017-07-07 08:57:16', NULL),
(2, 'renee', 'N', 3, 'renee', 'e547a2ca7b4525dc0400027ee1f432d3194940ed', NULL, NULL, 'male', NULL, NULL, '1', NULL, '21', '107', NULL, 'renee@mail.com', 'image/image/maleava.png', NULL, '1', '37590cb69618b23abb031e247bd5baa1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `usergroup`
--

CREATE TABLE IF NOT EXISTS `usergroup` (
  `groupid` int(11) NOT NULL,
  `groupname` varchar(45) DEFAULT NULL,
  `set_as_default` char(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usergroup`
--

INSERT INTO `usergroup` (`groupid`, `groupname`, `set_as_default`) VALUES
(1, 'administrator', '0'),
(2, 'Member', '1'),
(3, 'agen', '0'),
(4, 'logistik', '0'),
(5, 'Stokiest', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `usergroup_access`
--

CREATE TABLE IF NOT EXISTS `usergroup_access` (
  `accessid` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  `menusid` int(11) NOT NULL,
  `inserting` tinyint(1) DEFAULT '0',
  `updating` tinyint(1) DEFAULT '0',
  `deleting` tinyint(1) DEFAULT '0',
  `viewing` tinyint(1) DEFAULT '0',
  `printing` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usergroup_access`
--

INSERT INTO `usergroup_access` (`accessid`, `groupid`, `menusid`, `inserting`, `updating`, `deleting`, `viewing`, `printing`) VALUES
(1, 1, 24, 1, 1, 1, 1, 1),
(2, 1, 1, 0, 0, 0, 1, 0),
(3, 1, 2, 0, 0, 0, 1, 0),
(4, 1, 3, 0, 0, 0, 1, 0),
(5, 1, 9, 1, 1, 1, 1, 1),
(6, 1, 10, 1, 1, 1, 1, 1),
(7, 1, 12, 1, 1, 1, 1, 1),
(8, 1, 13, 1, 1, 1, 1, 1),
(9, 1, 17, 1, 1, 1, 1, 1),
(10, 1, 18, 0, 0, 1, 1, 0),
(11, 1, 19, 0, 0, 0, 1, 1),
(12, 1, 21, 1, 1, 0, 1, 0),
(13, 1, 29, 1, 1, 1, 1, 1),
(14, 1, 4, 0, 1, 0, 1, 0),
(15, 1, 22, 1, 1, 1, 1, 1),
(16, 1, 23, 1, 1, 1, 1, 1),
(17, 1, 5, 0, 0, 0, 1, 0),
(18, 1, 25, 1, 1, 1, 1, 1),
(19, 1, 34, 1, 1, 1, 1, 1),
(25, 1, 7, 0, 0, 0, 1, 0),
(26, 1, 8, 1, 1, 1, 1, 1),
(27, 1, 14, 1, 1, 1, 1, 1),
(28, 1, 15, 1, 1, 1, 1, 1),
(29, 1, 16, 1, 1, 1, 1, 1),
(30, 1, 26, 0, 0, 0, 1, 0),
(34, 1, 30, 1, 1, 1, 1, 1),
(35, 1, 31, 1, 1, 1, 1, 1),
(36, 1, 32, 1, 1, 1, 1, 1),
(37, 1, 33, 1, 1, 1, 1, 1),
(42, 3, 1, 0, 0, 0, 1, 0),
(43, 3, 2, 0, 0, 0, 1, 0),
(44, 3, 3, 0, 0, 0, 0, 0),
(45, 3, 9, 0, 0, 0, 0, 0),
(46, 3, 10, 0, 0, 0, 0, 0),
(47, 3, 12, 0, 0, 0, 0, 0),
(48, 3, 13, 0, 0, 0, 0, 0),
(49, 3, 17, 0, 0, 0, 0, 0),
(50, 3, 18, 0, 0, 0, 0, 0),
(51, 3, 19, 0, 0, 0, 0, 0),
(52, 3, 21, 0, 0, 0, 0, 0),
(53, 3, 29, 0, 0, 0, 0, 0),
(54, 3, 4, 0, 0, 0, 0, 0),
(55, 3, 22, 0, 0, 0, 0, 0),
(56, 3, 23, 0, 0, 0, 0, 0),
(57, 3, 5, 0, 0, 0, 0, 0),
(58, 3, 24, 0, 0, 0, 0, 0),
(59, 3, 25, 0, 0, 0, 0, 0),
(60, 3, 34, 0, 0, 0, 0, 0),
(64, 3, 7, 0, 0, 0, 0, 0),
(65, 3, 8, 0, 0, 0, 0, 0),
(66, 3, 14, 0, 0, 0, 0, 0),
(67, 3, 15, 0, 0, 0, 0, 0),
(68, 3, 16, 0, 0, 0, 0, 0),
(69, 3, 26, 0, 0, 0, 1, 0),
(75, 3, 30, 1, 1, 1, 1, 0),
(76, 3, 31, 1, 1, 1, 1, 0),
(77, 3, 32, 1, 1, 1, 1, 0),
(78, 3, 33, 1, 1, 1, 1, 0),
(94, 1, 36, 1, 1, 1, 1, 1),
(95, 1, 35, 1, 1, 1, 1, 1),
(96, 1, 37, 1, 1, 1, 1, 1),
(97, 1, 38, 1, 1, 1, 1, 1),
(98, 1, 39, 1, 1, 1, 1, 1),
(99, 1, 40, 1, 1, 1, 1, 1),
(100, 1, 41, 1, 1, 1, 1, 1),
(101, 1, 42, 1, 1, 1, 1, 1),
(102, 1, 43, 1, 1, 1, 1, 1),
(103, 1, 44, 0, 0, 0, 1, 0),
(104, 1, 45, 1, 1, 1, 1, 1),
(105, 1, 46, 1, 1, 1, 1, 1),
(106, 1, 47, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `visitor`
--

CREATE TABLE IF NOT EXISTS `visitor` (
  `id` int(11) NOT NULL,
  `ipaddress` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `browser` varchar(45) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `visitor`
--

INSERT INTO `visitor` (`id`, `ipaddress`, `country`, `browser`, `latitude`, `longitude`) VALUES
(1, '::1', NULL, 'Chrome 56.0.2924.87', NULL, NULL),
(2, '127.0.0.1', '', NULL, '0', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahasa`
--
ALTER TABLE `bahasa`
  ADD PRIMARY KEY (`idbahasa`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`), ADD KEY `fk_province` (`province`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`idconfig`), ADD KEY `fk_cgid` (`cgid`);

--
-- Indexes for table `config_groups`
--
ALTER TABLE `config_groups`
  ADD PRIMARY KEY (`cgid`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `css`
--
ALTER TABLE `css`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ip_filter`
--
ALTER TABLE `ip_filter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `javascript`
--
ALTER TABLE `javascript`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchant`
--
ALTER TABLE `merchant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id`), ADD KEY `announce_user_fk` (`redaktur`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`province_id`), ADD KEY `fk_country` (`country`);

--
-- Indexes for table `retribusi`
--
ALTER TABLE `retribusi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`), ADD KEY `fk_user_group` (`group`);

--
-- Indexes for table `usergroup`
--
ALTER TABLE `usergroup`
  ADD PRIMARY KEY (`groupid`);

--
-- Indexes for table `usergroup_access`
--
ALTER TABLE `usergroup_access`
  ADD PRIMARY KEY (`accessid`), ADD KEY `menusid_fk` (`menusid`), ADD KEY `uaid` (`groupid`);

--
-- Indexes for table `visitor`
--
ALTER TABLE `visitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahasa`
--
ALTER TABLE `bahasa`
  MODIFY `idbahasa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=502;
--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `idconfig` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `config_groups`
--
ALTER TABLE `config_groups`
  MODIFY `cgid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=254;
--
-- AUTO_INCREMENT for table `css`
--
ALTER TABLE `css`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ip_filter`
--
ALTER TABLE `ip_filter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `javascript`
--
ALTER TABLE `javascript`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `merchant`
--
ALTER TABLE `merchant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `province_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `retribusi`
--
ALTER TABLE `retribusi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `usergroup`
--
ALTER TABLE `usergroup`
  MODIFY `groupid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `usergroup_access`
--
ALTER TABLE `usergroup_access`
  MODIFY `accessid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `visitor`
--
ALTER TABLE `visitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `configuration`
--
ALTER TABLE `configuration`
ADD CONSTRAINT `fk_cgid` FOREIGN KEY (`cgid`) REFERENCES `config_groups` (`cgid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
ADD CONSTRAINT `fk_user_group` FOREIGN KEY (`group`) REFERENCES `usergroup` (`groupid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `usergroup_access`
--
ALTER TABLE `usergroup_access`
ADD CONSTRAINT `menusid_fk` FOREIGN KEY (`menusid`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `uaid_fk` FOREIGN KEY (`groupid`) REFERENCES `usergroup` (`groupid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
